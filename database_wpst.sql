-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 26, 2016 at 11:42 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wpst`
--

-- --------------------------------------------------------

--
-- Table structure for table `batteries`
--

CREATE TABLE `batteries` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8_bin NOT NULL,
  `number_tests` int(11) NOT NULL,
  `presentation` enum('as shown','random') COLLATE utf8_bin NOT NULL,
  `active` enum('0','1') COLLATE utf8_bin NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `batteries`
--

INSERT INTO `batteries` (`id`, `name`, `number_tests`, `presentation`, `active`) VALUES
(1, 'Sample_intro_bat', 0, 'random', '1'),
(2, 'First_level_bat', 1, 'random', '1');

-- --------------------------------------------------------

--
-- Table structure for table `batteries_tests`
--

CREATE TABLE `batteries_tests` (
  `battery_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `order` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `batteries_tests`
--

INSERT INTO `batteries_tests` (`battery_id`, `test_id`, `order`) VALUES
(2, 191, 1);

-- --------------------------------------------------------

--
-- Table structure for table `captcha`
--

CREATE TABLE `captcha` (
  `captcha_id` bigint(13) UNSIGNED NOT NULL,
  `captcha_time` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `captcha`
--

INSERT INTO `captcha` (`captcha_id`, `captcha_time`, `ip_address`, `word`) VALUES
(3, 1443804086, '::1', 'V6oJy'),
(4, 1443804099, '::1', 'F1k23'),
(5, 1444057976, '::1', 'GBcKz'),
(6, 1444640566, '127.0.0.1', 'z82Jh'),
(7, 1444641026, '127.0.0.1', 'p7xkh'),
(8, 1445358453, '127.0.0.1', '3vzQ9'),
(9, 1445358470, '127.0.0.1', 'XGdZ6'),
(10, 1445358605, '127.0.0.1', '5PBzb'),
(11, 1445358856, '127.0.0.1', '9lc4F'),
(12, 1445358932, '127.0.0.1', 'SLe24'),
(13, 1445358949, '127.0.0.1', 'Q2xNO'),
(14, 1445358966, '127.0.0.1', 'WIGDS'),
(15, 1445359094, '127.0.0.1', 'kbdbE'),
(16, 1445359149, '127.0.0.1', 'QyqdN'),
(17, 1445359160, '127.0.0.1', 'MH14X'),
(18, 1445359180, '127.0.0.1', 'OSxoD'),
(19, 1445359202, '127.0.0.1', 'CtwmD'),
(20, 1445359214, '127.0.0.1', 'zKeAP'),
(21, 1445359280, '127.0.0.1', 'tD4Pn'),
(22, 1445359291, '127.0.0.1', '01zTG'),
(23, 1445359300, '127.0.0.1', 'ypSGc'),
(24, 1445359339, '127.0.0.1', 'u1ddO'),
(25, 1445359340, '127.0.0.1', 'QE8xb'),
(26, 1445359383, '127.0.0.1', 'axMbO'),
(27, 1445359388, '127.0.0.1', 'ha4Y6'),
(28, 1445359389, '127.0.0.1', 'BfN7S'),
(29, 1445359407, '127.0.0.1', 'XTiOF'),
(30, 1445359417, '127.0.0.1', 'i2q9i'),
(31, 1445359443, '127.0.0.1', 'ZqjSY'),
(32, 1445359449, '127.0.0.1', '0uuUt'),
(33, 1445359450, '127.0.0.1', 'PFjCQ'),
(34, 1445359469, '127.0.0.1', 'YPzDF'),
(35, 1445359513, '127.0.0.1', 'DyMMA'),
(36, 1445359520, '127.0.0.1', 'DhD2L'),
(37, 1445359602, '127.0.0.1', 'iPqF8'),
(38, 1445359628, '127.0.0.1', 'zyK30'),
(39, 1445359809, '127.0.0.1', 'fD1Mv'),
(40, 1445359909, '127.0.0.1', '7a42u'),
(41, 1445359955, '127.0.0.1', 'fpSHT'),
(42, 1445359995, '127.0.0.1', 'goWWU'),
(43, 1445360028, '127.0.0.1', 'F2JeP'),
(44, 1445360035, '127.0.0.1', 'rC2IR'),
(45, 1445360104, '127.0.0.1', 'MpCi3'),
(46, 1445362757, '127.0.0.1', 'xvOmP'),
(47, 1445605232, '127.0.0.1', 'tNIic'),
(48, 1445681618, '127.0.0.1', '4tus8'),
(49, 1445681652, '127.0.0.1', 'KbKpC'),
(50, 1449566373, '127.0.0.1', 'ebiME'),
(51, 1449566481, '127.0.0.1', 'GJ12Q'),
(52, 1449566518, '127.0.0.1', 'hdB5Z'),
(53, 1449566535, '127.0.0.1', 'bpAeD'),
(54, 1449566538, '127.0.0.1', 'BG9CK'),
(55, 1449566549, '127.0.0.1', 'FZLTN'),
(56, 1449654550, '127.0.0.1', 'QUdqa'),
(57, 1449654641, '127.0.0.1', 'kFiKL'),
(58, 1449654656, '127.0.0.1', 'BQY4m'),
(59, 1449654983, '127.0.0.1', 'XZKyY'),
(60, 1449654985, '127.0.0.1', 'P1N1b'),
(61, 1449654998, '127.0.0.1', 'PK5OH'),
(62, 1449655018, '127.0.0.1', 'SeaRX'),
(63, 1449655067, '127.0.0.1', 'eEyqj'),
(64, 1449655092, '127.0.0.1', 'wOvD3'),
(65, 1449908339, '127.0.0.1', '9kblu'),
(66, 1449908356, '127.0.0.1', 'gdWCq'),
(67, 1449908442, '127.0.0.1', 'eltoo'),
(68, 1449910426, '127.0.0.1', '6vWKU'),
(69, 1449916101, '127.0.0.1', 'Olk8l'),
(70, 1455636058, '127.0.0.1', 'jP9LE'),
(71, 1460118275, '127.0.0.1', 'Ctvlo'),
(72, 1463130576, '127.0.0.1', 'Uvpxk'),
(73, 1463130624, '127.0.0.1', 'sMluF'),
(74, 1463130653, '127.0.0.1', '3L5aQ'),
(75, 1463134427, '127.0.0.1', 'Q2OA2'),
(76, 1463134428, '127.0.0.1', '4Zdrb'),
(77, 1463141352, '127.0.0.1', 'XeTdi'),
(78, 1463141352, '127.0.0.1', 'nk4ms'),
(79, 1463393913, '127.0.0.1', 'fu1Lf'),
(80, 1463393914, '127.0.0.1', 'Iyc4s'),
(81, 1463477575, '127.0.0.1', 'KUuIJ'),
(82, 1463477575, '127.0.0.1', 'A9824'),
(83, 1464266795, '127.0.0.1', 'tE6rg'),
(84, 1464266824, '127.0.0.1', 'HacBY'),
(85, 1464544100, '127.0.0.1', 'aBtDl'),
(86, 1464544378, '127.0.0.1', 'qYji4'),
(87, 1464964737, '127.0.0.1', 'egckO'),
(88, 1465125160, '127.0.0.1', 'GdzuF'),
(89, 1465487696, '127.0.0.1', 'in51d'),
(90, 1465651296, '127.0.0.1', '0a8fE'),
(91, 1465651320, '127.0.0.1', 'RoXaa'),
(92, 1465651358, '127.0.0.1', 'J3HoV'),
(93, 1465651390, '127.0.0.1', 'GxxVq'),
(94, 1465651403, '127.0.0.1', 'iTsU9'),
(95, 1465651405, '127.0.0.1', '6KU8o'),
(96, 1465651431, '127.0.0.1', 'dj7Iy'),
(97, 1465651457, '127.0.0.1', 'OT838'),
(98, 1465651483, '127.0.0.1', 'TTdT1'),
(99, 1465651486, '127.0.0.1', 'MCgzE'),
(100, 1465651516, '127.0.0.1', 'zFOf4'),
(101, 1465651518, '127.0.0.1', 'Bs82k');

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `email` varchar(256) NOT NULL,
  `title` varchar(128) NOT NULL,
  `message` text NOT NULL,
  `created` datetime NOT NULL,
  `read` datetime DEFAULT NULL,
  `read_by` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `name`, `email`, `title`, `message`, `created`, `read`, `read_by`) VALUES
(1, 'John Doe', 'john@doe.com', 'Test Message', 'This is only a test message. Notice that once you''ve read it, the button changes from blue to grey, indicating that it has been reviewed.', '2013-01-01 00:00:00', '2015-12-05 10:25:17', 1),
(2, 'Alberto', 'trippo@hotmail.it', 'prova', 'prova ciao cojsa d dssa dask dkajs das dkjasd', '2015-10-02 18:41:53', '2015-12-05 10:25:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) CHARACTER SET utf8 NOT NULL,
  `input_type` enum('input','textarea','radio','dropdown','timezones') CHARACTER SET latin1 NOT NULL,
  `options` text CHARACTER SET utf8 COMMENT 'Use for radio and dropdown: key|value on each line',
  `is_numeric` enum('0','1') CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT 'forces numeric keypad on mobile devices',
  `show_editor` enum('0','1') CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `input_size` enum('large','medium','small') CHARACTER SET utf8 DEFAULT NULL,
  `help_text` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `validation` varchar(128) CHARACTER SET utf8 NOT NULL,
  `sort_order` tinyint(3) UNSIGNED NOT NULL,
  `label` varchar(128) CHARACTER SET utf8 NOT NULL,
  `value` text CHARACTER SET utf8,
  `last_update` datetime DEFAULT NULL,
  `updated_by` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `input_type`, `options`, `is_numeric`, `show_editor`, `input_size`, `help_text`, `validation`, `sort_order`, `label`, `value`, `last_update`, `updated_by`) VALUES
(1, 'site_name', 'input', NULL, '0', '0', 'large', NULL, 'required|trim|min_length[3]|max_length[128]', 10, 'Site name', 'Wiesen Perception Speed Test', '2016-09-12 15:05:59', 1),
(2, 'per_page_limit', 'dropdown', '10|10\r\n25|25\r\n50|50\r\n75|75\r\n100|100', '1', '0', 'small', NULL, 'required|trim|numeric', 50, 'Items per page', '10', '2016-09-12 15:05:59', 1),
(3, 'meta_keywords', 'input', NULL, '0', '0', 'large', 'List of keywords separated by a comma.', 'trim', 20, 'Meta Keywords', '', '2016-09-12 15:05:59', 1),
(4, 'meta_description', 'textarea', NULL, '0', '0', 'large', 'Short description of the site', 'trim', 30, 'Meta Description', 'description for google', '2016-09-12 15:05:59', 1),
(5, 'site_email', 'input', NULL, '0', '0', 'medium', 'The basic email from which will depart from all the site email.', 'required|trim|valid_email', 40, 'Site email', 'youremail@yourdomain.com', '2016-09-12 15:05:59', 1),
(6, 'timezones', 'timezones', NULL, '0', '0', 'medium', NULL, 'required|trim', 60, 'Timezone', 'UM5', '2016-09-12 15:05:59', 1),
(7, 'welcome_message', 'textarea', NULL, '0', '1', 'large', 'Text viewed in homepage before login.', 'trim', 70, 'Login welcome message', '<h5 style="text-align: center;"><em>Welcome to the WPST. Enter the username and password combination you received. If you have not received a username or password, request one from the Department contact person.<br /></em></h5>', '2016-09-12 15:05:59', 1);

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `number` decimal(8,3) NOT NULL,
  `name` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `trigger` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `top_text` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `left_label` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `right_label` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `stimuli_type` enum('images','text','text-box','instructions') COLLATE utf8_bin DEFAULT NULL,
  `stimuli_number` smallint(6) DEFAULT NULL,
  `stimuli_layout` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `answer` varchar(250) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `test_id`, `number`, `name`, `trigger`, `top_text`, `left_label`, `right_label`, `stimuli_type`, `stimuli_number`, `stimuli_layout`, `answer`) VALUES
(43, 191, '1.000', '', '["mouse"]', '', '', '', 'images', 20, '20_image_spread', '["4","7","16"]'),
(44, 191, '3.000', '', '["mouse"]', 'right', '', '', 'instructions', 1, 'Instructions', '["right"]'),
(45, 191, '2.000', '', '["mouse"]', 'both', 'left', 'right', 'images', 1, 'IAT', '["left","right"]');

-- --------------------------------------------------------

--
-- Table structure for table `slides_files`
--

CREATE TABLE `slides_files` (
  `slide_id` int(11) NOT NULL,
  `file_name` varchar(250) COLLATE utf8_bin NOT NULL,
  `number` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `slides_files`
--

INSERT INTO `slides_files` (`slide_id`, `file_name`, `number`) VALUES
(43, '1.jpg', 1),
(43, '1.jpg', 2),
(43, '1.jpg', 3),
(43, '1.jpg', 4),
(43, '1.jpg', 5),
(43, '1.jpg', 6),
(43, '1.jpg', 7),
(43, '1.jpg', 8),
(43, '1.jpg', 9),
(43, '1.jpg', 10),
(43, '1.jpg', 11),
(43, '1.jpg', 12),
(43, '1.jpg', 13),
(43, '1.jpg', 14),
(43, '1.jpg', 15),
(43, '1.jpg', 16),
(43, '1.jpg', 17),
(43, '1.jpg', 18),
(43, '1.jpg', 19),
(43, '1.jpg', 20),
(44, 'istruzione2.txt', 1),
(45, '2.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `id` int(11) NOT NULL,
  `number` mediumint(8) UNSIGNED DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `slides` int(11) UNSIGNED DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `randomize_range` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `folder_name` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `active` enum('0','1') COLLATE utf8_bin NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `number`, `type`, `slides`, `description`, `randomize_range`, `folder_name`, `active`) VALUES
(189, 1, 'A', 0, '', '[]', 'A_1(2)', '0'),
(190, NULL, NULL, NULL, NULL, NULL, 'A_1', '0'),
(191, 1, 'DEBUG', 3, '', '[]', 'DEBUG_1', '1'),
(192, NULL, NULL, NULL, NULL, NULL, 'DEBUG_2', '0'),
(193, NULL, NULL, NULL, NULL, NULL, 'DEBUG_3', '0'),
(194, NULL, NULL, NULL, NULL, NULL, NULL, '0'),
(195, NULL, NULL, NULL, NULL, NULL, 'JKOdskasdjk_1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tests_result`
--

CREATE TABLE `tests_result` (
  `id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `battery_id` int(11) NOT NULL,
  `started` varchar(100) COLLATE utf8_bin NOT NULL,
  `ended` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `tests_result_slides`
--

CREATE TABLE `tests_result_slides` (
  `id` int(11) NOT NULL,
  `test_result_id` int(11) NOT NULL,
  `slide_id` int(11) NOT NULL,
  `start` varchar(100) COLLATE utf8_bin NOT NULL,
  `r_1` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `r_1_time` decimal(9,3) NOT NULL,
  `r_2` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `r_2_time` decimal(9,3) DEFAULT NULL,
  `r_3` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `r_3_time` decimal(9,3) DEFAULT NULL,
  `r_4` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `r_4_time` decimal(9,3) DEFAULT NULL,
  `r_5` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `r_5_time` decimal(9,3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(30) CHARACTER SET utf8 NOT NULL,
  `password` char(128) CHARACTER SET utf8 NOT NULL,
  `salt` char(128) CHARACTER SET utf8 NOT NULL,
  `firstname` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `lastname` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `is_admin` enum('0','1') CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `group_id` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `active` enum('0','1') COLLATE utf8_bin NOT NULL DEFAULT '1',
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `assigned_battery` int(11) DEFAULT NULL,
  `race` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `gender` enum('m','f') CHARACTER SET utf8 DEFAULT NULL,
  `numbers_login` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `is_admin`, `group_id`, `active`, `start_time`, `end_time`, `assigned_battery`, `race`, `birth_date`, `gender`, `numbers_login`) VALUES
(1, 'admin', 'd393e890d6c7860ff14f19441b6394efacb5cdd266030cd8a35c9f29ce1cc8a1ba9df3bc6e704adc0b07281537cfef564ca08963ea240fbb6591989e68fcc013', '26107a213c0df45fb67d3e538ffd9fe959d7c3ef7b23de05bc1bd81cd291bccb1b1c3858ae067e0b5260f1eca96e0777e3813cca16a388074f51602f57928075', '', '', 'trippo@hotmail.it', '1', '', '1', '2016-09-12 00:00:00', NULL, NULL, 'latin', '1996-09-24', 'f', 74),
(16, 'ceradini', '9adb4434d0c67f1da401a5399b5fa62826aa57e2480b0840033cc978e3e83d7cee6e79ca3f6b085f5f7bb1bff2707b56da4fd0eb2bd96070bcd3f9848ec2f2ce', 'a4c56dadde50d3096eb9dc939752cd1ff4691886d7837d3d475c6b4df09384dc3f942a0274e18d1cc97692faf5f4567bc0413557091b346b76f6ef72977f245c', 'Matteo', 'Ceradini', 'matteo.ceradini@gmail.com', '0', '', '1', '2016-09-05 00:00:00', NULL, 2, 'race', '1996-09-24', 'f', 73),
(29, 'ceradini2', '82720e86d036fce4882f6fa678298014d69dac6d845484c0542077dfc9fadd040fce8ff8e75b5f4e41889eb94a2131b6d3b69b36fa9a1fbd1f41439bdbc38c3f', '3b531ff66d8f649f496639b9ea12e33d4f475ae1926aff0fe496c2615e80c675caa460b0b1c04ebaab253fb8b24a03364b4d7730632b2bb93819f958e87ef0f1', 'Prova', 'PRovassda2', '', '0', '1', '0', NULL, NULL, 2, '', '1996-09-13', '', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `batteries`
--
ALTER TABLE `batteries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batteries_tests`
--
ALTER TABLE `batteries_tests`
  ADD PRIMARY KEY (`battery_id`,`test_id`),
  ADD KEY `batteries_tests_ibfk_3` (`test_id`);

--
-- Indexes for table `captcha`
--
ALTER TABLE `captcha`
  ADD PRIMARY KEY (`captcha_id`),
  ADD KEY `word` (`word`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `title` (`title`),
  ADD KEY `created` (`created`),
  ADD KEY `read` (`read`),
  ADD KEY `read_by` (`read_by`),
  ADD KEY `email` (`email`(78));

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`),
  ADD KEY `test_id` (`test_id`);

--
-- Indexes for table `slides_files`
--
ALTER TABLE `slides_files`
  ADD PRIMARY KEY (`slide_id`,`file_name`,`number`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tests_result`
--
ALTER TABLE `tests_result`
  ADD PRIMARY KEY (`id`),
  ADD KEY `test_id` (`test_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `battery_id` (`battery_id`);

--
-- Indexes for table `tests_result_slides`
--
ALTER TABLE `tests_result_slides`
  ADD PRIMARY KEY (`id`),
  ADD KEY `test_result_id` (`test_result_id`),
  ADD KEY `slide_id` (`slide_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assigned_battery` (`assigned_battery`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `batteries`
--
ALTER TABLE `batteries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `captcha`
--
ALTER TABLE `captcha`
  MODIFY `captcha_id` bigint(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=196;
--
-- AUTO_INCREMENT for table `tests_result`
--
ALTER TABLE `tests_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tests_result_slides`
--
ALTER TABLE `tests_result_slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `batteries_tests`
--
ALTER TABLE `batteries_tests`
  ADD CONSTRAINT `batteries_tests_ibfk_2` FOREIGN KEY (`battery_id`) REFERENCES `batteries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `batteries_tests_ibfk_3` FOREIGN KEY (`test_id`) REFERENCES `tests` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `slides`
--
ALTER TABLE `slides`
  ADD CONSTRAINT `slides_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `tests` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `slides_files`
--
ALTER TABLE `slides_files`
  ADD CONSTRAINT `slides_files_ibfk_1` FOREIGN KEY (`slide_id`) REFERENCES `slides` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tests_result`
--
ALTER TABLE `tests_result`
  ADD CONSTRAINT `tests_result_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `tests` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tests_result_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tests_result_ibfk_3` FOREIGN KEY (`battery_id`) REFERENCES `batteries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tests_result_slides`
--
ALTER TABLE `tests_result_slides`
  ADD CONSTRAINT `tests_result_slides_ibfk_1` FOREIGN KEY (`test_result_id`) REFERENCES `tests_result` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tests_result_slides_ibfk_2` FOREIGN KEY (`slide_id`) REFERENCES `slides` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`assigned_battery`) REFERENCES `batteries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
