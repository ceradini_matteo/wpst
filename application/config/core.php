<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Core Config File
 */

// Site Details
$config['site_version']          = "1.1.3";
$config['public_theme']          = "Default";
$config['admin_theme']           = "Admin";

// Pagination
$config['num_links']             = 8;
$config['full_tag_open']         = "<div class=\"pagination\">";
$config['full_tag_close']        = "</div>";

// Backup folders
$config['users_backup']          = '.././backup/users/';
$config['admins_backup']         = '.././backup/admins/';
$config['tests_backup']          = '.././backup/tests/';
$config['slides_backup']         = '.././backup/slides/';

// Miscellaneous
$config['profiler']              = FALSE;
$config['error_delimeter_left']  = "";
$config['error_delimeter_right'] = "<br />";

//Allowed fields
$config['user_fields'] = array('id','username','email','firstname','lastname','salt','password','is_admin','group_id','assigned_battery','mturk_key','start_time','end_time','race','birth_date','gender');
$config['test_fields'] = array('id','number','type','left_key','right_key','slides','description','randomize_range','range-from','range-to','slide_number','slide_name','top_text','left_label','right_label','trigger','timebar','index_slide','slide_exists','slide_id','stimuli_id','stimuli_types','stimuli_numbers','stimuli_layouts','stimuli_correct','slide_files','stimuli_uploads','stimuli_filters_length','stimuli_filters_type');
$config['battery_fields'] = array('id','name','presentation','number_tests','tests','mturk','mturk_link','mturk_attempts');

//Used to up memory limit (ini_set) when i have to manage lot of data from database
$config['memory_limit'] = '1024M';
