<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Tests Language File
 */

//Titles
$lang['tests title tests_list']               = "Tests Results List";
$lang['tests title test_delete']              = "Delete test result";

// Table Columns
$lang['tests col number']                     = "Number";
$lang['tests col type']                       = "Type";
$lang['tests col slides']                     = "Slides";
$lang['tests col description']                = "Description";
$lang['tests col attempts']                   = "Attempts";
$lang['tests col tests_unread']               = "Results unread";

// Form Inputs
$lang['tests input number']                   = "Number";
$lang['tests input type']                     = "Type";
$lang['tests input slides']                   = "Slides";
$lang['tests input description']              = "Description";

// Buttons
$lang['tests button tests_result_export']     = "Export all selected tests result";
$lang['tests button tests_result_slides_export'] = "Export all selected tests result slide";
$lang['tests button delete_tests_result']     = "Delete all selected tests result";
$lang['tests button save_test_result']        = "Export summary";
$lang['tests button save_test_result_slides'] = "Export data";

// Tooltips
$lang['tests tooltip tests_result_export']    = "Export as csv all selected tests result.";
$lang['tests tooltip tests_result_slides_export'] = "Export as csv all selected tests slides result.";
$lang['tests tooltip delete_tests_result']    = "Delete all selected tests result. This action can not be undone.";

// Messages
$lang['tests msg add_test_success']           = "%s was successfully added!";
$lang['tests msg delete_confirm']             = "Are you sure you want to delete this test result? If you delete this test result users assigned to this test will do it again. This action can not be undone.";
$lang['tests msg delete_multiple_confirm']    = "Are you sure you want to delete all tests results selected? If you delete tests result users assigned to those tests will do it again. This action can not be undone.";

//Errors
$lang['tests error edit_test_failed']         = 'The test wasn\'t successfully edit.';
