<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Batteries Language File
 */

//Titles
$lang['batteries title batteries_list']           = "Batteries List";
$lang['batteries title battery_delete']           = "Confirm delete battery";
$lang['batteries title batteries_delete']         = "Confirm delete batteries";
$lang['batteries title basics_information']       = "Basics information";
$lang['batteries title tests']                    = "Tests";
$lang['batteries title battery_edit']             = "Battery form";
$lang['batteries title test_details']             = "Test details";

// Table Columns
$lang['batteries col name']                       = "Name";
$lang['batteries col number_tests']               = "Numbers of tests";
$lang['batteries col presentation']               = "Presentation";
$lang['batteries col tests']                      = "Tests";
$lang['batteries col mturk']                      = "Mturk";
$lang['batteries col mturk_attempts']             = "Mturk attempts";

// Form Inputs
$lang['batteries input name']                     = "Name";
$lang['batteries input number_tests']             = "Number of tests";
$lang['batteries input presentation']             = "Presentation";
$lang['batteries input tests']                    = "Tests";
$lang['batteries input tests_already_added']      = "Tests already added";
$lang['batteries input mturk']                    = "Is for mturk";
$lang['batteries input mturk_enable']             = "YES";
$lang['batteries input mturk_disable']            = "NO";
$lang['batteries input as_shown']                 = "As shown";
$lang['batteries input random']                   = "Random";
$lang['batteries input mturk_link']               = 'Mturk link';

// Buttons
$lang['batteries button add_new_battery']         = "Add new battery";
$lang['batteries button edit_battery']            = "Edit";
$lang['batteries button remove_batteries']        = "Delete batteries selected";
$lang['batteries button delete_all_tests']        = "Delete all tests";
$lang['batteries button add_test']                = "Add new test";

// Tooltips
$lang['batteries tooltip add_new_battery']        = "Create a brand new battery.";
$lang['batteries tooltip remove_batteries']       = "Delete all selected batteries.";
$lang['batteries tooltip add_test']               = "Add new test for this battery.";
$lang['batteries tooltip delete_all_tests']       = "Delete all tests for this battery.";

// Messages
$lang['batteries msg delete_multiple_confirm']    = "Are you sure you want to delete all batteries selected? This can not be undone.";
$lang['batteries msg delete_confirm']             = "Are you sure you want to delete battery <strong>%s</strong>? This can not be undone.";
$lang['batteries msg edit_battery_success']       = "Battery %s was successfully modified!";

// Errors
$lang['batteries error edit_battery_failed']      = "%s could not be modified!";

// Text

$lang['batteries text no_tests_available']        = "<em>There aren't tests available</em>";