<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Tests Language File
 */

//Titles
$lang['tests title tests_list']             = "Tests List";
$lang['tests title test_edit']              = "Test Form";
$lang['tests title test_delete']            = "Confirm delte test";
$lang['tests title delete_all_tests']       = "Confirm delete tests";
$lang['tests title basics_information']     = "Basics information";
$lang['tests title randomize_range']        = "Randomize range";
$lang['tests title slides']                 = "Slides";
$lang['tests title stimuli_layout_preview'] = "Layout preview";
$lang['tests title manage_backups']         = "Manage backups";
$lang['tests title csv_import']             = "Import tests";
$lang['tests title csv_import_slides']      = "Import slides";
$lang['tests title delete_all_slides']      = "Confirm delete slides";
$lang['tests title remaining_tests']        = "Remaining tests:";
$lang['tests title no_tests_available']     = "There aren't tests available for you";
$lang['tests title end_test']               = "Test ended";
$lang['tests title remaining_time']         = "Remaining time: ";


// Table Columns
$lang['tests col number']                   = "Number";
$lang['tests col type']                     = "Type";
$lang['tests col slides']                   = "Slides";
$lang['tests col description']              = "Description";

// Form Inputs
$lang['tests input number']                      = "Number";
$lang['tests input type']                        = "Type";
$lang['tests input left_key']                    = "Left key (<em>default w</em>)";
$lang['tests input right_key']                   = "Right key (<em>default o</em>)";
$lang['tests input slides']                      = "Slides";
$lang['tests input description']                 = "Description";
$lang['tests input randomize_range']             = "Randomize range";
$lang['tests input slide_number']                = "Number";
$lang['tests input slide_name']                  = "Name";
$lang['tests input top_text']                    = "Top text";
$lang['tests input left_label']                  = "Left label";
$lang['tests input right_label']                 = "Right label";
$lang['tests input stimuli_number']              = "Number";
$lang['tests input stimuli_type']                = "Type ";
$lang['tests input stimuli_layout']              = "Layout";
$lang['tests input stimuli_count']               = "Number";
$lang['tests input stimuli_files']               = "Already added files";
$lang['tests input stimuli_file_upload']         = "Add new file";
$lang['tests input stimuli_correct']             = "Answer correct";
$lang['tests input stimuli_filters']             = "Stimuli filters:";
$lang['tests input stimuli_filter_length']       = "Max-length:";
$lang['tests input stimuli_filter_alphanumeric'] = "Alphanumeric";
$lang['tests input stimuli_filter_numeric']      = "Numeric";
$lang['tests input stimuli_filter_date']         = "Date";
$lang['tests input stimuli_filter_none']         = "None";
$lang['tests input trigger_time_value']          = "seconds";
$lang['tests input add_slide_number']            = "number";
$lang['tests input timebar_enabled']             = "timebar";

// Buttons
$lang['tests button add_new_test']          = "Add new test";
$lang['tests button edit_test']             = "Edit";
$lang['tests button remove_tests']          = "Remove tests selected";
$lang['tests button delete_all_slides']     = "Delete all slides";
$lang['tests button add_range']             = "Add range";
$lang['tests button add_slide']             = "Add slide";
$lang['tests button delete_slide']          = "Delete this slide";
$lang['tests button add_trigger']           = "Add trigger";
$lang['tests button add_stimuli']           = "Add stimuli";
$lang['tests button delete_stimuli']        = "Delete this stimuli";
$lang['tests button manage_backups']        = "Manage backups";
$lang['tests button csv_export']            = "Export tests";
$lang['tests button csv_import']            = "Import tests";
$lang['tests button csv_export_slides']     = "Export slides";
$lang['tests button file_manager']          = "File manager";
$lang['tests button reload_slide_files']    = "Reload slide files";
$lang['tests button csv_import_slides']     = "Import slides";
$lang['tests button begin_testing']         = "Begin testing";
$lang['tests button continue']              = "Continue";
$lang['tests button cancel']                = "Cancel";


// Tooltips
$lang['tests tooltip add_new_test']         = "Create a brand new test.";
$lang['tests tooltip remove_tests']         = "Delete all selected tests. This action can not be undone.";
$lang['tests tooltip remove_all_slides']    = "Remove all slides. This action can not be undone.";
$lang['tests tooltip add_range']            = "Add new randomize range.";
$lang['tests tooltip delete_range']         = "Delete this range.";
$lang['tests tooltip add_slide']            = "Add new slide.";
$lang['tests tooltip delete_slide']         = "Delete this slide. This action can not be undone.";
$lang['tests tooltip add_trigger']          = "Add new trigger.";
$lang['tests tooltip delete_trigger']       = "Delete this trigger. This action can not be undone.";
$lang['tests tooltip delete_stimuli']       = "Delete this stimuli. This action can not be undone.";
$lang['tests tooltip manage_backups']       = "Manage and restore backups.";
$lang['tests tooltip csv_export']           = "Export a CSV file of all tests with filters applied.";
$lang['tests tooltip csv_import']           = "Import tests from a CSV file.";
$lang['tests tooltip csv_export_slides']    = "Export a CSV file of all slides.";
$lang['tests tooltip csv_import_slides']    = "Import slides for this test from a CSV file.";
$lang['tests tooltip file_manager']         = "Open file manager and manage slides files.";
$lang['tests tooltip reload_slide_files']   = "Reload all slides files.";

// Messages
$lang['tests msg add_test_success']         = "%s was successfully added!";
$lang['tests msg edit_test_success']        = 'The test was successfully edit.';
$lang['tests msg delete_confirm']           = "Are you sure you want to delete test %s? This can not be undone.";
$lang['tests msg delete_multiple_confirm']  = "Are you sure you want to delete all tests selected? This can not be undone.";
$lang['tests msg delete_all_slides']        = "Are you sure you want to delete all slides? This can not be undone.";
$lang['tests msg delete_test']              = "The test was successfully deleted";
$lang['tests msg upload_success']           = 'Tests were uploaded successfully.';
$lang['tests msg upload_success_slides']    = 'Slides were uploaded successfully.';
$lang['tests msg upload_error']             = 'Something going wrong during uploading. Please contact administrator.';
$lang['tests msg backup_success']           = 'Backup file was upload successfully.';
$lang['tests msg backup_no_file']           = 'There isn\'t selected backup file.';
$lang['tests msg csv_error']                = 'Uploads fails, your CSV isn\'t well formatted, there are duplicate entries or there are some not-nullable field empty. Please correct it.<br /> ';

//Errors
$lang['tests error edit_test_failed']       = 'The test wasn\'t successfully edit.';
$lang['tests error no_file_found']          = 'Something going wrong during load the file for this test. Please contact administrators.';
$lang['tests error error_slide']            = 'Your answer is incorrect. Please try again.';
