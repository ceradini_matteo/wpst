<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Users Language File
 */

//Tabs
$lang['users tab users']                      = "Users list";

// Titles
$lang['users title forgot']                   = "Forgot Password";
$lang['users title login']                    = "Login";
$lang['users title reset_password']           = "Reset password";
$lang['users title register']                 = "Register";
$lang['users title user_add']                 = "Add User";
$lang['users title user_delete']              = "Confirm Delete User";
$lang['users title user_edit']                = "User Form";
$lang['users title user_list']                = "User List";
$lang['users title csv_import']               = "Import Users";
$lang['users title manage_backups']           = "Manage Backups";
$lang['users title login_message']            = "Login message";
$lang['users title firstname']                = "Firstname:";
$lang['users title lastname']                 = "Lastname:";
$lang['users title access_ends']              = "Access ends:";
$lang['users title access_ends_on']           = "on";

// Buttons
$lang['users button add_new_user']            = "Add new user";
$lang['users button edit_user']               = "Edit";
$lang['users button remove_user']             = "Delete users selected";
$lang['users button register']                = "Create Account";
$lang['users button reset_password']          = "Reset Password";
$lang['users button csv_export']              = "Export users";
$lang['users button csv_import']              = "Import users";
$lang['users button manage_backups']          = "Manage backups";
$lang['users button login_message']           = "Login message";
$lang['users button upload']                  = "Upload";
$lang['users button close']                   = "Close";
$lang['users button save']                    = "Save";

// Tooltips
$lang['users tooltip add_new_user']           = "Create a brand new user.";
$lang['users tooltip remove_user']            = "Delete all selected users.";
$lang['users tooltip csv_export']             = "Export a CSV file of all users with filters applied.";
$lang['users tooltip csv_import']             = "Import users from a CSV file.";
$lang['users tooltip manage_backups']         = "Manage and restore backups.";
$lang['users tooltip login_message']          = "Edit login welcome message.";

// Links
$lang['users link forgot_password']           = "Forgot your password?";
$lang['users link register_account']          = "Register for an account.";

// Table Columns
$lang['users col username']                   = "Username";
$lang['users col password']                   = "Password";
$lang['users col is_admin']                   = "Admin";
$lang['users col firstname']                  = "Firstname";
$lang['users col lastname']                   = "Lastname";
$lang['users col user_id']                    = "ID";
$lang['users col group_id']                   = "Group id";
$lang['users col start_time']                 = "Start time";
$lang['users col end_time']                   = "End time";
$lang['users col assigned_battery']           = "Battery";

// Form Inputs
$lang['users input email']                    = "Email";
$lang['users input email_or_username']        = "Email / Username";
$lang['users input firstname']                = "First Name";
$lang['users input is_admin']                 = "Is Admin";
$lang['users input lastname']                 = "Last Name";
$lang['users input password']                 = "Password";
$lang['users input password_repeat']          = "Repeat Password";
$lang['users input status']                   = "Status";
$lang['users input username']                 = "Username";
$lang['users input username_email']           = "Username or Email";
$lang['users input group_id']                 = "Group id";
$lang['users input start_time']               = "Start time";
$lang['users input end_time']                 = "End time";
$lang['users input datetime']                 = "Date time here";
$lang['users input assigned_battery']         = "Assigned battery";
$lang['users input race']                     = "Race";
$lang['users input gender']                   = "Gender";
$lang['users input birth_date']               = "Birth date";

// Help
$lang['users help passwords']                 = "Only enter passwords if you want to change it.";

// Messages
$lang['users msg add_user_success']           = "%s was successfully added!";
$lang['users msg delete_confirm']             = "Are you sure you want to delete <strong>%s</strong>? This can not be undone.";
$lang['users msg delete_multiple_confirm']    = "Are you sure you want to delete all users selected? This can not be undone.";
$lang['users msg delete_user']                = "You have succesfully deleted <strong>%s</strong>!";
$lang['users msg edit_profile_success']       = "Your profile was successfully modified!";
$lang['users msg edit_user_success']          = "%s was successfully modified!";
$lang['users msg register_success']           = "Thanks for registering, %s! Check your email for a confirmation message. Once
                                                 your account has been verified, you will be able to log in with the credentials
                                                 you provided.";
$lang['users msg password_reset_success']     = "Your password has been reset! Please check your email for your new temporary password.";
$lang['users msg validate_success']           = "Your account has been verified. You may now log in to your account.";
$lang['users msg email_new_account']          = "<p>Thank you for creating an account at %s. Click the link below to validate your
                                                 email address and activate your account.<br /><br /><a href=\"%s\">%s</a></p>";
$lang['users msg email_new_account_title']    = "New Account for %s";
$lang['users msg email_password_reset']       = "<p>You can now reset your password for %s<br />
												Click the link below to change your password:<br /><br /><a href=\"%s\">Reset Password</a><br />
												<br />Be sure to remember your new password.
												<br />The link will only work once.</p>";
$lang['users msg email_password_reset_title'] = "Password Reset for %s";
$lang['users msg upload_success']             = 'Users were uploaded successfully.';
$lang['users msg upload_error']               = 'Something going wrong during uploading. Please contact administrator.';
$lang['users msg csv_error']                  = 'Uploads fails, your CSV isn\'t well formatted, there are duplicate entries or there are some not-nullable field empty. Please correct it.<br />';
$lang['users msg backup_success']             = 'Backup file was upload successfully.';
$lang['users msg backup_no_file']             = 'There isn\'t selected backup file.';

// Errors
$lang['users error add_user_failed']          = "%s could not be added!";
$lang['users error delete_user']              = "<strong>%s</strong> could not be deleted!";
$lang['users error edit_profile_failed']      = "Your profile could not be modified!";
$lang['users error edit_user_failed']         = "%s could not be modified!";
$lang['users error email_exists']             = "The email <strong>%s</strong> already exists!";
$lang['users error email_not_exists']         = "That email does not exists!";
$lang['users error invalid_login']            = "Invalid username or password";
$lang['users error login_time']               = "You not yet authorized to login.";
$lang['users error password_reset_failed']    = "There was a problem resetting your password. Please try again.";
$lang['users error register_failed']          = "Your account could not be created at this time. Please try again.";
$lang['users error user_id_required']         = "A numeric user ID is required!";
$lang['users error user_not_exist']           = "That user does not exist!";
$lang['users error username_exists']          = "The username <strong>%s</strong> already exists!";
$lang['users error validate_failed']          = "There was a problem validating your account. Please try again.";
$lang['users error invalid_reset_code']       = "You're reset code is invalid";

/*
	Lang for admins view
*/

// Titles
$lang['admins title admin_edit']              = "Admin Form";
$lang['admins title admin_list']              = "Admin List";
$lang['admins title admin_delete']            = "Confirm Delete Admin";
$lang['admins title csv_import']              = "Import Admins";

// Buttons
$lang['admins button add_new_admin']          = "Add new admin";
$lang['admins button edit_admin']             = "Edit";
$lang['admins button remove_admin']           = "Delete admins selected";
$lang['admins button csv_export']             = "Export admins";
$lang['admins button csv_import']             = "Import admins";
$lang['admins button manage_backups']         = "Manage backups";

// Tooltips
$lang['admins tooltip add_new_admin']         = "Create a brand new admin.";
$lang['admins tooltip remove_admin']          = "Delete all selected admins.";
$lang['admins tooltip csv_export']            = "Export a CSV file of all admins with filters applied.";
$lang['admins tooltip csv_import']            = "Import admins from a CSV file.";
$lang['admins tooltip manage_backups']        = "Manage and restore backups.";

// Messages

$lang['admins msg delete_multiple_confirm']   = "Are you sure you want to delete all admins selected? This can not be undone.";