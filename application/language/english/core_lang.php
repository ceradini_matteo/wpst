<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Core Language File
 */

// Titles
$lang['core title welcome'] 				= "Welcome to %s";
$lang['core title info']                    = "Info";
$lang['core title clear_references']        = "Clear old references";

// Buttons
$lang['core button admin']                  = "Admin";
$lang['core button cancel']              	= "Cancel";
$lang['core button close']               	= "Close";
$lang['core button contact']               	= "Contact";
$lang['core button filter']              	= "Filter";
$lang['core button home']                	= "Home";
$lang['core button login']               	= "Login";
$lang['core button logout']              	= "Logout";
$lang['core button reset_password']         = "Reset password";
$lang['core button reset']               	= "Reset";
$lang['core button save']                	= "Save";
$lang['core button search']              	= "Search";
$lang['core button toggle_nav']          	= "Toggle navigation";
$lang['core button list']                	= "List";
$lang['core button upload']                	= "Upload";
$lang['core button menu']                	= "Menu";
$lang['core button ok']                   	= "Ok";
$lang['core button continue']               = "Continue";
$lang['core button clear_references']       = "Clear old references";

// Text
$lang['core text no']                    	= "No";
$lang['core text yes']                   	= "Yes";
$lang['core text male']                   	= "Male";
$lang['core text female']                   = "Female";
$lang['core text from']                     = "From";
$lang['core text to']                       = "To";
$lang['core text no_backups']               = "<em>There aren't backups available</em>";
$lang['core text seconds']                  = "seconds";

// Messagges
$lang['core message clear_references']      = "Are you sure you want to delete old references to test and users? You will not have access to test results for data that are no longer accessible. <br /><strong>This action can't be undone</strong>";

// Emails
$lang['core email start']                	= "<!DOCTYPE html><html><head><style>
                                                    body { background: #fff; color: #333; }
                                               </style></head><body>";
$lang['core email end']                  	= "</body></html>";

// Additional date_lang
$lang['UM75']	                         	= "(UTC -7:00) Arizona Time";

// Errors
$lang['core error no_results']              = "No results found!";
