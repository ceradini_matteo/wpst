<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Users Language File
 */

// Titles
$lang['users title forgot']                   = "Password Dimenticata";
$lang['users title login']                    = "Login";
$lang['users title profile']                  = "Profilo";
$lang['users title register']                 = "Registrati";
$lang['users title user_add']                 = "Aggiungi Utente";
$lang['users title user_delete']              = "Eliminazione Utente";
$lang['users title user_edit']                = "Modifica Utente";
$lang['users title user_list']                = "Utenti";

$lang['users text forgot']                   = "Questa è la procedura di recupero password. Inserisci la tua email e ti verrà inviata una nuova password. Successivamente se vorrai potrai cambiarla dal tuo profilo.";

// Buttons
$lang['users button add_new_user']            = "Crea un utente";
$lang['users button register']                = "Crea un account";
$lang['users button reset_password']          = "Resetta Password";

// Tooltips
$lang['users tooltip add_new_user']           = "Crea un nuovo utente.";

// Links
$lang['users link forgot_password']           = "Ti sei dimenticato la password?";
$lang['users link register_account']          = "Registra un account";

// Table Columns
$lang['users col firstname']                  = "Nome";
$lang['users col is_admin']                   = "Admin";
$lang['users col lastname']                   = "Cognome";
$lang['users col user_id']                    = "ID";
$lang['users col status']                     = "Stato";
$lang['users col username']                   = "Username";

// Form Inputs
$lang['users input email']                    = "Email";
$lang['users input email_repeat']             = "Ripeti Email";
$lang['users input firstname']               = "Nome";
$lang['users input is_admin']                 = "E' un amministratore?";
$lang['users input lastname']                = "Cognome";
$lang['users input password']                 = "Password";
$lang['users input password_repeat']          = "Ripeti Password";
$lang['users input status']                   = "Stato";
$lang['users input username']                 = "Username";
$lang['users input username_email']           = "Username o Email";

// Help
$lang['users help passwords']                 = "Compila la password solo se vuoi cambiarla.";

// Messages
$lang['users msg add_user_success']           = "%s è stato inserito correttamente!";
$lang['users msg delete_confirm']             = "Sei sicuro di volere cancellare <strong>%s</strong>? Questa operazione non può essere annullata.";
$lang['users msg delete_user']                = "Hai cancellato correttamente <strong>%s</strong>!";
$lang['users msg edit_profile_success']       = "Il tuo profilo è stato correttamente aggiornato!";
$lang['users msg edit_user_success']          = "%s è stato modificato correttamente!";
$lang['users msg register_success']           = "Grazie di esserti registrato, %s! <br/>Controlla la tua email per il messaggio di conferma. <br/><br/>Quando il tuo account sarà verificato, sarai in grado di accedere al sistema con le credenziali scelte.";
$lang['users msg password_reset_success']     = "La tua password è stata resettata, %s! Si prega di guardare nella'email dove abbiamo inviato la nuova password temporanea.";
$lang['users msg validate_success']           = "Il tuo account è stato verificato. Potrai accedere al tuo account non appena il servizio sarà reso pubblico.";//Ora è possibile accedere al proprio account.";
$lang['users msg email_new_account']          = "<p>Grazie per esserti iscritto a %s. Clicca il link qui sotto per verificare la tua email ed attivare il tuo account.<br /><br /><a href=\"%s\">%s</a></p>";
$lang['users msg email_new_account_title']    = "Nuovo account su %s";
$lang['users msg email_password_reset']       = "<p>La tua password %s è stata resettata.<br/> Clicca sul link qui sotto per fare il login con la nuova password:<br /><br /><strong>%s</strong><br /><br /><a href=\"%s\">%s</a><br/><br/>
                                                 Una volta effettuato il login ricordati di cambiare la password con una che puoi ricordare.</p>";
$lang['users msg email_password_reset_title'] = "Password Resettata %s";

// Errors
$lang['users error add_user_failed']          = "%s non può essere aggiunto!";
$lang['users error delete_user']              = "<strong>%s</strong> non può essere cancellato!";
$lang['users error edit_profile_failed']      = "Il tuo profilo non può essere modificato!";
$lang['users error edit_user_failed']         = "%s could not be modified!";
$lang['users error email_exists']             = "L' email <strong>%s</strong> è già presente!";
$lang['users error email_not_exists']         = "L'email non esiste!";
$lang['users error invalid_login']            = "Username o password invalidi";
$lang['users error password_reset_failed']    = "C'è stato un problema reimpostare la password. Si prega di riprovare.";
$lang['users error register_failed']          = "Il tuo account non è stato creato. Si prega di riprovare.";
$lang['users error user_id_required']         = "Un ID utente numerico è obbligatorio!";
$lang['users error user_not_exist']           = "Questo utente non esiste!";
$lang['users error username_exists']          = "L' username <strong>%s</strong> è già presente!";
$lang['users error validate_failed']          = "C'è stato un problema nel convalidare il tuo account. Si prega di riprovare.";
