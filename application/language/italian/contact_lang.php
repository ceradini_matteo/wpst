<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Contact Language File
 */

// Titles
$lang['contact title']                  = "Contattaci";
$lang['contact title messages_list']    = "Messaggi";

// Buttons
$lang['contact button read']            = "Leggi Messaggio";

// Table Columns
$lang['contact col message_id']         = "ID";
$lang['contact col name']               = "Nome";
$lang['contact col email']              = "Email";
$lang['contact col title']              = "Titolo";
$lang['contact col created']            = "Ricezione";

// Form Inputs
$lang['contact input name']             = "Nome";
$lang['contact input email']            = "Email";
$lang['contact input title']            = "Titolo";
$lang['contact input message']          = "Messaggio";
$lang['contact input captcha']          = "Testo CAPTCHA";
$lang['contact input created']          = "Ricevuto";

// Messages
$lang['contact msg send_success']       = "Grazie per averci contattato, %s! Il tuo messaggio è stato inviato.";
$lang['contact msg updated']            = "Messaggio Aggiornato!";

// Errors
$lang['contact error captcha']          = "Il testo CAPTCHA non corrisponde.";
$lang['contact error send_failed']      = "Scusa, %s. C'è stato un problema nell'invio del messaggio. Si prega di riprovare.";
$lang['contact error update_failed']    = "Aggiornamento non completato.";
