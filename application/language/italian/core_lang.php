<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Core Language File
 */

// Titles
$lang['core title welcome'] 				= "Benvenuto in %s";
$lang['core subtitle welcome'] 				= "SITO WEB IN COSTRUZIONE";

// Buttons
$lang['core button admin']                  = "Admin";
$lang['core button cancel']              	= "Annulla";
$lang['core button delete']              	= "Elimina";
$lang['core button close']               	= "Chiudi";
$lang['core button contact']              = "Contatti";
$lang['core button filter']              	= "Filtra";
$lang['core button home']                	= "Home";
$lang['core button login']               	= "Login";
$lang['core button register']		         = "Registrati";
$lang['core button logout']              	= "Logout";
$lang['core button profile']              	= "Profilo";
$lang['core button reset']               	= "Reset";
$lang['core button save']                	= "Salva";
$lang['core button search']              	= "Cerca";
$lang['core button toggle_nav']          	= "Toggle navigazione";
$lang['core button list']                  = "Elenco";

// Text
$lang['core text no']                    	= "No";
$lang['core text yes']                   	= "Sì";

// Emails
$lang['core email start']                	= "<!DOCTYPE html><html><head><style>
                                                    body { background: #fff; color: #333; }
                                               </style></head><body>";
$lang['core email end']                  	= "</body></html>";

// Additional date_lang
$lang['UM75']	                         	= "(UTC +1:00) Rome Time";

// Errors
$lang['core error no_results']              = "Nessun risultato trovato!";
$lang['table label rows']                 = "%s righe(a)";
