<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Language File
 */

// Titles
$lang['admin title admin']                = "Amministrazione";

// Buttons
$lang['admin button csv_export']          = "Esporta CSV";
$lang['admin button dashboard']           = "Dashboard";
$lang['admin button delete']              = "Cancella";
$lang['admin button edit']                = "Modifica";
$lang['admin button messages']            = "Messaggi";
$lang['admin button settings']            = "Impostazioni";
$lang['admin button users']               = "Utenti";
$lang['admin button users_add']           = "Aggiungi nuovo Utente";
$lang['admin button users_list']          = "Lista Utenti";
$lang['admin button fulfillments']        = "Adempimenti";
$lang['admin button tests']               = "Test";
$lang['admin button batteries']           = "Batterie";
$lang['admin button site']                = "Sito";

// Tooltips
$lang['admin tooltip csv_export']         = "Esporta un file CSV di tutti i risultati con un filtro applicato.";
$lang['admin tooltip filter']             = "Aggiorna i risultati in base al filtro impostato.";
$lang['admin tooltip filter_reset']       = "Azzera tutti i tuoi filtri.";

// Form Inputs
$lang['admin input active']               = "Attivo";
$lang['admin input inactive']             = "Disattivato";
$lang['admin input items_per_page']       = "elementi/pagina";
$lang['admin input select']               = "seleziona...";
$lang['admin input username']             = "Username";

// Table Columns
$lang['admin col actions']                = "Azioni";
$lang['admin col status']                 = "Stato";

// Form Labels
$lang['admin label rows']                 = "%s righe(a)";

