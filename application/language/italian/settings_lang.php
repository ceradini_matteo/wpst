<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Settings Language File
 */

// Titles
$lang['admin settings title']             = "Impostazioni";

// Messages
$lang['admin settings msg save_success']  = "Le impostazioni sono state salvate correttamente.";

// Errors
$lang['admin settings error save_failed'] = "Si è verificato un problema nel salvataggio delle impostazioni. Si prega di riprovare.";
