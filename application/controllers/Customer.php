<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends Private_Controller {

	/**
	 * Constructor
	 */
	function __construct()
	{
	    parent::__construct();
	
	    // load the users model
	    $this->load->model('customers_model','model');
	    
	    $this->lang->load('customers');
	    
	    // set constants
	    define('REFERRER', "referrer");
	    define('THIS_URL', base_url('customer'));
	    define('DEFAULT_LIMIT', $this->settings->per_page_limit);
	    define('DEFAULT_OFFSET', 0);
	    define('DEFAULT_SORT', "lastname");
	    define('DEFAULT_DIR', "asc");
	    
	    // use the url in session (if available) to return to the previous filter/sorted/paginated list
	    if ($this->session->userdata(REFERRER))
	    {
	        $this->_redirect_url = $this->session->userdata(REFERRER);
	    }
	    else
	    {
	        $this->_redirect_url = THIS_URL;
	    }
	}
	
	
	/**************************************************************************************
	 * PUBLIC FUNCTIONS
	 **************************************************************************************/
	
	
	/**
	 * Default
	 */
	function index() {
	  
	    // get parameters
	    $limit  = $this->input->get('limit')  ? $this->input->get('limit', TRUE)  : DEFAULT_LIMIT;
	    $offset = $this->input->get('offset') ? $this->input->get('offset', TRUE) : DEFAULT_OFFSET;
	    $sort   = $this->input->get('sort')   ? $this->input->get('sort', TRUE)   : DEFAULT_SORT;
	    $dir    = $this->input->get('dir')    ? $this->input->get('dir', TRUE)    : DEFAULT_DIR;
	    
	    // get filters
	    $filters = array();
	
	    if ($this->input->get('email'))
	    {
	        $filters['email'] = $this->input->get('email', TRUE);
	    }
	
	    if ($this->input->get('firstname'))
	    {
	        $filters['firstname'] = $this->input->get('firstname', TRUE);
	    }
	
	    if ($this->input->get('lastname'))
	    {
	        $filters['lastname'] = $this->input->get('lastname', TRUE);
	    }
	    
	    // build filter string
	    $filter = "";
	    foreach ($filters as $key => $value)
	    {
	        $filter .= "&{$key}={$value}";
	    } 
	    
	    // save the current url to session for returning
	    $this->session->set_userdata(REFERRER, THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");
			
			// are filters being submitted?
	    if ($this->input->post())
	    {
	        if ($this->input->post('clear'))
	        {
	            // reset button clicked
	            redirect(THIS_URL);
	        }
	        else
	        {
	            // apply the filter(s)
	            $filter = "";
	
	            if ($this->input->post('email'))
	            {
	                $filter .= "&email=" . $this->input->post('email', TRUE);
	            }
	
	            if ($this->input->post('firstname'))
	            {
	                $filter .= "&firstname=" . $this->input->post('firstname', TRUE);
	            }
	
	            if ($this->input->post('lastname'))
	            {
	                $filter .= "&lastname=" . $this->input->post('lastname', TRUE);
	            }
	
	            // redirect using new filter(s)
	            redirect(THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");
	        }
	    }
	    $filters['user_id'] =  $this->user['id'];
	    // get list
	    $customers = $this->model->get_all($limit, $offset, $filters, $sort, $dir);
	    
	    $config = array(
	        'base_url'   => THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}{$filter}",
	        'total_rows' => $customers['total'],
	        'per_page'   => $limit
	    );
      
	    $this->pagination->initialize($config);
	    
	    $this->set_title('<span class="ion-ios-people"></span> Elenco Clienti' );
	
			// setup page header data
			$this->add_js_theme( "customers.js")
				->set_page_header('Clienti');
			
			$data = $this->includes;
	
	    // set content data
	    $content_data = array(
	        'this_url'   => THIS_URL,
	        'customers' => $customers['results'],
	        'total'      => $customers['total'],
	        'filters'    => $filters,
	        'filter'     => $filter,
	        'pagination' => $this->pagination->create_links(),
	        'limit'      => $limit,
	        'offset'     => $offset,
	        'sort'       => $sort,
	        'dir'        => $dir
	    );
	
	    // load views
	    $data['content'] = $this->load->view('customer/customers_list', $content_data, TRUE);
	    $this->load->view($this->template, $data);
	    
	}
	
	
	/**
	 * Add and edit new user
	 */
	function form($id = null)
	{
	  $this->load->model('consultants_model');
	  
	  $customer = NULL;
	  if($id){
		  $customer=$this->model->get_customer($id);	    
	  }
		// validators
		$this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
		$this->form_validation->set_rules('firstname', lang('customers input firstname'), 'trim|min_length[2]|max_length[32]');
		$this->form_validation->set_rules('lastname', lang('customers input lastname'), 'trim|min_length[2]|max_length[32]');
		$this->form_validation->set_rules('email', lang('customers input email'), 'trim|max_length[128]|valid_email|callback__check_email[' . $customer['email'] . ']');
		$this->form_validation->set_rules('phone', lang('customers input phone'), 'trim|max_length[20]');
		$this->form_validation->set_rules('address', lang('customers input address'), 'trim|max_length[200]');
		$this->form_validation->set_rules('cod_fis', lang('customers input cod_fis'), 'trim|min_length[11]|max_length[16]|strtoupper');
		
		if ($this->form_validation->run() == FALSE){
			// setup page header data
			$this
				->add_js_theme( "customers.js", false );
				
			
			if($id){			
				$this->set_title( lang('customers title customer_edit') );
			}else{
				$this->set_title( lang('customers title customer_add') );	
			}
			
			$this->set_breadcrumb(array(
				'admin/users' => lang('customers title customer_list'),
				'' => lang('customers title customer_add') 
			));
			
			$data = $this->includes;
			$this->load->model('geo_model');
			$filters = array();
			$filters['user_id'] =  $this->user['id'];
		    // get list
		    $consultants = $this->consultants_model->get_all(null, null, $filters, 'lastname', 'asc');
		    $consultants_dropdown = array();
		    if($consultants['results']){
			    foreach($consultants['results'] as $consultant){
				    $consultants_dropdown[$consultant['id']] = $consultant['lastname']." ".$consultant['firstname'];
			    }
		    }
			// set content data
			$content_data = array(
			    'provinces'        => $this->geo_model->all_for_dropdown('provinces'),
			    'cancel_url'        => $this->_redirect_url,
			    'customer'              => $customer,
			    'consultants_dropdown' => $consultants_dropdown,
			    'password_required' => TRUE
			);
			
			// load views
			$data['content'] = $this->load->view('customer/customer_form', $content_data, TRUE);
			$this->load->view($this->template, $data);
			
		}else
		{
			// save the user
			$data = $this->input->post();
			$data = get_array_fields($data,$this->config->item('customer_fields'));
			$data = convert_array_dates_fields($data,$this->config->item('customer_dates_fields'));
			$data['user_id'] = $this->user['id'];
			if(!$data['stampa_registri']){
				$data['stampa_registri']=null;
			}
			
			if($data['consultant_id']=='') $data['consultant_id']=null;
			if($data['type_id']=='') $data['type_id']=null;
			
			
			if($id){
				$data['id'] = $id;
			}
			$saved = $this->model->save($data);
			
	    if ($saved)
	    {
		    if($id){
	        $this->session->set_flashdata('message', sprintf(lang('customers msg edit_customer_success'), $this->input->post('company') . " " .$this->input->post('firstname') . " " . $this->input->post('lastname')));
	      }else{
		      $this->session->set_flashdata('message', sprintf(lang('customers msg add_customer_success'), $this->input->post('company') . " " .$this->input->post('firstname') . " " . $this->input->post('lastname')));
	      }
	    }
	    else
	    {
		    if($id){
			    $this->session->set_flashdata('error', sprintf(lang('customers error edit_customer_failed'), $this->input->post('company') . " " .$this->input->post('firstname') . " " . $this->input->post('lastname')));
		    }else{
	        $this->session->set_flashdata('error', sprintf(lang('customers error add_customer_failed'), $this->input->post('company') . " " .$this->input->post('firstname') . " " . $this->input->post('lastname')));
	      }
	    }
		
		    // return to list and display message
		    redirect($this->_redirect_url);
		}
	}
	
	
	function get_detail($id){
		$this->model->user_permitted($this->user['id'],$id);
		$data['customer'] = $this->model->get_customer($id);
		$this->load->view('customer/customer_detail', $data);
		
	}
	
	/**
	 * Make sure email is available
	 *
	 * @param  string $email
	 * @param  string|null $current
	 * @return int|boolean
	 */
	function _check_email($email, $current)
	{
	    if (trim($email) != trim($current) && $this->model->email_exists($email,$this->user['id']))
	    {
	        $this->form_validation->set_message('_check_email', sprintf(lang('customers error email_exists'), $email));
	        return FALSE;
	    }
	    else
	    {
	        return $email;
	    }
	}
	
	function delete($id){
		if($id){
			$this->model->user_permitted($this->user['id'],$id);
			$this->model->delete($id);	
		}
	}

}
