<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Public_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the users model
        $this->load->model('users_model');

        // load the users language file
        $this->lang->load('users');
    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * Default
     */
    function index() {}


    /**
     * Validate login credentials
     */
    function login()
    {
        if ($this->session->userdata('logged_in'))
        {
            $logged_in_user = $this->session->userdata('logged_in');

            if ($logged_in_user['is_admin'])
            {
                redirect('admin');
            }
            else
            {
                redirect(base_url());
            }
        }

        // set form validation rules
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('username', lang('users input username_email'), 'required|trim');
        $this->form_validation->set_rules('password', lang('users input password'), 'required|trim|callback__check_login');

        if ($this->form_validation->run() == TRUE)
        {
            if ($this->session->userdata('redirect'))
            {
                $redirect = $this->session->userdata('redirect');

                $this->session->unset_userdata('redirect');
                redirect($redirect);
            }
            else
            {
                $logged_in_user = $this->session->userdata('logged_in');

                $this->users_model->increment_login_number($logged_in_user['id']);

                if ($logged_in_user['is_admin'])
                {
                    redirect('admin');
                }
                else
                {
                    redirect(base_url());
                }
            }
        }

        // setup page header data
		$this->add_css_theme('login.css');

        $this->set_title( '<span class="ion-log-in"></span> '.lang('users title login') );

        $data = $this->includes;
        $content_data = array(
            'welcome_message' => $this->settings->welcome_message
        );
        
        // load views
        $data['content'] = $this->load->view('user/login', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Logout
     */
    function logout()
    {
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
        redirect('login');
    }

    /**
     * Validate new account
     */
    function validate()
    {
        // get codes
        $encrypted_email = $this->input->get('e');
        $validation_code = $this->input->get('c');

        // validate account
        $validated = $this->users_model->validate_account($encrypted_email, $validation_code);

        if ($validated)
        {
            $this->session->set_flashdata('message', lang('users msg validate_success'));
        }
        else
        {
            $this->session->set_flashdata('error', lang('users error validate_failed'));
        }

        redirect(base_url());
    }


    /**
	 * Default
     */
	function forgot()
    {
        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('email', lang('users input email'), 'required|trim');

        if ($this->form_validation->run() == TRUE)
        {
            // save the changes
            @$email_or_username = $this->input->post('email');

            if($email_or_username)
            {
                $results = $this->users_model->forgot_password($email_or_username);

                if($results)
                {
                    $reset_url = site_url('user/reset/' . $results['reset_code']);

                    // build email
                    $email_msg  = lang('core email start');
                    $email_msg .= sprintf(lang('users msg email_password_reset'), $this->settings->site_name, $reset_url);
                    $email_msg .= lang('core email end');

                    // send email
                    $this->load->library('email');
                    $this->email->set_mailtype("html");
                    $this->email->clear();
                    $this->email->from($this->settings->site_email, $this->settings->site_name);
                    $this->email->reply_to($this->settings->site_email, $this->settings->site_name);
                    $this->email->to($results['email']);
                    $this->email->subject(sprintf(lang('users msg email_password_reset_title'), $results['username']));
                    $this->email->message($email_msg);
                    $this->email->send();
                    $this->session->set_flashdata('message', lang('users msg password_reset_success'));
                }
                else
                {
                    $this->session->set_flashdata('error', lang('users error password_reset_failed'));
                }

                redirect(base_url('login'));
            }
        }

        // setup page header data
        $this->set_title( lang('users title forgot') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url' => base_url(),
            'user'       => NULL
        );

        // load views
        $data['content'] = $this->load->view('user/forgot_form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    function reset($reset_code)
    {
        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('password', lang('users input password'), 'min_length[5]|matches[password_repeat]');
        $this->form_validation->set_rules('password_repeat', lang('users input password_repeat'), 'min_length[5]');

        // check if reset code is valid, else redirect to login page
        if(!$this->users_model->validate_user_reset_code($reset_code)){
            $this->session->set_flashdata('error', lang('users error invalid_reset_code'));
            redirect(base_url('login'));
        }

        $post = $this->input->post();

        if($this->form_validation->run() == TRUE)
        {
            $saved = $this->users_model->reset_password($post['reset_code'], $post['password']);

            if($saved)
            {
                $this->session->set_flashdata('message', lang('users msg edit_profile_success'));
            }
            else
            {
                $this->session->set_flashdata('error', lang('users error edit_profile_failed'));
            }

            // reload page and display message
            redirect(base_url('login'));
        }

        // setup page header data
        $this->set_title('<span class="ion-person"></span> '. lang('users title reset_password') );
        $this->add_js_theme('profile.js',  TRUE);
        $this->add_external_js('https://js.braintreegateway.com/web/dropin/1.3.1/js/dropin.min.js');

        $data = $this->includes;

        // set content data
        $content_data = array(
            'reset_code' => $reset_code
        );

        // load views
        $data['content'] = $this->load->view('user/reset_password_form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**************************************************************************************
     * PRIVATE VALIDATION CALLBACK FUNCTIONS
     **************************************************************************************/


    /**
     * Verify the login credentials and date_time to access
     *
     * @param  string $password
     * @return boolean
     */
    function _check_login($password)
    {
        $login = $this->users_model->login($this->input->post('username', TRUE), $password);

        if ($login)
        {
            if(!empty($login['start_time']) && strtotime($login['start_time']) > strtotime('now'))
            {
                $this->form_validation->set_message('_check_login', lang('users error login_time'));
                return FALSE;
            }

            if(!empty($login['end_time']) && strtotime('now') > strtotime($login['end_time']))
            {
                $this->form_validation->set_message('_check_login', lang('users error login_time'));
                return FALSE;
            }

            $this->session->set_userdata('logged_in', $login);

            return TRUE;
        }

        $this->form_validation->set_message('_check_login', lang('users error invalid_login'));
        return FALSE;
    }


    /**
     * Make sure username is available
     *
     * @param  string $username
     * @return int|boolean
     */
    function _check_username($username)
    {
        if ($this->users_model->username_exists($username))
        {
            $this->form_validation->set_message('_check_username', sprintf(lang('users error username_exists'), $username));
            return FALSE;
        }
        else
        {
            return $username;
        }
    }


    /**
     * Make sure email is available
     *
     * @param  string $email
     * @return int|boolean
     */
    function _check_email($email)
    {
        if ($this->users_model->email_exists($email))
        {
            $this->form_validation->set_message('_check_email', sprintf(lang('users error email_exists'), $email));
            return FALSE;
        }
        else
        {
            return $email;
        }
    }


    /**
     * Make sure email exists
     *
     * @param  string $email
     * @return int|boolean
     */
    function _check_email_exists($email)
    {
        if ( ! $this->users_model->email_exists($email))
        {
            $this->form_validation->set_message('_check_email_exists', sprintf(lang('users error email_not_exists'), $email));
            return FALSE;
        }
        else
        {
            return $email;
        }
    }
}
