<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tests_result extends Admin_Controller {

    /**
     * @var string
     */
    private $_redirect_url;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('tests_result');

        // load the tests model
        $this->load->model(array('tests_model'));

        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('admin/tests_result'));
        define('DEFAULT_LIMIT', $this->settings->per_page_limit);
        define('DEFAULT_OFFSET', 0);
        define('DEFAULT_SORT', "type");
        define('DEFAULT_DIR', "asc");
        ini_set('memory_limit', '40096M');

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL;
        }
    }

    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * tests list page
     */
    function index()
    { 
        // get parameters
        $limit  = $this->input->get('limit')  ? $this->input->get('limit', TRUE)  : DEFAULT_LIMIT;
        $offset = $this->input->get('offset') ? $this->input->get('offset', TRUE) : DEFAULT_OFFSET;
        $sort   = $this->input->get('sort')   ? $this->input->get('sort', TRUE)   : DEFAULT_SORT;
        $dir    = $this->input->get('dir')    ? $this->input->get('dir', TRUE)    : DEFAULT_DIR;

        $filters = array();

        if ($this->input->get('type'))
        {
            $filters['type'] = $this->input->get('type', TRUE);
        }

        if ($this->input->get('number'))
        {
            $filters['number'] = $this->input->get('number', TRUE);
        }

        if ($this->input->get('slides'))
        {
            $filters['slides'] = $this->input->get('slides', TRUE);
        }

        if ($this->input->get('description'))
        {
            $filters['description'] = $this->input->get('description', TRUE);
        }

        // build filter string
        $filter = "";

        foreach ($filters as $key => $value)
        {
            $filter .= "&{$key}={$value}";
        }

        // save the current url to session for returning
        $this->session->set_userdata(REFERRER, THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");

        // are filters being submitted?
        if ($this->input->post())
        {
            if ($this->input->post('clear'))
            {
                // reset button clicked
                redirect(THIS_URL);
            }
            else
            {
                // apply the filter(s)
                $filter = "";

                if ($this->input->post('number'))
                {
                    $filter .= "&number=" . $this->input->post('number', TRUE);
                }

                if ($this->input->post('type'))
                {
                    $filter .= "&type=" . $this->input->post('type', TRUE);
                }

                if ($this->input->post('slides'))
                {
                    $filter .= "&slides=" . $this->input->post('slides', TRUE);
                }

                if ($this->input->post('description'))
                {
                    $filter .= "&description=" . $this->input->post('description', TRUE);
                }

                // redirect using new filter(s)
                redirect(THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");
            }
        }

        // get list
        $tests           = $this->tests_model->get_all($limit, $offset, $filters, $sort, $dir);
        $no_test_results = false;

        foreach($tests['results'] as $key => $test)
        {
            $attempts = $this->tests_model->get_count_test_results($test['id']);
            $tests['results'][$key]['attempts'] = $attempts;
        }

        if($sort == 'tests' && $dir == 'desc')
        {
            arsort($tests['results']);  //reverse array
        }

        // build pagination
        $this->pagination->initialize(array(
            'base_url'   => THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}{$filter}",
            'total_rows' => $tests['total'],
            'per_page'   => $limit
        ));

        // setup page header data
        $this->add_js_theme(array('tests_result.js'));
        $this->set_title(lang('tests title tests_list'));
        $this->set_breadcrumb(array(
            '' => lang('tests title tests_list')
        ));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'        => THIS_URL,
            'tests'           => $tests['results'],
            'no_test_results' => $no_test_results,
            'total'           => $tests['total'],
            'filters'         => $filters,
            'filter'          => $filter,
            'pagination'      => $this->pagination->create_links(),
            'limit'           => $limit,
            'offset'          => $offset,
            'sort'            => $sort,
            'dir'             => $dir
        );

        // load views
        $data['content'] = $this->load->view('admin/tests_result/list', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }
    
    /**
     * Delete one or more tests
     *
     * @param  int $id
     */
    function delete($id = NULL)
    {
        if(is_null($id) && !empty($this->input->post('selected')))
        {
            $tests = $this->input->post('selected');

            foreach($tests as $test_id)
            {
                $test = $this->tests_model->get($test_id);

                if($test)
                {
                    $delete = $this->tests_model->delete_test_results($test_id);                        

                    if($delete)
                    {
                        $this->session->set_flashdata('message', sprintf(lang('tests msg delete_test')));
                    }
                    else
                    {
                        $this->session->set_flashdata('error', sprintf(lang('tests error delete_test')));
                        break;
                    }
                }                         
            }
        }
        else if(is_numeric($id))      // make sure we have a numeric id
        {
            // get test details
            $test = $this->tests_model->get($id);

            if ($test)
            {
                $delete = $this->tests_model->delete_test_results($id);

                if($delete)
                {
                    $this->session->set_flashdata('message', sprintf(lang('tests msg delete_test'), $test['firstname'] . " " . $test['lastname']));
                }
                else
                {
                    $this->session->set_flashdata('error', sprintf(lang('tests error delete_test'), $test['firstname'] . " " . $test['lastname']));
                }
            }
            else
            {
                $this->session->set_flashdata('error', lang('tests error test_not_exist'));
            }
        }
        else
        { 
            $this->session->set_flashdata('error', lang('tests error test_id_required'));
        }

        // return to list and display message
        redirect($this->_redirect_url);
    }

    function export_test_result($test_id)
    {
        $test         = $this->tests_model->get($test_id);
        $tests_result = $this->tests_model->get_test_results($test_id);

        $this->tests_model->set_test_read($test_id);

        foreach($tests_result as $key => $test_result)
        {
            $tests_result[$key]['started'] = date("m/d/Y H:i:s", $test_result['started']);
            $tests_result[$key]['ended']   = date("m/d/Y H:i:s", $test_result['ended']);
        }

        array_to_csv($tests_result, "summary_" . $test['type'] . "_" . $test['number'] . ".csv");

        exit;
    }

    function export_test_result_slides($test_id)
    {
        $test = $this->tests_model->get($test_id);

        $this->tests_model->set_test_read($test_id);

        $offset     = 0;
        $page       = 1;
        $page_limit = 50000;
        $header     = $test['type'] . "_" . $test['number'];

        list($offset, $tests_result_slides) = $this->tests_model->get_test_results_slides($test_id, $page_limit, $offset);

        while($tests_result_slides){
            $files[] = $this->save_result_slides_to_file($header, $tests_result_slides, $page, false, false);
            list($offset, $tests_result_slides) = $this->tests_model->get_test_results_slides($test_id, $page_limit, $offset);
            $page++;
        }

        $zip      = new ZipArchive();
        $zip_name = $header . ".zip";

        if ($zip->open($zip_name, ZIPARCHIVE::CREATE) !== TRUE) {
            exit("Unable to create the .zip file");
        }

        foreach($files as $file){
            $zip->addFile($file);
        }

        $zip->close();

        // set the headers for file download
        header("Content-type: application/zip");
        header("Content-Disposition: attachment; filename=$zip_name");
        header("Content-length: " . filesize($zip_name));
        header("Pragma: no-cache");
        header("Expires: 0");
        readfile("$zip_name");

        unlink($zip_name);

        foreach($files as $file){
            unlink($file);
        }

        exit;
    }

    function export_test_result_slides_big($test_id)
    {
        $test = $this->tests_model->get($test_id);

        $this->tests_model->set_test_read($test_id);

        $offset           = 0;
        $page_limit       = 25000;
        $header           = $test['type'] . "_" . $test['number'];
        $header_displayed = false;
        $file_name        = 'export_data/datafile_answer_' . $header . '.csv';

        if (file_exists($file_name)){
            unlink($file_name);
        }

        list($offset, $temp) = $this->tests_model->get_test_results_slides($test_id, $page_limit, $offset);

        while ($temp) {
            $this->save_result_slides_to_file($header, $temp, false, 'export_data/', $header_displayed);
            $header_displayed = true;
            list($offset, $temp) = $this->tests_model->get_test_results_slides($test_id, $page_limit, $offset);
        }

        redirect($this->_redirect_url);
    }


    private function save_result_slides_to_file($header, $tests_result_slides, $page = false, $folder = false, $header_displayed = true){
        $output = array();

        foreach($tests_result_slides as $key => $result_slide)
        {
            unset($tests_result_slides[$key]['id']);

            $timestamp = $result_slide['start'];

            // if ms is present: extract ms from timestamp and remove ms from timestamp
            if(strlen($timestamp) > 10){
                $ms        = substr($timestamp, strlen($timestamp) - 3);
                $timestamp = substr($timestamp, 0, strlen($timestamp) - 3);
            }
            else {
                $ms = '';
            }

            unset($tests_result_slides[$key]['start']);

            $tests_result_slides[$key]['start_date'] = date("m/d/Y", $timestamp);
            $tests_result_slides[$key]['start_hr']   = date("H", $timestamp);
            $tests_result_slides[$key]['start_min']  = date("i", $timestamp);
            $tests_result_slides[$key]['start_sec']  = date("s", $timestamp);
            $tests_result_slides[$key]['start_ms']   = $ms;

            $username = $result_slide['username'];

            if(!isset($output[$username]))
            {
                $output[$username] = array(
                    $header . "_user_ID" => $username
                );

                $question = 1;
            }

            $header_q = '_Q-' . $question;

            $output[$username][$header . "_start_date"]           = $tests_result_slides[$key]['start_date'];
            $output[$username][$header . $header_q . "_n_slide"]  = 's_' . $result_slide['number'];
            $output[$username][$header . $header_q . "_hour"]     = $tests_result_slides[$key]['start_hr'];
            $output[$username][$header . $header_q . "_minute"]   = $tests_result_slides[$key]['start_min'];
            $output[$username][$header . $header_q . "_second"]   = $tests_result_slides[$key]['start_sec'];
            $output[$username][$header . $header_q . "_ms"]       = $tests_result_slides[$key]['start_ms'];
            $output[$username][$header . $header_q . "_r_1"]      = $result_slide['r_1'];
            $output[$username][$header . $header_q . "_r_1_time"] = $result_slide['r_1_time'];

            if(!is_null($result_slide['r_2']) && $result_slide['r_2'] != "")
            {
                $output[$username][$header . $header_q . "_r_2"]      = $result_slide['r_2'];
                $output[$username][$header . $header_q . "_r_2_time"] = $result_slide['r_2_time'];
            }
            else
            {
                $output[$username][$header . $header_q . "_r_2"]      = null;
                $output[$username][$header . $header_q . "_r_2_time"] = null;
            }

            if(!is_null($result_slide['r_3']) && $result_slide['r_3'] != "")
            {
                $output[$username][$header . $header_q . "_r_3"]      = $result_slide['r_3'];
                $output[$username][$header . $header_q . "_r_3_time"] = $result_slide['r_3_time'];
            }
            else
            {
                $output[$username][$header . $header_q . "_r_3"]      = null;
                $output[$username][$header . $header_q . "_r_3_time"] = null;
            }

            if(!is_null($result_slide['r_4']) && $result_slide['r_4'] != "")
            {
                $output[$username][$header . $header_q . "_r_4"]      = $result_slide['r_4'];
                $output[$username][$header . $header_q . "_r_4_time"] = $result_slide['r_4_time'];
            }
            else
            {
                $output[$username][$header . $header_q . "_r_4"]      = null;
                $output[$username][$header . $header_q . "_r_4_time"] = null;
            }

            if(!is_null($result_slide['r_5']) && $result_slide['r_5'] != "")
            {
                $output[$username][$header . $header_q . "_r_5"]      = $result_slide['r_5'];
                $output[$username][$header . $header_q . "_r_5_time"] = $result_slide['r_5_time'];
            }
            else
            {
                $output[$username][$header . $header_q . "_r_5"]      = null;
                $output[$username][$header . $header_q . "_r_5_time"] = null;
            }

            $question++;
        }

        if($page) {
            $filename = "datafile_answer_" . $header . "_page_" . $page . ".csv";
        }
        else {
            $filename = "datafile_answer_" . $header . ".csv";
        }

        if($folder){
            $filename = $folder . $filename;
        }

        array_to_csv_file($output, $filename, $header_displayed, 'a');

        return $filename;
    }
}