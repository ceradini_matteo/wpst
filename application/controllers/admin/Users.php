<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Admin_Controller {

    /**
     * @var string
     */
    private $_redirect_url;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('users');

        // load the users model
        $this->load->model('users_model');

        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('admin/users'));
        define('DEFAULT_LIMIT', $this->settings->per_page_limit);
        define('DEFAULT_OFFSET', 0);
        define('DEFAULT_SORT', "username");
        define('DEFAULT_DIR', "asc");

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL;
        }
    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * User list page
     */
    function index()
    {     
        ini_set('memory_limit', $this->config->item('memory_limit'));       
        $this->load->model('batteries_model');
        // get parameters
        $limit  = $this->input->get('limit')  ? $this->input->get('limit', TRUE)  : DEFAULT_LIMIT;
        $offset = $this->input->get('offset') ? $this->input->get('offset', TRUE) : DEFAULT_OFFSET;
        $sort   = $this->input->get('sort')   ? $this->input->get('sort', TRUE)   : DEFAULT_SORT;
        $dir    = $this->input->get('dir')    ? $this->input->get('dir', TRUE)    : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get('username'))
        {
            $filters['username'] = $this->input->get('username', TRUE);
        }

        if ($this->input->get('firstname'))
        {
            $filters['firstname'] = $this->input->get('firstname', TRUE);
        }

        if ($this->input->get('lastname'))
        {
            $filters['lastname'] = $this->input->get('lastname', TRUE);
        }

        if ($this->input->get('group_id'))
        {
            $filters['group_id'] = $this->input->get('group_id', TRUE);
        }

        if ($this->input->get('start_time'))
        {
            $filters['start_time'] = $this->input->get('start_time', TRUE);
        }

        if ($this->input->get('end_time'))
        {
            $filters['end_time'] = $this->input->get('end_time', TRUE);
        }

        if ($this->input->get('assigned_battery'))
        {
            $filters['assigned_battery'] = $this->input->get('assigned_battery', TRUE);
        }

        // build filter string
        $filter = "";

        foreach ($filters as $key => $value)
        {
            $filter .= "&{$key}={$value}";
        }

        // save the current url to session for returning
        $this->session->set_userdata(REFERRER, THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");

        // are filters being submitted?
        if ($this->input->post())
        {
            if ($this->input->post('clear'))
            {
                // reset button clicked
                redirect(THIS_URL);
            }
            else
            {
                // apply the filter(s)
                $filter = "";

                if ($this->input->post('username'))
                {
                    $filter .= "&username=" . $this->input->post('username', TRUE);
                }

                if ($this->input->post('firstname'))
                {
                    $filter .= "&firstname=" . $this->input->post('firstname', TRUE);
                }

                if ($this->input->post('lastname'))
                {
                    $filter .= "&lastname=" . $this->input->post('lastname', TRUE);
                }

                if ($this->input->post('group_id'))
                {
                    $filter .= "&group_id=" . $this->input->post('group_id', TRUE);
                }

                if ($this->input->post('start_time'))
                {
                    //format date
                    $filter .= "&start_time=" . date('Y-m-d H:i', strtotime($this->input->post('start_time', TRUE)));
                }

                if ($this->input->post('end_time'))
                {
                    //format date
                    $filter .= "&end_time=" . date('Y-m-d H:i', strtotime($this->input->post('end_time', TRUE)));
                }

                if ($this->input->post('assigned_battery'))
                {
                    $filter .= "&assigned_battery=" . $this->input->post('assigned_battery', TRUE);
                }

                // redirect using new filter(s)
                redirect(THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");
            }
        }

        // get list
        $users = $this->users_model->get_all($limit, $offset, $filters, $sort, $dir, TRUE);

        
        // build pagination
        $config = array(
            'base_url'   => THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}{$filter}",
            'total_rows' => $users['total'],
            'per_page'   => $limit
        );
      
        $this->pagination->initialize($config);

		// setup page header data
		$this->add_js_theme(array('users.js','tinymce/tinymce.min.js'))
             ->set_title( lang('users title user_list') );
		$this->set_breadcrumb(array(
			'' => lang('users title user_list')
		));
		
        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'   => THIS_URL,
            'users'      => $users['results'],
            'total'      => $users['total'],
            'filters'    => $filters,
            'filter'     => $filter,
            'pagination' => $this->pagination->create_links(),
            'limit'      => $limit,
            'offset'     => $offset,
            'sort'       => $sort,
            'dir'        => $dir,
            'batteries'  => $this->batteries_model->get(),
            'backups'    => $this->get_backups()
        );

        // load views
        $data['content'] = $this->load->view('admin/users/list', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }
    
    function form($id = false)
    {
        $this->load->model('batteries_model');

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('username', lang('users input username'), 'required|trim|min_length[5]|max_length[30]|callback__check_username[]');
        $this->form_validation->set_rules('email', lang('users input email'), 'trim|max_length[128]|valid_email|callback__check_email[]');
        $this->form_validation->set_rules('firstname', lang('users input firstname'), 'trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('lastname', lang('users input lastname'), 'trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('group_id', lang('users input group_id'), 'trim|max_length[50]');
        $this->form_validation->set_rules('assigned_battery', lang('users input assigned_battery'), 'trim');
        $this->form_validation->set_rules('start_time', lang('users input start_time'), 'trim');
        $this->form_validation->set_rules('end_time', lang('users input end_time'), 'trim');        
        $this->form_validation->set_rules('race', lang('users input race'), 'trim|max_length[30]');        
        $this->form_validation->set_rules('gender', lang('users input gender'), 'trim');
        $this->form_validation->set_rules('birth_date', lang('users input birth_date'), 'trim');    

        if($id)
        {
            $this->form_validation->set_rules('password', lang('users input password'), 'trim|min_length[5]|matches[password_repeat]');
            $this->form_validation->set_rules('password_repeat', lang('users input password_repeat'), 'trim|matches[password]');
        }
        else
        {
            $this->form_validation->set_rules('password', lang('users input password'), 'required|trim|min_length[5]');
            $this->form_validation->set_rules('password_repeat', lang('users input password_repeat'), 'required|trim|matches[password]');
        }
        

        if($id)
        {
            // get the data
            $user = $this->users_model->get_user($id);

            // if empty results, return to list
            if (!$user)
            {
                redirect($this->_redirect_url);
            }

            $this->form_validation->set_rules('username', lang('users input username'), 'required|trim|min_length[5]|max_length[30]|callback__check_username[' . $user['username'] . ']');
            $this->form_validation->set_rules('email', lang('users input email'), 'trim|max_length[128]|valid_email|callback__check_email[' . $user['email'] . ']');
        }   

        if ($this->form_validation->run() == TRUE)
        {
            // save the changes
            $data = $this->input->post();
            $data = get_array_fields($data,$this->config->item('user_fields'));         

            //format date
            if(!empty($data['start_time'])){
                $data['start_time'] = date('Y/m/d H:i', strtotime($data['start_time']));
            }
            else {
               $data['start_time'] = NULL; 
            }
            
            if(!empty($data['end_time'])){
                $data['end_time'] = date('Y/m/d H:i', strtotime($data['end_time']));
            }
            else {
               $data['end_time'] = NULL; 
            }

            if(empty($data['birth_date'])){
                $data['birth_date'] = NULL;
            }

            if($data['assigned_battery'] == 0){
                $data['assigned_battery'] = NULL;
            }

            if($id)
            {
                $saved = $this->users_model->edit_user($data);
            }
            else
            {
                $saved = $this->users_model->add_user($data);
            }

            if ($saved)
            {
                $this->session->set_flashdata('message', sprintf(lang('users msg edit_user_success'), $this->input->post('firstname') . " " . $this->input->post('lastname')));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('users error edit_user_failed'), $this->input->post('firstname') . " " . $this->input->post('lastname')));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data
        $this->set_title( lang('users title user_edit') );
        $this->set_breadcrumb(array(
            'admin/users' => lang('users title user_list'),
            '' => lang('users title user_edit') 
        ));
        
        $data = $this->includes;        

        // set content data
        $content_data = array(
            'user'              => NULL,
            'cancel_url'        => $this->_redirect_url,
            'password_required' => FALSE,
            'batteries'         => $this->batteries_model->get()
        );

        if($id)
        {
            $content_data['user']   = $user;
            $content_data['user_id'] = $id;
        }
        else
        {
            $content_data['password_required'] = TRUE;
        }

        // load views
        $data['content'] = $this->load->view('admin/users/form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }    

	function getEditableForm()
    {
        $this->load->model('batteries_model');

		$data = $this->input->post();

        if($data['type'] == 'select'){
            $data['batteries'] = $this->batteries_model->get();
        }

		echo $this->load->view('admin/users/editable_form', $data, TRUE);
	}

    //For editable inline
    function save()
    {
        if(!empty($this->input->post()))
        {
            $fields = $this->input->post();

            $data['id'] = $fields['id'];

            if($fields['type'] == 'datetime')       //format date
            {
                if(!empty($fields['value'])){
                    $data[$fields['name']] = date('Y/m/d H:i', strtotime($fields['value']));
                }
                else {
                    $data[$fields['name']] = NULL; 
                }
            }
            else 
            {
                if($fields['type'] == 'select' && $fields['value'] == 0){
                    $fields['value'] = NULL;
                }

                $data[$fields['name']] = $fields['value'];
            } 

            $this->users_model->edit_user($data);
        }
    }

    /**
     * Delete a user
     *
     * @param  int $id
     */
    function delete($id = NULL)
    {
        if(is_null($id) && !empty($this->input->post('selected')))
        {
            $users = $this->input->post('selected');

            foreach($users as $user_id)
            {
                if($user_id > 1)    //I can't delete principal user
                {
                    $user = $this->users_model->get_user($user_id);

                    if ($user)
                    {
                        // delete the user
                        $delete = $this->users_model->delete_user($user_id);
                    }      
                }                          
            }
        }
        else if (is_numeric($id))      // make sure we have a numeric id
        {
            // get user details
            $user = $this->users_model->get_user($id);

            if ($user)
            {
                // delete the user
                $delete = $this->users_model->delete_user($id);

                if($delete)
                {
                    $this->session->set_flashdata('message', sprintf(lang('users msg delete_user'), $user['firstname'] . " " . $user['lastname']));
                }
                else
                {
                    $this->session->set_flashdata('error', sprintf(lang('users error delete_user'), $user['firstname'] . " " . $user['lastname']));
                }
            }
            else
            {
                $this->session->set_flashdata('error', lang('users error user_not_exist'));
            }
        }
        else
        { 
            $this->session->set_flashdata('error', lang('users error user_id_required'));
        }

        // return to list and display message
        redirect($this->_redirect_url);
    }


    /**
     * Export list to CSV
     */
    function export()
    {        
        ini_set('memory_limit', $this->config->item('memory_limit'));
        // get parameters
        $sort = $this->input->get('sort') ? $this->input->get('sort', TRUE) : DEFAULT_SORT;
        $dir  = $this->input->get('dir')  ? $this->input->get('dir', TRUE)  : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get('username'))
        {
            $filters['username'] = $this->input->get('username', TRUE);
        }

        if ($this->input->get('firstname'))
        {
            $filters['firstname'] = $this->input->get('firstname', TRUE);
        }

        if ($this->input->get('lastname'))
        {
            $filters['lastname'] = $this->input->get('lastname', TRUE);
        }

        if ($this->input->get('group_id'))
        {
            $filters['group_id'] = $this->input->get('group_id', TRUE);
        }

        if ($this->input->get('start_time'))
        {
            $filters['start_time'] = $this->input->get('start_time', TRUE);
        }

        if ($this->input->get('end_time'))
        {
            $filters['end_time'] = $this->input->get('end_time', TRUE);
        }

        // get all users (no admin)
        $users = $this->users_model->get_all(0, 0, $filters, $sort, $dir, TRUE, TRUE);

        if ($users['total'] > 0)
        {
            // manipulate the output array
            foreach ($users['results'] as $key=>$user)
            {
                $users['results'][$key]['password'] = '';

                if(!empty($users['results'][$key]['start_time'])){
                    $users['results'][$key]['start_time'] = date('m/d/Y h:i A', strtotime($users['results'][$key]['start_time']));
                }

                if(!empty($users['results'][$key]['end_time'])){
                    $users['results'][$key]['end_time'] = date('m/d/Y h:i A', strtotime($users['results'][$key]['end_time']));
                }
                
                unset($users['results'][$key]['salt']);
                unset($users['results'][$key]['is_admin']);
                unset($users['results'][$key]['assigned_battery']);
            }

            // export the file
            array_to_csv($users['results'], "users.csv");
        }
        else
        {
            // nothing to export
            $this->session->set_flashdata('error', lang('core error no_results'));
            redirect($this->_redirect_url);
        }

        exit;
    }

    /**
        Import users from CSV
     */
    function import()
    {
        ini_set('memory_limit', $this->config->item('memory_limit'));
        $config['upload_path']   = './uploads/csv/';
        $config['allowed_types'] = 'csv';
        $this->load->library('upload', $config);
        $this->load->model('batteries_model');

        if (!$this->upload->do_upload('userfile'))
        {
            $this->session->set_flashdata('error', lang('users msg upload_error'));
            redirect($this->_redirect_url);
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $file_name = $data['upload_data']['file_name'];            
            $users = csv_to_array($config['upload_path'], $file_name);
            unlink($config['upload_path'] . $file_name);                 //delete csv uploaded file

            $users_db      = $this->users_model->get_user_by_id(false);  
            $fields_error  = FALSE;      //TRUE if ther are errors in fields format or other
            $error_msg     = '<div class="m-t-1"></div>Rows affected: ';
            $new_usernames = array();
            $new_emails    = array();

            //This cicle check if there are duplicate field with db in CSV file uploads
            foreach($users as $key => $user)
            {
                $row_error = FALSE;

                if(!isset($users_db[$user['id']]))      //new user
                {
                    if($this->users_model->username_exists($user['username']) || isset($new_usernames[$user['username']]))
                    {
                        $row_error = TRUE;
                    }
                    else
                    {
                        if(!empty($user['email']) && ($this->users_model->email_exists($user['email']) || isset($new_emails[$user['email']])))
                        {
                            $row_error = TRUE;
                        }
                        else
                        {
                            //check if password isn't empty
                            if(empty($user['password']))
                            {
                                $row_error = TRUE;
                            }
                            else
                            {
                                $new_usernames[$user['username']] = 1;
                                $new_emails[$user['email']] = 1;
                            }
                        }                        
                    }
                }
                else
                {
                    $old_user = $users_db[$user['id']];

                    if($old_user['username'] != $user['username'] && $this->users_model->username_exists($user['username']))
                    {
                        $row_error = TRUE;
                    }
                    else if($old_user['email'] != $user['email'] && $this->users_model->email_exists($user['email']))
                    {
                        $row_error = TRUE;
                    }
                }

                if($row_error)
                {
                    if(!$fields_error)
                    {
                        $fields_error = TRUE;
                        $error_msg .= ($key + 1);
                    }
                    else
                    {
                        $error_msg .= ' , ' . ($key + 1);
                    }

                    $row_error = FALSE;                    
                }
            }

            if($fields_error)   //There are mistakes in CSV files
            {
                $this->session->set_flashdata('error', lang('users msg csv_error') .  $error_msg);
                redirect($this->_redirect_url);
            }

            //Create a backup of db before load CSV
            backup_to_csv($this->config->item('users_backup'), $users_db);

            foreach($users as $user)
            {
                if(!empty($user['start_time'])){
                    $user['start_time'] = date('Y/m/d H:i', strtotime($user['start_time']));
                }
                else {
                    $user['start_time'] = NULL; 
                }
                
                if(!empty($user['end_time'])){
                    $user['end_time'] = date('Y/m/d H:i', strtotime($user['end_time']));
                }
                else {
                   $user['end_time'] = NULL; 
                }

                if(!empty($user['birth_date'])){
                    $user['birth_date'] = str_replace('/', '-', $user['birth_date']);
                    $user['birth_date'] = date('Y-m-d', strtotime($user['birth_date']));
                }
                else {
                    $user['birth_date'] = NULL;
                }

                if(!empty($user['battery']))        //calculate battery id from name
                {
                    $battery = $this->batteries_model->get_battery_from_name($user['battery']);
                    $user['assigned_battery'] = $battery['id'];                    
                }
                else
                {
                    $user['assigned_battery'] = NULL;
                }

                unset($user['battery']);

                $user['active'] = '1';

                if(!isset($users_db[$user['id']]))      //new user
                {
                    unset($user['id']);
                    $this->users_model->add_user($user);                    
                }
                else                                    //old user
                {            
                    $this->users_model->edit_user($user);    
                    unset($users_db[$user['id']]);                
                }
            }

            //delete old user that is not present in CSV file
            foreach($users_db as $user)
            {
                $this->users_model->delete_user($user['id']);
            }

            $this->session->set_flashdata('message', lang('users msg upload_success')); 
            redirect($this->_redirect_url);    
        }
    }

    function get_backups()
    {
        $dir     = $this->config->item('users_backup');
        @$backups = scandir($dir, 1);
        $results = array();

        if(!empty($backups))
        {
            foreach($backups as $backup)
            {
                if($backup != '.' && $backup != '..')
                {
                    //format date from file name (Y_m_d_h_m_s_a to m/d/Y h:i:s a)
                    $file_name = $backup;
                    $backup = basename($backup, '.csv');
                    $date = explode('_', $backup);
                    //check before if name file are well formatted
                    if(count($date) >= 6){
                        $backup = "$date[1]/$date[2]/$date[0] $date[3]:$date[4]:$date[5] $date[6]"; 
                        $results[$file_name] = $backup;
                    }                
                }            
            } 
        }        

        return $results;
    }

    function load_backup()
    {
        ini_set('memory_limit', $this->config->item('memory_limit'));
        
        if(empty($this->input->post('backup')))
        {
            $this->session->set_flashdata('error', lang('users msg backup_no_file')); 
            redirect($this->_redirect_url);
        }

        $file_name = $this->input->post('backup');
        $path      = $this->config->item('users_backup');        
        $users     = csv_to_array($path, $file_name);
        $users_db  = $this->users_model->get_user_by_id(false);         

        foreach($users as $user)
        {
            if(empty($user['start_time'])){
                $user['start_time'] = NULL;
            }

            if(empty($user['end_time'])){
                $user['end_time'] = NULL;
            }

            if(empty($user['assigned_battery'])){
                $user['assigned_battery'] = NULL;
            }

            $user['active'] = '1';

            if(isset($users_db[$user['id']]))   //old_user
            {
                $this->users_model->edit_user($user, FALSE);
                unset($users_db[$user['id']]);
            }
            else                                //new_user
            {
                $this->users_model->add_user($user, FALSE);
            }
        }        

        foreach($users_db as $user)
        {
            $this->users_model->delete_user($user['id']);
        }

        $this->session->set_flashdata('message', lang('users msg backup_success')); 
        redirect($this->_redirect_url);
    }


    /**************************************************************************************
     * PRIVATE VALIDATION CALLBACK FUNCTIONS
     **************************************************************************************/


    /**
     * Make sure username is available
     *
     * @param  string $username
     * @param  string|null $current
     * @return int|boolean
     */
    function _check_username($username, $current)
    {
        if (trim($username) != trim($current) && $this->users_model->username_exists($username))
        {
            $this->form_validation->set_message('_check_username', sprintf(lang('users error username_exists'), $username));
            return FALSE;
        }
        else
        {
            return $username;
        }
    }


    /**
     * Make sure email is available
     *
     * @param  string $email
     * @param  string|null $current
     * @return int|boolean
     */
    function _check_email($email, $current)
    {
        if (trim($email) != trim($current) && $this->users_model->email_exists($email))
        {
            $this->form_validation->set_message('_check_email', sprintf(lang('users error email_exists'), $email));
            return FALSE;
        }
        else
        {
            return $email;
        }
    }

}
