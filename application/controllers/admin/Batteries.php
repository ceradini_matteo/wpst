<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Batteries extends Admin_Controller {

    /**
     * @var string
     */
    private $_redirect_url;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('batteries');

        // load the batteries model
        $this->load->model(array('batteries_model'));

        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('admin/batteries'));
        define('DEFAULT_LIMIT', $this->settings->per_page_limit);
        define('DEFAULT_OFFSET', 0);
        define('DEFAULT_SORT', "name");
        define('DEFAULT_DIR', "asc");

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL;
        }
    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/

    /**
     * batteries list page
     */
    function index()
    { 
        // get parameters
        $limit  = $this->input->get('limit')  ? $this->input->get('limit', TRUE)  : DEFAULT_LIMIT;
        $offset = $this->input->get('offset') ? $this->input->get('offset', TRUE) : DEFAULT_OFFSET;
        $sort   = $this->input->get('sort')   ? $this->input->get('sort', TRUE)   : DEFAULT_SORT;
        $dir    = $this->input->get('dir')    ? $this->input->get('dir', TRUE)    : DEFAULT_DIR;

        $filters = array();

        if ($this->input->get('name'))
        {
            $filters['name'] = $this->input->get('name', TRUE);
        }

        if ($this->input->get('number_tests'))
        {
            $filters['number_tests'] = $this->input->get('number_tests', TRUE);
        }

        if ($this->input->get('presentation'))
        {
            $filters['presentation'] = $this->input->get('presentation', TRUE);
        }

        // build filter string
        $filter = "";

        foreach ($filters as $key => $value)
        {
            $filter .= "&{$key}={$value}";
        }

        // save the current url to session for returning
        $this->session->set_userdata(REFERRER, THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");

        // are filters being submitted?
        if ($this->input->post())
        {
            if ($this->input->post('clear'))
            {
                // reset button clicked
                redirect(THIS_URL);
            }
            else
            {
                // apply the filter(s)
                $filter = "";

                if ($this->input->post('name'))
                {
                    $filter .= "&name=" . $this->input->post('name', TRUE);
                }

                if ($this->input->post('number_tests'))
                {
                    $filter .= "&number_tests=" . $this->input->post('number_tests', TRUE);
                }

                if ($this->input->post('presentation'))
                {
                    $filter .= "&presentation=" . $this->input->post('presentation', TRUE);
                }

                if ($this->input->post('batteries'))
                {
                    $filter .= "&batteries=" . $this->input->post('batteries', TRUE);
                }

                // redirect using new filter(s)
                redirect(THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");
            }
        }

        // get list
        $batteries = $this->batteries_model->get_all($limit, $offset, $filters, $sort, $dir);

        foreach($batteries['results'] as $key => $battery)
        {
            $tests = $this->batteries_model->get_battery_tests($battery['id']);

            foreach($tests as $test)
            {
                $var = $test['type'] . '_' . $test['number'];

                if(!isset($batteries['results'][$key]['tests'])){
                   $batteries['results'][$key]['tests'] = $var; 
                }
                else{
                    $batteries['results'][$key]['tests'] .= ' ; ' . $var;
                }                
            }

            if(!isset($batteries['results'][$key]['tests'])){
                $batteries['results'][$key]['tests'] = null; 
            }
        }

        if($sort == 'tests' && $dir == 'desc')
        {
            arsort($batteries['results']);  //reverse array
        }

        // build pagination
        $this->pagination->initialize(array(
            'base_url'   => THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}{$filter}",
            'total_rows' => $batteries['total'],
            'per_page'   => $limit
        ));

        // setup page header data
        $this->add_js_theme(array('batteries.js','jquery-ui.min.js'));
        $this->set_title(lang('batteries title batteries_list'));
        $this->set_breadcrumb(array(
            '' => lang('batteries title batteries_list')
        ));
        
        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'   => THIS_URL,
            'batteries'  => $batteries['results'],
            'total'      => $batteries['total'],
            'filters'    => $filters,
            'filter'     => $filter,
            'pagination' => $this->pagination->create_links(),
            'limit'      => $limit,
            'offset'     => $offset,
            'sort'       => $sort,
            'dir'        => $dir
        );

        // load views
        $data['content'] = $this->load->view('admin/batteries/list', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    function form($id = false)
    {
        $this->load->model('tests_model');
        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('name', lang('batteries input name'), 'required|trim|max_length[30]');
        $this->form_validation->set_rules('presentation', lang('batteries input presentation'), 'required|trim');
        $this->form_validation->set_rules('mturk', lang('batteries col mturk'), 'required|trim');
        $this->form_validation->set_rules('mturk_link', lang('batteries col mturk'), 'trim');

        if($id)
        {
            // get the data
            $battery = $this->batteries_model->get($id);

            // if empty results, return to list
            if (!$battery)
            {
                redirect($this->_redirect_url);
            }
        }   

        if ($this->form_validation->run() == TRUE)
        {
            // save the changes
            $data = $this->input->post();
            $data = get_array_fields($data,$this->config->item('battery_fields'));  

            if(!empty($data['tests'])){
                $data['tests'] = explode(',', $data['tests']);    
            }
            
            $tests = $data['tests'];                
            unset($data['tests']);

            if(empty($tests))
            {
                $data['number_tests'] = 0;
            }
            else
            {
                $data['number_tests'] = count($tests);
            }

            if($id)
            {    
                $saved = $this->batteries_model->edit_battery($data);
            }
            else
            {
                $saved = $this->batteries_model->add_battery($data);
                $id    = $saved;
            }

            $this->session->set_flashdata('message', sprintf(lang('batteries msg edit_battery_success'), $this->input->post('name')));
            $this->batteries_model->delete_all_test($id);
            
            $order = 1;

            foreach($tests as $test_id)
            {
                $this->batteries_model->add_test($id, $test_id, $order);
                $order++;           
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data
        $this->add_css_theme(array('jquery-ui.min.css'));
        $this->add_js_theme(array('batteries.js','jquery-ui.min.js'));
        $this->set_title( lang('batteries title battery_edit') );
        $this->set_breadcrumb(array(
            'admin/batteries' => lang('batteries title batteries_list'),
            '' => lang('batteries title battery_edit') 
        ));
        
        $data = $this->includes;        

        // set content data
        $content_data = array(
            'battery'           => NULL,
            'cancel_url'        => $this->_redirect_url,
            'password_required' => FALSE,
            'batteries'         => $this->batteries_model->get(),
            'tests'             => $this->tests_model->get()
        );

        if($id)
        {
            $content_data['battery']     = $battery;
            $content_data['battery_id']  = $id;
            $content_data['tests_added'] =  $this->batteries_model->get_battery_tests($id);
        }
        else
        {
            $content_data['password_required'] = TRUE;
        }

        // load views
        $data['content'] = $this->load->view('admin/batteries/form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }    

    /**
     * Delete one or more batteries
     *
     * @param  int $id
     */
    function delete($id = NULL)
    {
        if(is_null($id) && !empty($this->input->post('selected')))
        {
            $batteries = $this->input->post('selected');

            foreach($batteries as $battery_id)
            {
                $battery = $this->batteries_model->get($battery_id);

                if($battery)
                {
                    $delete = $this->batteries_model->delete_battery($battery_id);                        

                    if($delete)
                    {
                        $this->session->set_flashdata('message', sprintf(lang('batteries msg delete_battery')));
                    }
                    else
                    {
                        $this->session->set_flashdata('error', sprintf(lang('batteries error delete_battery')));
                        break;
                    }
                }                         
            }
        }
        else if(is_numeric($id))      // make sure we have a numeric id
        {
            // get battery details
            $battery = $this->batteries_model->get($id);

            if ($battery)
            {
                $delete = $this->batteries_model->delete_battery($id);

                if($delete)
                {
                    $this->session->set_flashdata('message', sprintf(lang('batteries msg delete_battery'), $battery['firstname'] . " " . $battery['lastname']));
                }
                else
                {
                    $this->session->set_flashdata('error', sprintf(lang('batteries error delete_battery'), $battery['firstname'] . " " . $battery['lastname']));
                }
            }
            else
            {
                $this->session->set_flashdata('error', lang('batteries error battery_not_exist'));
            }
        }
        else
        { 
            $this->session->set_flashdata('error', lang('batteries error battery_id_required'));
        }

        // return to list and display message
        redirect($this->_redirect_url);
    }
}