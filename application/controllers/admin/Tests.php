<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tests extends Admin_Controller {

    /**
     * @var string
     */
    private $_redirect_url;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('tests');

        // load the tests model
        $this->load->model(array('tests_model','slides_model'));

        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('admin/tests'));
        define('DEFAULT_LIMIT', $this->settings->per_page_limit);
        define('DEFAULT_OFFSET', 0);
        define('DEFAULT_SORT', "type");
        define('DEFAULT_DIR', "asc");

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL;
        }
    }

    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/

    /**
     * tests list page
     */
    function index()
    {
        // get parameters
        $limit  = $this->input->get('limit')  ? $this->input->get('limit', TRUE)  : DEFAULT_LIMIT;
        $offset = $this->input->get('offset') ? $this->input->get('offset', TRUE) : DEFAULT_OFFSET;
        $sort   = $this->input->get('sort')   ? $this->input->get('sort', TRUE)   : DEFAULT_SORT;
        $dir    = $this->input->get('dir')    ? $this->input->get('dir', TRUE)    : DEFAULT_DIR;

        $filters = array();

        if ($this->input->get('number'))
        {
            $filters['number'] = $this->input->get('number', TRUE);
        }

        if ($this->input->get('type'))
        {
            $filters['type'] = $this->input->get('type', TRUE);
        }

        if ($this->input->get('slides'))
        {
            $filters['slides'] = $this->input->get('slides', TRUE);
        }

        if ($this->input->get('description'))
        {
            $filters['description'] = $this->input->get('description', TRUE);
        }

        // build filter string
        $filter = "";

        foreach ($filters as $key => $value)
        {
            $filter .= "&{$key}={$value}";
        }

        // save the current url to session for returning
        $this->session->set_userdata(REFERRER, THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");

        // are filters being submitted?
        if ($this->input->post())
        {
            if ($this->input->post('clear'))
            {
                // reset button clicked
                redirect(THIS_URL);
            }
            else
            {
                // apply the filter(s)
                $filter = "";

                if ($this->input->post('number'))
                {
                    $filter .= "&number=" . $this->input->post('number', TRUE);
                }

                if ($this->input->post('type'))
                {
                    $filter .= "&type=" . $this->input->post('type', TRUE);
                }

                if ($this->input->post('slides'))
                {
                    $filter .= "&slides=" . $this->input->post('slides', TRUE);
                }

                if ($this->input->post('description'))
                {
                    $filter .= "&description=" . $this->input->post('description', TRUE);
                }

                // redirect using new filter(s)
                redirect(THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");
            }
        }

        // get list
        $tests = $this->tests_model->get_all($limit, $offset, $filters, $sort, $dir);

        // build pagination
        $this->pagination->initialize(array(
            'base_url'   => THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}{$filter}",
            'total_rows' => $tests['total'],
            'per_page'   => $limit
        ));

        // setup page header data
        $this->add_js_theme(array('tests.js'));
        $this->set_title(lang('tests title tests_list'));
        $this->set_breadcrumb(array(
            '' => lang('tests title tests_list')
        ));
        
        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'   => THIS_URL,
            'tests'      => $tests['results'],
            'total'      => $tests['total'],
            'filters'    => $filters,
            'filter'     => $filter,
            'pagination' => $this->pagination->create_links(),
            'limit'      => $limit,
            'offset'     => $offset,
            'sort'       => $sort,
            'dir'        => $dir,
            'backups'    => $this->get_backups()
        );

        // load views
        $data['content'] = $this->load->view('admin/tests/list', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    /**
     * Delete one or more tests
     *
     * @param  int $id
     */
    function delete($id = NULL)
    {
        $this->load->model('batteries_model');

        if(is_null($id) && !empty($this->input->post('selected')))
        {
            $tests = $this->input->post('selected');

            foreach($tests as $test_id)
            {             
                $test = $this->tests_model->get($test_id);

                if ($test)
                {      
                    // delete the test ,slides associated and stimulis_files associated
                    $this->slides_model->delete_all($test_id);

                    $batteries = $this->batteries_model->get_batteries_from_test($test_id);

                    foreach($batteries as $battery)
                    {
                        $battery['number_tests'] = $battery['number_tests'] - 1;

                        $this->batteries_model->edit_battery($battery);
                        $this->batteries_model->delete_battery_test($battery['id'], $test_id);
                    }

                    $delete = $this->tests_model->delete_test($test_id);                        

                    if($delete)
                    {
                        $this->session->set_flashdata('message', sprintf(lang('tests msg delete_test')));
                    }
                    else
                    {
                        $this->session->set_flashdata('error', sprintf(lang('tests error delete_test')));
                        break;
                    }
                }                         
            }
        }
        else if(is_numeric($id))      // make sure we have a numeric id
        {
            // get test details
            $test = $this->tests_model->get($id);

            if ($test)
            {                
                delete_directory('./uploads/slides_files/' . $id . '/'); 
                delete_directory('./uploads/slides_thumbs/' . $id . '/');   
                
                // delete the test ,slides associated and stimulis_files associated
                $this->slides_model->delete_all($id);
                $batteries = $this->batteries_model->get_batteries_from_test($id);

                foreach($batteries as $battery)
                {
                    $battery['number_tests'] = $battery['number_tests'] - 1;

                    $this->batteries_model->edit_battery($battery);
                    $this->batteries_model->delete_battery_test($battery['id'], $id);
                }

                $delete = $this->tests_model->delete_test($id);

                if($delete)
                {
                    $this->session->set_flashdata('message', sprintf(lang('tests msg delete_test'), $test['firstname'] . " " . $test['lastname']));
                }
                else
                {
                    $this->session->set_flashdata('error', sprintf(lang('tests error delete_test'), $test['firstname'] . " " . $test['lastname']));
                }
            }
            else
            {
                $this->session->set_flashdata('error', lang('tests error test_not_exist'));
            }
        }
        else
        { 
            $this->session->set_flashdata('error', lang('tests error test_id_required'));
        }

        // return to list and display message
        redirect($this->_redirect_url);
    }

    function form($id = false)
    {
        $this->load->model('batteries_model');

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('number', lang('tests input number'), 'trim');
        $this->form_validation->set_rules('type', lang('tests input type'), 'trim');
        $this->form_validation->set_rules('left_key', lang('tests input left_key'), 'trim');
        $this->form_validation->set_rules('right_key', lang('tests input right_key'), 'trim');
        $this->form_validation->set_rules('slide_number[]', lang('tests input slide_number'), 'trim');
        $this->form_validation->set_rules('slide_name[]', lang('tests input slide_name'), 'trim');
        $this->form_validation->set_rules('slide_name[]', lang('tests input slide_name'), 'trim');
        $this->form_validation->set_rules('top_text[]', lang('tests input top_text'), 'trim');
        $this->form_validation->set_rules('left_label[]', lang('tests input left_label'), 'trim');
        $this->form_validation->set_rules('right_label[]', lang('tests input right_label'), 'trim');

        if($this->input->post('values'))
        {
            // save the changes
            $file_slide_index = json_decode($this->input->post('file_slide_index'));
            $values           = $this->input->post('values');
            $data             = array();
            $data             = my_parse_str($values);
            $data             = get_array_fields($data, $this->config->item('test_fields'));

            if(isset($data['range-from']) && isset($data['range-to']))
            {
                $from = $data['range-from'];
                $to   = $data['range-to'];
            }
            else
            {
                $from = array();
                $to = array();
            }

            $randomize_range = array();

            for($i = 0; $i < count($from); $i++)
            {
                if(!empty($from[$i]) && !empty($to[$i]) && $from[$i] < $to[$i])
                {
                    $randomize_range[$from[$i]] = $to[$i];
                }
            }

            $data['randomize_range'] = json_encode($randomize_range);

            if(isset($data['range-from']) && isset($data['range-to']))
            {
                unset($data['range-from']);
                unset($data['range-to']);
            }

            $slides = false;

            if(isset($data['slide_number']))
            {
                $slides = array(
                    'slide_number'           => $data['slide_number'],
                    'slide_name'             => $data['slide_name'],
                    'top_text'               => $data['top_text'],
                    'left_label'             => $data['left_label'],
                    'right_label'            => $data['right_label'],
                    'index_slide'            => $data['index_slide'],
                    'slide_exists'           => $data['slide_exists'],
                    'slide_id'               => $data['slide_id'],
                    'stimuli_type'           => $data['stimuli_types'],
                    'stimuli_filters_length' => $data['stimuli_filters_length'],
                    'stimuli_filters_type'   => $data['stimuli_filters_type'],
                    'file_slide_index'       => $file_slide_index
                );

                if(isset($data['stimuli_numbers']))
                {
                    $slides['stimuli_number'] = $data['stimuli_numbers'];
                }

                if(isset($data['stimuli_layouts']))
                {
                    $slides['stimuli_layout'] = $data['stimuli_layouts'];   
                }

                if(isset($data['slide_files']))
                {
                    $slides['slide_files'] = $data['slide_files'];
                    unset($data['slide_files']);
                }
                else
                {
                    $slides['slide_files'] = array();
                }

                if(isset($data['stimuli_correct']))
                {
                    $slides['stimuli_correct'] = $data['stimuli_correct'];
                    unset($data['stimuli_correct']);
                }
                else
                {
                    $slides['stimuli_correct'] = NULL;
                }

                unset($data['slide_number']);
                unset($data['slide_name']);
                unset($data['top_text']);
                unset($data['left_label']);
                unset($data['right_label']);
                unset($data['index_slide']);
                unset($data['slide_exists']);
                unset($data['slide_id']);
                unset($data['stimuli_types']);
                unset($data['stimuli_filters_length']);
                unset($data['stimuli_filters_type']);
                unset($data['stimuli_numbers']);
                unset($data['stimuli_layouts']);
                unset($data['file_slide_index']);

                if(isset($data['trigger']))
                {
                    $slides['trigger'] = $data['trigger'];

                    unset($data['trigger']);
                }

                if(isset($data['timebar'])){
                    $slides['timebar_enabled'] = $data['timebar'];

                    unset($data['timebar']);
                }
            }
            else
            {
                $saved = $this->slides_model->delete_all($data['id']);
            }

            $data['slides'] = count($slides['slide_number']);
            $data['active'] = '1';

            if(!$data['left_key']){
                $data['left_key'] = 'w';
            }

            if(!$data['right_key']) {
                $data['right_key'] = 'o';
            }

            $saved = $this->tests_model->edit_test($data);

            if($saved && $slides){
                $slides['test_id'] = $data['id'];
                $slides['folder_name'] = $this->tests_model->get_test_folder_name($slides['test_id'])['folder_name'];
                $saved = $this->edit_slides($slides);
            }

            if($saved)
            {
                //update test slides counter
                $count = $this->slides_model->get_slides_count($data['id']);
                $this->tests_model->edit_test_slides_num($data['id'], $count);
                $this->session->set_flashdata('message', sprintf(lang('tests msg edit_test_success')));
            }
            else{
                $this->session->set_flashdata('error', sprintf(lang('tests error edit_test_failed')));
            }

            // return to list and display message
            //redirect($this->_redirect_url);
            // return;
        }

        if($id)
        {
            // get the data
            $test        = $this->tests_model->get($id);    
            $folder_name = $test['folder_name']; 

            // if empty results, return to list
            if (!$test)
            {
                redirect($this->_redirect_url);
            }

            $slides = $this->slides_model->get_slides_by_id($id);

            foreach ($slides as $slide_id => $slide) 
            {
                $slides[$slide_id]['slide_files'] = $this->slides_model->get_slide_files($slide_id);
            }
        }
        else
        {
            $data        = array('active' => '0');
            $id          = $this->tests_model->add_test($data);
            $folder_name = "";
        }

        // setup page header data
        $this->add_js_theme(array('tests.js'));
        $this->set_title( lang('tests title test_edit') );
        $this->set_breadcrumb(array(
            'admin/tests' => lang('tests title tests_list'),
            '' => lang('tests title test_edit')
        ));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'          => THIS_URL,
            'test'              => NULL,
            'test_id'           => $id,
            'folder_name'       => $folder_name,
            'cancel_url'        => $this->_redirect_url,
            'password_required' => FALSE,
            'batteries'         => $this->batteries_model->get()
        );

        if(isset($test))
        {
            $content_data['test']                    = $test;
            $content_data['test']['randomize_range'] = json_decode($content_data['test']['randomize_range']);
            $content_data['slides']                  = $slides;
            $content_data['backups']                 = $this->get_backups_slides($id);
        }

        // load views
        $data['content'] = $this->load->view('admin/tests/form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    function create_test_folder()
    {
        $test_id     = $this->input->get('test_id');
        $type        = $this->input->get('type');
        $number      = $this->input->get('number');
        $folder_name = $type . '_' . $number;
        $path        = './uploads/slides_files/' . $folder_name; 

        if(file_exists($path))
        {
            $i           = 2;
            $folder_name = $type . '_' . $number . '(' . $i . ')';
            $path        = './uploads/slides_files/' . $folder_name;

            while(file_exists($path))
            {
                $i++; 
                $folder_name = $type . '_' . $number . '(' . $i . ')';
                $path = './uploads/slides_files/' . $folder_name;
            }
        }

        mkdir($path);
        mkdir($path . '/images');
        mkdir($path . '/instructions');
        mkdir($path . '/text');

        $this->tests_model->edit_test_folder($test_id, $folder_name);

        echo $folder_name;
    }

    //This function parse ad edit slides
    function edit_slides($slides)
    {
        $this->load->library('image_lib');
        $return = TRUE;
        if($slides)
        {
            $redirect = THIS_URL . '/form/' . $slides['test_id'];
            $old_slides = $this->slides_model->get_slides_by_id($slides['test_id']);
            $_files = $_FILES;

            foreach($slides['slide_number'] as $key => $number)
            {
                $slide_id = $slides['slide_id'][$key];
                $index    = $slides['index_slide'][$key];

                $data = array(
                    'id'                     => $slide_id,
                    'test_id'                => $slides['test_id'],
                    'number'                 => $slides['slide_number'][$key],
                    'name'                   => $slides['slide_name'][$key],
                    'top_text'               => $slides['top_text'][$key],
                    'left_label'             => $slides['left_label'][$key],
                    'right_label'            => $slides['right_label'][$key],
                    'stimuli_type'           => $slides['stimuli_type'][$index]
                );

                // if empy stimuli_filters -> set to NULL
                if($slides['stimuli_filters_length'][$index] || $slides['stimuli_filters_type'][$index]){
                    $val = array();

                    if($slides['stimuli_filters_length'][$index]){
                        $val['length'] = $slides['stimuli_filters_length'][$index];
                    }

                    if($slides['stimuli_filters_type'][$index]){
                        $val['type'] = $slides['stimuli_filters_type'][$index];
                    }

                    $data['stimuli_filters'] = json_encode($val);
                }
                else {
                    $data['stimuli_filters'] = NULL;
                }

                if(isset($slides['stimuli_number'][$index]))
                {
                    $data['stimuli_number'] = $slides['stimuli_number'][$index];
                }

                if(isset($slides['stimuli_layout'][$index]))
                {
                    $data['stimuli_layout'] = $slides['stimuli_layout'][$index];
                }

                if(isset($slides['trigger'][$index]))
                {
                    $data['trigger'] = json_encode($slides['trigger'][$index]);

                    if(isset($slides['timebar_enabled'][$index])){
                        $data['timebar_enabled'] = '1';
                    }
                    else{
                        $data['timebar_enabled'] = NULL;
                    }
                }
                else
                {
                    $data['trigger'] = null;
                }

                if(!empty($slides['stimuli_correct'][$key]))
                {
                    $data['answer'] = json_encode($slides['stimuli_correct'][$index]);
                }
                else
                {
                    $data['answer'] = null;
                }

                if($slides['slide_exists'][$key])
                {
                    $this->slides_model->edit_slide($data);
                }
                else
                {
                    unset($data['id']);
                    $slide_id = $this->slides_model->add_slide($data);
                }

                $stimuli_type = $slides['stimuli_type'][$index];

                if($stimuli_type != 'text-box')
                {
                    $this->slides_model->delete_all_slide_files($slide_id);     //delete all old slide files

                    if(isset($slides['stimuli_number'][$index]))
                    {
                        $stimuli_number = $slides['stimuli_number'][$index];
                    }
                    else
                    {
                        $stimuli_number = 0;
                    }

                    if(isset($slides['slide_files'][$index]))
                    {
                        $slide_files = $slides['slide_files'][$index]; 
                    }
                    else
                    {
                        $slide_files = array();
                    }

                    if(isset($slides['file_slide_index'][$index]))
                    {
                        $file_slide_index = $slides['file_slide_index'][$index];
                    }
                    else
                    {
                        $file_slide_index = null;
                    }

                    foreach($file_slide_index as $file_number => $file_index)
                    {                              
                        if($file_number > 0)    //Correzzione di un errore che dell'array che parte da 0 e non da 1
                        {             
                            if($file_index >= 0 && !is_null($file_index))
                            {
                                $uploaded_files_name     = $_files[$file_index]['name'];
                                $uploaded_files_type     = $_files[$file_index]['type'];
                                $uploaded_files_tmp_name = $_files[$file_index]['tmp_name'];
                                $uploaded_files_error    = $_files[$file_index]['error'];
                                $uploaded_files_size     = $_files[$file_index]['size'];

                                $path = './uploads/slides_files/' . $slides['folder_name'] . '/' . $stimuli_type . '/';
                                $config['upload_path']   = $path;
                                $config['allowed_types'] = '*';
                                $config['overwrite']     = TRUE;

                                $this->load->library('upload', $config);                                

                                $_FILES['stimuli_uploads'] = array(
                                    'name'     => $uploaded_files_name,  
                                    'type'     => $uploaded_files_type,
                                    'tmp_name' => $uploaded_files_tmp_name,
                                    'error'    => $uploaded_files_error,
                                    'size'     => $uploaded_files_size
                                );      

                                if (!$this->upload->do_upload('stimuli_uploads'))
                                {
                                    $this->session->set_flashdata('error', lang('tests msg upload_error'));
                                    redirect($redirect);
                                }
                                else
                                {
                                    $upload_data = array('data' => $this->upload->data());
                                    $file_name   = $upload_data['data']['file_name']; 

                                    if($stimuli_type == 'images')
                                    {
                                        $full_img   = './uploads/slides_files/' . $slides['folder_name'] . '/' . $stimuli_type . '/' . $file_name;
                                        $thumb_path =  './uploads/slides_thumbs/' . $slides['folder_name'] . '/' . $stimuli_type . '/' .  $file_name;

                                        create_img($full_img, $thumb_path, 121, 91);
                                    }  

                                    $this->slides_model->add_stimuli_file($slide_id, $file_name, $file_number);                           
                                }
                            }  
                            else
                            {
                                if(isset($slide_files[$file_number - 1]))
                                {
                                    $this->slides_model->add_stimuli_file($slide_id, $slide_files[$file_number - 1], $file_number); 
                                }
                            }                                
                        }                                
                    }  
                }

                if(isset($data['id']) && isset($old_slides[$data['id']]))
                {
                    unset($old_slides[$data['id']]);
                }
            }

            //delete slides not set
            foreach($old_slides as $id => $slide)
            {
                $return = $this->slides_model->delete_slide($id);   
            }
        }
        else
        {
            $return = FALSE;
        }

        return $return;
    }

    /* GET DYNAMICS FORM FOR TESTS FORM */

    function getSlide()
    {
        $data = array();

        echo $this->load->view('admin/tests/form/slide', $data, TRUE);
    }

    function getRange()
    {
        $data = array();

        echo $this->load->view('admin/tests/form/range', $data, TRUE);
    }

    function getTrigger()
    {
        if(!is_null($this->input->post('index')))
        {
            $data = array(
                'index' => $this->input->post('index')
            );

            echo $this->load->view('admin/tests/form/trigger', $data, TRUE);
        }        
    }

    function getTriggerTime()
    {         
        if(!is_null($this->input->post('index')))
        {
            $data = array(
                'index' => $this->input->post('index')
            );

            echo $this->load->view('admin/tests/form/trigger_time', $data, TRUE); 
        } 
    }

    function getTriggerValue()
    {         
        if(!is_null($this->input->post('index')))
        {
            $data = array(
                'index' => $this->input->post('index')
            );

            echo $this->load->view('admin/tests/form/trigger_value', $data, TRUE); 
        } 
    }

    function getStimuli()
    {
        if(!is_null($this->input->post('index')) && !is_null($this->input->post('count')))
        {
            $data = array(
                'index' => $this->input->post('index'),
                'count' => $this->input->post('count')
            );

            echo $this->load->view('admin/tests/form/stimuli', $data, TRUE);
        }        
    }

    function getStimuliNumber()
    {
        if(!is_null($this->input->post('index')) && !is_null($this->input->post('type')))
        {
            $data = array(
                'index' => $this->input->post('index'),
                'type'  => $this->input->post('type')
            );

            echo $this->load->view('admin/tests/form/stimuli_number', $data, TRUE);
        }
    }

    function getStimuliLayout()
    {
        if(!is_null($this->input->post('index')) && !is_null($this->input->post('type')) && !is_null($this->input->post('number')))
        {
            $data = array(
                'index'  => $this->input->post('index'),
                'type'   => $this->input->post('type'),
                'number' => $this->input->post('number')
            );

            echo $this->load->view('admin/tests/form/stimuli_layout', $data, TRUE);
        }
    }

    function getStimuliLayoutPreview()
    {
        if(!is_null($this->input->post('layout')) && !is_null($this->input->post('type')))
        {
            $data = array(
                'layout'  => $this->input->post('layout'),
                'type'   => $this->input->post('type')
            );

            echo $this->load->view('admin/tests/form/stimuli_layout_preview', $data, TRUE);
        }
    }

    function getStimuliUpload()
    {
        if(!is_null($this->input->post('index')))
        {
            $this->load->model('slides_model');

            $conta = 1;

            while($conta <= $this->input->post('number'))
            {                
                $data = array(
                    'index'   => $this->input->post('index'),
                    'count'   => $conta,
                    'type'    => $this->input->post('type'),
                    'test_id' => $this->input->post('test_id')
                );

                if(!is_null($this->input->post('file_selected')))
                {
                    $data['file_selected'] = $this->input->post('file_selected');
                }
                
                $conta++;

                echo $this->load->view('admin/tests/form/stimuli_upload', $data, TRUE);                 
            }            
        } 
    }

    function getStimuliAnswer()
    {
        if(!is_null($this->input->post('index')))
        {
            $data = array(
                'index'  => $this->input->post('index'),
                'type'   => $this->input->post('type'),
                'number' => $this->input->post('number')
            );

            echo $this->load->view('admin/tests/form/stimuli_answer', $data, TRUE);
        }
    }

    /**
     * Export tests to CSV
     */
    function export()
    {
        // get parameters
        $sort = $this->input->get('sort') ? $this->input->get('sort', TRUE) : DEFAULT_SORT;
        $dir  = $this->input->get('dir')  ? $this->input->get('dir', TRUE)  : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get('type'))
        {
            $filters['type'] = $this->input->get('type', TRUE);
        }

        if ($this->input->get('number'))
        {
            $filters['number'] = $this->input->get('number', TRUE);
        }

        if ($this->input->get('slides'))
        {
            $filters['slides'] = $this->input->get('slides', TRUE);
        }

        if ($this->input->get('description'))
        {
            $filters['description'] = $this->input->get('description', TRUE);
        }

        // get all tests (no admin)
        $tests = $this->tests_model->get_all(0, 0, $filters, $sort, $dir, TRUE);

        if ($tests['total'] > 0)
        {
            // manipulate the output array
            foreach ($tests['results'] as $key=>$test)
            {
                unset($tests['results'][$key]['slides']);
                unset($tests['results'][$key]['randomize_range']);
            }

            // export the file
            array_to_csv($tests['results'], "tests.csv");
        }
        else
        {
            // nothing to export
            $this->session->set_flashdata('error', lang('core error no_results'));
            redirect($this->_redirect_url);
        }

        exit;
    }

    /**
        Import tests from CSV
     */
    function import()
    {
        $config['upload_path']          = './uploads/csv/';
        $config['allowed_types']        = 'csv';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile'))
        {
            $this->session->set_flashdata('error', lang('tests msg upload_error'));
            redirect($this->_redirect_url);
        }
        else
        {
            $data      = array('upload_data' => $this->upload->data());
            $file_name = $data['upload_data']['file_name'];            
            $tests     = csv_to_array($config['upload_path'], $file_name);
            unlink($config['upload_path'] . $file_name);            //delete csv uploaded file

            $fields_error  = FALSE;      //TRUE if ther are errors in fields format or other
            $error_msg     = '<div class="m-t-1"></div>Rows affected: ';
            $new_tests     = array();

            //This cicle check if there are duplicate field with db in CSV file uploads
            foreach($tests as $key => $test)
            {
                $type = $test['type'];

                if(!isset($new_tests[$type]))
                {
                    $new_tests[$type] = array();                        
                }

                if(!isset($new_tests[$type][$test['number']]))
                {
                    $new_tests[$type][$test['number']] = true; 
                }
                else
                {
                    if(!$fields_error)
                    {
                        $fields_error = TRUE;
                        $error_msg .= ($key + 1);
                    }
                    else
                    {
                        $error_msg .= ' , ' . ($key + 1);
                    }
                }
            }

            if($fields_error)   //There are mistakes in CSV files
            {
                $this->session->set_flashdata('error', lang('tests msg csv_error') .  $error_msg);
                redirect($this->_redirect_url);
            }

            //Create a backup of db before load CSV            
            $date_time = date('Y_m_d_h_i_s_a', time());

            $tests_backup = $this->tests_model->get();
            $file_name    =  $date_time . '.csv';
            backup_to_csv($this->config->item('tests_backup'), $tests_backup, $file_name);  

            $slides_backup = $this->slides_model->get();            
            $file_name     =  $date_time . '(slides).csv';                      
            backup_to_csv($this->config->item('tests_backup'), $slides_backup, $file_name);

            $tests_db = $this->tests_model->get_tests_by_id(true);  

            foreach($tests as $test)
            {
                $test['active'] = '1';

                if(!isset($tests_db[$test['id']]))      //new test
                {
                    unset($test['id']);
                    $this->tests_model->add_test($test);                    
                }
                else                                    //old test
                {                    
                    $this->tests_model->edit_test($test);    
                    unset($tests_db[$test['id']]);                
                }
            }

            //delete old test that is not present in CSV file
            foreach($tests_db as $test)
            {
                $this->tests_model->delete_test($test['id']);
            }

            $this->session->set_flashdata('message', lang('tests msg upload_success')); 
            redirect($this->_redirect_url);    
        }
    }

    /**
     * Export slides to CSV
     */
    function export_slides($test_id)
    {
        if(!isset($test_id))
        {
            // nothing to export
            $this->session->set_flashdata('error', lang('core error no_results'));
            redirect($this->_redirect_url); 
        }

        // get all tests (no admin)
        $slides = $this->slides_model->get_slides_by_id($test_id);

        if (count($slides) > 0)
        {
            // manipulate the output array
            foreach ($slides as $key=>$slide)
            {
                unset($slides[$key]['test_id']);

                // trigger
                $trigger = json_decode($slide['trigger']);
                $temp    = array();
                
				if(!empty($trigger))
				{
					foreach($trigger as $value)
					{
						$temp[] = $value;
					}
				}                

                $slides[$key]['trigger'] = implode(';', $temp);

                // slide_files
                $slide_files = $this->slides_model->get_slide_files($slide['id'], TRUE);
                $temp        = array();

                foreach($slide_files as $slide_file)
                {
                    $temp[] = $slide_file['file_name'];
                }

                $slides[$key]['slide_files'] = implode(';', $temp);

                // answer
                $answer  = json_decode($slide['answer']);
                $temp    = array();
				
				if(!empty($answer))
				{
					foreach($answer as $value)
					{
						$temp[] = $value;
					}
				}

                $slides[$key]['answer'] = implode(';', $temp);
            }
			
            // export the file
            array_to_csv($slides, "slides.csv");
        }
        else
        {
            // nothing to export
            $this->session->set_flashdata('error', lang('core error no_results'));
            redirect($this->_redirect_url);
        }

        exit;
    }

    /**
        Import slides from CSV
     */
    function import_slides($test_id)
    {
        $config['upload_path']          = './uploads/csv/';
        $config['allowed_types']        = 'csv';
        $this->load->library('upload', $config);

        // Clear form for $_FILES from ajax to php formatting array.
        if(!empty($_FILES))
        {
            $tmp = array(
                'name'     => $_FILES[0]['name'],
                'type'     => $_FILES[0]['type'],
                'tmp_name' => $_FILES[0]['tmp_name'],
                'error'    => $_FILES[0]['error'],
                'size'     => $_FILES[0]['size'],
            );

            $_FILES = array(
                'userfile' => $tmp
            );
        }

        if($this->upload->do_upload('userfile'))
        {
            $data = array('upload_data' => $this->upload->data());
            $file_name = $data['upload_data']['file_name'];            
            $slides = csv_to_array($config['upload_path'], $file_name);
            unlink($config['upload_path'] . $file_name);            //delete uploaded file

            //Create a backup of db before load CSV
            $date_time     = date('Y_m_d_h_i_s_a', time());
            $slides_backup = $this->slides_model->get_slides_by_id($test_id);
            $file_name     =  $date_time . '(' . $test_id . ').csv';
            backup_to_csv($this->config->item('slides_backup'), $slides_backup, $file_name);       

            $slides_db = $this->slides_model->get_slides_by_id($test_id); 
            $index     = 0;

            foreach($slides as $slide)
            {
                $slide['index'] = $index;

                if($slide['id'] == "")
                {
                    $slide['slide_id'] = NULL;
                    $slide['exists']   = FALSE; 
                }
                else
                {
                    $slide['slide_id'] = $slide['id'];
                    $slide['exists']   = TRUE; 
                }
                
                $slide['number']      = floatval($slide['number']);                
                $slide['folder_name'] = $this->tests_model->get_test_folder_name($test_id)['folder_name'];                
                $temp                 = explode(';', $slide['trigger']);
                $slide['trigger']     = json_encode($temp);
                $slide_files          = explode(';', $slide['slide_files']);
                $slide['slide_files'] = array();       

                $temp = array();

                foreach($slide_files as $key => $slide_file)
                {
                    $temp = array(
                        'file_name' => $slide_file,
                        'number'    => ($key + 1)
                    );

                    $slide['slide_files'][] = $temp;
                }

                $temp            = explode(';', $slide['answer']);
                $slide['answer'] = json_encode($temp);
                $slide['slides'] = array(0 => $slide);

                echo $this->load->view('admin/tests/form/slide', $slide, TRUE); 
                $index++;
            } 
        }
    }

    function get_backups()
    {
        $dir      = $this->config->item('tests_backup');
        @$backups = scandir($dir, 1);
        $results  = array();

        if(!empty($backups))
        {
            foreach($backups as $backup)
            {
                if($backup != '.' && $backup != '..')
                {
                    //format date from file name (Y_m_d_h_m_s_a to m/d/Y h:i:s a)
                    $file_name = $backup;
                    $backup = basename($backup, '.csv');
                    $date = explode('_', $backup);
                    //check before if name file are well formatted
                    if(count($date) >= 6)
                    {
                        if(!strpos($date[6], '(slides)'))
                        {
                            $backup = "$date[1]/$date[2]/$date[0] $date[3]:$date[4]:$date[5] $date[6]"; 
                            $results[$file_name] = $backup;
                        }                    
                    }                
                }            
            }
        }

        return $results;
    }

    function get_backups_slides($test_id)
    {
        $dir     = $this->config->item('slides_backup');
        @$backups = scandir($dir, 1);
        $results = array();

        if(!empty($backups))
        {
            foreach($backups as $backup)
            {
                if($backup != '.' && $backup != '..')
                {
                    //format date from file name (Y_m_d_h_m_s_a to m/d/Y h:i:s a)
                    $file_name = $backup;
                    $backup = basename($backup, '.csv');
                    $date = explode('_', $backup);
                    //check before if name file are well formatted
                    if(count($date) >= 6)
                    {
                        $test = strpos($date[6], '(' . $test_id . ')');

                        if($test)
                        {
                            $date[6] = substr($date[6], 0, $test);  //remove '(test_id)' suffix
                            $backup = "$date[1]/$date[2]/$date[0] $date[3]:$date[4]:$date[5] $date[6]"; 
                            $results[$file_name] = $backup;
                        }                    
                    }                 
                }            
            }
        }        

        return $results;
    }

    function load_backup()
    {
        if(empty($this->input->post('backup')))
        {
            $this->session->set_flashdata('error', lang('tests msg backup_no_file')); 
            redirect($this->_redirect_url);
        }

        $file_name = $this->input->post('backup');
        $path      = $this->config->item('tests_backup');        
        $tests     = csv_to_array($path, $file_name);
        $tests_db  = $this->tests_model->get_tests_by_id(false);         

        foreach($tests as $test)
        {
            if(isset($tests_db[$test['id']]))   //old_test
            {
                $this->tests_model->edit_test($test, FALSE);
                unset($tests_db[$test['id']]);
            }
            else                                //new_test
            {
                $this->tests_model->add_test($test, FALSE);
            }
        }   

        $file_name = basename($this->input->post('backup'), '.csv') . '(slides).csv' ;
        $slides    = csv_to_array($path, $file_name); 
        $slides_db = $this->slides_model->get_slides_by_id();

        foreach($slides as $slide)
        {
            if(isset($slides_db[$slide['id']]))   //old_test
            {
                $this->slides_model->edit_slide($slide);
                unset($slides_db[$slide['id']]);
            }
            else                                //new_test
            {
                $this->slides_model->add_slide($slide);
            }
        }

        foreach($tests_db as $test)
        {
            $this->slides_model->delete_all($test['id']);
            $this->tests_model->delete_test($test['id']);
        }

        $this->session->set_flashdata('message', lang('users msg backup_success')); 
        redirect($this->_redirect_url);
    }

    function load_backup_slides($test_id)
    {
        $redirect = THIS_URL . '/form/' . $test_id;

        if(!isset($test_id))
        {
            // nothing to export
            $this->session->set_flashdata('error', lang('core error no_results'));
            redirect($this->_redirect_url); 
        }

        if(empty($this->input->post('backup')))
        {
            $this->session->set_flashdata('error', lang('tests msg backup_no_file')); 
            redirect($redirect);
        }

        $file_name = $this->input->post('backup');
        $path      = $this->config->item('slides_backup');        
        $slides    = csv_to_array($path, $file_name);
        $slides_db = $this->slides_model->get_slides_by_id($test_id);   

        foreach($slides as $slide)
        {
            if(isset($slides_db[$slide['id']]))   //old_slide
            {
                $this->slides_model->edit_slide($slide);
                unset($slides_db[$slide['id']]);
            }
            else                                //new_slide
            {
                $this->slides_model->add_slide($slide);
            }
        }   

        foreach($slides_db as $slide)
        {
            $this->slides_model->delete_slide($slide['id']);
        }

        $this->session->set_flashdata('message', lang('tests msg backup_success')); 
        redirect($redirect);
    }
}