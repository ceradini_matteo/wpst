<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends Public_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        $this->load->model(array('users_model','batteries_model'));
        $this->lang->load(array('tests','users'));
    }

    /**
	 * Default
     */
	function index()
	{
        if (!$this->session->userdata('logged_in'))
        {
            redirect(base_url('login'));
        }

        $user = $this->session->userdata('logged_in');

        if(empty($user['assigned_battery']))
        {
            $user['remaining_tests'] = 0;
        }
        else
        {
            $battery       = $this->batteries_model->get($user['assigned_battery']);
            $battery_tests = $this->batteries_model->get_battery_tests($user['assigned_battery']);
            $tests_done    = $this->users_model->get_user_tests_done($user['id'], $user['assigned_battery']);

            if(!empty($tests_done))
            {
                foreach($tests_done as $test)
                {
                    unset($battery_tests[$test['test_id']]);
                }
            }

            if($battery['presentation'] == 'random'){
                shuffle($battery_tests);
            }

            $user['remaining_tests'] = count($battery_tests);
        }

        if($user['is_admin'] == 1)
        {
           redirect(base_url('admin')); 
        }

        // setup page header data
        $this->set_title(sprintf(lang('core title welcome'), $this->settings->site_name) );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'user' => $user,
            'welcome_text' => $this->settings_model->get_setting_value('login_message')
        );

        if(isset($battery_tests))
        {
            $content_data['test_id'] = current($battery_tests)['id'];
        }

        // load views
        $data['content'] = $this->load->view('welcome', $content_data, TRUE);

        $this->load->view($this->template, $data);
	}
}
