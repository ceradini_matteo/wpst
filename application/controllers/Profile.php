<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends Private_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language file
        $this->lang->load('users');

        // load the users model
        $this->load->model('users_model');
    }

    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/

    /**
	 * Profile Editor
     */
	function index()
	{
        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('password', lang('users input password'), 'min_length[5]|matches[password_repeat]');
        $this->form_validation->set_rules('password_repeat', lang('users input password_repeat'), 'min_length[5]');

        $post    = $this->input->post();
        $user_id = $this->user['id'];

        if($this->form_validation->run() == TRUE)
        {
            // save the changes
            $saved = $this->users_model->edit_profile($post, $user_id);

            if($saved)
            {
                $this->session->set_flashdata('message', lang('users msg edit_profile_success'));

                // reload the new user data and store in session
                $this->user = $this->users_model->get_user($user_id);
                unset($this->user['password']);
                unset($this->user['salt']);
                $this->session->set_userdata('logged_in', $this->user);
            }
            else
            {
                $this->session->set_flashdata('error', lang('users error edit_profile_failed'));
            }

            // reload page and display message
            redirect('profile');
        }

        // setup page header data
		$this->set_title('<span class="ion-person"></span> '. lang('users title reset_password') );
		$this->add_js_theme('profile.js',  TRUE);
        $this->add_external_js('https://js.braintreegateway.com/web/dropin/1.3.1/js/dropin.min.js');

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'            => base_url(),
            'user'                  => $this->users_model->get_user($user_id),
            'password_required'     => FALSE
        );

        // load views
        $data['content'] = $this->load->view('user/profile_form', $content_data, TRUE);
        $this->load->view($this->template, $data);
	}

    function edit_profile()
    {
        @$profile_form = $this->input->post('form');

        if($profile_form)
        {
            $user_data = array(
                'id' => $this->user['id']
            );

            $address_data = array();

            foreach($profile_form as $input){
                switch($input['name'])
                {
                    case 'address'     : $address_data['address']     = $input['value']; break;
                    case 'city'        : $address_data['city']        = $input['value']; break;
                    case 'zip'         : $address_data['zip']         = $input['value']; break;
                    case 'province_id' : $address_data['province_id'] = $input['value']; break;
                    case 'company'     : $user_data['company']        = $input['value']; break;
                    case 'phone'       : $user_data['phone']          = $input['value']; break;
                    case 'cf'          : $user_data['cf']             = $input['value']; break;
                    case 'piva'        : $user_data['piva']           = $input['value']; break;
                }
            }

            if(!$user_data['piva']){
                $user_data['piva'] = NULL;
            }

            $user_data['address_data'] = json_encode($address_data);

            $this->users_model->edit_user($user_data);
        }
    }
}
