<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends Public_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        $this->load->model(array('users_model','tests_model','batteries_model','slides_model'));
        $this->lang->load(array('tests','users'));
        $this->add_js_theme(array('test.js'));
    }

    /**
     * Default
     */
    function index($id) {
        redirect(base_url());
    }

    function notification($test_result_id)
    {
        $admins     = $this->users_model->get_admins_email();
        $user       = $this->users_model->get_user($this->user['id']);
        $test_id    = $this->tests_model->get_test_id_from_result($test_result_id);
        $test       = $this->tests_model->get($test_id);
        $wpst_email = $this->users_model->get_wpst_email()['email'];

        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->from($wpst_email, 'WPST');
        $this->email->to($admins[0]['email']);

        for($i = 1; $i < count($admins); $i++){
            $this->email->cc($admins[$i]['email']);
        }

        $this->email->subject('User take a test');
        $this->email->message('<strong>Username</strong>: ' . $user['username'] . '<br /><strong>Group-id</strong>: ' . $user['group_id'] . '<br /><strong>Test-type</strong>: ' . $test['type'] . '<br /><strong>Test-description</strong>:' . $test['description'] . '<br /><strong>Test-number</strong>: ' . $test['number']);
        $this->email->send();
    }

    function do_test()
    {
        // check if user is logged
        if (!$this->session->userdata('logged_in'))
        {
            redirect(base_url('login'));
        }

        $user = $this->session->userdata('logged_in');

        // check if user is not admin
        if($user['is_admin'] == 1)
        {
           redirect(base_url('admin'));
        }

        if(empty($user['assigned_battery']))
        {
            redirect(base_url(''));
        }
        else
        {
            $tests_done    = $this->users_model->get_user_tests_done($user['id'], $user['assigned_battery']);
            $battery       = $this->batteries_model->get($user['assigned_battery']);
            $battery_tests = $this->batteries_model->get_battery_tests($user['assigned_battery']);

            if(!empty($tests_done))
            {
                foreach($tests_done as $test)
                {
                    unset($battery_tests[$test['test_id']]);
                }
            }

            if($battery['presentation'] == 'random'){
                shuffle($battery_tests);
            }

            $test_id = current($battery_tests)['id'];
        }

        $test = $this->tests_model->get($test_id);

        // check if this test exists
        if(empty($test))
        {
            redirect(base_url());
        }

        // check if user can access to this test and it is the first for his battery
        $battery_tests = $this->batteries_model->get_battery_tests($user['assigned_battery']);
        $tests_done    = $this->users_model->get_user_tests_done($user['id'], $user['assigned_battery']);

        if(count($battery_tests) == count($tests_done))
        {
            redirect(base_url());
        }

        if(!empty($tests_done))
        {
            foreach($tests_done as $test_done)
            {
                unset($battery_tests[$test_done['test_id']]);
            }
        }

        if($test_id != current($battery_tests)['id'])
        {
            redirect(base_url());
        }

        $slides = $this->slides_model->get_slides($test_id);     

        if(count($slides) > 0)
        {
            if(isset($test['randomize_range']))
            {
                $slides = $this->randomize_slides($slides, $test['randomize_range']);
            }

            $slide_files = $this->slides_model->get_slides_files($test_id);

            $data = $this->includes;

            // set content data
            $content_data = array(
                'title'         => 'Wpst | test' ,
                'test_id'       => $test_id,
                'left_key'      => $test['left_key'],
                'right_key'     => $test['right_key'],
                'test'          => current($battery_tests),
                'slides'        => $slides,
                'stimuli_files' => $slide_files,
                'folder_name'   => $test['folder_name']
            );

            $template = "../../htdocs/themes/{$this->settings->theme}/template_test.php";

            // load views
            $data['content'] = $this->load->view('do_test', $content_data, TRUE);

            $this->load->view($template, $data);
        }
        else
        {
            redirect(base_url());
        }
    }

    function randomize_slides($old_slides, $randomize_range)
    {
        $randomize_range = json_decode($randomize_range);

        if(empty($randomize_range)){
            return $old_slides;
        }

        foreach($old_slides as $key => $slide)
        {
            $old_slides[$key]['files'] = $this->slides_model->get_slide_files($slide['id'], true);
        }

        $final  = array();
        $slides = array();
        $number = round(current($old_slides)['number']);

        foreach($old_slides as $slide)
        {
            if(round($slide['number']) == $number)
            {
                $slides[$number][] = $slide;
            }
            else
            {
                $number = round($slide['number']);
                $slides[$number][] = $slide;                    
            }
        }

        $current          = current(current($slides))['number'];
        $slides_randomize = array();
        $last_to          = false;

        foreach($randomize_range as $from => $to)
        {
            if($current < $from)
            {
                foreach($slides as $num => $slide)
                {
                    if($num < $current){
                        continue;
                    }

                    if($num >= $from)
                    {
                        $current = $num;
                        break;
                    }

                    $randomize_slides[] = $slide;                    
                }
            }

            $temp = array();

            foreach($slides as $num => $slide)
            {
                if($num < $current){
                    continue;
                }

                if($num > $to)
                {
                    $current = $num;
                    break;
                }

                $temp[] = $slide;
            }

            shuffle($temp);

            foreach($temp as $slide)
            {
                $randomize_slides[] = $slide;
            }

            $last_to = $to;
        }

        if(count($randomize_slides) < count($slides))
        {
            foreach($slides as $num => $slide)
            {
                if($num <= $last_to)
                {
                    continue;
                }

                $randomize_slides[] = $slide;
            }
        } 

        //check if for single image file there are the same slide file, then try to separate those slides

        foreach($randomize_range as $from => $to)
        {
            $last_slide    = null;
            $last_file     = null;
            $current_slide = null;
            $current_file  = null;

            for($key = ($from - 1); $key < $to && $key < count($randomize_slides); $key++)
            {
                $current_slide = $randomize_slides[$key][0];

                if($this->is_critical_file($current_slide))
                {
                    if(is_null($last_file))
                    {
                        $last_file = $current_slide['files'][0]['file_name'];
                    }
                    else
                    {
                        $current_file = $current_slide['files'][0]['file_name'];

                        if($last_file == $current_file)
                        {
                            $done = false;

                            for($index = ($from - 1); $index < $to && !$done && $index < count($randomize_slides); $index++)
                            {
                                $index_slide = $randomize_slides[$index][0];
                                $is_file     = false;

                                if($this->is_critical_file($index_slide))
                                {
                                    $index_file = $randomize_slides[$index][0]['files'][0]['file_name'];
                                }
                                else
                                {
                                    $index_file = null;
                                }

                                if($index != ($key - 1) && $index != ($key) && ($index_file != $current_file || !$is_file))
                                {
                                    //first member of couple

                                    //before check
                                    $first  = null;
                                    $second = null;
                                    $third  = null;

                                    if(isset($randomize_slides[($index - 1)]) && $this->is_critical_file($randomize_slides[($index - 1)][0]))
                                    {
                                        $first = $randomize_slides[($index - 1)][0]['files'][0]['file_name'];
                                    }

                                    $second = $current_file;

                                    if(($index + 1) != ($key  -1) && isset($randomize_slides[($index + 1)]) && $this->is_critical_file($randomize_slides[($index + 1)][0]))
                                    {
                                        $third = $randomize_slides[($index + 1)][0]['files'][0]['file_name'];
                                    }

                                    //after check
                                    if($this->same_file_name_resolution($first, $second, $third))
                                    {
                                        $first  = null;
                                        $second = null;
                                        $third  = null;

                                        if(($key - 2) != $index && isset($randomize_slides[($key - 2)]) && $this->is_critical_file($randomize_slides[($key - 2)][0]))
                                        {
                                            $first = $randomize_slides[($key - 2)][0]['files'][0]['file_name'];
                                        }

                                        $second = $index_file;
                                        $third  = $current_file;

                                        if($this->same_file_name_resolution($first, $second, $third))
                                        {   
                                            $temp = $randomize_slides[$index];
                                            $randomize_slides[$index] = $randomize_slides[($key - 1)];
                                            $randomize_slides[($key - 1)] = $temp;
                                            $done = true;
                                        }
                                    }

                                    //second member of couple
                                    if(!$done)
                                    {
                                        //before check
                                        if(($index - 1) != $key && isset($randomize_slides[($index - 1)]) && $this->is_critical_file($randomize_slides[($index - 1)][0]))
                                        {
                                            $first = $randomize_slides[($index - 1)][0]['files'][0]['file_name'];
                                        }
                                        else
                                        {
                                            $first  = null;
                                        }

                                        $second = $current_file;

                                        if(isset($randomize_slides[($index + 1)]) && $this->is_critical_file($randomize_slides[($index + 1)][0]))
                                        {
                                            $third = $randomize_slides[($index + 1)][0]['files'][0]['file_name'];
                                        }
                                        else
                                        {
                                            $third = null;
                                        }

                                        //after check
                                        if($this->same_file_name_resolution($first, $second, $third))
                                        {
                                            $first  = $current_file;
                                            $second = $index_file;

                                            if(($key + 1) != $index && isset($randomize_slides[($key + 1)]) && $this->is_critical_file($randomize_slides[($key + 1)][0]))
                                            {
                                                $third = $randomize_slides[($key + 1)][0]['files'][0]['file_name'];
                                            }
                                            else
                                            {
                                                $third  = null;
                                            }

                                            if($this->same_file_name_resolution($first, $second, $third))
                                            {   
                                                $temp = $randomize_slides[$index];
                                                $randomize_slides[$index] = $randomize_slides[$key];
                                                $randomize_slides[$key] = $temp;
                                                $done = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if($this->is_critical_file($randomize_slides[$key][0]))
                        {
                            $last_file = $randomize_slides[$key][0]['files'][0]['file_name'];
                        }
                        else
                        {
                            $last_file = null;
                        }
                    }
                }   
                else
                {                    
                    $last_slide = null;
                    $last_file  = null;
                }             
            }
        }

        //end

        foreach($randomize_slides as $slides)
        {
            foreach($slides as $slide)
            {
                $final[] = $slide;
            }
        }

        return $final;
    }

    function same_file_name_resolution($first, $second, $third)
    {        
        $return = false;

        if($first == null || $first != $second)
        {
            if($third == null || $second != $third)
            {
                $return = true;
            }
        }

        //echo '<b>ECCOLI: </b> .' . $first . ". ." . $second . ". ." . $third . '.' . (($return) ? ' true' : ' false') . '<br>';

        return $return;
    }

    function is_critical_file($slide)
    {
        if(($slide['stimuli_type'] == 'images' || $slide['stimuli_type'] == 'instructions') && $slide['stimuli_number'] == '1')
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function start_test()
    {
        $test_id = $this->input->get('test_id');
        $started = $this->input->get('started');

        if(!empty($test_id) && !empty($started))
        {
            $user       = $this->session->userdata('logged_in');
            $user_id    = $user['id'];
            $battery_id = $user['assigned_battery'];
            $id         = $this->tests_model->start_test($test_id, $user_id, $battery_id, $started);
            $answers    = $this->slides_model->get_slides_answers($test_id);

            $result = array(
                'id' => $id,
                'answers' => $answers
            );

            echo json_encode($result);
        }
    }

    function save_slide_result()
    {
        $answers = $this->input->post('answers');

        if(!empty($answers))
        {
            $data = array(
                'test_result_id' => $answers[0],
                'slide_id'       => $answers[1],
                'start'          => $answers[2],
                'r_1'            => $answers[3],
                'r_1_time'       => $answers[4]
            );

            $r = 2;

            for($i = 5; isset($answers[$i]) && isset($answers[$i + 1]); $i = $i + 2)
            {
                $data['r_' . $r]           = $answers[$i];
                $data['r_' . $r . '_time'] = $answers[$i + 1];
                $r++;
            }

            $this->tests_model->save_test_result_slide($data);
        }
    }

    function save_test_result()
    {
        $result_id   = $this->input->post('result_id');
        $ended       = $this->input->post('ended');
        $answers     = $this->input->post('answers');
        $last_answer = $this->input->post('last_answer');

        if(!empty($answers) && !empty($last_answer) && !empty($ended) && !empty($result_id))
        {
            $data = array(
                'test_result_id' => $last_answer[0],
                'slide_id'       => $last_answer[1],
                'start'          => $last_answer[2],
                'r_1'            => $last_answer[3],
                'r_1_time'       => $last_answer[4]
            );

            $r = 2;

            for($i = 5; isset($last_answer[$i]) && isset($last_answer[$i + 1]); $i = $i + 2)
            {
                $data['r_' . $r]           = $last_answer[$i];
                $data['r_' . $r . '_time'] = $last_answer[$i + 1];
                $r++;
            }

            $this->tests_model->save_test_result_slide($data);
            $this->tests_model->edit_test_ended($result_id, $ended);

            /*$data  = array();

            foreach($answers as $key => $answer)
            {
                $data[$key] = array(
                    'test_result_id' => $answer[0],
                    'slide_id'       => $answer[1],
                    'start'          => $answer[2],
                    'r_1'            => $answer[3],
                    'r_1_time'       => $answer[4]
                );

                $r = 2;

                for($i = 5; isset($answer[$i]) && isset($answer[$i + 1]); $i = $i + 2)
                {
                    $data[$key]['r_' . $r]           = $answer[$i];
                    $data[$key]['r_' . $r . '_time'] = $answer[$i + 1];

                    $r++;
                }
            }

            $this->tests_model->edit_test_result($data);*/
            $this->notification($result_id);
        }
    }

    function delete_test_attempt()
    {
        $test_result_id = $this->input->post('result_id');

        if(!empty($test_result_id))
        {
            $this->tests_model->delete_test_result($test_result_id);
        }
    }
}
