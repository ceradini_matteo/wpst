<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tests_model extends CI_Model {

    /**
     * @vars
     */
    private $_table;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // define primary table
        $this->_table = 'tests';
    }

    function get($id = FALSE)
    {
        $this->db->from($this->_table);
        $this->db->where('active','1');

        if($id){
            $this->db->where('id', $id);
            return $this->db->get()->row_array();
        }

        return $this->db->get()->result_array();
    }

    /**
     * Get list of tests
     *
     * @param  int $limit
     * @param  int $offset
     * @param  array $filters
     * @param  string $sort
     * @param  string $dir
     * @return array|boolean
     */
    function get_all($limit = 0, $offset = 0, $filters = array(), $sort = 'id', $dir = 'ASC')
    {
        $this->db->select('id, number, type, slides, description, randomize_range, tests_unread');
        $this->db->from($this->_table);
        $this->db->where('active','1');

        if(!empty($filters))
        {
            foreach ($filters as $key=>$value)
            {  
                $this->db->like($key, $value); 
            }
        }

        if($sort != 'attempts')
        {
            $this->db->order_by($sort, $dir); 
        }        

        if($limit)
        {
            $this->db->limit($limit, $offset);
        }

        $result = $this->db->get()->result_array();      

        if(!is_null($result))
        {
            $results['results'] = $result;
        }
        else
        {
            $results['results'] = NULL;
        }

        $this->db->select('count(*) as total');
        $this->db->from($this->_table);
        $this->db->where('active','1');

        if(!empty($filters))
        {
            foreach ($filters as $key=>$value)
            {  
                $this->db->like($key, $value); 
            }
        }

        $results['total'] = $this->db->get()->row_array()['total'];

        return $results;
    }

    /**
     * Return all tests as array with key equals test_id
     *
     * @return array
     */
    function get_tests_by_id($active = true)
    {
        $this->db->from($this->_table);

        if($active){
            $this->db->where('active','1');   
        }        

        $result = $this->db->get()->result_array();
        $tests = array();

        foreach($result as $test)
        {
            $tests[$test['id']] = $test;
        }

        return $tests;
    }

    function get_test_folder_name($test_id)
    {        
        $this->db->select('folder_name');
        $this->db->from($this->_table);
        $this->db->where('id', $test_id);

        return $this->db->get()->row_array();
    }

    function get_test_id_from_result($test_result_id = false)
    {
        if($test_result_id)
        {
            $this->db->select('test_id');
            $this->db->from('tests_result');
            $this->db->where('id', $test_result_id);
            $this->db->limit(1);

            return current($this->db->get()->row_array());
        }
    }

    /**
     * Add a new test
     *
     * @param  array $data
     * @return mixed|boolean
     */
    function add_test($data = array())
    {
        if ($data)
        {
            $this->db->insert($this->_table, $data);

            if ($id = $this->db->insert_id())
            {
                return $id;
            }
        }

        return FALSE;
    }

    /**
     * Edit an existing test
     *
     * @param  array $data
     * @return boolean
     */
    function edit_test($data = array())
    {
        if($data)
        {
            $this->db->where('id', $data['id']);
            $this->db->update($this->_table, $data);

            return TRUE;
        }

        return FALSE;
    }

    /**
     * Edit number of slides for this test
     *
     * @param  array $data
     * @return boolean
     */
    function edit_test_slides_num($test_id, $count = 0)
    {
        $data = array(
            'slides' => $count
        );

        $this->db->where('id', $test_id);
        $this->db->update($this->_table, $data);
                
        if ($this->db->affected_rows())
        {
            return TRUE;
        }

        return FALSE;
    }

    function edit_test_folder($test_id, $folder_name)
    {
        if($test_id && $folder_name)
        {
            $data = array(
                'folder_name' => $folder_name
            );

            $this->db->where('id', $test_id);
            $this->db->update($this->_table, $data);
        }
    }

    /**
     * Delete an existing test
     *
     * @param  int $id
     * @return boolean
     */
    function delete_test($test_id)
    {        
        $this->db->where('id', $test_id);
        $this->db->set('active', '0');
        $this->db->update($this->_table);

        if ($this->db->affected_rows())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    function start_test($test_id, $user_id, $battery_id, $started)
    {
        if($test_id && $user_id)
        {
            $data = array(
                'test_id'    => $test_id,
                'user_id'    => $user_id,
                'battery_id' => $battery_id,
                'started'    => $started
            );

            $this->db->insert("tests_result", $data);

            if ($id = $this->db->insert_id())
            {
                return $id;
            }
        }

        return FALSE;
    }

    function save_test_result_slide($data)
    {
        if($data)
        {
            $this->db->insert("tests_result_slides", $data);
        }
    }

    function get_test_results($test_id)
    {
        $this->db->select('t.type, t.number, u.username, u.group_id, u.firstname, u.lastname, u.numbers_login, tr.started, tr.ended, u.mturk_key');
        $this->db->from('tests_result as tr');
        $this->db->join('tests as t', 't.id = tr.test_id');
        $this->db->join('users as u', 'u.id = tr.user_id');
        $this->db->where('tr.test_id', $test_id);

        return $this->db->get()->result_array();
    }

    function get_test_results_slides($test_id, $limit = 50000, $offset = 1)
    {
        $this->db->select('t.type as test_type, t.number as test_number, trs.id, trs.test_result_id, u.username, s.number, trs.start, trs.r_1, trs.r_1_time, trs.r_2, trs.r_2_time, trs.r_3, trs.r_3_time, trs.r_4, trs.r_4_time, trs.r_5, trs.r_5_time');
        $this->db->from('tests_result_slides as trs');
        $this->db->join('slides as s', 's.id = trs.slide_id');
        $this->db->join('tests_result as tr', 'tr.id = trs.test_result_id');
        $this->db->join('tests as t', 't.id = tr.test_id');
        $this->db->join('users as u', 'u.id = tr.user_id');
        $this->db->where('tr.test_id', $test_id);

        if($limit > 0){
            $this->db->limit($limit, $offset);
        }

        $result = $this->db->get()->result_array();

        $offset += $limit;

        if($result){
            $last_test_result = end($result)['test_result_id'];
            $last_test_result_id = end($result)['id'];

            $this->db->select('t.type as test_type, t.number as test_number, trs.test_result_id, u.username, s.number, trs.start, trs.r_1, trs.r_1_time, trs.r_2, trs.r_2_time, trs.r_3, trs.r_3_time, trs.r_4, trs.r_4_time, trs.r_5, trs.r_5_time');
            $this->db->from('tests_result_slides as trs');
            $this->db->join('slides as s', 's.id = trs.slide_id');
            $this->db->join('tests_result as tr', 'tr.id = trs.test_result_id');
            $this->db->join('tests as t', 't.id = tr.test_id');
            $this->db->join('users as u', 'u.id = tr.user_id');
            $this->db->where('tr.test_id', $test_id);
            $this->db->where('trs.test_result_id', $last_test_result);
            $this->db->where('trs.id > ' . $last_test_result_id);

            $plus_result = $this->db->get()->result_array();

            if($plus_result){
                $result = array_merge($result, $plus_result);

                $offset += count($plus_result);
            }
        }

        return array($offset, $result);
    }

    function get_count_test_results($test_id)
    {
        $this->db->from('tests_result');
        $this->db->where('test_id', $test_id);

        return $this->db->get()->num_rows();
    }

    function get_tests_unred()
    {
        $this->db->from($this->_table);
        $this->db->where('tests_unread', '1');

        return $this->db->count_all_results();
    }

    function edit_test_ended($result_id, $ended)
    {
        $data = array(
            'ended' => $ended
        );

        $this->db->where('id', $result_id);
        $this->db->update('tests_result', $data);
                
        if($this->db->affected_rows())
        { 
            $this->db->select('test_id');
            $this->db->from('tests_result');
            $this->db->where('id', $result_id);
            $this->db->limit(1);

            $test_id = $this->db->get()->row_array()['test_id'];
            $this->new_test_result($test_id);

            return TRUE;
        }

        return FALSE;
    }

    function edit_test_result($data)
    {
        foreach($data as $result)
        {
            $this->db->select('id');
            $this->db->from('tests_result_slides');            
            $this->db->where('slide_id', $result['slide_id']);
            $this->db->where('test_result_id', $result['test_result_id']);
            $this->db->limit(1);

            $result_slide = $this->db->get()->row_array();

            if(empty($result_slide))
            {
                $this->db->insert("tests_result_slides", $result);
            }
        }
    }

    function new_test_result($test_id = NULL)
    {
        if($test_id)
        {
            $this->db->from($this->_table);
            $this->db->where('id', $test_id);
            $this->db->set('tests_unread', '1');
            $this->db->update();
        }
    }

    function set_test_read($test_id = NULL)
    {
        if($test_id)
        {
            $this->db->from($this->_table);
            $this->db->where('id', $test_id);
            $this->db->set('tests_unread', '0');
            $this->db->update();
        }
    }

    function delete_test_result($test_result_id)
    {
        if($test_result_id)
        {
            $this->db->from('tests_result');
            $this->db->where('id', $test_result_id);
            $this->db->delete();
        }
    }

    function delete_test_results($test_id)
    {
        if($test_id)
        {
            $this->db->from('tests_result');
            $this->db->where('test_id', $test_id);
            $this->db->delete();

            $this->db->set('tests_unread', 0);
            $this->db->where('id', $test_id);
            $this->db->update('tests');

            return TRUE;
        }

        return FALSE;
    }
}