<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Batteries_model extends CI_Model {

	private $_table;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // define primary table
        $this->_table = 'batteries';
    }

    function get($id = FALSE)
    {
        $this->db->from($this->_table);
        $this->db->where('active','1');

        if($id){
            $this->db->where('id', $id);

            return $this->db->get()->row_array();
        }
       
        return $this->db->get()->result_array();
    }

    /**
     * Get list of tests
     *
     * @param  int $limit
     * @param  int $offset
     * @param  array $filters
     * @param  string $sort
     * @param  string $dir
     * @return array|boolean
     */
    function get_all($limit = 0, $offset = 0, $filters = array(), $sort = 'id', $dir = 'ASC')
    {
        $this->db->from($this->_table);
        $this->db->where('active','1');

        if(!empty($filters))
        {
            foreach ($filters as $key=>$value)
            { 
                $this->db->like($key, $value);             
            }
        }        

        if($sort != 'tests'){
            $this->db->order_by($sort, $dir);
        }        

        if($limit)
        {
            $this->db->limit($limit, $offset);
        }

        $result = $this->db->get()->result_array();      

        if(!is_null($result))
        {
            $results['results'] = $result;
        }
        else
        {
            $results['results'] = NULL;
        }

        $this->db->select('count(*) as total');
        $this->db->from($this->_table);
        $this->db->where('active','1');

        if(!empty($filters))
        {
            foreach ($filters as $key=>$value)
            { 
                $this->db->like($key, $value);             
            }
        }        

        if($sort != 'tests'){
            $this->db->order_by($sort, $dir);
        }                

        $results['total'] = $this->db->get()->row_array()['total'];

        return $results;
    }

    function get_battery_from_name($name)
    {
    	$this->db->from($this->_table);
    	$this->db->where('name', $name);
        $this->db->where('active','1');
    	$this->db->limit(1);

    	return $this->db->get()->row_array();
    }

    function get_batteries_from_test($test_id)
    {
        $this->db->select('b.*');
        $this->db->from("$this->_table as b");
        $this->db->join('batteries_tests as bt','bt.battery_id = b.id');
        $this->db->where('bt.test_id', $test_id);
        $this->db->where('b.active','1');

        return $this->db->get()->result_array();
    }

    function get_battery_tests($battery_id)
    {
        $this->db->select('t.*');
        $this->db->from("$this->_table as b");
        $this->db->where('b.id', $battery_id);
        $this->db->where('b.active','1');
        $this->db->join('batteries_tests as bt','b.id = bt.battery_id');
        $this->db->join('tests as t','bt.test_id = t.id');
        $this->db->order_by('bt.order');
       
        $result = $this->db->get()->result_array();
        $tests = array();

        foreach($result as $test)           //use test_id as key
        {
            $tests[$test['id']] = $test;
        }

        return $tests;
    }

    function get_mturk_battery($mturk_link)
    {
        $this->db->from($this->_table);
        $this->db->where('mturk_link', $mturk_link);
        $this->db->limit(1);

        return $this->db->get()->row_array();
    }

    function update_mturk_attempts($battery_id, $attempts)
    {
        $data = array('mturk_attempts' => $attempts);
        
        $this->db->where('id', $battery_id);
        $this->db->update($this->_table, $data);
    }

    /**
     * Add a new battery
     *
     * @param  array $data
     * @return mixed|boolean
     */
    function add_battery($data = array())
    {
        if ($data)
        {            
            $this->db->insert($this->_table, $data);

            if ($id = $this->db->insert_id())
            {
                return $id;
            }
        }

        return FALSE;
    }

    function add_test($battery_id, $test_id, $order)
    {
        if($battery_id && $test_id && $order)
        {
            $data['battery_id'] = $battery_id;
            $data['test_id']    = $test_id;
            $data['order']      = $order;
            
            $this->db->insert('batteries_tests', $data);

            if($id = $this->db->insert_id())
            {
                return $id;
            }
        }
    }

    /**
     * Edit an existing battery
     *
     * @param  array $data
     * @return boolean
     */
    function edit_battery($data = array())
    {
        if ($data)
        {
            $this->db->where('id', $data['id']);
            $this->db->update($this->_table, $data);
            
            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * Delete an existing battery
     *
     * @param  int $id
     * @return boolean
     */
    function delete_battery($battery_id)
    {        
        $this->db->where('id', $battery_id);
        $this->db->set('active', '0');
        $this->db->update($this->_table);

        if ($this->db->affected_rows())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    function delete_battery_test($battery_id, $test_id)
    {
        $this->db->from('batteries_tests');
        $this->db->where('battery_id', $battery_id);
        $this->db->where('test_id', $test_id);
        $this->db->delete();

        if ($this->db->affected_rows())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    function delete_all_test($battery_id)
    {        
        $this->db->from('batteries_tests');
        $this->db->where('battery_id', $battery_id);
        $this->db->delete();

        if ($this->db->affected_rows())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}