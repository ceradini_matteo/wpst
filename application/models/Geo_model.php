<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Geo_model extends CI_Model {

   public function all_for_dropdown($table) {
	  $res = $this->db->from($table)
	    ->order_by('name','asc')
	    ->get()->result();

	  return $this->result_to_map($res, 'id', 'name');
	}
	
	public function get_provinces_from_region_id($id) {
	  $res = $this->db->from('provinces')
	    ->where('area_id',$id)
	    ->get()->result();

	  return $this->result_to_map($res, 'id', 'name');
	}
	
	public function get_istance($id,$table) {
		if(!$id) return NULL;
	  $res = $this->db->from($table)
	    ->where('id',$id)
	    ->get()->row();

	  return $res;
	}
	
	// transform the value returned from result() into a map
	// array($key_index => $value_index)
	public function result_to_map($result, $key_index, $value_index) {
		$map = array();
		foreach ($result as $item) {
			$map[$item->{$key_index}] = $item->{$value_index};
		}
		return $map;
	}

}
