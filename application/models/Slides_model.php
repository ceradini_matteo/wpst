<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Slides_model extends CI_Model {

    /**
     * @vars
     */
    private $_table;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // define primary table
        $this->_table = 'slides';
    }

    function get($slide_id = false)
    {
        $this->db->from($this->_table);

        if($slide_id)
        {
            $this->db->where('id', $slide_id);

            return $this->db->get()->row_array();
        }  

        return $this->db->get()->result_array();
    }

    function get_slides_count($test_id)
    {
        $this->db->select('count(*) as count');
        $this->db->from($this->_table);
        $this->db->where('test_id', $test_id);
        $this->db->limit(1);         
        
        return $this->db->get()->row_array()['count'];   
    }

    function get_slides($test_id)
    {
        $this->db->from($this->_table);
        
        if($test_id)
        {
            $this->db->where('test_id', $test_id);
        }

        $this->db->order_by('number');

        return $this->db->get()->result_array();
    }

    function get_slides_by_id($test_id = false)
    {
        $this->db->from($this->_table);

        if($test_id)
        {
            $this->db->where('test_id', $test_id);
        }

        $this->db->order_by('number');

        $result = $this->db->get()->result_array();
        $slides = array();

        foreach($result as $slide)
        {
            $slides[$slide['id']] = $slide;
        }

        return $slides;
    }

    function get_slides_answers($test_id)
    {
        if($test_id)
        {
            $this->db->select('id, answer');
            $this->db->from($this->_table);
            $this->db->where('test_id', $test_id);

            $result = $this->db->get()->result_array();
            $slides = array();

            foreach($result as $slide)
            {
                $slides[$slide['id']] = $slide;
            }

            return $slides;
        }

        return FALSE;
    }

    function get_slides_files($test_id)
    {
        $this->db->from('slides_files as sf');
        $this->db->join('slides as s', 's.id = sf.slide_id');
        $this->db->where('s.test_id', $test_id);
        $this->db->order_by('sf.number');

        return $this->db->get()->result_array();
    }

    function get_slide_files($slide_id, $only_file_name = FALSE)
    {
        if($only_file_name){
            $this->db->select('file_name');
        }

        $this->db->from('slides_files');
        $this->db->where('slide_id', $slide_id);
        $this->db->order_by('number');

        return $this->db->get()->result_array();
    }
    
    function add_slide($data)
    {
        if($data)
        {
            $this->db->insert($this->_table, $data);

            if($id = $this->db->insert_id())
            {
                return $id;
            }
        }

        return FALSE;
    }

    function add_stimuli_file($slide_id, $file_name, $number)
    {
        if($slide_id && $file_name && $number)
        {           
            $data = array(
                'slide_id'  => $slide_id,
                'file_name' => $file_name,
                'number'    => $number
            );

            $this->db->insert('slides_files', $data);            

            if($id = $this->db->insert_id())
            {
                return $id;
            }
        }

        return FALSE;
    }

    function edit_slide($data)
    {
        if($data)
        {
            $this->db->where('id', $data['id']);
            $this->db->update($this->_table, $data);

            return TRUE;
        }

        return FALSE;
    }

    function delete_slide($slide_id)
    {
        $this->db->from($this->_table);
        $this->db->where('id', $slide_id);
        $this->db->delete();

        if ($this->db->affected_rows())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    function delete_all($test_id)
    {
        $this->db->from($this->_table);
        $this->db->where('test_id', $test_id);
        $this->db->delete();

        if ($this->db->affected_rows())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    function delete_all_slide_files($slide_id)
    {
        $this->db->from('slides_files');
        $this->db->where('slide_id', $slide_id);
        $this->db->delete();

        if ($this->db->affected_rows())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}