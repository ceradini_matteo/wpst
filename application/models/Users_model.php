<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

    /**
     * @vars
     */
    private $_table;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // define primary table
        $this->_table = 'users';
    }

    /**
     * Get list of users
     *
     * @param  int $limit
     * @param  int $offset
     * @param  array $filters
     * @param  string $sort
     * @param  string $dir
     * @return array|boolean
     */
    function get_all($limit = 0, $offset = 0, $filters = array(), $sort = 'id', $dir = 'ASC', $no_admin = FALSE, $export = FALSE)
    {
        $this->db->select('u.id, u.username, u.password, u.firstname, u.lastname, u.email, u.race, u.birth_date, u.gender, u.is_admin, u.group_id, u.start_time, u.end_time, u.assigned_battery , b.name as battery');
        $this->db->from("$this->_table as u");
        $this->db->join('batteries as b','b.id = u.assigned_battery','left');
        $this->db->where('u.active','1');

        if($no_admin){
            $this->db->where('is_admin','0');
        }
        else{
            $this->db->where('is_admin','1');
        }

        if(!empty($filters))
        {
            foreach ($filters as $key=>$value)
            {  
                $this->db->like($key, $value); 
            }
        }

        $this->db->order_by($sort, $dir);

        if($limit)
        {
            $this->db->limit($limit, $offset);
        }

        $result = $this->db->get()->result_array();      

        if(!is_null($result))
        {
            $results['results'] = $result;
        }
        else
        {
            $results['results'] = NULL;
        }

        $this->db->select('count(*) as total');
        $this->db->from("$this->_table as u");
        $this->db->join('batteries as b','b.id = u.assigned_battery','left');
        $this->db->where('u.active','1');

        if($no_admin){
            $this->db->where('is_admin','0');
        }
        else{
            $this->db->where('is_admin','1');
        }

        if(!empty($filters))
        {
            foreach ($filters as $key=>$value)
            {  
                $this->db->like($key, $value); 
            }
        }
        
        $results['total'] = $this->db->get()->row_array()['total'];

        return $results;
    }

    /**
     * Return all users as array with key equals test_id
     *
     * @return array
     */
    function get_user_by_id($active = true)
    {
        $this->db->from($this->_table);
        $this->db->where('is_admin','0');

        if($active){
            $this->db->where('active','1');
        }

        $result = $this->db->get()->result_array();
        $users = array();

        foreach($result as $user)
        {
            $users[$user['id']] = $user;
        }

        return $users;
    }

    /**
     * Get specific user
     *
     * @param  int $id
     * @return array|boolean
     */
    function get_user($id = NULL, $fronted = FALSE)
    {
        if($id)
        {
            if($fronted){
                $this->db->select('id,username,firstname,lastname,email,is_admin,start_time,end_time,group_id,assigned_battery');
            }

            $this->db->from($this->_table);
            $this->db->where('id', $id);
            $this->db->where('active','1');

            $result = $this->db->get()->row_array();
            
            if(!is_null($result))
            {
                return $result;
            }
        }

        return FALSE;
    }

    function get_user_id_from_username($username)
    {
        $this->db->from($this->_table);
        $this->db->where('username', $username);
        $this->db->where('active','1');

        $result = $this->db->get()->row_array();
            
        if(!is_null($result))
        {
            return $result;
        }

        return FALSE;
    }

    function get_user_tests_done($user_id, $battery_id)
    {
        $this->db->from('tests_result');
        $this->db->where('user_id', $user_id);
        $this->db->where('battery_id', $battery_id);

        return $this->db->get()->result_array();
    }

    function get_admins_email()
    {
        $this->db->select('email');
        $this->db->from($this->_table);
        $this->db->where('is_admin', '1');

        return $this->db->get()->result_array();
    }

    function get_wpst_email()
    {
        $this->db->select('value as email');
        $this->db->from('settings');
        $this->db->where('name', 'site_email');

        return $this->db->get()->row_array();
    }

    /**
     * Add a new user
     *
     * @param  array $data
     * @return mixed|boolean
     */
    function add_user($data = array(), $encrypt = TRUE)
    {
        if ($data)
        {
            if($encrypt)
            {
                // secure password
                $data['salt']     = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), TRUE));
                $data['password'] = hash('sha512', $data['password'] . $data['salt']); 
            }
            
            $data['active'] = '1';

            $this->db->insert($this->_table, $data);
            
            if ($id = $this->db->insert_id())
            {
                return $id;
            }
        }

        return FALSE;
    }

    function add_mturk_key($user_id = NULL, $key = NULL)
    {
        if($user_id && $key)
        {
            $this->db->from($this->_table);
            $this->db->where('id', $user_id);
            $this->db->set('mturk_key', $key);
            $this->db->update();
        }
    }

    /**
     * User creates their own profile
     *
     * @param  array $data
     * @return mixed|boolean
     */
    function create_profile($data = array())
    {
        if ($data)
        {
            // secure password and create validation code
            $salt            = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), TRUE));
            $password        = hash('sha512', $data['password'] . $salt);
            $validation_code = sha1(microtime(TRUE) . mt_rand(10000, 90000));

            $sql = "
                INSERT INTO {$this->_table} (
                    username,
                    password,
                    salt,
                    firstname,
                    lastname,
                    email,
                    is_admin,
                    validation_code,
                    created,
                    updated
                ) VALUES (
                    " . $this->db->escape($data['username']) . ",
                    " . $this->db->escape($password) . ",
                    " . $this->db->escape($salt) . ",
                    " . $this->db->escape($data['firstname']) . ",
                    " . $this->db->escape($data['lastname']) . ",
                    " . $this->db->escape($data['email']) . ",
                    '0',
                    " . $this->db->escape($validation_code) . ",
                    '" . date('Y-m-d H:i:s') . "',
                    '" . date('Y-m-d H:i:s') . "'
                )
            ";

            $this->db->query($sql);
            if ($this->db->insert_id())
            {
                return $validation_code;
            }
        }

        return FALSE;
    }

    /**
     * Edit an existing user
     *
     * @param array $data
     * @param boolean $encrypt
     * @return boolean
     */
    function edit_user($data = array(), $encrypt = TRUE)
    {
        if ($data)
        {
            if (isset($data['password']) && $data['password'] != '')
            {
                if($encrypt)
                {
                   // secure password
                    $data['salt']     = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), TRUE));
                    $data['password'] = hash('sha512', $data['password'] . $data['salt']); 
                }                
            }
            else
            {
                unset($data['password']);
            }

            $this->db->where('id', $data['id']);
            $this->db->update($this->_table, $data);
            
            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }

    function increment_login_number($user_id)
    {
        $this->db->where('id', $user_id);
        $this->db->set('numbers_login', 'numbers_login + 1', FALSE);
        $this->db->update($this->_table);
    }

    /**
     * User edits their own profile
     *
     * @param  array $data
     * @param  int $user_id
     * @return boolean
     */
    function edit_profile($data = array(), $user_id = NULL)
    {
        if($data && $user_id)
        {
            $data = get_array_fields($data, $this->config->item('user_fields'));

            if($data['password'] != ''){
                $salt     = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), TRUE));
                $password = hash('sha512', $data['password'] . $salt);

                $this->db->set('salt', $salt);
                $this->db->set('password', $password);
            }

            $this->db->where('id', $user_id);
            $this->db->update($this->_table);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }


    /**
     * Delete an existing user (soft delete)
     *
     * @param  int $id
     * @return boolean
     */
    function delete_user($user_id)
    {        
        $this->db->where('id', $user_id);
        $this->db->set('active', '0');
        $this->db->update($this->_table);

        if ($this->db->affected_rows())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }


    /**
     * Check for valid login credentials
     *
     * @param  string $username
     * @param  string $password
     * @return array|boolean
     */
    function login($username = NULL, $password = NULL)
    {
        if ($username && $password)
        {
            $this->db->select('id, username, password, salt, firstname, lastname, email, is_admin, start_time, end_time, group_id, assigned_battery');
            $this->db->from($this->_table);
            $this->db->where('username', $username);            
            $this->db->where('active','1');
            $this->db->or_where('email', $username);
            $this->db->limit(1);

            $results = $this->db->get()->row_array();
            $salted_password = hash('sha512', $password . $results['salt']);

            if ($results['password'] == $salted_password)
            {
                unset($results['password']);
                unset($results['salt']);

                return $results;
            }
        }

        return FALSE;
    }


    /**
     * Validate a user-created account
     *
     * @param  string $encrypted_email
     * @param  string $validation_code
     * @return boolean
     */
    function validate_account($encrypted_email = NULL, $validation_code = NULL)
    {
        if ($encrypted_email && $validation_code)
        {
            $sql = "
                SELECT id
                FROM {$this->_table}
                WHERE SHA1(email) = " . $this->db->escape($encrypted_email) . "
                    AND validation_code = " . $this->db->escape($validation_code) . "
                    AND active = '0'
                    AND deleted = '0'
                LIMIT 1
            ";

            $query = $this->db->query($sql);

            if ($query->num_rows())
            {
                $results = $query->row_array();

                $sql = "
                    UPDATE {$this->_table}
                    SET active = '0',
                        validation_code = NULL
                    WHERE id = '" . $results['id'] . "'
                ";

                $this->db->query($sql);

                if ($this->db->affected_rows())
                {
                    return TRUE;
                }
            }
        }

        return FALSE;
    }


    /**
     * Reset password
     *
     * @param  array $data
     * @return mixed|boolean
     */
    function forgot_password($email_or_username = false)
    {
        if($email_or_username)
        {
            $this->db->select('id, username, email');
            $this->db->from($this->_table);
            $this->db->where('active', '1');
            $this->db->where('email', $email_or_username);
            $this->db->or_where('username', $email_or_username);
            $this->db->limit(1);

            $user = $this->db->get()->row_array();

            if($user)
            {
                // create new random password
                $user_data['username']     = $user['username'];
                $user_data['email']        = $user['email'];
                $user_data['reset_code']   = substr(md5(uniqid(mt_rand(), true)) , 0, 16);

                $this->db->set('reset_code', $user_data['reset_code']);
                $this->db->where('id', $user['id']);
                $this->db->update($this->_table);

                if($this->db->affected_rows())
                {
                    return $user_data;
                }
            }
        }

        return FALSE;
    }

    function reset_password($reset_code = false, $password = false)
    {
        if($reset_code && $password)
        {
            $this->db->select('id, username');
            $this->db->from($this->_table);
            $this->db->where('active', '1');
            $this->db->where('reset_code', $reset_code);
            $this->db->limit(1);

            $user = $this->db->get()->row_array();

            if($user)
            {
                $salt     = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), TRUE));
                $password = hash('sha512', $password . $salt);

                // create new random password
                $user_data = array(
                    'password'   => $password,
                    'salt'       => $salt,
                    'reset_code' => NULL
                );

                $this->db->where('id', $user['id']);
                $this->db->update($this->_table, $user_data);

                return $this->db->affected_rows() ? true : false;
            }
        }
    }

    function validate_user_reset_code($reset_code = false)
    {
        if($reset_code)
        {
            $this->db->select('id');
            $this->db->from($this->_table);
            $this->db->where('active', '1');
            $this->db->where('reset_code', $reset_code);
            $this->db->limit(1);

            return $this->db->count_all_results() == 0 ? false : true;
        }
    }


    /**
     * Check to see if a username already exists
     *
     * @param  string $username
     * @return boolean
     */
    function username_exists($username)
    {
        $this->db->select('id');
        $this->db->from($this->_table);
        $this->db->where('username', $username);        
        $this->db->where('active','1');
        $this->db->limit(1);
        $result = $this->db->get()->row_array();

        if (!is_null($result))
        {
            return TRUE;
        }

        return FALSE;
    }


    /**
     * Check to see if an email already exists
     *
     * @param  string $email
     * @return boolean
     */
    function email_exists($email)
    {
        $this->db->select('id');
        $this->db->from($this->_table);
        $this->db->where('email', $email);
        $this->db->where('active','1');
        $this->db->limit(1);
        $result = $this->db->get()->row_array();

        if (!is_null($result))
        {
            return TRUE;
        }

        return FALSE;
    }
    

}
