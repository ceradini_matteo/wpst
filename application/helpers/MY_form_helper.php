<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Form Value
*
* Grabs a value from the POST array for the specified field so you can
* re-populate an input field or textarea.  If Form Validation
* is active it retrieves the info from the validation class
* NEW: Added a third parameter prep to skip htmlspecialchars escaping
*
* @access public
* @param string
* @return mixed
*/
if ( ! function_exists('set_value'))
{
	function set_value($field = '', $default = '', $prep = true)
	{
		if (FALSE === ($OBJ =& _get_validation_object()))
		{
			if ( ! isset($_POST[$field]))
			{
				return $default;
			}
			
			return $prep ? form_prep($_POST[$field], $field) : $_POST[$field];
		}
		
		return $prep ? form_prep($OBJ->set_value($field, $default), $field) : $OBJ->set_value($field, $default);
	}
}