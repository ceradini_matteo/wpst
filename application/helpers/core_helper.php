<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Outputs an array in a user-readable JSON format
 *
 * @param array $array
 */
if (!function_exists('display_json'))
{
    function display_json($array)
    {
        $data = json_indent($array);

        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');

        echo $data;
    }
}


/**
 * Convert an array to a user-readable JSON string
 *
 * @param array $array - The original array to convert to JSON
 * @return string - Friendly formatted JSON string
 */
if (!function_exists('json_indent'))
{
    function json_indent($array = array())
    {
        // make sure array is provided
        if (empty($array))
            return NULL;

        //Encode the string
        $json = json_encode($array);

        $result        = '';
        $pos           = 0;
        $str_len       = strlen($json);
        $indent_str    = '  ';
        $new_line      = "\n";
        $prev_char     = '';
        $out_of_quotes = true;

        for ($i = 0; $i <= $str_len; $i++)
        {
            // grab the next character in the string
            $char = substr($json, $i, 1);

            // are we inside a quoted string?
            if ($char == '"' && $prev_char != '\\')
            {
                $out_of_quotes = !$out_of_quotes;
            }
            // if this character is the end of an element, output a new line and indent the next line
            elseif (($char == '}' OR $char == ']') && $out_of_quotes)
            {
                $result .= $new_line;
                $pos--;

                for ($j = 0; $j < $pos; $j++)
                {
                    $result .= $indent_str;
                }
            }

            // add the character to the result string
            $result .= $char;

            // if the last character was the beginning of an element, output a new line and indent the next line
            if (($char == ',' OR $char == '{' OR $char == '[') && $out_of_quotes)
            {
                $result .= $new_line;

                if ($char == '{' OR $char == '[')
                {
                    $pos++;
                }

                for ($j = 0; $j < $pos; $j++)
                {
                    $result .= $indent_str;
                }
            }

            $prev_char = $char;
        }

        // return result
        return $result . $new_line;
    }
}


/**
 * Save data to a CSV file
 *
 * @param array $array
 * @param string $filename
 * @return bool
 */
if (!function_exists('array_to_csv'))
{
    function array_to_csv($array = array(), $filename = "export.csv", $header_displayed = FALSE)
    {
        $CI = get_instance();

        // disable the profiler otherwise header errors will occur
        $CI->output->enable_profiler(FALSE);

        if ( ! empty($array))
        {
            // ensure proper file extension is used
            if ( ! substr(strrchr($filename, '.csv'), 1))
            {
                $filename .= '.csv';
            }

            try
            {
                // set the headers for file download
                header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
                header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
                header("Cache-Control: no-cache, must-revalidate");
                header("Pragma: no-cache");
                header("Content-type: text/csv");
                header("Content-Description: File Transfer");
                header("Content-Disposition: attachment; filename={$filename}");

                $output = @fopen('php://output', 'w');

                foreach ($array as $row)
                {
                    if ( ! $header_displayed)
                    {
                        // use the array keys as the header row
                        fputcsv($output, array_keys($row), ',');
                        $header_displayed = TRUE;
                    }

                    // clean the data
                    /*$allowed = '/[^a-zA-Z0-9_ %@\|\[\]\.\(\)%&-]/s';
                    foreach ($row as $key => $value)
                    {
                        $row[$key] = preg_replace($allowed, '', $value);
                    }*/

                    // insert the data
                    fputcsv($output, $row, ',');
                }

                fclose($output);

            }
            catch (Exception $e) {}
        }

        exit;
    }
}

/**
 * Save data to a CSV file
 *
 * @param array $array
 * @param string $filename
 * @return bool
 */
if (!function_exists('array_to_csv_file'))
{
    function array_to_csv_file($array = array(), $filename = "export.csv", $header_displayed = FALSE, $mode = 'w')
    {
        $CI = get_instance();

        // disable the profiler otherwise header errors will occur
        $CI->output->enable_profiler(FALSE);

        if(!empty($array))
        {
            // ensure proper file extension is used
            if(!substr(strrchr($filename, '.csv'), 1)){
                $filename .= '.csv';
            }

            try
            {
                $output = @fopen($filename, $mode);

                if($output){
                    foreach($array as $row)
                    {
                        if(!$header_displayed){
                            // use the array keys as the header row
                            fputcsv($output, array_keys($row), ',');
                            $header_displayed = TRUE;
                        }

                        // insert the data
                        fputcsv($output, $row, ',');
                    }

                    fclose($output);
                }
                else {
                    die("ERROR");
                }
            }
            catch (Exception $e) {}
        }
    }
}

/**
 * Read csv file
 *
 * @param array $array
 * @return bool
 */
if (!function_exists('csv_to_array'))
{
    function csv_to_array($path, $file_name)
    { 
        $results = array();

        if (($handle = fopen($path . $file_name, "r")) !== FALSE) 
        {    
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
            {
                if (array_filter($data))
                {
                    $num = count($data);
                    $row = array();

                    for ($c = 0; $c < $num; $c++) 
                    {
                        if(isset($fields))
                        {
                            $row[$fields[$c]] = $data[$c];
                        }
                        else
                        {
                            $row[] = $data[$c];
                        }                       
                    }  

                    $results[] = $row;

                    if(!isset($fields))
                    {
                        $fields = $results[0];  
                        unset($results[0]);   //Unset first label coloumn
                    }   
                }                
            }
            fclose($handle);
        }
        
        return $results;
    }
}

/**
 * Save backup of db to csv
 *
 * @param array $array
 * @return bool
 */
if (!function_exists('backup_to_csv'))
{
    function backup_to_csv($folder, $backup, $file_name = false)
    { 
        if($file_name)
        {
            $fp = fopen($folder . $file_name, 'w');
        }
        else
        {
            $fp = fopen($folder . date('Y_m_d_h_i_s_a', time()) . '.csv', 'w'); 
        }
        
        $header_displayed = FALSE;

        foreach ($backup as $fields) 
        {
            unset($fields['battery']);

            if (!$header_displayed)
            {
                // use the array keys as the header row
                fputcsv($fp, array_keys($fields), ',');
                $header_displayed = TRUE;
            }

            fputcsv($fp, $fields, ',');
        }

        fclose($fp);
    }
}

/**
 * Generates a random password
 *
 * @return string
 */
if (!function_exists('generate_random_password'))
{
    function generate_random_password()
    {
        $characters = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
        $alpha_length = strlen($characters) - 1;

        for ($i = 0; $i < 8; $i++)
        {
            $n = rand(0, $alpha_length);
            $pass[] = $characters[$n];
        }

        return implode($pass);
    }
}

/**
 * Get only a part of array
 *
 * @access public
 * @param array $array
 * @param array $fields
 * @return array
 */
if (!function_exists('get_array_fields'))
{
    function get_array_fields($array,$fields)
    {
	    $return = array();
			foreach($array as $key => $val) {
				if(in_array($key, $fields)){
					$return[$key] = $val;
				}
			}
			return $return;
    }
}

/**
 * Get only a part of array
 *
 * @access public
 * @param array $array
 * @param array $fields
 * @return array
 */
if (!function_exists('convert_array_dates_fields'))
{
    function convert_array_dates_fields($array,$fields)
    {
	    $return = $array;
			foreach($fields as $key => $val) {
				if(isset($return[$val])){
					$return[$val] = from_italian_date($return[$val]);
				}
			}
			return $return;
    }
}

if (!function_exists('delete_directory'))
{
    function delete_directory($dir) 
    {
        if (!file_exists($dir)) {            
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) 
        {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!delete_directory($dir . '/' . $item)) 
            {
                return false;
            }
        }

        return rmdir($dir);
    }
}

if (!function_exists('create_img'))
{
    /**
    * Create new image from existing file
    *
    * @param  string  $imgfile    Source image file name
    * @param  string  $imgthumb   Thumbnail file name
    * @param  int     $newwidth   Thumbnail width
    * @param  int     $newheight  Optional thumbnail height
    * @param  string  $option     Type of resize
    *
    * @return bool
    * @throws \Exception
    */
    function create_img($imgfile, $imgthumb, $newwidth, $newheight = null, $option = "crop")
    {
       
        $result = false;
        if(file_exists($imgfile) || strpos($imgfile,'http')===0){

            $timeLimit = ini_get('max_execution_time');
            set_time_limit(30);
            if (strpos($imgfile,'http')===0 || image_check_memory_usage($imgfile, $newwidth, $newheight))
            {  
                require_once('php_image_magician.php');
                $magicianObj = new imageLib($imgfile);
                $magicianObj->resizeImage($newwidth, $newheight, $option);
                $magicianObj->saveImage($imgthumb, 80);
                $result = true;
            }
            set_time_limit($timeLimit);
        }
        return $result;
    }
}

if (!function_exists('image_check_memory_usage'))
{
    /**
    * Check if memory is enough to process image
    *
    * @param  string  $img
    * @param  int     $max_breedte
    * @param  int     $max_hoogte
    *
    * @return bool
    */
    function image_check_memory_usage($img, $max_breedte, $max_hoogte)
    {
        if (file_exists($img))
        {
            $K64 = 65536; // number of bytes in 64K
            $memory_usage = memory_get_usage();
            $memory_limit = abs(intval(str_replace('M', '', ini_get('memory_limit')) * 1024 * 1024));
            $image_properties = getimagesize($img);
            $image_width = $image_properties[0];
            $image_height = $image_properties[1];
            if (isset($image_properties['bits']))
                $image_bits = $image_properties['bits'];
            else
                $image_bits = 0;
            $image_memory_usage = $K64 + ($image_width * $image_height * ($image_bits) * 2);
            $thumb_memory_usage = $K64 + ($max_breedte * $max_hoogte * ($image_bits) * 2);
            $memory_needed = intval($memory_usage + $image_memory_usage + $thumb_memory_usage);

            if ($memory_needed > $memory_limit)
            {
                ini_set('memory_limit', (intval($memory_needed / 1024 / 1024) + 5) . 'M');
                if (ini_get('memory_limit') == (intval($memory_needed / 1024 / 1024) + 5) . 'M')
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }
}

function fix_strtolower($str)
{
    if (function_exists('mb_strtoupper'))
    {
        return mb_strtolower($str);
    }
    else
    {
        return strtolower($str);
    }
}

if (!function_exists('my_parse_str'))
{
    function my_parse_str($string) 
    {
        if ( '' == $string ) {
            return false;
        }

        $result = array();
        $pairs  = explode('&', $string);

        foreach($pairs as $key => $pair)
        {
            parse_str($pair, $params);

            $k = key($params);

            if(!isset($result[$k]))
            {
                $result += $params;
            } 
            else 
            {
                $result[$k] = my_array_merge_recursive_distinct($result[$k], $params[$k]);
            }
        }

        return $result;
    }
}

if(!function_exists('my_array_merge_recursive_distinct'))
{
    function my_array_merge_recursive_distinct(array $array1, array $array2) 
    {
        $merged = $array1;

        foreach ($array2 as $key => $value)
        {
            if (is_array($value) && isset($merged[$key]) && is_array($merged[$key])) 
            {
                $merged[$key] = my_array_merge_recursive_distinct($merged[$key], $value);
            } 
            else if (is_numeric($key) && isset($merged[$key])) 
            {
                $merged[] = $value;  
            } 
            else
            {
                $merged[$key] = $value;
            }
        }

        return $merged;
    }
}

