<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Layout for th on admin list
 *
 * @param array $array
 */
if ( ! function_exists('print_th_list'))
{
    function print_th_list($field_name,$sort,$dir,$limit,$offset,$lang_line,$filter)
    {
	    ?>
        <th <?php echo (($sort == $field_name) ? ' class="table-active"' : ''); ?>>
          <a href="<?php echo current_url(); ?>?sort=<?php echo $field_name; ?>&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang($lang_line); ?></a>
          <?php if ($sort == $field_name) : ?><span class="pull-xs-right ion-chevron-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
        </th>
      <?php
    }
}

/**
 * Layout for td on admin list
 *
 * @param array $array
 */
if ( ! function_exists('print_td_list'))
{
    function print_td_list($field_name,$sort,$value,$row_id = 1,$type='text',$optional=false,$value_zero=false)
    {
	    ?>        
        <td class="<?php echo (($sort == $field_name) ? ' table-active' : '');  ?>">
            <a id="<?php echo $field_name.$row_id; ?>" role="button" class="editable" data-type="<?php echo $type; ?>" data-id="<?php echo $row_id; ?>" data-name="<?php echo $field_name; ?>" data-value="<?php echo $value; ?>" data-optional="<?php echo $optional; ?>">
              <?php 

                if(!empty($value) || $value_zero && $value == 0){
                  echo $value;
                }
                else{
                  echo 'n/a';
                }
              ?>
            </a>
        </td>
      <?php
    }
}



/**
 * Print input form
 *
 */
if ( ! function_exists('print_input_form'))
{
    function print_input_form($field_name,$label,$obj,$div_class="col-sm-3",$field_class="form-control",$type='text',$required=false)
    {
	    ?>        
        <div class="form-group <?php echo $div_class; ?><?php echo form_error($field_name) ? ' has-error' : ''; ?>">
            <?php echo form_label($label, $field_name, array('class'=>'control-label')); ?>
            <?php 
	            $data = array('type'=>$type,'name'=>$field_name, 'value'=>set_value($field_name, $obj[$field_name]), 'class'=>$field_class,'id'=>$field_name);
	            if($required){
		            $data['required']='required';
	            }
	            
	            echo form_input($data); ?>
        </div>
      <?php
    }
}


/**
 * Print date form
 *
 */
if ( ! function_exists('print_date_form'))
{
    function print_date_form($field_name,$label,$obj,$div_class="col-sm-3",$field_class="form-control",$required=false)
    {
	    ?>        
        <div class="form-group <?php echo $div_class; ?><?php echo form_error($field_name) ? ' has-error' : ''; ?>">
          <?php echo form_label($label, $field_name, array('class'=>'control-label')); ?>
           <div class="input-group date">
             <?php 
	             
	            $data = array('name'=>$field_name, 'value'=>set_value($field_name, print_date('d/m/Y',@$obj[$field_name])), 'class'=>$field_class,'id'=>$field_name);
	            if($required){
		            $data['required']='required';
	            } 
	            echo form_input($data); ?>
              <span class="input-group-addon"><span class="ion-ios-calendar-outline"></span></span>
          </div>
        </div>
      <?php
    }
}

/**
 * Print switch form
 *
 */
if ( ! function_exists('print_switch_form'))
{
    function print_switch_form($field_name,$label,$obj,$div_class="col-sm-3",$on_field='Si',$off_field="No",$field_class='form_control',$required=false)
    {
	    ?>        
        <div class="form-group <?php echo $div_class; ?><?php echo form_error($field_name) ? ' has-error' : ''; ?>">
            <?php echo form_label($label, $field_name, array('class'=>'control-label')); ?><br/>
            <?php
	            $array_options = array(
	            'name'=>$field_name, 
	            'value'=>1,
	            'checked'=>set_value($field_name, @$obj[$field_name]), 
	            'class'=>$field_class." input-checkbox",
	            'id'=>$field_name,
	            'data-on-text'=>$on_field,
	            'data-off-text'=>$off_field
	            );
	            
	            if($required){
		            $array_options['required']='required';
	            }
	            
							echo form_checkbox($array_options); ?>
        </div>
      <?php
    }
}


/**
 * Print dropdown form
 *
 */
if ( ! function_exists('print_dropdown_form'))
{
    function print_dropdown_form($field_name,$label,$options,$obj,$div_class="col-sm-3",$field_class="form-control",$search=false,$required=false)
    {
	    
	    ?>        
        
        <div class="form-group <?php echo $div_class; ?><?php echo form_error($field_name) ? ' has-error' : ''; ?>">
            <?php echo form_label($label, $field_name, array('class'=>'control-label')); ?>
            <?php 
	            $search_option ='';
	            if($search){
		            $search_option = 'data-live-search="true"';
	            }
	            $req='';
	            if($required){
		            $req=' required';
	            }
	            echo form_dropdown($field_name,$options,set_value($field_name, @$obj[$field_name]), 'class="selectpicker form-control" data-size="8" title="seleziona" '.$search_option.' id="'.$field_name.'"'.$req); ?>
        </div>
      <?php
    }
}
