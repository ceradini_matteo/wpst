<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_settings_field extends CI_migration {

	public function up()
	{
		$this->db->query("INSERT INTO `settings` (`id`, `name`, `input_type`, `options`, `is_numeric`, `show_editor`, `input_size`, `help_text`, `validation`, `sort_order`, `label`, `value`, `last_update`, `updated_by`) VALUES (NULL, 'login_message', 'textarea', NULL, '0', '1', 'large', 'Text viewed in homepage after login.', 'trim', '70', 'Homepage message', 'There aren\'t tests available for you', '2017-10-24 15:05:59', '1')");
	}

	public function down()
	{
	}
}
