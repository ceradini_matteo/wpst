<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Fix_result_slide_time_type extends CI_migration {

	public function up()
	{
		$this->db->query("ALTER TABLE `tests_result_slides` CHANGE `r_2_time` `r_2_time` INT UNSIGNED NULL DEFAULT NULL, CHANGE `r_3_time` `r_3_time` INT UNSIGNED NULL DEFAULT NULL, CHANGE `r_4_time` `r_4_time` INT UNSIGNED NULL DEFAULT NULL, CHANGE `r_5_time` `r_5_time` INT UNSIGNED NULL DEFAULT NULL;");
	}

	public function down()
	{
	}
}
