<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_mturk extends CI_migration {
	
	public function up()
	{
		$this->db->query("ALTER TABLE `batteries` ADD `mturk` TINYINT NOT NULL DEFAULT '0' AFTER `active`;");
		$this->db->query("ALTER TABLE `batteries` ADD `mturk_link` VARCHAR(128) NULL DEFAULT NULL AFTER `mturk`;");		
		$this->db->query("ALTER TABLE `batteries` ADD `mturk_attempts` INT NULL DEFAULT '0' AFTER `mturk_link`;");
	}
	
	public function down()
	{
		$this->db->query("ALTER TABLE `batteries` DROP `mturk_attempts`");
		$this->db->query("ALTER TABLE `batteries` DROP `mturk_link`");
		$this->db->query("ALTER TABLE `batteries` DROP `mturk`");
	}
	
}
