<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_users_reset_code extends CI_migration {

	public function up()
	{
		$this->db->query("ALTER TABLE `users` ADD `reset_code` VARCHAR(32) NULL DEFAULT NULL AFTER `salt`;");
	}

	public function down()
	{
		$this->db->query("ALTER TABLE `users` DROP `reset_code`;");
	}
}
