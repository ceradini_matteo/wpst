<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_test_dynamic_key extends CI_migration {

	public function up()
	{
		$this->db->query("ALTER TABLE `tests` ADD `left_key` CHAR(1) NOT NULL DEFAULT 'w' AFTER `description`, ADD `right_key` CHAR(1) NOT NULL DEFAULT 'o' AFTER `left_key`;");
	}

	public function down()
	{
		$this->db->query("ALTER TABLE `tests` DROP `left_key`, DROP `right_key`;");
	}
}
