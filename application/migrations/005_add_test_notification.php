<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_test_notification extends CI_migration {
	
	public function up()
	{
		$this->db->query("ALTER TABLE `tests` ADD `tests_unread` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `active`;");
	}
	
	public function down()
	{
		$this->db->query("ALTER TABLE `tests` DROP `tests_unread`;");
	}
	
}
