<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_timebar extends CI_migration {
	
	public function up()
	{
		$this->db->query('ALTER TABLE `slides` ADD `timebar_enabled` TINYINT(1) NULL DEFAULT NULL AFTER `trigger`;');		
	}
	
	public function down()
	{
		$this->db->query('ALTER TABLE `slides` DROP `timebar_enabled`;');
	}
	
}
