<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Up_mturk extends CI_migration {
	
	public function up()
	{
		$this->db->query("ALTER TABLE `users` ADD `mturk_key` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `assigned_battery`;");
	}
	
	public function down()
	{
		$this->db->query("ALTER TABLE `users` DROP `mturk_key`;");
	}
	
}
