<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_textbox_filters extends CI_migration {

	public function up()
	{
		$this->db->query("ALTER TABLE `slides` ADD `stimuli_filters` TEXT NULL DEFAULT NULL COMMENT 'JSON with stimuli filters' AFTER `stimuli_layout`;");
	}

	public function down()
	{
		$this->db->query("ALTER TABLE `slides` DROP `stimuli_filters`;");
	}
}
