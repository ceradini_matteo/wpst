<div class="card">
    <div class="card-header">
      <h3><?php echo lang('core button list'); ?></h3>
    </div>

		<div class="card-block no-padding">
    	<table class="table table-striped table-hover">
        <thead>
				
            <?php // sortable headers ?>
            <tr>
                <?php echo print_th_list('deadline',$sort,$dir,$limit,$offset,'customers col deadline',$filter); ?>
                <?php echo print_th_list('fulfillment_id',$sort,$dir,$limit,$offset,'customers col fulfillment_id',$filter); ?>
                <?php echo print_th_list('company',$sort,$dir,$limit,$offset,'customers col company',$filter); ?>
                <?php echo print_th_list('firstname',$sort,$dir,$limit,$offset,'customers col firstname',$filter); ?>
                <?php echo print_th_list('lastname',$sort,$dir,$limit,$offset,'customers col lastname',$filter); ?>
                <th width="110"></th>
            </tr>

            <?php // search filters ?>
            <tr>
                <?php echo form_open("{$this_url}?sort={$sort}&dir={$dir}&limit={$limit}&offset=0{$filter}", array('role'=>'form', 'id'=>"filters")); ?>
                    <th<?php echo ((isset($filters['deadline'])) ? ' class="has-success"' : ''); ?>>
	                    
	                        <div class="input-group date input-group-sm">
		                        <span class="input-group-addon"><small>da</small></span>
	                            <?php echo form_input(array('name'=>'deadline', 'id'=>'deadline', 'class'=>'form-control', 'placeholder'=>lang('customers input deadline'), 'value'=>set_value('deadline', ((isset($filters['deadline'])) ? print_date('d/m/Y',$filters['deadline']) : '')))); ?>
	                        </div>
	                    </th>
                    <th class="<?php echo (($sort == 'fulfillment_id') ? 'table-active' : ''); ?><?php echo ((isset($filters['fulfillment_id'])) ? ' has-success' : ''); ?>">
	                    	<?php echo form_dropdown('fulfillment_id',array(''=>'Tutti')+$fulfillments_array,
		                    	(isset($filters['fulfillment_id'])) ? $filters['fulfillment_id'] : '', 'class="form-control form-control-sm" title="seleziona" id="fulfillment_id"'); ?>
                    </th>
                    <th class="<?php echo (($sort == 'company') ? 'table-active' : ''); ?><?php echo ((isset($filters['company'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'company', 'id'=>'company', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('customers input company'), 'value'=>set_value('company', ((isset($filters['company'])) ? $filters['company'] : '')))); ?>
                    </th>
                    <th class="<?php echo (($sort == 'firstname') ? 'table-active' : ''); ?><?php echo ((isset($filters['firstname'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'firstname', 'id'=>'firstname', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('customers input firstname'), 'value'=>set_value('firstname', ((isset($filters['firstname'])) ? $filters['firstname'] : '')))); ?>
                    </th>
                    <th class="<?php echo (($sort == 'lastname') ? 'table-active' : ''); ?><?php echo ((isset($filters['lastname'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'lastname', 'id'=>'email', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('customers input lastname'), 'value'=>set_value('lastname', ((isset($filters['lastname'])) ? $filters['lastname'] : '')))); ?>
                    </th>
                    
                    <th>
	                    <div class="btn-group pull-right" role="group" aria-label="Action">
                            <a href="<?php echo $this_url; ?>" class="btn btn-secondary btn-sm tooltips" data-toggle="tooltip" data-placement="left" title="<?php echo lang('customers tooltip filter_reset'); ?>"><span class="ion-backspace-outline"></span></a>
                            <button type="submit" name="submit" value="<?php echo lang('core button filter'); ?>" class="btn btn-sm btn-primary tooltips" data-toggle="tooltip" data-placement="left" title="<?php echo lang('customers tooltip deadline filter'); ?>"><span class="ion-funnel"></span></button>
                        </div>
                    </th>
                <?php echo form_close(); ?>
            </tr>

        </thead>
        <tbody>

            <?php // data rows ?>
            <?php if ($total) : ?>
                <?php foreach ($fulfillments as $fulfillment) : ?>
                    <tr id="row_<?php echo $fulfillment['id']; ?>">
	                    	<td<?php echo (($sort == 'deadline') ? ' class="table-active"' : ''); ?>>
								          <b><?php echo print_date('d/m/Y',$fulfillment['deadline']); ?></b> 
								        </td>
	                    	<?php echo print_td_list('fulfillment_id',$sort,"<span class='type'>".$fulfillments_array[$fulfillment['fulfillment_id']]."</span>"); ?>
	                    	<?php echo print_td_list('company',$sort,"<span class='company'>".$fulfillment['company']."</span>"); ?>
	                    	<?php echo print_td_list('firstname',$sort,"<span class='firstname'>".$fulfillment['firstname']."</span>"); ?>
	                    	<?php echo print_td_list('lastname',$sort,"<span class='lastname'>".$fulfillment['lastname']."</span>"); ?>

	                    	<td class="text-sm-right"><button type="button" class="btn btn-success btn-sm open-modal" data-id="<?php echo $fulfillment['id']; ?>" data-customer_id="<?php echo $fulfillment['customer_id']; ?>" data-fulfillment_id="<?php echo $fulfillment['fulfillment_id']; ?>" data-date="<?php echo $fulfillment['deadline']; ?>"><span class="ion-android-checkbox-outline"></span> completa</button></td>
                    </tr>
                <?php endforeach; ?>
            <?php else : ?>
                <tr>
                    <td colspan="6">
                        <?php echo lang('core error no_results'); ?>
                    </td>
                </tr>
            <?php endif; ?>

        </tbody>
    	</table>
		</div>
    <?php // list tools ?>
    <div class="card-footer">
        <div class="col-md-2 text-left">
            <label><?php echo sprintf(lang('table label rows'), $total); ?></label>
        </div>
        <div class="col-md-2 text-left">
            <?php if ($total > 10) : ?>
                <select id="limit" class="form-control">
                    <option value="10"<?php echo ($limit == 10 OR ($limit != 10 && $limit != 25 && $limit != 50 && $limit != 75 && $limit != 100)) ? ' selected' : ''; ?>>10 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="25"<?php echo ($limit == 25) ? ' selected' : ''; ?>>25 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="50"<?php echo ($limit == 50) ? ' selected' : ''; ?>>50 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="75"<?php echo ($limit == 75) ? ' selected' : ''; ?>>75 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="100"<?php echo ($limit == 100) ? ' selected' : ''; ?>>100 <?php echo lang('admin input items_per_page'); ?></option>
                </select>
            <?php endif; ?>
        </div>
        <div class="col-md-2 text-xs-right">
            
        </div>
        <div class="col-md-6 text-center">
            <?php echo $pagination; ?>
        </div>
    </div>

</div>


<div class="modal  fade" id="fulfillmentModal" tabindex="-1" role="dialog" aria-labelledby="modal-label-date" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>Completa adempimento</h4>
            </div>
            <div class="modal-body">
	            	<div class="row">
		            	<div class="col-sm-12">
			            	<p class="lead" id="modalContent"></p>
		            	</div>
	            	</div>
                <div class="row" id="modalFormContent">
	                																																			 
                </div>
            </div>
            <div class="modal-footer">
	            <div class="pull-right">
                <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>

                <button type="button" class="btn btn-primary" id="updateFulfillment">Salva</button>
                <button type="button" class="btn btn-success" id="completeFulfillment"><span class="ion-android-checkbox-outline"></span> Segna come completato</button>                	            </div>
            </div>
        </div>
    </div>
</div>
