<form id="fulfillmentForm">
<div class="row">
	<div class="col-sm-12">
	<?php
	$fulfillment_fields = json_decode($fulfillment['fields']);
	
	$dd = json_decode($fulfillment['deadlines']);
	$deadlines = array();
	foreach($dd as $d){
		$deadlines[$d]=print_date('d/m/Y',$d);
	}
	echo print_dropdown_form('deadline','Scadenza',$deadlines,$customer_fulfillment,"col-sm-3","form-control",false,true);
	
	foreach($fulfillment_fields as $f){
		$obj=array();
		if(isset($f->default)){
			$obj[$f->name] = $f->default;
		}
		if(isset($fields->{$f->name})){
			$obj[$f->name] = $fields->{$f->name};
		}
		if($f->description){
			$description_tooltip = " <span class=\"ion-help-circled\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"".$f->description."\"></span>";	
		}else{
			$description_tooltip = "";
		}
		$required='<span class="error">*</span> ';
		if($f->optional){
			$required='';
		}
		
		switch($f->type){
			case 1: //flag
				
				echo print_switch_form($f->name,$required.$f->name.$description_tooltip,$obj,"col-sm-3",$on_field='Fatto',$off_field="da fare",'form_control',!$f->optional);

			break;
			case 2: //Dropdown
				echo print_dropdown_form($f->name,$required.$f->name.$description_tooltip,[''=>'seleziona']+$f->values,$obj,"col-sm-3","form-control",false,!$f->optional);
			break;
			case 3: //string
				echo print_input_form($f->name,$required.$f->name.$description_tooltip,$obj,"col-sm-3","form-control",'text',!$f->optional);
			break;
			case 4: //number
				echo print_input_form($f->name,$required.$f->name.$description_tooltip,$obj,"col-sm-3","form-control",'number',!$f->optional);
			break;
		}
	}
	echo form_hidden('customer_id',$customer_fulfillment['customer_id']);
	echo form_hidden('fulfillment_id',$customer_fulfillment['fulfillment_id']);
	echo form_hidden('id',$customer_fulfillment['id']);
	?>
	
	</div>
</div>
</form>
