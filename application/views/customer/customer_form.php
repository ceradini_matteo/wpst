<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="well">
<?php echo form_open('', array('role'=>'form')); ?>


    <div class="row">
	    <?php print_dropdown_form('type_id',lang('customers input type_id'),$this->config->item('customer_types'),$customer); ?>
    </div>
    <div class="row">
        <?php // last name ?>
        <div class="no-display form-group col-sm-3<?php echo form_error('lastname') ? ' has-error' : ''; ?>" id="lastname_field">
            <?php echo form_label(lang('customers input lastname'), 'lastname', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'lastname', 'value'=>set_value('lastname', $customer['lastname']), 'class'=>'form-control','id'=>'lastname')); ?>
        </div>
        <?php // first name ?>
        <div class="no-display form-group col-sm-3<?php echo form_error('firstname') ? ' has-error' : ''; ?>" id="firstname_field">
            <?php echo form_label(lang('customers input firstname'), 'firstname', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'firstname', 'value'=>set_value('firstname', $customer['firstname']), 'class'=>'form-control','id'=>'firstname')); ?>
        </div>
        <?php // company ?>
        <div class="no-display form-group col-sm-3<?php echo form_error('company') ? ' has-error' : ''; ?>" id="company_field">
            <?php echo form_label(lang('customers input company'), 'company', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'company', 'value'=>set_value('company', $customer['company']), 'class'=>'form-control','id'=>'company')); ?>
        </div>
    </div>
    <fieldset class="no-display  bordered form-group" id="office">
	    <legend><span class="ion-ios-briefcase-outline"></span> Sede Legale</legend>
    <div class="row">
        <?php // office ?>
        <?php print_input_form('office',lang('customers input office'),$customer,'col-sm-3'); ?>
        
        <?php // city ?>
        <?php print_input_form('office_city',lang('customers input office_city'),$customer,'col-sm-3'); ?>
        
        <?php // provinces ?>
        <?php print_dropdown_form('office_province_id',lang('customers input office_province_id'),$provinces,$customer,'col-sm-3','form-control',true); ?>
        
        <?php // number ?>
        <div class="form-group col-sm-3<?php echo form_error('offices_number') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('customers input offices_number'), 'offices_number', array('class'=>'control-label')); ?>
            <?php echo form_input(array('type'=>'number','name'=>'offices_number', 'value'=>set_value('offices_number', $customer['offices_number']), 'class'=>'form-control','id'=>'offices_number')); ?>
        </div>
    </div>
    </fieldset>
    
    <fieldset class="no-display bordered form-group" id="home">
	    <legend><span class="ion-ios-home-outline"></span> Residenza</legend>
	    <div class="row">
	    <?php // office ?>
        <div class="form-group col-sm-3<?php echo form_error('home') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('customers input home'), 'home', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'home', 'value'=>set_value('home', $customer['home']), 'class'=>'form-control')); ?>
        </div>
        <?php // city ?>
        <div class="form-group col-sm-3<?php echo form_error('home_city') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('customers input home_city'), 'home_city', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'home_city', 'value'=>set_value('home_city', $customer['home_city']), 'class'=>'form-control','id'=>'city')); ?>
        </div>
        <?php // provinces ?>
        <?php print_dropdown_form('home_province_id',lang('customers input home_province_id'),$provinces,$customer,'col-sm-3','form-control',true); ?>
        
	    </div>
    </fieldset>
    
    <fieldset class="form-group bordered">
	    <legend><span class="ion-ios-bookmarks-outline"></span> Anagrafica</legend>
	    <div class="row">
	    <?php // pec ?>
        <?php print_input_form('email',lang('customers input email'),$customer,'col-sm-3','form-control','email'); ?>
        <?php print_input_form('phone',lang('customers input phone'),$customer,'col-sm-3','form-control','tel'); ?>
        <?php print_input_form('fax',lang('customers input fax'),$customer,'col-sm-3','form-control','tel'); ?>
        
        <?php // partita iva ?>
        <div class="form-group col-sm-3<?php echo form_error('codice_attivita') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('customers input codice_attivita'), 'piva', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'codice_attivita', 'value'=>set_value('codice_attivita',$customer['codice_attivita']), 'class'=>'form-control')); ?>
        </div>
	    </div>
			<div class="row">

        <?php // codice fiscale ?>
        <div class="form-group col-sm-3<?php echo form_error('cod_fis') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('customers input cod_fis'), 'cod_fis', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'cod_fis', 'value'=>set_value('cod_fis', $customer['cod_fis']), 'class'=>'form-control')); ?>
        </div>
        <?php // partita iva ?>
        <div class="form-group col-sm-3<?php echo form_error('piva') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('customers input piva'), 'piva', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'piva', 'value'=>set_value('piva',$customer['piva']), 'class'=>'form-control')); ?>
        </div>
        
        <?php // scadenza attribuzione_piva ?>
        <?php print_date_form('data_piva',lang('customers input data_piva'),$customer,'col-sm-3 col-xs-6'); ?>
        
        <?php // scadenza cessazione_piva ?>
        <?php print_date_form('data_piva_cessazione',lang('customers input data_piva_cessazione'),$customer,'col-sm-3 col-xs-6'); ?>
        
    	</div>
    </fieldset>

    <fieldset class="form-group bordered">
	    <legend><span class="ion-ios-calculator-outline"></span> Contabilità</legend>
			<div class="row">
        <?php // contabilita ?>    
        <?php print_switch_form('contabilita',lang('customers input contabilita'),$customer,'col-sm-3 col-xs-6','Interna','Esterna'); ?>
            
    
        <?php // tipo contabilità ?>
        <?php print_dropdown_form('tipo_contabilita',lang('customers input tipo_contabilita'),$this->config->item('tipo_contabilita'),$customer); ?>
        
        <?php // periodicità iva ?>
        <?php print_dropdown_form('periodicita_iva',lang('customers input periodicita_iva'),$this->config->item('periodicita_iva'),$customer); ?>

        <?php // rate ?>
        <?php print_dropdown_form('rate',lang('customers input rate'),$this->config->item('rate'),$customer); ?> 
			</div>
    </fieldset>
    
    <fieldset class="form-group bordered">
	    <legend><span class="ion-ios-analytics-outline"></span> Altri Dati</legend>
			<div class="row">       
        <?php // Contribuzione previdenziale ?>
        <?php print_dropdown_form('contribuzione_previdenziale',lang('customers input contribuzione_previdenziale'),$this->config->item('contribuzione_previdenziale'),$customer); ?>
        
        
        <?php // f24 ?>
        <?php print_switch_form('pagamento_F24',lang('customers input pagamento_F24'),$customer,'col-sm-3 col-xs-6'); ?>
        
        <?php // vies ?>
        <?php print_switch_form('vies',lang('customers input vies'),$customer,'col-sm-3 col-xs-6'); ?>   
        
        <?php // irap ?>
        <?php print_switch_form('irap',lang('customers input irap'),$customer,'col-sm-3 col-xs-6'); ?>
        
    	</div>
    	<div class="row">  
				<?php // inail ?>
        <?php print_dropdown_form('inail',lang('customers input inail'),$this->config->item('inail'),$customer); ?>
        
        <?php // consultant ?>
        <div id="consultant" class="no-display">
        
					<?php // consultant ?>
	        <?php print_dropdown_form('consultant_id',lang('customers input consultant'),$consultants_dropdown,$customer,'col-sm-3','form-control',true); ?>
	        
        </div> 
    	</div>
    </fieldset>
    
    <fieldset class="form-group bordered">
	    <legend><span class="ion-ios-email-outline"></span> PEC</legend>
	    <div class="row">
	    <?php // pec ?>
        <?php print_input_form('pec',lang('customers input pec'),$customer,'col-sm-3','form-control','email'); ?>
        
        <?php // pass ?>
        <?php print_input_form('pec_password',lang('customers input pec_password'),$customer,'col-sm-3'); ?>
        
        <?php // controllata ?>
        <?php print_switch_form('pec_controllata',lang('customers input pec_controllata'),$customer,'col-sm-3 col-xs-6'); ?>   
        
        <?php // scadenza ?>
        <?php print_date_form('pec_scadenza',lang('customers input pec_scadenza'),$customer,'col-sm-3 col-xs-6'); ?>
      </div>
    </fieldset>
    <fieldset class="form-group bordered">
	    <legend><span class="ion-ios-calendar-outline"></span> Scadenze</legend>
    <div class="row">
        <?php // scadenza cassetto_scadenza ?>        
        <?php print_date_form('cassetto_scadenza',lang('customers input cassetto_scadenza'),$customer,'col-sm-3 col-xs-6'); ?>
        
        <?php // scadenza smart_card ?>
        <?php print_date_form('smart_card',lang('customers input smart_card'),$customer,'col-sm-3 col-xs-6'); ?>
    </div>
    </fieldset>
    <fieldset class="form-group bordered">
	    <legend><span class="ion-ios-box-outline"></span> Documenti</legend>
    <div class="row">
        <?php // delega_cassetto_fiscale ?>
        <?php print_switch_form('delega_cassetto_fiscale',lang('customers input delega_cassetto_fiscale'),$customer,'col-sm-3 col-xs-6'); ?>
        
        <?php // antiriciclaggio ?>
        <?php print_switch_form('antiriciclaggio',lang('customers input antiriciclaggio'),$customer,'col-sm-3 col-xs-6'); ?>
        
        <?php // delega_inps ?>
        <?php print_switch_form('delega_inps',lang('customers input delega_inps'),$customer,'col-sm-3 col-xs-6'); ?>
        
        <?php // mandato ?>
        <?php print_switch_form('mandato',lang('customers input mandato'),$customer,'col-sm-3 col-xs-6'); ?>
        
        <?php // deposito_scritture ?>
        <?php print_switch_form('deposito_scritture',lang('customers input deposito_scritture'),$customer,'col-sm-3 col-xs-6'); ?>
        
        
        <?php // pagamento_F24 ?>
        <div id="delega_F24">
        <?php print_switch_form('delega_pagamento_F24',lang('customers input delega_pagamento_F24'),$customer,'col-sm-3 col-xs-6'); ?>
        </div>
        
        
    </div>
    </fieldset>
    
    <fieldset class="form-group bordered">
	    <legend><span class="ion-ios-printer-outline"></span> Stampe</legend>
    <div class="row">
        <?php // stampa ?>
        <?php print_input_form('stampa_registri',lang('customers input stampa_registri'),$customer,'col-sm-3 col-xs-6'); ?>
        
        <?php // scadenza visura ?>
        <?php print_date_form('visura',lang('customers input visura'),$customer,'col-sm-3 col-xs-6'); ?>
        
        <?php // scadenza libri_sociali ?>
        <?php print_date_form('libri_sociali',lang('customers input libri_sociali'),$customer,'col-sm-3 col-xs-6'); ?>
    </div>
    </fieldset>
    
		
    <?php // buttons ?>
    <div class="row text-xs-right">
	    <div class="col-sm-12">
        <a class="btn btn-default" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
        <button type="submit" name="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> <?php echo lang('core button save'); ?></button>
	    </div>
    </div>

<?php echo form_close(); ?>
</div>