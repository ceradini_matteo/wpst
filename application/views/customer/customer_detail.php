<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-xs-12">
	<dl class="dl-horizontal">
	  <dt class="col-sm-4 col-md-2"><?php echo lang('customers input type_id'); ?></dt>
	  <dd class="col-sm-8 col-md-4"><?php echo $this->config->item('customer_types')[$customer['type_id']]; ?></dd>
	  
	  <?php if($customer['type_id']!=3 && $customer['type_id']!=7){ ?>
	  <dt class="col-sm-4 col-md-2"><?php echo lang('customers input company'); ?></dt>
	  <dd class="col-sm-8 col-md-4"><?php echo $customer['company']; ?></dd>
	  <?php } ?>
	  
	  <?php if($customer['type_id']==3 || $customer['type_id']==7){ ?>
	  <dt class="col-sm-4 col-md-2">&nbsp;</dt>
	  <dd class="col-sm-4">&nbsp;</dd>
	  <dt class="col-sm-4 col-md-2"><?php echo lang('customers input lastname'); ?></dt>
	  <dd class="col-sm-4"><?php echo $customer['lastname']; ?></dd>
	  <dt class="col-sm-4 col-md-2"><?php echo lang('customers input firstname'); ?></dt>
	  <dd class="col-sm-8 col-md-4"><?php echo $customer['firstname']; ?></dd>
	  <?php } ?>
	</dl>
</div>
<?php if($customer['type_id']!=3){ ?>

	<fieldset class="bordered form-group" id="office">
		<legend style="background:white;"><span class="ion-ios-briefcase-outline"></span> Sede Legale</legend>
		<dl>
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input office'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$customer['office']; ?>&nbsp;</dd>
			
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input office_city'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$customer['office_city']; ?>&nbsp;</dd>
		</dl><div class="clearfix"></div>
		<hr/>
	    <dl>	
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input office_province_id'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$provinces[$customer['office_province_id']]; ?>&nbsp;</dd>
			
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input offices_number'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$customer['offices_number']; ?>&nbsp;</dd>
		</dl>
	</fieldset>
<?php } ?>

<?php if($customer['type_id']==3 || $customer['type_id']==7){ ?>
    <fieldset class="bordered form-group" id="home">
	    <legend style="background:white;"><span class="ion-ios-home-outline"></span> Residenza</legend>
		<dl>
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input home'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$customer['home']; ?>&nbsp;</dd>
			
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input home_city'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$customer['home_city']; ?>&nbsp;</dd>
		</dl><div class="clearfix"></div>
		<hr/>
	    <dl>	
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input home_province_id'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$provinces[$customer['home_province_id']]; ?>&nbsp;</dd>
			
		</dl>
	</fieldset>
<?php } ?>

    <fieldset class="form-group bordered">
	    <legend style="background:white;"><span class="ion-ios-bookmarks-outline"></span> Anagrafica</legend>
	    <dl>
		    <dt class="col-sm-4 col-md-2"><?php echo lang('customers input codice_attivita'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$customer['codice_attivita']; ?>&nbsp;</dd>
			
		    <dt class="col-sm-4 col-md-2"><?php echo lang('customers input email'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$customer['email']; ?>&nbsp;</dd>
		</dl><div class="clearfix"></div>
		<hr/>
	    <dl>	
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input phone'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$customer['phone']; ?>&nbsp;</dd>
			
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input fax'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$customer['fax']; ?>&nbsp;</dd>
		</dl><div class="clearfix"></div>
		<hr/>
	    <dl>	
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input cod_fis'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$customer['cod_fis']; ?>&nbsp;</dd>
			
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input piva'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$customer['piva']; ?>&nbsp;</dd>
		</dl><div class="clearfix"></div>
		<hr/>
	    <dl>	
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input data_piva'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @print_date('d/m/Y',$customer['data_piva']); ?>&nbsp;</dd>
			
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input data_piva_cessazione'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @print_date('d/m/Y',$customer['data_piva_cessazione']); ?>&nbsp;</dd>
		</dl>
    </fieldset>


    <fieldset class="form-group bordered">
	    <legend style="background:white;"><span class="ion-ios-calculator-outline"></span> Contabilità</legend>
	    <dl>
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input contabilita'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo ($customer['contabilita'])?'Interna':'Esterna'; ?></dd>
			
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input tipo_contabilita'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$this->config->item('tipo_contabilita')[$customer['tipo_contabilita']]; ?>&nbsp;</dd>
	    </dl><div class="clearfix"></div>
		<hr/>
	    <dl>
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input periodicita_iva'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$this->config->item('periodicita_iva')[$customer['periodicita_iva']]; ?>&nbsp;</dd>
			
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input rate'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$this->config->item('rate')[$customer['rate']]; ?>&nbsp;</dd>
	    </dl>
    </fieldset>
    
    
    
    <fieldset class="form-group bordered">
	    <legend style="background:white;"><span class="ion-ios-analytics-outline"></span> Altri Dati</legend>
	    
	    <dl>
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input contribuzione_previdenziale'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$this->config->item('contribuzione_previdenziale')[$customer['contribuzione_previdenziale']]; ?>&nbsp;</dd>
			
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input pagamento_F24'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo ($customer['pagamento_F24'])?'Si':'No'; ?></dd>
		</dl><div class="clearfix"></div>
		<hr/>
	    <dl>
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input vies'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo ($customer['vies'])?'Si':'No'; ?></dd>
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input irap'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo ($customer['irap'])?'Si':'No'; ?></dd>
			
		</dl><div class="clearfix"></div>
		<hr/>
	    <dl>	
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input inail'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$this->config->item('inail')[$customer['inail']]; ?>&nbsp;</dd>
			
			<?php if($customer['inail']==2){ ?>
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input consultant'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$consultants_dropdown[$customer['consultant_id']]; ?>&nbsp;</dd>
			<?php } ?>
			
	    </dl>
	    
    </fieldset>


    <fieldset class="form-group bordered">
	    <legend style="background:white;"><span class="ion-ios-email-outline"></span> PEC</legend>
	    
	    <dl>
		    <dt class="col-sm-4 col-md-2"><?php echo lang('customers input pec'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$customer['pec']; ?>&nbsp;</dd>
		    <dt class="col-sm-4 col-md-2"><?php echo lang('customers input pec_password'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @$customer['pec_password']; ?>&nbsp;</dd>
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input pec_controllata'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo ($customer['pec_controllata'])?'Si':'No'; ?></dd>
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input pec_scadenza'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @print_date('d/m/Y',$customer['pec_scadenza']); ?>&nbsp;</dd>
	    </dl>
    </fieldset>
     <fieldset class="form-group bordered">
	    <legend style="background:white;"><span class="ion-ios-calendar-outline"></span> Scadenze</legend>
		<dl>
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input cassetto_scadenza'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @print_date('d/m/Y',$customer['cassetto_scadenza']); ?>&nbsp;</dd>
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input smart_card'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @print_date('d/m/Y',$customer['smart_card']); ?>&nbsp;</dd>
		</dl>
     </fieldset>
    
    <fieldset class="form-group bordered">
	    <legend style="background:white;"><span class="ion-ios-box-outline"></span> Documenti</legend>
	    <dl>
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input delega_cassetto_fiscale'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo ($customer['delega_cassetto_fiscale'])?'Si':'No'; ?></dd>
			
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input antiriciclaggio'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo ($customer['antiriciclaggio'])?'Si':'No'; ?></dd>
			</dl><div class="clearfix"></div>
		<hr/>
	    <dl>
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input delega_inps'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo ($customer['delega_inps'])?'Si':'No'; ?></dd>
			
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input mandato'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo ($customer['mandato'])?'Si':'No'; ?></dd>
		</dl><div class="clearfix"></div>
		<hr/>
	    <dl>
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input deposito_scritture'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo ($customer['deposito_scritture'])?'Si':'No'; ?></dd>
			<?php if($customer['pagamento_F24']){ ?>
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input delega_pagamento_F24'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo ($customer['delega_pagamento_F24'])?'Si':'No'; ?></dd>
			<?php } ?>
	    </dl>
    </fieldset>
    
    <fieldset class="form-group bordered">
	    <legend style="background:white;"><span class="ion-ios-printer-outline"></span> Stampe</legend>
	    <dl>
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input stampa_registri'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo $customer['stampa_registri']; ?></dd>
			
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input visura'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @print_date('d/m/Y',$customer['visura']); ?>&nbsp;</dd>
		</dl><div class="clearfix"></div>
		<hr/>
	    <dl>	
			<dt class="col-sm-4 col-md-2"><?php echo lang('customers input libri_sociali'); ?></dt>
			<dd class="col-sm-8 col-md-4"><?php echo @print_date('d/m/Y',$customer['libri_sociali']); ?>&nbsp;</dd>
			
	    </dl>
	   
    </fieldset>
    