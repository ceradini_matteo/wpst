<div class="card">
    <div class="card-header">
	     <a class="pull-xs-right btn btn-sm btn-outline-primary tooltips" href="<?php echo base_url('customer/form'); ?>" title="<?php echo lang('customers tooltip add_new_customer'); ?>" data-toggle="tooltip"><span class="ion-plus"></span> <?php echo lang('customers button add_new_customer'); ?></a>
      <h3><?php echo lang('core button list'); ?></h3>
    </div>
		<div class="card-block no-padding">
    	<table class="table table-striped table-hover">
        <thead>

            <?php // sortable headers ?>
            <tr>
                <?php echo print_th_list('company',$sort,$dir,$limit,$offset,'customers col company',$filter); ?>
                <?php echo print_th_list('firstname',$sort,$dir,$limit,$offset,'customers col firstname',$filter); ?>
                <?php echo print_th_list('lastname',$sort,$dir,$limit,$offset,'customers col lastname',$filter); ?>
                <?php echo print_th_list('cod_fis',$sort,$dir,$limit,$offset,'customers col cod_fis',$filter); ?>
                <?php echo print_th_list('piva',$sort,$dir,$limit,$offset,'customers col piva',$filter); ?>
                <th class="text-xs-right" width="170">
                    <?php echo lang('customers col actions'); ?>
                </th>
            </tr>

            <?php // search filters ?>
            <tr>
                <?php echo form_open("{$this_url}?sort={$sort}&dir={$dir}&limit={$limit}&offset=0{$filter}", array('role'=>'form', 'id'=>"filters")); ?>
                    <th class="<?php echo (($sort == 'company') ? 'table-active' : ''); ?><?php echo ((isset($filters['company'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'company', 'id'=>'company', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('customers input company'), 'value'=>set_value('company', ((isset($filters['company'])) ? $filters['company'] : '')))); ?>
                    </th>
                    <th class="<?php echo (($sort == 'firstname') ? 'table-active' : ''); ?><?php echo ((isset($filters['firstname'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'firstname', 'id'=>'firstname', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('customers input firstname'), 'value'=>set_value('firstname', ((isset($filters['firstname'])) ? $filters['firstname'] : '')))); ?>
                    </th>
                    <th class="<?php echo (($sort == 'lastname') ? 'table-active' : ''); ?><?php echo ((isset($filters['lastname'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'lastname', 'id'=>'email', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('customers input lastname'), 'value'=>set_value('lastname', ((isset($filters['lastname'])) ? $filters['lastname'] : '')))); ?>
                    </th>
                    <th class="<?php echo (($sort == 'cod_fis') ? 'table-active' : ''); ?><?php echo ((isset($filters['cod_fis'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'cod_fis', 'id'=>'cod_fis', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('customers input cod_fis'), 'value'=>set_value('cod_fis', ((isset($filters['cod_fis'])) ? $filters['cod_fis'] : '')))); ?>
                    </th>
                    <th class="<?php echo (($sort == 'piva') ? 'table-active' : ''); ?><?php echo ((isset($filters['piva'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'piva', 'id'=>'piva', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('customers input piva'), 'value'=>set_value('piva', ((isset($filters['piva'])) ? $filters['piva'] : '')))); ?>
                    </th>
                    <th>
	                    <div class="btn-group pull-right" role="group" aria-label="Action">
                            <a href="<?php echo $this_url; ?>" class="btn btn-secondary btn-sm tooltips" data-toggle="tooltip" data-placement="left" title="<?php echo lang('customers tooltip filter_reset'); ?>"><span class="ion-backspace-outline"></span></a>
                            <button type="submit" name="submit" value="<?php echo lang('core button filter'); ?>" class="btn btn-sm btn-primary tooltips" data-toggle="tooltip" data-placement="left" title="<?php echo lang('customers tooltip filter'); ?>"><span class="ion-funnel"></span></button>
                        </div>
                    </th>
                <?php echo form_close(); ?>
            </tr>

        </thead>
        <tbody>

            <?php // data rows ?>
            <?php if ($total) : ?>
                <?php foreach ($customers as $customer) : ?>
                    <tr id="row_<?php echo $customer['id']; ?>">
	                    	<?php echo print_td_list('company',$sort,"<span class='company'>".$customer['company']."</span>"); ?>
	                    	<?php echo print_td_list('firstname',$sort,"<span class='firstname'>".$customer['firstname']."</span>"); ?>
	                    	<?php echo print_td_list('lastname',$sort,"<span class='lastname'>".$customer['lastname']."</span>"); ?>
	                    	<?php echo print_td_list('cod_fis',$sort,$customer['cod_fis']); ?>
	                    	<?php echo print_td_list('piva',$sort,$customer['piva']); ?>
		                    	<td>
                                <div class="btn-group pull-right">
                                  <a data-id="<?php echo $customer['id']; ?>" class="btn btn-sm btn-secondary btn-delete-customer" title="<?php echo lang('admin button delete'); ?>"><span class="ion-trash-b"></span></a>
                                  <a href="<?php echo $this_url; ?>/form/<?php echo $customer['id']; ?>" class="btn btn-sm btn-secondary" title="<?php echo lang('admin button edit'); ?>"><span class="ion-edit"></span></a>
                                  <button type="button" class="btn btn-primary btn-sm open-detail" data-id="<?php echo $customer['id']; ?>" title="<?php echo lang('admin button show'); ?>">mostra</button>
                                </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else : ?>
                <tr>
                    <td colspan="4">
                        <?php echo lang('core error no_results'); ?>
                    </td>
                </tr>
            <?php endif; ?>

        </tbody>
    	</table>
		</div>
    <?php // list tools ?>
    <div class="card-footer">
        <div class="col-md-2 text-left">
            <label><?php echo sprintf(lang('table label rows'), $total); ?></label>
        </div>
        <div class="col-md-2 text-left">
            <?php if ($total > 10) : ?>
                <select id="limit" class="form-control">
                    <option value="10"<?php echo ($limit == 10 OR ($limit != 10 && $limit != 25 && $limit != 50 && $limit != 75 && $limit != 100)) ? ' selected' : ''; ?>>10 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="25"<?php echo ($limit == 25) ? ' selected' : ''; ?>>25 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="50"<?php echo ($limit == 50) ? ' selected' : ''; ?>>50 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="75"<?php echo ($limit == 75) ? ' selected' : ''; ?>>75 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="100"<?php echo ($limit == 100) ? ' selected' : ''; ?>>100 <?php echo lang('admin input items_per_page'); ?></option>
                </select>
            <?php endif; ?>
        </div>
        <div class="col-md-2 text-xs-right">
            
        </div>
        <div class="col-md-6 text-center">
            <?php echo $pagination; ?>
        </div>
    </div>

</div>


<div class="modal  fade" id="customerModal" tabindex="-1" role="dialog" aria-labelledby="modal-label-date" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close hidden-print" data-dismiss="modal" aria-hidden="true">&times;</button>
                <button class="close close-margin hidden-print" onclick="PrintElem('#customerModalContent');"><span class="ion-printer"></span></button>
                <h4 id="customerModalTitle"></h4>
            </div>
            <div class="modal-body"  id="customerModalContent">
		               
            </div>
        </div>
    </div>
</div>
