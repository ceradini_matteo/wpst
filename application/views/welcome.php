<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">	
	<div class="col-sm-12 col-md-6 p-b-1">
		<h4 class="card-header"><?php echo lang('core title info'); ?></h4>
		<div class="lead m-l-1">
			<?php if(!empty($user['firstname']) && !empty($user['lastname'])) { ?>
				<strong><?php echo lang('users title firstname'); ?></strong> <?php echo $user['firstname']; ?>
				<div class="m-t-1"></div>
				<strong><?php echo lang('users title lastname'); ?></strong> <?php echo $user['lastname']; ?>
				<div class="m-t-1"></div>
			<?php } ?>		
			<?php if(!empty($user['end_time'])) { ?>
				<strong><?php echo lang('users title access_ends'); ?></strong> <?php echo date('m/d/Y', strtotime($user['end_time'])); ?>
				<strong><?php echo lang('users title access_ends_on'); ?></strong> <?php echo date('h:i A', strtotime($user['end_time'])); ?>
				<div class="m-t-1"></div>
			<?php } ?>
			<strong><?php echo lang('tests title remaining_tests'); ?></strong> <?php echo $user['remaining_tests']; ?>			
		</div>
	</div>
	<div class="col-sm-12 col-md-6">
		<?php if($user['remaining_tests'] > 0 && isset($test_id)) { ?>
			<button type="button" id="btnDoTest" class="btn btn-lg btn-outline-primary w-100 p-a-3" data-id="<?php echo $test_id ?>" data-user="<?php echo $user['id'] ?>"><?php echo lang('tests button begin_testing'); ?></button>
		<?php } else { ?>
			<em class="lead"><?php echo $welcome_text; ?></em>
		<?php } ?>
	</div>
</div>
