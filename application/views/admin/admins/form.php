<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php echo form_open('', array('role'=>'form')); ?>

    <?php // hidden id ?>
    <?php if (isset($user_id)) : ?>       
        <?php echo form_hidden('id', $user_id); ?>
    <?php endif; ?>

    <div class="row">
        <?php // username ?>
        <div class="form-group col-sm-6<?php echo form_error('username') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('users input username'), 'username', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_input(array('name'=>'username', 'value'=>set_value('username', (isset($user['username']) ? $user['username'] : '')), 'class'=>'form-control')); ?>
        </div>

        <?php // email ?>
        <div class="form-group col-sm-6<?php echo form_error('email') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('users input email'), 'email', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'email', 'value'=>set_value('email', (isset($user['email']) ? $user['email'] : '')), 'class'=>'form-control')); ?>
        </div>
    </div>

    <div class="row">
        <?php // first name ?>
        <div class="form-group col-sm-6<?php echo form_error('firstname') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('users input firstname'), 'firstname', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'firstname', 'value'=>set_value('firstname', (isset($user['firstname']) ? $user['firstname'] : '')), 'class'=>'form-control')); ?>
        </div>

        <?php // last name ?>
        <div class="form-group col-sm-6<?php echo form_error('lastname') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('users input lastname'), 'lastname', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'lastname', 'value'=>set_value('lastname', (isset($user['lastname']) ? $user['lastname'] : '')), 'class'=>'form-control')); ?>
        </div>
    </div>

    <div class="row">
        <?php // group_id ?>
        <div class="form-group col-sm-6<?php echo form_error('group_id') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('users input group_id'), 'group_id', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'group_id', 'value'=>set_value('group_id', (isset($user['group_id']) ? $user['group_id'] : '')), 'class'=>'form-control')); ?>
        </div> 

        <?php // start_time ?>
        <div class="form-group col-sm-3<?php echo form_error('start_time') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('users input start_time'), 'start_time', array('class'=>'control-label')); ?>
              <?php echo form_input(array(
                  'name'=>'start_time', 
                  'id' => 'start_time',
                  'class'=>'form-control datetimepicker', 
                  'placeholder'=>lang('users input start_time'), 
                  'value'=>(isset($user['start_time'])) ? $user['start_time'] : ''
                )
                ); ?>
        </div> 
        <?php // end_time ?>
        <div class="form-group col-sm-3<?php echo form_error('end_time') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('users input end_time'), 'end_time', array('class'=>'control-label')); ?>
            <?php echo form_input(array(
              'name'=>'end_time', 
              'class'=>'form-control datetimepicker', 
              'placeholder'=>lang('users input end_time'), 
              'value'=>(isset($user['end_time'])) ? $user['end_time'] : ''
            )
            ); ?>
        </div>
    </div>
    <div class="row">
        <?php // race ?>
        <div class="form-group col-sm-2<?php echo form_error('race') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('users input race'), 'race', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'race', 'value'=>set_value('race', (isset($user['race']) ? $user['race'] : '')), 'class'=>'form-control')); ?>
        </div>
        <?php // birth_date ?>
        <div class="form-group col-sm-2<?php echo form_error('birth_date') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('users input birth_date'), 'birth_date', array('class'=>'control-label')); ?>
            <?php 
            
            echo form_input(array(
              'name'=>'birth_date', 
              'class'=>'form-control datepicker', 
              'placeholder'=>lang('users input birth_date'), 
              'value'=>(isset($user['birth_date'])) ? $user['birth_date'] : ''
            )
            ); ?>
        </div>
        <?php // gender ?>
        <div class="form-group col-sm-2<?php echo form_error('gender') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('users input gender'), 'birth_day', array('class'=>'control-label')); ?>
            <div class="radio">
                <label>
                    <?php echo form_radio(array('name'=>'gender', 'id'=>'radio-gender-m', 'value'=>'m', 'checked'=> ($user['gender'] == 'm') ? 'checked' : FALSE)); ?>
                    <?php echo lang('core text male'); ?>
                </label>
            </div>
            <div class="radio">
                <label>
                    <?php echo form_radio(array('name'=>'gender', 'id'=>'radio-gender-f', 'value'=>'f', 'checked'=> ($user['gender'] == 'f') ? 'checked' : FALSE)); ?>
                    <?php echo lang('core text female'); ?>
                </label>
            </div>
        </div>    
    </div>

    <?php if (!$password_required) : ?>
        <p class="help-block"><br /><?php echo lang('users help passwords'); ?></p>
    <?php endif; ?>
    <div class="row">
        <?php // password ?>
        <div class="form-group col-sm-6<?php echo form_error('password') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('users input password'), 'password', array('class'=>'control-label')); ?>
            <?php if ($password_required) : ?><span class="required">*</span><?php endif; ?>
            <?php echo form_password(array('name'=>'password', 'class'=>'form-control', 'autocomplete'=>'off')); ?>
        </div>

        <?php // password repeat ?>
        <div class="form-group col-sm-6<?php echo form_error('password_repeat') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('users input password_repeat'), 'password_repeat', array('class'=>'control-label')); ?>
            <?php if ($password_required) : ?><span class="required">*</span><?php endif; ?>
            <?php echo form_password(array('name'=>'password_repeat', 'value'=>'', 'class'=>'form-control', 'autocomplete'=>'off')); ?>
        </div>
    </div>

    <?php // buttons ?>
    <div class="row pull-right">
	    <div class="col-sm-12">
        <a class="btn btn-default" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
        <button type="submit" name="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> <?php echo lang('core button save'); ?></button>
	    </div>
    </div>

<?php echo form_close(); ?>
