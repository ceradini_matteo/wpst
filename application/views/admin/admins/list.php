<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="card">
    <div class="card-header">
        <a href="#" role="button" id="removeUsers" class="pull-xs-right btn btn-sm btn-outline-danger tooltips" title="<?php echo lang('admins tooltip remove_admin') ?>"><span class="ion-trash-b"></span> <?php echo lang('admins button remove_admin'); ?></a>
        <a class="pull-xs-right btn btn-sm btn-outline-primary tooltips" href="<?php echo base_url('admin/admins/form'); ?>" title="<?php echo lang('admins tooltip add_new_admin') ?>"><span class="ion-plus"></span> <?php echo lang('admins button add_new_admin'); ?></a>        
        <?php if ($total) : ?>    
        <button class="pull-xs-right btn btn-sm btn btn-outline-info tooltips" title="<?php echo lang('users tooltip manage_backups'); ?>" data-toggle="modal" data-target="#backupModal">
            <span class="ion-archive"></span> <?php echo lang('admins button manage_backups'); ?>            
        </button>  
        <a href="<?php echo $this_url; ?>/export?sort=<?php echo $sort; ?>&dir=<?php echo $dir; ?><?php echo $filter; ?>" class="pull-xs-right btn btn-sm btn-outline-success tooltips" title="<?php echo lang('admins tooltip csv_export'); ?>"><span class="ion-ios-download-outline"></span> <?php echo lang('admins button csv_export'); ?></a>
        <button class="pull-xs-right btn btn-sm btn-outline-success tooltips" title="<?php echo lang('admins tooltip csv_import'); ?>" data-toggle="modal" data-target="#uploadModal">
            <span class="ion-ios-upload-outline"></span> <?php echo lang('admins button csv_import'); ?>
        </button>       
        <?php endif; ?>
      <h3><?php echo lang('core button list'); ?></h3>
    </div>
        <div class="card-block table-responsive">
        <table class="table table-striped table-hover">
        <thead>
            <?php // sortable headers ?>
            <tr>
                <th width="30"></th>
                <?php echo print_th_list('username',$sort,$dir,$limit,$offset,'users col username',$filter); ?>
                <?php echo print_th_list('password',$sort,$dir,$limit,$offset,'users col password',$filter); ?>
                <?php echo print_th_list('firstname',$sort,$dir,$limit,$offset,'users col firstname',$filter); ?>
                <?php echo print_th_list('lastname',$sort,$dir,$limit,$offset,'users col lastname',$filter); ?>
                <?php echo print_th_list('group_id',$sort,$dir,$limit,$offset,'users col group_id',$filter); ?>
                <?php echo print_th_list('start_time',$sort,$dir,$limit,$offset,'users col start_time',$filter); ?>
                <?php echo print_th_list('end_time',$sort,$dir,$limit,$offset,'users col end_time',$filter); ?>               
                <th class="text-xs-right" width="140">
                    <?php echo lang('admin col actions'); ?>
                </th>
            </tr>

            <?php // search filters ?>
            <tr>
                <?php echo form_open("{$this_url}?sort={$sort}&dir={$dir}&limit={$limit}&offset=0{$filter}", array('role'=>'form', 'id'=>"filters")); ?>
                    <th width="30">
                        <?php 
                            $data = array(
                                    'id'      => 'checkAll',
                                    'checked' => FALSE
                            );

                            echo form_checkbox($data);
                        ?>
                    </th>
                    <th class="<?php echo (($sort == 'username') ? 'table-active' : ''); ?><?php echo ((isset($filters['username'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'username', 'id'=>'username', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('users input username'), 'value'=>set_value('username', ((isset($filters['username'])) ? $filters['username'] : '')))); ?>
                    </th>
                    <th></th>
                    <th class="<?php echo (($sort == 'firstname') ? 'table-active' : ''); ?><?php echo ((isset($filters['firstname'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'firstname', 'id'=>'firstname', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('users input firstname'), 'value'=>set_value('firstname', ((isset($filters['firstname'])) ? $filters['firstname'] : '')))); ?>
                    </th>
                    <th class="<?php echo (($sort == 'lastname') ? 'table-active' : ''); ?><?php echo ((isset($filters['lastname'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'lastname', 'id'=>'username', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('users input lastname'), 'value'=>set_value('lastname', ((isset($filters['lastname'])) ? $filters['lastname'] : '')))); ?>
                    </th>
                    <th class="<?php echo (($sort == 'group_id') ? 'table-active' : ''); ?><?php echo ((isset($filters['group_id'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'group_id', 'id'=>'group_id', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('users input group_id'), 'value'=>set_value('group_id', ((isset($filters['group_id'])) ? $filters['group_id'] : '')))); ?>
                    </th>
                    <th class="<?php echo (($sort == 'start_time') ? 'table-active' : ''); ?><?php echo ((isset($filters['start_time'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array(
                            'name'=>'start_time', 
                            'id' => 'start_time',
                            'class'=>'form-control form-control-sm datetimepicker', 
                            'placeholder'=>lang('users input start_time'), 
                            'value'=>(isset($filters['start_time'])) ? $filters['start_time'] : ''
                            )
                        ); ?>  
                    </th>
                    <th class="<?php echo (($sort == 'end_time') ? 'table-active' : ''); ?><?php echo ((isset($filters['end_time'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array(
                            'name'=>'end_time', 
                            'id' => 'end_time',
                            'class'=>'form-control form-control-sm datetimepicker', 
                            'placeholder'=>lang('users input end_time'), 
                            'value'=>(isset($filters['end_time'])) ? $filters['end_time'] : ''
                            )
                        ); ?> 
                    </th>
                    <th class="text-xs-right">
                        <div class="btn-group" role="group" aria-label="Action">
                            <a href="<?php echo $this_url; ?>" class="btn btn-secondary btn-sm tooltips" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter_reset'); ?>"><span class="ion-backspace-outline"></span></a>
                            <button type="submit" name="submit" value="<?php echo lang('core button filter'); ?>" class="btn btn-sm btn-primary tooltips" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter'); ?>"><span class="ion-funnel"></span> <?php echo lang('core button filter'); ?></button>
                        </div>
                    </th>
                <?php echo form_close(); ?>
            </tr>
        </thead>
        <tbody>
            <?php // data rows ?>
            <?php if ($total) : ?>
                <?php foreach ($users as $user) : ?>
                    <tr>
                        <td width="30">	                        
                            <?php 
                                $data = array(
                                        'name'          => 'selected[]',
                                        'class'         => 'check',
                                        'value'         => $user['id'],
                                        'checked'       => FALSE
                                );

                                echo form_checkbox($data);
                            ?>                            
                        </td>
                        <?php echo print_td_list('username',$sort,$user['username'],$user['id']); ?>
                        <?php echo print_td_list('password',$sort,'********',$user['id'],'password'); ?>
                        <?php echo print_td_list('firstname',$sort,$user['firstname'],$user['id'],'text',TRUE); ?>
                        <?php echo print_td_list('lastname',$sort,$user['lastname'],$user['id'],'text',TRUE); ?>
                        <?php 
                            echo print_td_list('group_id',$sort,$user['group_id'],$user['id'],'text',TRUE);
                        ?>                        
                        <?php 
                            if(!is_null($user['start_time'])){
                                $user['start_time'] = date('m/d/Y h:i A', strtotime($user['start_time']));
                            }
                            else{
                                $user['start_time'] = '';
                            }
                            
                            echo print_td_list('start_time',$sort,$user['start_time'],$user['id'],'datetime',TRUE); 
                        ?>
                        <?php 
                            if(!is_null($user['end_time'])){
                                $user['end_time'] = date('m/d/Y h:i A', strtotime($user['end_time']));
                            }
                            else{
                                $user['end_time'] = '';
                            }

                            echo print_td_list('end_time',$sort,$user['end_time'],$user['id'],'datetime',TRUE); 
                        ?>
                        <td>
                            <div class="text-xs-right">
                                <div class="btn-group">
                                    <?php if ($user['id'] > 1) : ?>
                                        <a class="btn btn-sm btn-secondary btn-delete" data-id="<?php echo $user['id']; ?>" title="<?php echo lang('admin button delete'); ?>"><span class="ion-trash-b"></span></a>
                                    <?php endif; ?>
                                    <a href="<?php echo $this_url; ?>/form/<?php echo $user['id']; ?>" class="btn btn-sm btn-primary" title="<?php echo lang('admin button edit'); ?>"><span class="ion-edit"></span> <?php echo lang('users button edit_user') ?></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else : ?>
                <tr>
                    <td colspan="7">
                        <?php echo lang('core error no_results'); ?>
                    </td>
                </tr>
            <?php endif; ?>
        </tbody>
        </table>
        </div>
    <?php // list tools ?>
    <div class="card-footer">
        <div class="col-md-2 text-left">
            <label><?php echo sprintf(lang('admin label rows'), $total); ?></label>
        </div>
        <div class="col-md-2 text-left">
            <?php if ($total > 10) : ?>
                <select id="limit" class="form-control">
                    <option value="10"<?php echo ($limit == 10 OR ($limit != 10 && $limit != 25 && $limit != 50 && $limit != 75 && $limit != 100)) ? ' selected' : ''; ?>>10 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="25"<?php echo ($limit == 25) ? ' selected' : ''; ?>>25 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="50"<?php echo ($limit == 50) ? ' selected' : ''; ?>>50 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="75"<?php echo ($limit == 75) ? ' selected' : ''; ?>>75 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="100"<?php echo ($limit == 100) ? ' selected' : ''; ?>>100 <?php echo lang('admin input items_per_page'); ?></option>
                </select>
            <?php endif; ?>
        </div>
        <div class="col-md-6">
            <?php echo $pagination; ?>
        </div>
        <div class="col-md-2 text-xs-right">            
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="deleteModalTitle"><?php echo lang('admins title admin_delete');  ?></h4>
            </div>
            <div class="modal-body">
                <p><?php echo sprintf(lang('users msg delete_confirm'), "<span id='modalUsername'></span>"); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                <button type="button" class="btn btn-primary" id="btn-delete-user" data-id=""><?php echo lang('admin button delete'); ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteMultipleModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="deleteModalTitle"><?php echo lang('admins title admin_delete');  ?></h4>
            </div>
            <div class="modal-body">
                <p><?php echo sprintf(lang('admins msg delete_multiple_confirm')); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                <button type="button" class="btn btn-primary" id="btn-delete-users" data-selected=""><?php echo lang('admin button delete'); ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="uploadModalLabel"><?php echo lang('admins title csv_import') ?></h4>
            </div>
            <?php echo form_open_multipart("{$this_url}/import");?>
            <div class="modal-body">
                <input name="userfile" type="file" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo lang('core button close') ?></button>
                <input type="submit" id="upload-csv" class="btn btn-primary" value="<?php echo lang('core button upload') ?>" />
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>  

<div class="modal fade" id="backupModal" tabindex="-1" role="dialog" aria-labelledby="backupModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="backupModalLabel"><?php echo lang('users title manage_backups') ?></h4>
        </div>
        <?php echo form_open("{$this_url}/load_backup") ?>
        <div class="modal-body"> 
            <?php if(count($backups) > 0) { ?>              
                <div class="list-group backupsList">
                <?php 
                    $count = 1;

                    foreach($backups as $file_name => $backup)
                    {
                        if($count <= 10)
                        {
                            echo '<button type="button" class="list-group-item list-group-item-action backups" value="' . $file_name . '">' .  $backup . '</button>';
                        }
                        else
                        {
                            echo '<button type="button" class="list-group-item list-group-item-action backups hide" value="' . $file_name . '">' .  $backup . '</button>';
                        }

                        $count++;
                    }

                    $count--;

                    echo '
                    </div>
                    <div class="m-t-1"></div>
                    <nav aria-label="...">
                        <ul class="pagination pagination-sm">
                            <li id="previous-backup-page" class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                                </a>
                            </li>';

                    $pages = ($count / 10);

                    for($p = 0; $p < $pages; $p++)
                    {
                        if($p == 0){
                            $active = 'active';
                        }
                        else{
                            $active = '';
                        }

                        echo '<li class="page-item ' . $active . ' backup-page" data-page="' . ($p + 1) . '"><a class="page-link" href="#">' . ($p + 1) . '</a></li>';
                    }
                ?>
            
                    <li id="next-backup-page" class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            <?php } else { ?>
                <?php echo lang('core text no_backups'); ?>
            <?php } ?> 
            </nav>        
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo lang('users button close') ?></button>
            <input type="hidden" id="backup-selected" name="backup" value="" />
            <input type="submit" id="upload-csv" class="btn btn-primary" value="<?php echo lang('users button upload') ?>" />
        </div>
        <?php echo form_close(); ?>
    </div>
  </div>
</div>