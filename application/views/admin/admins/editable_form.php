<div class="text-xs-center">
	<div class="msg"></div>

	<?php if($type == 'text') { ?>

	<input type="text" value="<?php echo $value; ?>" class="form-control" />
	
	<?php } else if($type == 'datetime') { ?>	
	 
	<?php 
		echo form_input(array(
		    'name'=>'datetime', 
		    'id' => 'datetime',
		    'class'=>'form-control form-control-sm datetimepicker', 
		    'placeholder'=>lang('users input datetime'), 
		    'value'=>(isset($value)) ? $value : 'n/a')
		); 
	?>
    <script>
		$('.datetimepicker').datetimepicker();
	</script>

	<?php } else if($type == 'select') { ?>
		<?php
			$options = array();
			$options[] = NULL;
			$my_battery_id = 0;

	        foreach($batteries as $battery)
	        {
	            $options[$battery['id']] = $battery['name'];

	            if($battery['name'] == $value){
	            	$my_battery_id = $battery['id'];
	            }
	        }

	        echo form_dropdown('assigned_battery', $options, $my_battery_id, 'class="form-control form-control-sm"');
        ?>

	<?php } else if($type == 'password') { ?>

		<label for="password"><?php echo lang('users input password') ?></label>
		<input type="password" name="password" id="password" class="form-control form-control-sm" />	
		<div class="m-t-1"></div>
		<label for="password"><?php echo lang('users input password_repeat') ?></label>	
		<input type="password" name="password_repeat" id="password_repeat" class="form-control form-control-sm" />

	<?php } ?>

	
	<div class="m-t-1"></div>
	<button class="btn btn-link hide-popover" type="button">cancel</button>
	<button class="btn btn-primary save-editable" type="button" data-type="<?php echo $type; ?>" data-id="<?php echo $id; ?>" data-name="<?php echo $name; ?>" data-optional="<?php echo $optional; ?>">save</button>	
</div>

