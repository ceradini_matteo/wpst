<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="card">
	<div class="card-header">
        <a href="#" role="button" id="removeTests" class="pull-xs-right btn btn-sm btn-outline-danger tooltips" title="<?php echo lang('tests tooltip remove_tests') ?>"><span class="ion-trash-b"></span> <?php echo lang('tests button remove_tests'); ?></a>
        <a class="pull-xs-right btn btn-sm btn-outline-primary tooltips" href="<?php echo base_url('admin/tests/form'); ?>" title="<?php echo lang('tests tooltip add_new_test') ?>"><span class="ion-plus"></span> <?php echo lang('tests button add_new_test'); ?></a>  
        <button class="pull-xs-right btn btn-sm btn btn-outline-info tooltips" title="<?php echo lang('tests tooltip manage_backups'); ?>" data-toggle="modal" data-target="#backupModal">
            <span class="ion-archive"></span> <?php echo lang('tests button manage_backups'); ?>            
        </button>  
        <a href="<?php echo $this_url; ?>/export?sort=<?php echo $sort; ?>&dir=<?php echo $dir; ?><?php echo $filter; ?>" class="pull-xs-right btn btn-sm btn-outline-success tooltips" title="<?php echo lang('tests tooltip csv_export'); ?>"><span class="ion-ios-download-outline"></span> <?php echo lang('tests button csv_export'); ?></a>
        <button class="pull-xs-right btn btn-sm btn-outline-success tooltips" title="<?php echo lang('tests tooltip csv_import'); ?>" data-toggle="modal" data-target="#uploadModal">
            <span class="ion-ios-upload-outline"></span> <?php echo lang('tests button csv_import'); ?>
        </button>         
      <h3><?php echo lang('core button list'); ?></h3>
    </div>
        <div class="card-block table-responsive">
        <table class="table table-striped table-hover">
        <thead>
            <?php // sortable headers ?>
            <tr>
                <th></th>
                <?php echo print_th_list('type',$sort,$dir,$limit,$offset,'tests col type',$filter); ?>
                <?php echo print_th_list('number',$sort,$dir,$limit,$offset,'tests col number',$filter); ?>
                <?php echo print_th_list('slides',$sort,$dir,$limit,$offset,'tests col slides',$filter); ?>
                <?php echo print_th_list('description',$sort,$dir,$limit,$offset,'tests col description',$filter); ?>
                <th class="text-xs-right" width="140">
                    <?php echo lang('admin col actions'); ?>
                </th>
            </tr>

            <?php // search filters ?>
            <tr>
                <?php echo form_open("{$this_url}?sort={$sort}&dir={$dir}&limit={$limit}&offset=0{$filter}", array('role'=>'form', 'id'=>"filters")); ?>
                    <th width="30">                        
                        <?php 
                            $data = array(
                                    'id'      => 'checkAll',
                                    'checked' => FALSE
                            );

                            echo form_checkbox($data);
                        ?>  
                    </th>                    
                    <th width="350" class="<?php echo (($sort == 'type') ? 'table-active' : ''); ?><?php echo ((isset($filters['type'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'type', 'id'=>'type', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('tests input type'), 'value'=>set_value('type', ((isset($filters['type'])) ? $filters['type'] : '')))); ?>
                    </th>
                    <th width="150" class="<?php echo (($sort == 'number') ? 'table-active' : ''); ?><?php echo ((isset($filters['number'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'number', 'id'=>'number', 'type'=>'number', 'min'=>'0', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('tests input number'), 'value'=>set_value('number', ((isset($filters['number'])) ? $filters['number'] : '')))); ?>
                    </th>
                    <th width="150" class="<?php echo (($sort == 'slides') ? 'table-active' : ''); ?><?php echo ((isset($filters['slides'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'slides', 'id'=>'slides', 'type'=>'number', 'min'=>'0', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('tests input slides'), 'value'=>set_value('slides', ((isset($filters['slides'])) ? $filters['slides'] : '')))); ?>
                    </th>
                    <th class="<?php echo (($sort == 'description') ? 'table-active' : ''); ?><?php echo ((isset($filters['description'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'description', 'id'=>'description', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('tests input description'), 'value'=>set_value('description', ((isset($filters['description'])) ? $filters['description'] : '')))); ?>
                    </th>
                    <th class="text-xs-right">
                        <div class="btn-group" role="group" aria-label="Action">
                            <a href="<?php echo $this_url; ?>" class="btn btn-secondary btn-sm tooltips" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter_reset'); ?>"><span class="ion-backspace-outline"></span></a>
                            <button type="submit" name="submit" value="<?php echo lang('core button filter'); ?>" class="btn btn-sm btn-primary tooltips" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter'); ?>"><span class="ion-funnel"></span> <?php echo lang('core button filter'); ?></button>
                        </div>
                    </th>
                <?php echo form_close(); ?>
            </tr>
        </thead>
        <tbody>
            <?php // data rows ?>
            <?php if ($total) : ?>
                <?php foreach ($tests as $test) : ?>
                    <tr>
                        <td>	                        
                            <?php 
                                $data = array(
                                        'name'          => 'selected[]',
                                        'class'         => 'check',
                                        'value'         => $test['id'],
                                        'checked'       => FALSE
                                );

                                echo form_checkbox($data);
                            ?>                            
                        </td>  
                        <?php echo print_td_list('type',$sort,$test['type'],$test['id']); ?>
                        <?php echo print_td_list('number',$sort,$test['number'],$test['id']); ?>
                        <?php echo print_td_list('slides',$sort,$test['slides'],$test['id'],'text',false,true); ?>
                        <?php echo print_td_list('description',$sort,$test['description'],$test['id']); ?>        
                        <td>
                            <div class="text-xs-right">
                                <div class="btn-group">
									<a class="btn btn-sm btn-secondary btn-delete" data-id="<?php echo $test['id']; ?>" title="<?php echo lang('admin button delete'); ?>"><span class="ion-trash-b"></span></a>
                                    <a href="<?php echo $this_url; ?>/form/<?php echo $test['id']; ?>" class="btn btn-sm btn-primary" title="<?php echo lang('admin button edit'); ?>"><span class="ion-edit"></span> <?php echo lang('tests button edit_test') ?></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else : ?>
                <tr>
                    <td colspan="7">
                        <?php echo lang('core error no_results'); ?>
                    </td>
                </tr>
            <?php endif; ?>
        </tbody>
        </table>
        </div>
    <?php // list tools ?>
    <div class="card-footer">
        <div class="col-md-2 text-left">
            <label><?php echo sprintf(lang('admin label rows'), $total); ?></label>
        </div>
        <div class="col-md-2 text-left">
            <?php if ($total > 10) : ?>
                <select id="limit" class="form-control">
                    <option value="10"<?php echo ($limit == 10 OR ($limit != 10 && $limit != 25 && $limit != 50 && $limit != 75 && $limit != 100)) ? ' selected' : ''; ?>>10 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="25"<?php echo ($limit == 25) ? ' selected' : ''; ?>>25 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="50"<?php echo ($limit == 50) ? ' selected' : ''; ?>>50 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="75"<?php echo ($limit == 75) ? ' selected' : ''; ?>>75 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="100"<?php echo ($limit == 100) ? ' selected' : ''; ?>>100 <?php echo lang('admin input items_per_page'); ?></option>
                </select>
            <?php endif; ?>
        </div>
        <div class="col-md-6">
            <?php echo $pagination; ?>
        </div>
        <div class="col-md-2 text-xs-right">            
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="deleteModalTitle"><?php echo lang('tests title test_delete');  ?></h4>
            </div>
            <div class="modal-body">
                <p><?php echo sprintf(lang('tests msg delete_confirm'), "number <span id='modalNumber'></span> type <span id='modalType'></span>"); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                <button type="button" class="btn btn-primary" id="btn-delete-test" data-id=""><?php echo lang('admin button delete'); ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteMultipleModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="deleteModalTitle"><?php echo lang('tests title delete_all_tests');  ?></h4>
            </div>
            <div class="modal-body">
                <p><?php echo sprintf(lang('tests msg delete_multiple_confirm')); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                <button type="button" class="btn btn-primary" id="btn-delete-tests" data-selected=""><?php echo lang('admin button delete'); ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="uploadModalLabel"><?php echo lang('tests title csv_import') ?></h4>
            </div>
            <?php echo form_open_multipart("{$this_url}/import");?>
            <div class="modal-body">
                <input name="userfile" type="file" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo lang('core button close') ?></button>
                <input type="submit" id="upload-csv" class="btn btn-primary" value="<?php echo lang('core button upload') ?>" />
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div> 

<div class="modal fade" id="backupModal" tabindex="-1" role="dialog" aria-labelledby="backupModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="backupModalLabel"><?php echo lang('tests title manage_backups') ?></h4>
        </div>
        <?php echo form_open("{$this_url}/load_backup") ?>
        <div class="modal-body"> 
            <?php if(count($backups) > 0) { ?>           
                <div class="list-group backupsList">
                <?php 
                    $count = 1;

                    foreach($backups as $file_name => $backup)
                    {
                        if($count <= 10)
                        {
                            echo '<button type="button" class="list-group-item list-group-item-action backups" value="' . $file_name . '">' .  $backup . '</button>';
                        }
                        else
                        {
                            echo '<button type="button" class="list-group-item list-group-item-action backups hide" value="' . $file_name . '">' .  $backup . '</button>';
                        }

                        $count++;
                    }

                    $count--;

                    echo '
                    </div>
                    <div class="m-t-1"></div>
                    <nav aria-label="...">
                        <ul class="pagination pagination-sm">
                            <li id="previous-backup-page" class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                                </a>
                            </li>';

                    $pages = ($count / 10);

                    for($p = 0; $p < $pages; $p++)
                    {
                        if($p == 0)
                        {
                            $active = 'active';
                        }
                        else
                        {
                            $active = '';
                        }

                        echo '<li class="page-item ' . $active . ' backup-page" data-page="' . ($p + 1) . '"><a class="page-link" href="#">' . ($p + 1) . '</a></li>';
                    }
                ?>
            
                    <li id="next-backup-page" class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            <?php } else { ?>
                <?php echo lang('core text no_backups'); ?>
            <?php } ?>    
            </nav>        
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo lang('core button close') ?></button>
            <input type="hidden" id="backup-selected" name="backup" value="" />
            <input type="submit" id="upload-csv" class="btn btn-primary" value="<?php echo lang('core button upload') ?>" />
        </div>
        <?php echo form_close(); ?>
    </div>
  </div>
</div>