<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php echo form_open_multipart('', array('role'=>'form','class'=>'test-form')); ?>

    <?php // hidden input ?>
    <?php echo form_hidden('id', $test_id); ?>
    <?php echo form_hidden('folder_name', $folder_name); ?>
    <div class="loader"></div>
    <div class="card">
        <div class="card-block">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="card-header"><?php echo lang('tests title basics_information'); ?></h3>
                    <div class="row">
                        <?php // type ?>
                        <div class="form-group col-sm-3<?php echo form_error('type') ? ' has-error' : ''; ?>">
                            <?php echo form_label(lang('tests input type'), 'type', array('class'=>'control-label')); ?>
                            <span class="required">*</span>
                            <?php echo form_input(array('name'=>'type', 'value'=>set_value('type', (isset($test['type']) ? $test['type'] : '')), 'class'=>'form-control form-control-sm test-type', 'required'=>'')); ?>
                        </div>
                        <?php // number ?>
                        <div class="form-group col-sm-3<?php echo form_error('number') ? ' has-error' : ''; ?>">
                            <?php echo form_label(lang('tests input number'), 'number', array('class'=>'control-label')); ?>
                            <span class="required">*</span>
                            <?php echo form_input(array('type'=>'number', 'min'=>'0', 'name'=>'number', 'value'=>set_value('number', (isset($test['number']) ? $test['number'] : '')), 'class'=>'form-control form-control-sm test-number', 'required'=>'')); ?>
                        </div>
                        <?php // description ?>
                        <div class="form-group col-sm-6<?php echo form_error('slides') ? ' has-error' : ''; ?>">
                            <?php echo form_label(lang('tests input description'), 'description', array('class'=>'control-label')); ?>
                            <?php echo form_textarea(array('name'=>'description', 'value'=>set_value('description', (isset($test['description']) ? $test['description'] : ''), false), 'class'=>'form-control form-control-sm','rows'=>'4')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <?php // left key ?>
                        <div class="form-group col-sm-3<?php echo form_error('type') ? ' has-error' : ''; ?>">
                            <?php echo form_label(lang('tests input left_key'), 'left_key', array('class'=>'control-label')); ?>
                            <?php echo form_input(array('name'=>'left_key', 'value'=>set_value('type', isset($test['left_key']) ? $test['left_key'] : 'w'), 'class'=>'form-control form-control-sm test-left-key')); ?>
                        </div>
                        <?php // right key ?>
                        <div class="form-group col-sm-3<?php echo form_error('type') ? ' has-error' : ''; ?>">
                            <?php echo form_label(lang('tests input right_key'), 'right_key', array('class'=>'control-label')); ?>
                            <?php echo form_input(array('name'=>'right_key', 'value'=>set_value('type', isset($test['right_key']) ? $test['right_key'] : 'o'), 'class'=>'form-control form-control-sm test-left-key')); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <h3 class="card-header"><?php echo lang('tests title randomize_range'); ?></h3>
                    <div class="randomize-range">
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="#" role="button" class="btn btn-sm btn-outline-primary prevent" id="add-range" title="<?php echo lang('tests tooltip add_range') ?>"><span class="ion-plus"></span> <?php echo lang('tests button add_range'); ?></a>
                            </div>
                        </div>
                        <div class="m-t-1"></div> 
                        <?php 
                            if(isset($test['randomize_range']) && !empty($test['randomize_range'])) 
                            { 
                                foreach($test['randomize_range'] as $from => $to)
                                {
                                    $range = array(
                                        'from' => $from,
                                        'to'   => $to
                                    );

                                    echo $this->load->view('admin/tests/form/range', $range, TRUE); 
                                }
                            } 
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-block"> 
            <h3 class="card-header">   
                <?php echo lang('tests title slides'); ?>  
                <a href="#" role="button" id="delete-slides" class="pull-xs-right btn btn-sm btn btn-outline-danger tooltips" title="<?php echo lang('tests tooltip remove_all_slides'); ?>" data-toggle="modal" data-target="#deleteMultipleModal">
                    <span class="ion-trash-b"></span> <?php echo lang('tests button delete_all_slides'); ?>            
                </a>
                <input type="number" class="pull-xs-right form-control form-control-sm number-add-slide" min="1" step="1" value="1" placeholder="<?php echo lang('tests input add_slide_number') ?>" />   
                <button type="button" class="pull-xs-right btn btn-sm btn-outline-primary tooltips" id="add-slide" title="<?php echo lang('tests tooltip add_slide') ?>">
                    <span class="ion-plus"></span> <?php echo lang('tests button add_slide'); ?> 
                </button> 
                <a href="#" role="button" class="pull-xs-right btn btn-sm btn btn-outline-info tooltips" title="<?php echo lang('tests tooltip manage_backups'); ?>" data-toggle="modal" data-target="#backupModal">
                    <span class="ion-archive"></span> <?php echo lang('tests button manage_backups'); ?>            
                </a>  
                <?php if(isset($test_id) && isset($slides) && count($slides) > 0) { ?>
                    <a href="<?php echo $this_url; ?>/export_slides/<?php echo $test_id; ?>" role="button" class="pull-xs-right btn btn-sm btn-outline-success tooltips prevent" title="<?php echo lang('tests tooltip csv_export_slides'); ?>"><span class="ion-ios-download-outline"></span> <?php echo lang('tests button csv_export_slides'); ?></a>
                <?php } ?>               
                <a href="#" role="button" class="pull-xs-right btn btn-sm btn-outline-success tooltips" title="<?php echo lang('tests tooltip csv_import_slides'); ?>" data-toggle="modal" data-target="#uploadModal">
                    <span class="ion-ios-upload-outline"></span> <?php echo lang('tests button csv_import_slides'); ?>
                </a>
                <a href="#" role="button" class="pull-xs-right btn btn-sm btn-outline-success tooltips" title="<?php echo lang('tests tooltip file_manager') ?>"data-toggle="modal" data-target="#filemanagerModal">
                    <span class="ion-ios-upload-outline"></span> <?php echo lang('tests button file_manager'); ?>
                </a>
                <button type="button" class="pull-xs-right btn btn-sm btn-link tooltips" id="reloadSlidesFiles" title="<?php echo lang('tests tooltip reload_slide_files') ?>"><span class="ion-ios-refresh-outline"></span> <?php echo lang('tests button reload_slide_files'); ?>
                </button> 
            </h3>          
            <div class="slides"> 
                <div class="m-t-1"></div>
                <?php 
                    if(isset($slides)) 
                    {
                        $index = 0;

                        foreach($slides as $slide)
                        {
                            $slide['index']          = $index;
                            $slide['exists']         = TRUE; 
                            $slide['number']         = floatval($slide['number']);
                            $slide['folder_name']    = $folder_name;
                            
                            echo $this->load->view('admin/tests/form/slide', $slide, TRUE); 
                            $index++;
                        } 
                    } 
                ?>          
            </div>            
            <h3 class="card-header second-card<?php echo (isset($slides) && !empty($slides)) ? '' : ' hide'; ?>">   
                <a href="#" role="button" id="delete-slides" class="pull-xs-right btn btn-sm btn btn-outline-danger tooltips" title="<?php echo lang('tests tooltip manage_backups'); ?>" data-toggle="modal" data-target="#deleteMultipleModal">
                    <span class="ion-trash-b"></span> <?php echo lang('tests button delete_all_slides'); ?>            
                </a>  
                <input type="number" class="pull-xs-right form-control form-control-sm number-add-slide" min="1" step="1" value="1" placeholder="<?php echo lang('tests input add_slide_number') ?>" />            
                <button type="button" class="pull-xs-right btn btn-sm btn-outline-primary tooltips" id="add-slide" title="<?php echo lang('tests tooltip add_slide') ?>"><span class="ion-plus"></span> <?php echo lang('tests button add_slide'); ?></button> 
                <a href="#" role="button" class="pull-xs-right btn btn-sm btn btn-outline-info tooltips" title="<?php echo lang('tests tooltip manage_backups'); ?>" data-toggle="modal" data-target="#backupModal">
                    <span class="ion-archive"></span> <?php echo lang('tests button manage_backups'); ?>            
                </a>  
                <?php if(isset($test_id) && isset($slides) && count($slides) > 0) { ?>
                    <a href="<?php echo $this_url; ?>/export_slides/<?php echo $test_id; ?>" role="button" class="pull-xs-right btn btn-sm btn-outline-success tooltips prevent" title="<?php echo lang('tests tooltip csv_export_slides'); ?>"><span class="ion-ios-download-outline"></span> <?php echo lang('tests button csv_export_slides'); ?></a>
                <?php } ?>               
                <a href="#" role="button" class="pull-xs-right btn btn-sm btn-outline-success tooltips" title="<?php echo lang('tests tooltip csv_import_slides'); ?>" data-toggle="modal" data-target="#uploadModal">
                    <span class="ion-ios-upload-outline"></span> <?php echo lang('tests button csv_import_slides'); ?>
                </a> 
                <a href="#" role="button" class="pull-xs-right btn btn-sm btn-outline-success tooltips" title="<?php echo lang('tests tooltip file_manager') ?>"data-toggle="modal" data-target="#filemanagerModal">
                    <span class="ion-ios-upload-outline"></span> <?php echo lang('tests button file_manager'); ?>
                </a>
                <button type="button" class="pull-xs-right btn btn-sm btn-link tooltips" id="reloadSlidesFiles" title="<?php echo lang('tests tooltip reload_slide_files') ?>"><span class="ion-ios-refresh-outline"></span> <?php echo lang('tests button reload_slide_files'); ?>
                </button> 
            </h3>
        </div>
    </div>    
    <div class="m-t-1"></div>
    <?php // buttons ?>
    <div class="row pull-right">
        <div class="col-sm-12">
        <a class="btn btn-default" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
        <button type="button" name="submit" class="btn btn-primary btn-save"><span class="glyphicon glyphicon-save"></span> <?php echo lang('core button save'); ?></button>
        </div>
    </div>

<?php echo form_close(); ?>

<div class="modal fade" id="deleteMultipleModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="deleteModalTitle"><?php echo lang('tests title delete_all_slides'); ?></h4>
            </div>
            <div class="modal-body">
                <p><?php echo sprintf(lang('tests msg delete_all_slides')); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                <button type="button" class="btn btn-primary" id="btn-delete-slides" data-selected=""><?php echo lang('admin button delete'); ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="uploadModalLabel"><?php echo lang('tests title csv_import_slides') ?></h4>
            </div>
            <?php echo form_open_multipart("{$this_url}/import_slides/");?>
            <div class="modal-body">
                <input name="userfile" type="file" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo lang('core button close') ?></button>
                <input type="button" id="upload-csv" class="btn btn-primary" value="<?php echo lang('core button upload') ?>" />
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div> 
<div class="modal fade" id="filemanagerModal" tabindex="-1" role="dialog" aria-labelledby="filemanagerModal" aria-hidden="true">
<div class="modal-dialog  modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="filemanagerModal">Filemanager</h4>
    </div>
    <div class="modal-body">
      <iframe id="fileManagerIFrame" width="100%" height="600"style="overflow:hidden;width:100%;overflow: scroll; overflow-x: hidden; overflow-y: scroll;" src="/filemanager/dialog.php?fldr=<?php echo $folder_name ?>" frameborder="0"></iframe>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>

<div class="modal fade" id="backupModal" tabindex="-1" role="dialog" aria-labelledby="backupModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="backupModalLabel"><?php echo lang('tests title manage_backups') ?></h4>
        </div>
        <?php echo form_open("{$this_url}/load_backup_slides/$test_id") ?>
        <div class="modal-body">          
            <?php if(count($backups) > 0) { ?>
                <div class="list-group backupsList">   
                <?php                      
                    $count = 1;

                    foreach($backups as $file_name => $backup)
                    {
                        if($count <= 10)
                        {
                            echo '<button type="button" class="list-group-item list-group-item-action backups" value="' . $file_name . '">' .  $backup . '</button>';
                        }
                        else
                        {
                            echo '<button type="button" class="list-group-item list-group-item-action backups hide" value="' . $file_name . '">' .  $backup . '</button>';
                        }

                        $count++;
                    }

                    $count--;

                    echo '
                    </div>
                    <div class="m-t-1"></div>
                    <nav aria-label="...">
                        <ul class="pagination pagination-sm">
                            <li id="previous-backup-page" class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                                </a>
                            </li>';

                    $pages = ($count / 10);

                    for($p = 0; $p < $pages; $p++)
                    {
                        if($p == 0)
                        {
                            $active = 'active';
                        }
                        else
                        {
                            $active = '';
                        }

                        echo '<li class="page-item ' . $active . ' backup-page" data-page="' . ($p + 1) . '"><a class="page-link" href="#">' . ($p + 1) . '</a></li>';
                    }
                ?>                
                    <li id="next-backup-page" class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            <?php } else { ?>
                <?php echo lang('core text no_backups'); ?>
            <?php } ?>       
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo lang('core button close') ?></button>
            <input type="hidden" id="backup-selected" name="backup" value="" />
            <input type="submit" id="upload-csv" class="btn btn-primary" value="<?php echo lang('core button upload') ?>" />
        </div>
        <?php echo form_close(); ?>
    </div>
  </div>
</div>

<div class="modal fade" id="testInformationModal" tabindex="-1" role="dialog" aria-labelledby="testInformationModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="testInformationModal">Basics information</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col-sm-12<?php echo form_error('type') ? ' has-error' : ''; ?>">
                    <?php echo form_label(lang('tests input type'), 'type', array('class'=>'control-label')); ?>
                    <?php echo form_input(array('name'=>'type', 'value'=>'', 'class'=>'form-control test-basic-type', 'required'=>'')); ?>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-12<?php echo form_error('number') ? ' has-error' : ''; ?>">
                    <?php echo form_label(lang('tests input number'), 'number', array('class'=>'control-label')); ?>
                    <?php echo form_input(array('type'=>'number', 'min'=>'0', 'name'=>'number', 'value'=>'', 'class'=>'form-control test-basic-number', 'required'=>'')); ?>
                </div>
            </div>            
        </div>
        <div class="modal-footer">
            <input type="submit" id="save-basic-test" class="btn btn-primary disabled" value="<?php echo lang('core button continue') ?>" />
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>