<?php
	if($type == "images")
    {
        $options = array(
	   		'1'  => '1',
	   		'20' => '20'
	   	);	
    }
    else if($type == "text")
    {
    	$options = array(
	   		'1' => '1',
	   		'4' => '4',
	   		'5' => '5'
	   	);
    }
    else
    {
    	$options = array(
	   		'1' => '1'
	   	);
    }
    
    echo form_dropdown('stimuli_numbers[' . $index. ']', $options, (isset($number) ? $number : ''), 'class="form-control form-control-sm stimuli-number"');
?>