<?php 
	if(!is_null($this->input->post('index'))){
        $index = $this->input->post('index');
    }  
    
    echo '<span class="trigger-time">';
    echo form_input(array('type'=>'number', 'min'=>'0', 'step'=>'0.5', 'name'=>'trigger[' . $index. '][]', 'value'=>set_value('trigger[' . $index. '][]', isset($value) ? $value : ''), 'placeholder'=>lang('tests input trigger_time_value'), 'class'=>'form-control form-control-sm trigger-value')); 

    echo form_checkbox('timebar[' . $index. '][]', '1', isset($timebar_enabled) && !$timebar_enabled ? FALSE : TRUE);
    echo '<span class="small">' . lang('tests input timebar_enabled') . '</span>';
    echo '</span>';
?>