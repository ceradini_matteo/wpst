<?php
    if(!is_null($this->input->post('index'))){
        $index = $this->input->post('index');
    }
?>

<?php if(isset($type) && $type != 'text-box') { ?>
<select class="form-control form-control-sm selectpicker stimuli-answer" name="stimuli_correct[<?php echo $index; ?>][]" data-selected-text-format="count" multiple>
    <?php
        $options = array();

        // if number possible question is 1 only type is left or right otherwise answer will be a number 1,2,3 ... number
        if($number == '1')
        {
            $options['left']  = 'left';
            $options['right'] = 'right';
        }
        else
        {
            for($i = 1; $i <= $number; $i++){
                $options[$i] = $i;
            }
        }

        $options['none'] = 'none';

        foreach($options as $key => $option)
        {
            $selected = '';

            if(isset($answer))
            {
                foreach($answer as $value)
                {
                    if($option == $value)
                    {
                        $selected = ' selected';
                        break;
                    }
                }
            }

            echo '<option value="' . $key . '"' . $selected . '>' . $option . '</option>';
        }

    ?>
</select>

<?php } ?>


