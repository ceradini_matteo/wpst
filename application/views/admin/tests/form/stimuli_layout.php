<?php 
$options = array();   
   	if($type == 'images')
    {
        switch($number)
        {
            case '1': $options['IAT'] = 'IAT'; break;
            case '20': $options['20_image_spread'] = '20_image_spread'; break;
        }
    }
    else if($type == 'text')
    {
        switch($number)
        {
            case '1': $options['IAT'] = 'IAT'; break;
            case '4': $options['MC_4_Q'] = 'MC_4_Q'; break;
            case '5': $options['MC_5_Q'] = 'MC_5_Q'; break;
        }
    }
    else if($type == 'text-box')
    {
        switch($number)
        {
            case '1': $options['TB_1'] = 'TB_1'; break;
        }
    }
    else if($type == 'instructions')
    {
        switch($number)
        {
            case '1': $options['Instructions'] = 'Instructions'; break;
        }
    }          

    echo form_dropdown('stimuli_layouts[' . $index. ']', $options, (isset($layout) ? $layout : ''), 'class="form-control form-control-sm stimuli-layout"');
?> 