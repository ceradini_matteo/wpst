<?php
    if(!is_null($this->input->post('index'))){
        $index = $this->input->post('index');
    }    
?>
<div class="trigger">
    <div class="row">
        <div class="col-sm-12 col-md-5">
            <?php
                $options = array(
                    ''      => '',
                    'key'   => 'Key',
                    'mouse' => 'Mouse',
                    'time'  => 'Time'
                );       
                echo form_dropdown('trigger[' . $index. '][]', $options, (isset($trigger) ? $trigger : ''), 'class="form-control form-control-sm select-trigger"');
            ?>            
        </div>
        <div class="col-sm-12 col-md-5 trigger-time-container">
            <?php
                if(isset($trigger) && $trigger == 'time')
                {
                    $data = array(
                        'index'           => $index,
                        'value'           => $value,
                        'timebar_enabled' => $timebar_enabled ? TRUE : FALSE                       
                    );

                    echo $this->load->view('admin/tests/form/trigger_time', $data, TRUE);
                }
            ?>
        </div>
        <div class="col-sm-2 col-md-2">
            <button type="button" class="btn btn-sm btn-outline-danger pull-xs-right delete-trigger" title="<?php echo lang('tests tooltip delete_trigger') ?>"><span class="ion-close-round"></span></button>
        </div>
    </div> 
</div>