<?php
	if(!isset($folder_name)){
		$folder_name = $this->input->post('folder_name');
	}
?>
<tr class="stimuli-upload">
	<th>
		<strong class="file-number" data-number="<?php echo $count ?>"><?php echo $count ?>.</strong>
	</th>
	<td>
		<select name="slide_files[<?php echo $index ?>][]" class="form-control form-control-sm stimuli-files-added selectpicker" data-live-search='true' data-size='20'>
			<?php
				if($type == 'images')
				{
					$path = './uploads/slides_thumbs/' . $folder_name . '/images';
				}
				else
				{
					$path = './uploads/slides_files/' . $folder_name . '/' . $type;
				}				

				$files = scandir($path);

				foreach($files as $file_name)
				{
					if($file_name != '.' && $file_name != '..')
					{
						if($file_selected == $file_name)
						{
							$select = ' selected="selected" ';
						}
						else
						{
							$select  = '';
						}

						if($type == 'images')
						{
							$content = "<img src='/uploads/slides_thumbs/" . $folder_name . '/images/' . $file_name . "' height='35'/> &nbsp;" . $file_name;
						}
						else
						{
							$content = $file_name;
						}

						echo '<option value="' . $file_name . '"' . $select . ' data-content="' . $content . '"></option>';
					}
				}
			?>			
		</select>
	</td>
	<td>
		<?php
			$accept = '';

			switch($type)
			{
				case 'images': $accept = 'image/*'; break;
				default: $accept = 'media_type'; break;
			}
		?>
	    <input class="stimuli-files-uploads" name='stimuli_uploads[<?php echo $index ?>][]' type="file" accept="<?php echo $accept ?>" />
	</td>
</tr>