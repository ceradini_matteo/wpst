
<?php
    if(!is_null($this->input->post('index'))){
        $index = $this->input->post('index');
        $folder_name = $this->input->post('folder_name');
    }   
?>
<div class="slide p-a-1" data-index="<?php echo $index; ?>">
    <div class="row">
        <div class="col-md-2 col-xl-1">
            <h4>Slide <span class="slide-number"><?php echo isset($number) ? $number : ''; ?></span></h4>
        </div> 
        <div class="col-md-10 col-xl-11">
            <button type="button"  class="btn btn-sm btn-outline-danger delete-slide prevent" title="<?php echo lang('tests tooltip delete_slide') ?>"><span class="ion-close-round"></span> <?php echo lang('tests button delete_slide') ?></button>
        </div>       
    </div>
    <div class="row">
        <?php // slide_number ?>
        <div class="form-group col-md-6 col-xl-1">
            <?php echo form_label(lang('tests input slide_number'), 'slide_number', array('class'=>'control-label')); ?> <span class="required">*</span>
            <?php echo form_input(array('type'=>'number', 'min'=>'1', 'step'=>'0.1', 'name'=>'slide_number[]', 'value'=>set_value('slide_number[]', isset($number) ? $number : ''), 'class'=>'form-control form-control-sm slide-input-number', 'required'=>'')); ?>
        </div>                    
        <?php // slide_name ?>
        <div class="form-group col-md-6 col-xl-2">
            <?php echo form_label(lang('tests input slide_name'), 'slide_name', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'slide_name[]', 'value'=>set_value('slide_name[]', (isset($name) ? $name : ''), false), 'class'=>'form-control form-control-sm ')); ?>
        </div>
        <?php // top_text ?>
        <div class="form-group col-md-6 col-xl-3">
            <?php echo form_label(lang('tests input top_text'), 'top_text', array('class'=>'control-label')); ?>
            <?php echo form_textarea(array('name'=>'top_text[]', 'value'=>set_value('top_text[]', (isset($top_text) ? $top_text : ''), false), 'class'=>'form-control form-control-sm','rows'=>'2')); ?>
        </div>
        <?php // left_label ?>
        <div class="form-group col-md-6 col-xl-3">
            <?php echo form_label(lang('tests input left_label'), 'left_label', array('class'=>'control-label')); ?>
            <?php echo form_textarea(array('name'=>'left_label[]', 'value'=>set_value('left_label[]', (isset($left_label) ? $left_label : ''), false), 'class'=>'form-control form-control-sm','rows'=>'2')); ?>
        </div>
        <?php // right_label ?>
        <div class="form-group col-md-6 col-xl-3">
            <?php echo form_label(lang('tests input right_label'), 'right_label', array('class'=>'control-label')); ?>
            <?php echo form_textarea(array('name'=>'right_label[]', 'value'=>set_value('right_label[]', (isset($right_label) ? $right_label : ''), false), 'class'=>'form-control form-control-sm','rows'=>'2')); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div class="lead"><strong>Triggers</strong></div>
            <div class="triggers">
                <div class="row">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-sm btn-outline-primary" id="add-trigger" title="<?php echo lang('tests tooltip add_trigger') ?>"><span class="ion-plus"></span> <?php echo lang('tests button add_trigger'); ?></button>                     
                    </div>
                    <div class="m-t-1"></div>                 
                </div>                
                <?php   
                    if(isset($trigger))
                    {
                        $triggers = json_decode($trigger);
                    }
                    else
                    {
                        $triggers = array('0'=>null);
                    }

                    if(!empty($triggers[0])) 
                    {  
                        foreach($triggers as $key => $t)
                        {
                            $data = false;

                            if($t == 'time')
                            {
                                $data = array(
                                    'trigger'         => $t,
                                    'value'           => $triggers[$key + 1],
                                    'timebar_enabled' => $timebar_enabled,
                                    'index'           => $index
                                );
                            }
                            else
                            {
                                if($t == 'key' || $t == 'mouse')
                                {
                                    $data = array(
                                        'trigger' => $t,
                                        'index'   => $index
                                    );
                                }
                            }

                            if($data){ 
                                echo $this->load->view('admin/tests/form/trigger', $data, TRUE);    
                            } 
                        }
                    }
                    else
                    {
                        $data = array(
                            'trigger' => '',
                            'value'   => '',
                            'index'   => $index
                        );
                        echo $this->load->view('admin/tests/form/trigger', $data, TRUE);   
                    } 
                ?>
            </div>
        </div>
        <div class="col-sm-9">
            <div class="lead"><strong>Stimuli</strong></div>
            <div class="stimulis">
                <?php  
                    if(isset($slides))
                    {
                        if(isset($slides[$id]))
                        {
                            $slide = $slides[$id];
                        }
                        else
                        {
                            $slide = $slides[0];
                        } 

                        $data = array(
                            'type'        => $slide['stimuli_type'],
                            'number'      => $slide['stimuli_number'],
                            'layout'      => $slide['stimuli_layout'],
                            'answer'      => json_decode($slide['answer']),
                            'slide_id'    => $slide['id'],
                            'slide_files' => $slide_files,
                            'folder_name' => $folder_name
                        );
                    }
                    else
                    {
                        $data = array(
                            'folder_name' => $folder_name
                        );
                    }  
                    
                    echo $this->load->view('admin/tests/form/stimuli', $data, TRUE);   
                ?> 
            </div>
        </div>
    </div> 
    <?php echo form_input(array('type'=>'hidden', 'name'=>'index_slide[]', 'value'=>set_value('index_slide[]', (isset($index) ? $index : '')), 'class'=>'slide-index')); ?>
    <?php echo form_input(array('type'=>'hidden', 'name'=>'slide_id[]', 'value'=>set_value('slide_id[]', (isset($id) ? $id : '')))); ?>
    <?php echo form_input(array('type'=>'hidden', 'name'=>'slide_exists[]', 'value'=>set_value('slide_exists[]', (isset($exists) && $exists ? TRUE : FALSE)))); //this indicate if slide already exist in database or not?>
</div> 