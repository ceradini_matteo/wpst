<div class="range">
    <div class="row">
        <div class="form-group col-md-12 col-lg-5">
            <?php echo form_input(array('type'=>'number', 'min'=>'0', 'name'=>'range-from[]', 'value'=>set_value('range_from[]', isset($from) ? $from : ''), 'class'=>'form-control form-control-sm range-from', 'placeholder' => lang('core text from'), 'required'=>'')); ?> 
        </div>
        <div class="form-group col-md-12 col-lg-5">
            <?php echo form_input(array('type'=>'number', 'min'=>'0', 'name'=>'range-to[]', 'value'=>set_value('range_to[]', isset($to) ? $to : ''), 'class'=>'form-control form-control-sm range-to', 'placeholder' => lang('core text to'), 'required'=>'')); ?>
        </div>
        <div class="col-md-12 col-lg-1">
            <button type="button" class="btn btn-sm btn-outline-danger delete-range prevent" title="<?php echo lang('tests tooltip delete_range') ?>"><span class="ion-close-round"></span></button>
        </div>
    </div> 
</div>