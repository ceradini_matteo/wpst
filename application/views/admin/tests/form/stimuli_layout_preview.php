<div class="row">
    <div class="col-sm-12">
        <?php echo lang('tests title stimuli_layout_preview'); ?>
        <div class="m-t-1"></div>
        <img class="img-fluid stimuli-img" src="/uploads/stimulis_layout/<?php echo $type ?>/<?php echo $layout ?>.png" width="450" />
    </div>
</div>