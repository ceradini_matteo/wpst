<?php
    if(!is_null($this->input->post('index'))){
        $index = $this->input->post('index');
    }
?>
<div class="stimuli">
    <div class="row">
        <div class="col-sm-8">
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th><?php echo form_label(lang('tests input stimuli_type'), 'stimuli_type', array('class'=>'control-label')); ?><span class="required">*</span></th>
                        <th><?php echo form_label(lang('tests input stimuli_number'), 'stimuli_number', array('class'=>'control-label')); ?><span class="required">*</span></th>
                        <th><?php echo form_label(lang('tests input stimuli_layout'), 'stimuli_layout', array('class'=>'control-label')); ?><span class="required">*</span></th>
                        <th><?php echo form_label(lang('tests input stimuli_correct'), 'stimuli_correct', array('class'=>'control-label')); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="stimuli-types">
                            <?php
                                $options = array(
                                    ''             => null,
                                    'images'       => 'Images',
                                    'instructions' => 'Instructions',
                                    'text'         => 'Text',
                                    'text-box'     => 'Text-Box'
                                );

                                echo form_dropdown('stimuli_types[' . $index. ']', $options, (isset($type) ? $type : ''), array('class'=>'form-control form-control-sm stimuli-type'));
                            ?>
                        </td>
                        <td class="stimuli-numbers">
                            <?php
                                if(isset($type) && $type != "" && isset($number))
                                {
                                    $data = array(
                                        'type' => $type
                                    );

                                    echo $this->load->view('admin/tests/form/stimuli_number', $data, TRUE);
                                }
                            ?>
                        </td>
                        <td class="stimuli-layouts">
                            <?php
                                if(isset($type) && $type != ""  && isset($number) && isset($layout))
                                {
                                    $data = array(
                                        'index'  => $index,
                                        'type'   => $type,
                                        'number' => $number
                                    );

                                    echo $this->load->view('admin/tests/form/stimuli_layout', $data, TRUE);
                                }
                            ?>
                        </td>
                        <td class="stimuli-answers">
                            <?php
                                if(isset($type) && $type != ""  && isset($number) && isset($layout))
                                {
                                    $data = array(
                                        'index'  => $index,
                                        'type'   => $type,
                                        'number' => $number,
                                        'answer' => $answer
                                    ); 

                                    echo $this->load->view('admin/tests/form/stimuli_answer', $data, TRUE);
                                }
                            ?>
                        </td>
                    </tr>
                    <!-- show only for text box -->
                    <tr class="stimuli-filters <?php echo isset($type) && $type == 'text-box' ? '' : 'hidden'; ?>">
                        <td colspan="4">
                            <?php
                                $data = array(
                                    'index'           => $index,
                                    'stimuli_filters' => isset($stimuli_filters) ? $stimuli_filters : NULL
                                );

                                echo $this->load->view('admin/tests/form/stimuli_filters', $data, true);
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="m-t-1"></div>
            <hr />
            <?php if(isset($type) && $type != "" && $type != 'text-box') { ?>
            <table class="table table-sm table-hover table-striped table-bordered stimuli-upload-table">
                <thead>
                    <tr>
                        <th><?php echo form_label(lang('tests input stimuli_count'), 'stimuli_type', array('class'=>'control-label')); ?><span class="required">*</span></th>
                        <th><?php echo form_label(lang('tests input stimuli_files'), 'stimuli_files', array('class'=>'control-label')); ?> <span class="required">*</span></th>
                        <th><?php echo form_label(lang('tests input stimuli_file_upload'), 'stimuli_file_upload', array('class'=>'control-label')); ?><span class="required">*</span></th>
                    </tr>
                </thead>
                <tbody class="stimuli-uploads" data-count="0">
                    <?php  
                    if(isset($slide_files))
                    {             
                        $conta = 1;
                        $files_added = array();
                        $selected = '';

                        foreach($slide_files as $slide_file)
                        {     
                            $data = array(
                                'index'         => $index,
                                'file_selected' => $slide_file['file_name'],
                                'count'         => $conta,
                                'type'          => $type,
                                'folder_name'   => $folder_name
                            );

                            $conta++;

                            echo $this->load->view('admin/tests/form/stimuli_upload', $data, TRUE); 
                        }

                        if($conta == 1 || $conta < $number)
                        {
                            while($conta <= $number)
                            {
                                $data = array(
                                    'index'         => $index,
                                    'file_selected' => '',
                                    'count'         => $conta,
                                    'type'          => $type,
                                    'folder_name'   => $folder_name
                                );

                                $conta++;

                                echo $this->load->view('admin/tests/form/stimuli_upload', $data, TRUE); 
                            }
                        }
                    }
                ?>
                </tbody>
            </table>
            <?php } else { ?>
            <table class="table table-sm table-hover table-striped table-bordered stimuli-upload-table hide">
                <thead>
                    <tr>
                        <th><?php echo form_label(lang('tests input stimuli_count'), 'stimuli_type', array('class'=>'control-label')); ?><span class="required">*</span></th>
                        <th><?php echo form_label(lang('tests input stimuli_files'), 'stimuli_files', array('class'=>'control-label')); ?> <span class="required">*</span></th>
                        <th><?php echo form_label(lang('tests input stimuli_file_upload'), 'stimuli_file_upload', array('class'=>'control-label')); ?><span class="required">*</span></th>
                    </tr>
                </thead>
                <tbody class="stimuli-uploads" data-count="0">
                </tbody>
            </table>
            <?php } ?>  
        </div> 
        <div class="col-sm-4 stimuli-layout-preview">
            <?php
                if(isset($type) && $type != ""  && isset($number) && isset($layout))
                {    
                    $data = array(
                        'layout' => $layout,
                        'type'   => $type
                    );

                    echo $this->load->view('admin/tests/form/stimuli_layout_preview', $data, TRUE);  
                }
            ?>
        </div>   
    </div> 
</div>