<?php
    if(!is_null($this->input->post('index'))){
        $index = $this->input->post('index');
    }

    $length = NULL;
    $type   = NULL;

    if(isset($stimuli_filters) && $stimuli_filters) {
    	$stimuli_filters = json_decode($stimuli_filters, TRUE);

    	$length = isset($stimuli_filters['length']) ? $stimuli_filters['length'] : NULL;
    	$type   = isset($stimuli_filters['type']) ? $stimuli_filters['type'] : NULL;
    }
?>
<?php echo lang('tests input stimuli_filter_length'); ?> &nbsp;
<input type="number" min="0" class="stimuli-filter" name="stimuli_filters_length[<?php echo $index; ?>]" value="<?php echo $length ? $length : NULL; ?>" />

<?php
	$options = array(
		''             => lang('tests input stimuli_filter_none'),
		'alphanumeric' => lang('tests input stimuli_filter_alphanumeric'),
		'numeric'      => lang('tests input stimuli_filter_numeric'),
		'date'         => lang('tests input stimuli_filter_date')
	);

	echo form_dropdown('stimuli_filters_type[' . $index. ']', $options, $type);
?>