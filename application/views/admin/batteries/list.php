<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="card">
	<div class="card-header">
        <a href="#" role="button" id="removeBatteries" class="pull-xs-right btn btn-sm btn-outline-danger tooltips" title="<?php echo lang('batteries tooltip remove_batteries') ?>"><span class="ion-trash-b"></span> <?php echo lang('batteries button remove_batteries'); ?></a>
        <a class="pull-xs-right btn btn-sm btn-outline-primary tooltips" href="<?php echo base_url('admin/batteries/form'); ?>" title="<?php echo lang('batteries tooltip add_new_battery') ?>"><span class="ion-plus"></span> <?php echo lang('batteries button add_new_battery'); ?></a>    
      <h3><?php echo lang('core button list'); ?></h3>
    </div>
        <div class="card-block table-responsive">
        <table class="table table-striped table-hover">
        <thead>
            <?php // sortable headers ?>
            <tr>
                <th></th>
                <?php echo print_th_list('name',$sort,$dir,$limit,$offset,'batteries col name',$filter); ?>
                <?php echo print_th_list('presentation',$sort,$dir,$limit,$offset,'batteries col presentation',$filter); ?>
                <?php echo print_th_list('mturk',$sort,$dir,$limit,$offset,'batteries col mturk',$filter); ?>
                <?php echo print_th_list('mturk_attempts',$sort,$dir,$limit,$offset,'batteries col mturk_attempts',$filter); ?>
                <?php echo print_th_list('number_tests',$sort,$dir,$limit,$offset,'batteries col number_tests',$filter); ?>
                <?php echo print_th_list('tests',$sort,$dir,$limit,$offset,'batteries col tests',$filter); ?>
                <th class="text-xs-right" width="140">
                    <?php echo lang('admin col actions'); ?>
                </th>
            </tr>

            <?php // search filters ?>
            <tr>
                <?php echo form_open("{$this_url}?sort={$sort}&dir={$dir}&limit={$limit}&offset=0{$filter}", array('role'=>'form', 'id'=>"filters")); ?>
                    <th width="30">                        
                        <?php 
                            $data = array(
                                    'id'      => 'checkAll',
                                    'checked' => FALSE
                            );

                            echo form_checkbox($data);
                        ?>  
                    </th>                    
                    <th width="350" class="<?php echo (($sort == 'name') ? 'table-active' : ''); ?><?php echo ((isset($filters['name'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'name', 'id'=>'name', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('batteries input name'), 'value'=>set_value('name', ((isset($filters['name'])) ? $filters['name'] : '')))); ?>
                    </th>
                    <th width="200" class="<?php echo (($sort == 'presentation') ? 'table-active' : ''); ?><?php echo ((isset($filters['presentation'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'presentation', 'id'=>'presentation', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('batteries input presentation'), 'value'=>set_value('presentation', ((isset($filters['presentation'])) ? $filters['presentation'] : '')))); ?>
                    </th>
                    <th width="200">
                    </th>
                    <th></th>
                    <th width="200" class="<?php echo (($sort == 'number_tests') ? 'table-active' : ''); ?><?php echo ((isset($filters['number_tests'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'number_tests', 'id'=>'number_tests', 'type'=>'number', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('batteries input number_tests'), 'value'=>set_value('number_tests', ((isset($filters['number_tests'])) ? $filters['number_tests'] : '')))); ?>
                    </th>
                    <th></th>
                    <th class="text-xs-right">
                        <div class="btn-group" role="group" aria-label="Action">
                            <a href="<?php echo $this_url; ?>" class="btn btn-secondary btn-sm tooltips" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter_reset'); ?>"><span class="ion-backspace-outline"></span></a>
                            <button type="submit" name="submit" value="<?php echo lang('core button filter'); ?>" class="btn btn-sm btn-primary tooltips" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter'); ?>"><span class="ion-funnel"></span> <?php echo lang('core button filter'); ?></button>
                        </div>
                    </th>
                <?php echo form_close(); ?>
            </tr>
        </thead>
        <tbody>
            <?php // data rows ?>
            <?php if ($total) : ?>
                <?php foreach ($batteries as $battery) : ?>
                    <tr>
                        <td>	                        
                            <?php 
                                $data = array(
                                        'name'          => 'selected[]',
                                        'class'         => 'check',
                                        'value'         => $battery['id'],
                                        'checked'       => FALSE
                                );

                                echo form_checkbox($data);
                            ?>                            
                        </td>  
                        <?php echo print_td_list('name',$sort,$battery['name'],$battery['id']); ?>                        
                        <?php echo print_td_list('presentation',$sort,$battery['presentation'],$battery['id']); ?>
                        <?php 
                            $mturk = 'NO';

                            if($battery['mturk'] == '1'){
                                $mturk = 'YES ';
                                $mturk .= '(' . $battery['mturk_link'] . ')';
                            }
                            
                            echo print_td_list('mturk',$sort,$mturk,$battery['id']); 
                        ?>                        
                        <?php 
                            $mturk_attempts = '';

                            if($battery['mturk'] == '1'){
                                $mturk_attempts = $battery['mturk_attempts'];
                            }    

                            echo print_td_list('mturk_attempts',$sort,$mturk_attempts,$battery['id'],'text',false,true);
                        ?>
                        <?php echo print_td_list('number_tests',$sort,$battery['number_tests'],$battery['id'],'text',false,true); ?>
                        <?php echo print_td_list('tests',$sort,$battery['tests'],$battery['id']); ?>
                        <td>
                            <div class="text-xs-right">
                                <div class="btn-group">
									<a class="btn btn-sm btn-secondary btn-delete" data-id="<?php echo $battery['id']; ?>" title="<?php echo lang('admin button delete'); ?>"><span class="ion-trash-b"></span></a>
                                    <a href="<?php echo $this_url; ?>/form/<?php echo $battery['id']; ?>" class="btn btn-sm btn-primary" title="<?php echo lang('admin button edit'); ?>"><span class="ion-edit"></span> <?php echo lang('batteries button edit_battery') ?></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else : ?>
                <tr>
                    <td colspan="7">
                        <?php echo lang('core error no_results'); ?>
                    </td>
                </tr>
            <?php endif; ?>
        </tbody>
        </table>
        </div>
    <?php // list tools ?>
    <div class="card-footer">
        <div class="col-md-2 text-left">
            <label><?php echo sprintf(lang('admin label rows'), $total); ?></label>
        </div>
        <div class="col-md-2 text-left">
            <?php if ($total > 10) : ?>
                <select id="limit" class="form-control">
                    <option value="10"<?php echo ($limit == 10 OR ($limit != 10 && $limit != 25 && $limit != 50 && $limit != 75 && $limit != 100)) ? ' selected' : ''; ?>>10 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="25"<?php echo ($limit == 25) ? ' selected' : ''; ?>>25 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="50"<?php echo ($limit == 50) ? ' selected' : ''; ?>>50 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="75"<?php echo ($limit == 75) ? ' selected' : ''; ?>>75 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="100"<?php echo ($limit == 100) ? ' selected' : ''; ?>>100 <?php echo lang('admin input items_per_page'); ?></option>
                </select>
            <?php endif; ?>
        </div>
        <div class="col-md-6">
            <?php echo $pagination; ?>
        </div>
        <div class="col-md-2 text-xs-right">            
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="deleteModalTitle"><?php echo lang('batteries title battery_delete');  ?></h4>
            </div>
            <div class="modal-body">
                <p><?php echo sprintf(lang('batteries msg delete_confirm'), "<span id='modalBattery'></span>"); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                <button type="button" class="btn btn-primary" id="btn-delete-battery" data-id=""><?php echo lang('admin button delete'); ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteMultipleModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="deleteModalTitle"><?php echo lang('batteries title batteries_delete');  ?></h4>
            </div>
            <div class="modal-body">
                <p><?php echo sprintf(lang('batteries msg delete_multiple_confirm')); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                <button type="button" class="btn btn-primary" id="btn-delete-batteries" data-selected=""><?php echo lang('admin button delete'); ?></button>
            </div>
        </div>
    </div>
</div>
