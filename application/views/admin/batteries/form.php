<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php echo form_open_multipart('', array('role'=>'form')); ?>    
    <div class="card">
        <div class="card-block">
            <h3 class="card-header"><?php echo lang('batteries title basics_information'); ?></h3>
            <div class="row">                
                <?php // name ?>
                <div class="form-group col-md-4<?php echo form_error('name') ? ' has-error' : ''; ?>">
                    <?php echo form_label(lang('batteries input name'), 'name', array('class'=>'control-label')); ?>
                    <span class="required">*</span>
                    <?php echo form_input(array('name'=>'name', 'value'=>set_value('name', (isset($battery['name']) ? $battery['name'] : '')), 'class'=>'form-control')); ?>
                </div>
                <?php // presentation ?>
                <div class="form-group col-xs-6 col-md-2<?php echo form_error('presentation') ? ' has-error' : ''; ?>">
                    <?php echo form_label(lang('batteries input presentation'), 'presentation', array('class'=>'control-label')); ?>
                    <span class="required">*</span>
                    <div class="radio">
                        <label>
                            <?php echo form_radio(array('name'=>'presentation', 'id'=>'radio-presentation-shown', 'value'=>'as shown', 'checked'=> ($battery['presentation'] == 'as shown') ? 'checked' : FALSE)); ?>
                            <?php echo lang('batteries input as_shown'); ?>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <?php echo form_radio(array('name'=>'presentation', 'id'=>'radio-presentation-random', 'value'=>'random', 'checked'=> ($battery['presentation'] == 'random') ? 'checked' : FALSE)); ?>
                            <?php echo lang('batteries input random'); ?>
                        </label>
                    </div>
                </div>
                <?php // mturk ?>
                <div class="form-group col-xs-6 col-md-2<?php echo form_error('mturk') ? ' has-error' : ''; ?>">
                    <?php echo form_label(lang('batteries input mturk'), 'mturk', array('class'=>'control-label')); ?>
                    <span class="required">*</span>
                    <div class="radio">
                        <label>
                            <?php echo form_radio(array('name'=>'mturk', 'id'=>'radio-mturk-shown', 'value'=>'1', 'checked'=> ($battery['mturk'] == '1') ? 'checked' : FALSE)); ?>
                            <?php echo lang('batteries input mturk_enable'); ?>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <?php echo form_radio(array('name'=>'mturk', 'id'=>'radio-mturk-random', 'value'=>'0', 'checked'=> ($battery['mturk'] == '0') ? 'checked' : FALSE)); ?>
                            <?php echo lang('batteries input mturk_disable'); ?>
                        </label>
                    </div>
                </div>
                <?php // name ?>
                <div class="form-group col-xs-12 col-md-3<?php echo form_error('mturk_link') ? ' has-error' : ''; ?>">
                    <?php echo form_label(lang('batteries input mturk_link'), 'mturk_link', array('class'=>'control-label')); ?>
                    <?php echo form_input(array('name'=>'mturk_link', 'value'=>set_value('mturk_link', (isset($battery['mturk_link']) ? $battery['mturk_link'] : '')), 'class'=>'form-control')); ?>
                </div>

            </div>
        </div>
        <div class="card-block">
            <h3 class="card-header"> <?php echo lang('batteries title tests'); ?></h3>           
            <div class="tests"> 
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo form_label(lang('batteries input tests_already_added'), 'name', array('class'=>'control-label')); ?>
                        <?php if(isset($tests) && !empty($tests)) {  ?> 
                        <ul id="tests_added" class="droptrue">
                            <?php
                                if(isset($tests_added))
                                {
                                    foreach($tests_added as $test)
                                    {
                                        $test_string = $test['type'] . '_' . $test['number'];
                                        $test_details = 'Type:    ' . $test['type'] . '<br />Number:    ' . $test['number'] . '<br />Slides:    ' . $test['slides'] . '<br />Description:    ' . $test['description'];
                                        
                                        echo '<li class="ui-state-default item" data-id="' . $test['id'] . '"><a class="get-info" title="' . $test_details . '">
                                                <i class="ion-ios-information"></i></a>' . $test_string . '</li>';  
                                    }
                                }
                            ?>
                        </ul>
                        <?php } ?>
                    </div>
                    <div class="col-sm-6">
                        <?php echo form_label(lang('batteries input tests'), 'name', array('class'=>'control-label')); ?>   
                        <?php if(isset($tests) && !empty($tests)) {  ?> 
                        <ul id="tests" class="droptrue">
                            <?php 
                                if(isset($tests))
                                {
                                    foreach($tests as $test)
                                    {
                                        if(!isset($tests_added[$test['id']]))
                                        {
                                            $test_string = $test['type'] . '_' . $test['number'];

                                            $test_details = 'Type:    ' . $test['type'] . '<br />Number:    ' . $test['number'] . '<br />Slides:    ' . $test['slides'] . '<br />Description:    ' . $test['description'];

                                            echo '<li class="ui-state-default item" data-id="' . $test['id'] . '"><a class="get-info" title="' . $test_details . '">
                                                    <i class="ion-ios-information"></i></a>' . $test_string . '</li>';  
                                        }                                        
                                    }                                  
                                }                            
                            ?>
                        </ul> 
                        <?php } else{ echo '<br />' .  lang('batteries text no_tests_available'); } ?>  
                    </div>                                 
                </div> 
            </div>
        </div>
    </div>    
    <div class="m-t-1"></div>
    <?php // buttons ?>
    <div class="row pull-right">
        <div class="col-sm-12">
        <a class="btn btn-default" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
        <button type="submit" name="submit" id="btnSave" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> <?php echo lang('core button save'); ?></button>
        </div>
    </div>
    <?php // hidden id ?>
    <?php if (isset($battery_id)) : ?>       
        <?php echo form_hidden('id', $battery_id); ?>        
    <?php endif; ?>     
    <?php echo form_hidden('tests', ''); ?>      
<?php echo form_close(); ?>

<div class="modal fade" id="deleteMultipleModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="deleteModalTitle"><?php echo lang('batteries title delete_all_slides'); ?></h4>
            </div>
            <div class="modal-body">
                <p><?php echo sprintf(lang('batteries msg delete_all_slides')); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                <button type="button" class="btn btn-primary" id="btn-delete-slides" data-selected=""><?php echo lang('admin button delete'); ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="deleteModalTitle"><?php echo lang('batteries title test_details'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-3"><strong>Type:</strong></div>
                    <div class="col-sm-9"><span id="type"></span></div>
                </div>
                <div class="m-t-1"></div>
                <div class="row">
                    <div class="col-sm-3"><strong>Number:</strong></div>
                    <div class="col-sm-9"><span id="number"></span></div>
                </div>
                <div class="m-t-1"></div>
                <div class="row">
                    <div class="col-sm-3"><strong>Slides:</strong></div>
                    <div class="col-sm-9"><span id="slides"></span></div>
                </div>
                <div class="m-t-1"></div>
                <div class="row">
                    <div class="col-sm-3"><strong>Description:</strong></div>
                    <div class="col-sm-9"><span id="description"></span></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo lang('core button close'); ?></button>
            </div>
        </div>
    </div>
</div>