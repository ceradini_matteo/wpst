<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="card">
    <div class="card-header">
        <a href="#" role="button" id="removeTests" class="pull-xs-right btn btn-sm btn-outline-danger tooltips" title="<?php echo lang('tests tooltip delete_tests_result') ?>"><span class="ion-trash-b"></span> <?php echo lang('tests button delete_tests_result'); ?></a>
        <h3><?php echo lang('core button list'); ?></h3>
    </div>
    <div class="card-block table-responsive">
        <?php if (!$no_test_results) { ?>
            <table class="table table-striped table-hover">
                <thead>
                    <?php // sortable headers 
                    ?>
                    <tr>
                        <th></th>
                        <?php echo print_th_list('type', $sort, $dir, $limit, $offset, 'tests col type', $filter); ?>
                        <?php echo print_th_list('number', $sort, $dir, $limit, $offset, 'tests col number', $filter); ?>
                        <?php echo print_th_list('slides', $sort, $dir, $limit, $offset, 'tests col slides', $filter); ?>
                        <?php echo print_th_list('description', $sort, $dir, $limit, $offset, 'tests col description', $filter); ?>
                        <?php echo print_th_list('attempts', NULL, $dir, $limit, $offset, 'tests col attempts', $filter); ?>
                        <?php echo print_th_list('tests_unread', NULL, $dir, $limit, $offset, 'tests col tests_unread', $filter); ?>
                        <th class="text-xs-right" width="140">
                            <?php echo lang('admin col actions'); ?>
                        </th>
                    </tr>

                    <?php // search filters 
                    ?>
                    <tr>
                        <?php echo form_open("{$this_url}?sort={$sort}&dir={$dir}&limit={$limit}&offset=0{$filter}", array('role' => 'form', 'id' => "filters")); ?>
                        <th width="30">
                            <?php
                            $data = array(
                                'id'      => 'checkAll',
                                'checked' => FALSE
                            );

                            echo form_checkbox($data);
                            ?>
                        </th>
                        <th class="<?php echo (($sort == 'type') ? 'table-active' : ''); ?><?php echo ((isset($filters['type'])) ? ' has-success' : ''); ?>">
                            <?php echo form_input(array('name' => 'type', 'id' => 'type', 'class' => 'form-control form-control-sm', 'placeholder' => lang('tests input type'), 'value' => set_value('type', ((isset($filters['type'])) ? $filters['type'] : '')))); ?>
                        </th>
                        <th class="<?php echo (($sort == 'number') ? 'table-active' : ''); ?><?php echo ((isset($filters['number'])) ? ' has-success' : ''); ?>">
                            <?php echo form_input(array('name' => 'number', 'id' => 'number', 'type' => 'number', 'min' => '0', 'class' => 'form-control form-control-sm', 'placeholder' => lang('tests input number'), 'value' => set_value('number', ((isset($filters['number'])) ? $filters['number'] : '')))); ?>
                        </th>
                        <th class="<?php echo (($sort == 'slides') ? 'table-active' : ''); ?><?php echo ((isset($filters['slides'])) ? ' has-success' : ''); ?>">
                            <?php echo form_input(array('name' => 'slides', 'id' => 'slides', 'type' => 'number', 'min' => '0', 'class' => 'form-control form-control-sm', 'placeholder' => lang('tests input slides'), 'value' => set_value('slides', ((isset($filters['slides'])) ? $filters['slides'] : '')))); ?>
                        </th>
                        <th class="<?php echo (($sort == 'description') ? 'table-active' : ''); ?><?php echo ((isset($filters['description'])) ? ' has-success' : ''); ?>">
                            <?php echo form_input(array('name' => 'description', 'id' => 'description', 'class' => 'form-control form-control-sm', 'placeholder' => lang('tests input description'), 'value' => set_value('description', ((isset($filters['description'])) ? $filters['description'] : '')))); ?>
                        </th>
                        <th></th>
                        <th></th>
                        <th class="text-xs-right">
                            <div class="btn-group" role="group" aria-label="Action">
                                <a href="<?php echo $this_url; ?>" class="btn btn-secondary btn-sm tooltips" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter_reset'); ?>"><span class="ion-backspace-outline"></span></a>
                                <button type="submit" name="submit" value="<?php echo lang('core button filter'); ?>" class="btn btn-sm btn-primary tooltips" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter'); ?>"><span class="ion-funnel"></span> <?php echo lang('core button filter'); ?></button>
                            </div>
                        </th>
                        <?php echo form_close(); ?>
                    </tr>
                </thead>
                <tbody>
                    <?php // data rows 
                    ?>
                    <?php if ($total) : ?>
                        <?php foreach ($tests as $test) : ?>
                            <?php if ($test['attempts'] > 0) { ?>
                                <tr>
                                    <td>
                                        <?php
                                        $data = array(
                                            'name'          => 'selected[]',
                                            'class'         => 'check',
                                            'value'         => $test['id'],
                                            'checked'       => FALSE
                                        );

                                        echo form_checkbox($data);
                                        ?>
                                    </td>
                                    <?php echo print_td_list('type', $sort, $test['type'], $test['id']); ?>
                                    <?php echo print_td_list('number', $sort, $test['number'], $test['id']); ?>
                                    <?php echo print_td_list('slides', $sort, $test['slides'], $test['id'], 'text', false, true); ?>
                                    <?php echo print_td_list('description', $sort, $test['description'], $test['id']); ?>
                                    <?php echo print_td_list('attempts', $sort, $test['attempts'], $test['id']); ?>
                                    <?php if ($test['tests_unread'] == 0) : ?>
                                        <td>NO</td>
                                    <?php else : ?>
                                        <th>
                                            <h5><span class="badge badge-danger">YES</span></h5>
                                        </th>
                                    <?php endif; ?>
                                    <td width="500">
                                        <div class="text-xs-right">
                                            <div class="btn-group">
                                                <a href="#" role="button" class="btn btn-sm btn-outline-danger btn-delete" data-id="<?php echo $test['id']; ?>" title="<?php echo lang('admin button delete'); ?>"><span class="ion-trash-b"></span></a>
                                                <a class="btn btn-sm btn-outline-primary" href="<?php echo $this_url; ?>/export_test_result/<?php echo $test['id']; ?>" class="btn btn-sm btn-primary" title="<?php echo lang('admin button save_test_result'); ?>"><span class="ion-ios-download-outline"></span> &nbsp;<?php echo lang('tests button save_test_result') ?></a>
                                                <a class="btn btn-sm btn-outline-primary" href="<?php echo $this_url; ?>/export_test_result_slides/<?php echo $test['id']; ?>" class="btn btn-sm btn-primary" title="<?php echo lang('admin button save_test_result'); ?>"><span class="ion-ios-download-outline"></span> &nbsp;<?php echo lang('tests button save_test_result_slides') ?></a>
                                                <a class="btn btn-sm btn-outline-primary" href="<?php echo $this_url; ?>/export_test_result_slides_big/<?php echo $test['id']; ?>" class="btn btn-sm btn-primary" title="Export data as a whole file in the website folder"><span class="ion-ios-download-outline"></span> &nbsp;Export data (BIG)</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="7">
                                <?php echo lang('core error no_results'); ?>
                            </td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        <?php } ?>
    </div>
    <?php // list tools 
    ?>
    <div class="card-footer">
        <div class="col-md-2 text-left">
            <label><?php echo sprintf(lang('admin label rows'), $total); ?></label>
        </div>
        <div class="col-md-2 text-left">
            <?php if ($total > 10) : ?>
                <select id="limit" class="form-control">
                    <option value="10" <?php echo ($limit == 10 or ($limit != 10 && $limit != 25 && $limit != 50 && $limit != 75 && $limit != 100)) ? ' selected' : ''; ?>>10 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="25" <?php echo ($limit == 25) ? ' selected' : ''; ?>>25 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="50" <?php echo ($limit == 50) ? ' selected' : ''; ?>>50 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="75" <?php echo ($limit == 75) ? ' selected' : ''; ?>>75 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="100" <?php echo ($limit == 100) ? ' selected' : ''; ?>>100 <?php echo lang('admin input items_per_page'); ?></option>
                </select>
            <?php endif; ?>
        </div>
        <div class="col-md-6">
            <?php echo $pagination; ?>
        </div>
        <div class="col-md-2 text-xs-right">
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="deleteModalTitle"><?php echo lang('tests title test_delete'); ?></h4>
            </div>
            <div class="modal-body">
                <p><?php echo sprintf(lang('tests msg delete_confirm')); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                <button type="button" class="btn btn-primary" id="btn-delete-test-result" data-id=""><?php echo lang('admin button delete'); ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteMultipleModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="deleteModalTitle"><?php echo lang('tests title test_delete');  ?></h4>
            </div>
            <div class="modal-body">
                <p><?php echo sprintf(lang('tests msg delete_multiple_confirm')); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                <button type="button" class="btn btn-primary" id="btn-delete-tests" data-selected=""><?php echo lang('admin button delete'); ?></button>
            </div>
        </div>
    </div>
</div>