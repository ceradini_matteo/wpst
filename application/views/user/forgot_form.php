<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php echo form_open('', array('role'=>'form')); ?>

		<div class="row">
			<div class="col-sm-6 offset-sm-3">
				<p>
					<?php echo lang('users text forgot'); ?>
				</p>
			</div>
		</div>
    <div class="row">
        <?php // email ?>
        <div class="col-sm-6 offset-sm-3<?php echo form_error('email') ? ' has-error' : ''; ?>">
	        <div class="form-group">
            <?php echo form_label(lang('users input email_or_username'), 'email', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_input(array('name'=>'email', 'value'=>set_value('email', (isset($user['email']) ? $user['email'] : '')), 'class'=>'form-control')); ?>
	        </div>
	        <div class="form-group text-center">
		        <a class="btn btn-default" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
						<button type="submit" name="submit" class="btn btn-primary"><span class="ion-checkmark"></span> <?php echo lang('users button reset_password'); ?></button>
	        </div>
        </div>
    </div>

<?php echo form_close(); ?>
