<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php setlocale(LC_TIME, 'it_IT.UTF-8'); ?>

<div class="well">
	<?php echo form_open('', array('role'=>'form','id'=>'profileForm','data-toggle'=>'validator')); ?>
	    <div class="row">
	        <?php // password ?>
	        <div class="form-group col-sm-6 <?php echo form_error('password') ? ' has-error' : ''; ?>">
	            <?php echo form_label(lang('users input password'), 'password', array('class'=>'control-label')); ?>
	            <span class="required">*</span>
	            <?php echo form_password(array('name'=>'password', 'value'=>'', 'class'=>'form-control', 'id'=>'password', 'autocomplete'=>'off')); ?>
	        </div>

	        <?php // password repeat ?>
	        <div class="form-group col-sm-6 <?php echo form_error('password_repeat') ? ' has-error' : ''; ?>">
	            <?php echo form_label(lang('users input password_repeat'), 'password_repeat', array('class'=>'control-label')); ?>
	           	<span class="required">*</span>
	            <?php echo form_password(array('name'=>'password_repeat', 'value'=>'', 'class'=>'form-control', 'autocomplete'=>'off','data-match'=>'#password')); ?>
	        </div>
	    </div>
	    <?php // buttons ?>
	    <div class="row">
		    <div class="col-sm-12 text-sm-right">
	            <button type="submit" name="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> <?php echo lang('core button save'); ?></button>
		    </div>
	    </div>
	    <input type="hidden" name="reset_code" value="<?php echo $reset_code; ?>" />
	<?php echo form_close(); ?>
</div>