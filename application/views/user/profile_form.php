<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php setlocale(LC_TIME, 'it_IT.UTF-8'); ?>

<div class="well">
	<?php echo form_open('', array('role'=>'form','id'=>'profileForm','data-toggle'=>'validator')); ?>
		<?php // buttons ?>
        <?php if ( ! $password_required) : ?>
            <p class="help-block"><br /><?php echo lang('users help passwords'); ?></p>
        <?php endif; ?>
	    <div class="row">
	        <?php // password ?>
	        <div class="form-group col-sm-6 <?php echo form_error('password') ? ' has-error' : ''; ?>">
	            <?php echo form_label(lang('users input password'), 'password', array('class'=>'control-label')); ?>
	            <?php if ($password_required) : ?><span class="required">*</span><?php endif; ?>
	            <?php echo form_password(array('name'=>'password', 'value'=>'', 'class'=>'form-control', 'id'=>'password', 'autocomplete'=>'off')); ?>
	        </div>

	        <?php // password repeat ?>
	        <div class="form-group col-sm-6 <?php echo form_error('password_repeat') ? ' has-error' : ''; ?>">
	            <?php echo form_label(lang('users input password_repeat'), 'password_repeat', array('class'=>'control-label')); ?>
	            <?php if ($password_required) : ?><span class="required">*</span><?php endif; ?>
	            <?php echo form_password(array('name'=>'password_repeat', 'value'=>'', 'class'=>'form-control', 'autocomplete'=>'off','data-match'=>'#password')); ?>
	        </div>
	    </div>
	    <?php // buttons ?>
	    <div class="row">
		    <div class="col-sm-12 text-sm-right">
	        <?php if ($this->session->userdata('logged_in')) : ?>
	            <button type="submit" name="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> <?php echo lang('core button save'); ?></button>
	        <?php else : ?>
	            <button type="submit" name="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> <?php echo lang('users button register'); ?></button>
	        <a class="btn btn-default" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>

	        <?php endif; ?>
		    </div>
	    </div>
	<?php echo form_close(); ?>
</div>