<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="login-container">
<div class="card card-container text-center">
  <img class="img-fluid" src="/assets/images/logo2.png" />
  <?php echo form_open('', array('class'=>'form-signin')); ?>
	  <fieldset class="form-group">
      <?php echo form_input(array('name'=>'username', 'id'=>'username', 'class'=>'form-control', 'placeholder'=>lang('users input username_email'))); ?>
	  </fieldset>
	  <fieldset class="form-group">
    <?php echo form_password(array('name'=>'password', 'id'=>'password', 'class'=>'form-control', 'placeholder'=>lang('users input password'), 'autocomplete'=>'off')); ?>
	  </fieldset>
    <?php echo form_submit(array('name'=>'submit', 'class'=>'btn btn-lg btn-outline-primary btn-block'), lang('core button login')); ?>
  <?php echo form_close(); ?>
	<p class="text-xs-center"><br /><a href="<?php echo base_url('user/forgot'); ?>"><?php echo lang('users link forgot_password'); ?></a></p>
</div><!-- /card-container -->
</div>

<?php echo $welcome_message; ?>
