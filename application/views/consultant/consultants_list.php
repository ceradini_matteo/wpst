<div class="card">
    <div class="card-header">
	     <a class="pull-xs-right btn btn-sm btn-outline-primary tooltips" href="<?php echo base_url('consultant/form'); ?>" title="<?php echo lang('consultants tooltip add_new_consultant'); ?>" data-toggle="tooltip"><span class="ion-plus"></span> <?php echo lang('consultants button add_new_consultant'); ?></a>
      <h3><?php echo lang('core button list'); ?></h3>
    </div>
		<div class="card-block no-padding">
    	<table class="table table-striped table-hover">
        <thead>

            <?php // sortable headers ?>
            <tr>
                <?php echo print_th_list('company',$sort,$dir,$limit,$offset,'consultants col company',$filter); ?>
                <?php echo print_th_list('firstname',$sort,$dir,$limit,$offset,'consultants col firstname',$filter); ?>
                <?php echo print_th_list('lastname',$sort,$dir,$limit,$offset,'consultants col lastname',$filter); ?>
                <th class="text-xs-right" width="110">
                    <?php echo lang('consultants col actions'); ?>
                </th>
            </tr>

            <?php // search filters ?>
            <tr>
                <?php echo form_open("{$this_url}?sort={$sort}&dir={$dir}&limit={$limit}&offset=0{$filter}", array('role'=>'form', 'id'=>"filters")); ?>
                    <th class="<?php echo (($sort == 'email') ? 'table-active' : ''); ?><?php echo ((isset($filters['email'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'company', 'id'=>'company', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('consultants input company'), 'value'=>set_value('company', ((isset($filters['company'])) ? $filters['company'] : '')))); ?>
                    </th>
                    <th class="<?php echo (($sort == 'firstname') ? 'table-active' : ''); ?><?php echo ((isset($filters['firstname'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'firstname', 'id'=>'firstname', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('consultants input firstname'), 'value'=>set_value('firstname', ((isset($filters['firstname'])) ? $filters['firstname'] : '')))); ?>
                    </th>
                    <th class="<?php echo (($sort == 'lastname') ? 'table-active' : ''); ?><?php echo ((isset($filters['lastname'])) ? ' has-success' : ''); ?>">
                        <?php echo form_input(array('name'=>'lastname', 'id'=>'email', 'class'=>'form-control form-control-sm', 'placeholder'=>lang('consultants input lastname'), 'value'=>set_value('lastname', ((isset($filters['lastname'])) ? $filters['lastname'] : '')))); ?>
                    </th>
                    <th class="text-xs-right">
	                    <div class="btn-group" role="group" aria-label="Action">
                            <a href="<?php echo $this_url; ?>" class="btn btn-secondary btn-sm tooltips" data-toggle="tooltip" data-placement="left" title="<?php echo lang('consultants tooltip filter_reset'); ?>"><span class="ion-backspace-outline"></span></a>
                            <button type="submit" name="submit" value="<?php echo lang('core button filter'); ?>" class="btn btn-sm btn-primary tooltips" data-toggle="tooltip" data-placement="left" title="<?php echo lang('consultants tooltip filter'); ?>"><span class="ion-funnel"></span></button>
                        </div>
                    </th>
                <?php echo form_close(); ?>
            </tr>

        </thead>
        <tbody>

            <?php // data rows ?>
            <?php if ($total) : ?>
                <?php foreach ($consultants as $consultant) : ?>
                    <tr id="row_<?php echo $consultant['id']; ?>">
	                    	<?php echo print_td_list('company',$sort,$consultant['company']); ?>
	                    	<?php echo print_td_list('firstname',$sort,$consultant['firstname']); ?>
	                    	<?php echo print_td_list('lastname',$sort,$consultant['lastname']); ?>
		                    	<td>
                            <div class="text-xs-right">
                                <div class="btn-group">
                                  <a class="btn btn-sm btn-secondary btn-delete-consultant" data-id="<?php echo $consultant['id']; ?>" title="<?php echo lang('common button delete'); ?>"><span class="ion-trash-b"></span></a>
                                  <a href="<?php echo $this_url; ?>/form/<?php echo $consultant['id']; ?>" class="btn btn-sm btn-primary" title="<?php echo lang('common button edit'); ?>"><span class="ion-edit"></span></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else : ?>
                <tr>
                    <td colspan="4">
                        <?php echo lang('core error no_results'); ?>
                    </td>
                </tr>
            <?php endif; ?>

        </tbody>
    	</table>
		</div>
    <?php // list tools ?>
    <div class="card-footer">
        <div class="col-md-2 text-left">
            <label><?php echo sprintf(lang('table label rows'), $total); ?></label>
        </div>
        <div class="col-md-2 text-left">
            <?php if ($total > 10) : ?>
                <select id="limit" class="form-control">
                    <option value="10"<?php echo ($limit == 10 OR ($limit != 10 && $limit != 25 && $limit != 50 && $limit != 75 && $limit != 100)) ? ' selected' : ''; ?>>10 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="25"<?php echo ($limit == 25) ? ' selected' : ''; ?>>25 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="50"<?php echo ($limit == 50) ? ' selected' : ''; ?>>50 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="75"<?php echo ($limit == 75) ? ' selected' : ''; ?>>75 <?php echo lang('admin input items_per_page'); ?></option>
                    <option value="100"<?php echo ($limit == 100) ? ' selected' : ''; ?>>100 <?php echo lang('admin input items_per_page'); ?></option>
                </select>
            <?php endif; ?>
        </div>
        <div class="col-md-2 text-xs-right">
            
        </div>
        <div class="col-md-6 text-center">
            <?php echo $pagination; ?>
        </div>
    </div>

</div>

