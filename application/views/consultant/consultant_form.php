<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="well">
<?php echo form_open('', array('role'=>'form')); ?>

    <div class="row">
        <?php // first name ?>
        <div class="form-group col-sm-4<?php echo form_error('company') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('consultants input company'), 'company', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'company', 'value'=>set_value('company', $consultant['company']), 'class'=>'form-control')); ?>
        </div>
        <?php // first name ?>
        <div class="form-group col-sm-4<?php echo form_error('firstname') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('consultants input firstname'), 'firstname', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_input(array('name'=>'firstname', 'value'=>set_value('firstname', $consultant['firstname']), 'class'=>'form-control')); ?>
        </div>
        <?php // last name ?>
        <div class="form-group col-sm-4<?php echo form_error('lastname') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('consultants input lastname'), 'lastname', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_input(array('name'=>'lastname', 'value'=>set_value('lastname', $consultant['lastname']), 'class'=>'form-control')); ?>
        </div>
    </div>
    
    <div class="row">
        <?php // first name ?>
        <div class="form-group col-sm-4<?php echo form_error('address') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('consultants input address'), 'address', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'address', 'value'=>set_value('address', $consultant['address']), 'class'=>'form-control')); ?>
        </div>

        <?php // last name ?>
        <div class="form-group col-sm-4<?php echo form_error('phone') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('consultants input phone'), 'phone', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'phone', 'value'=>set_value('phone',$consultant['phone']), 'class'=>'form-control')); ?>
        </div>
        <?php // last name ?>
        <div class="form-group col-sm-4<?php echo form_error('cod_fis') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('consultants input cod_fis'), 'cod_fis', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'cod_fis', 'value'=>set_value('cod_fis',$consultant['cod_fis']), 'class'=>'form-control')); ?>
        </div>
    </div>


    <div class="row">
        <?php // email ?>
        <div class="form-group col-sm-6<?php echo form_error('email') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('consultants input email'), 'email', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'email', 'value'=>set_value('email', $consultant['email']), 'class'=>'form-control')); ?>
        </div>        
    </div>
    <div class="row">
	    <div class="col-sm-12 required">
		    <small>i campi contrassegnati con * sono obbligatori</small>
	    </div>
    </div>
		
    <?php // buttons ?>
    <div class="row text-xs-right">
	    <div class="col-sm-12">
        <a class="btn btn-default" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
        <button type="submit" name="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> <?php echo lang('core button save'); ?></button>
	    </div>
    </div>

<?php echo form_close(); ?>
</div>