<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php echo form_hidden('test_id', $test_id); ?>
<?php echo form_hidden('left_key', $left_key); ?>
<?php echo form_hidden('right_key', $right_key); ?>

<?php

	$first = true;

	foreach($slides as $slide) :
		$type    = $slide['stimuli_type'];
		$trigger = $slide['trigger'];

		//Convert carraige return with <br />
		$slide['top_text']    = nl2br(htmlspecialchars($slide['top_text']));
		$slide['left_label']  = nl2br(htmlspecialchars($slide['left_label']));
		$slide['right_label'] = nl2br(htmlspecialchars($slide['right_label']));

		?>

			<div class="slide <?php echo (!$first) ? 'hidden' : 'first' ?> " data-id="<?php echo $slide['id'] ?>" data-type="<?php echo $type ?>" data-layout="<?php echo $slide['stimuli_layout'] ?>" data-triggers='<?php echo (!empty($trigger) ? implode(';', json_decode($trigger)) : '') ?>' data-timebar="<?php echo $slide['timebar_enabled'] ? '1' : '0'; ?>" data-left="<?php echo $slide['left_label'] ?>" data-right="<?php echo $slide['right_label'] ?>">

		<?php

		switch($type)
		{
			case 'instructions': ?>
					<div class="top-text">
					<?php echo $slide['top_text']; ?>
					</div>
					<div class="slide-text">
						<?php
							$file_name = "";

							foreach($stimuli_files as $key => $stimuli)
							{
								if($stimuli['slide_id'] == $slide['id'])
								{
									$file_name = $stimuli['file_name'];
									unset($stimuli_files[$key]);
									break;
								}
							}

							if($file_name != "")
							{
								@$file = fopen('uploads/slides_files/' . $folder_name . '/instructions/' . $file_name , "r");

								if($file)
								{
									echo @nl2br(utf8_encode(fread($file,filesize('uploads/slides_files/' . $folder_name . '/instructions/' . $file_name))));
									@fclose($file);
								}
								else
								{
									echo ' <div class="alert alert-danger" role="alert"><span class="error">' . lang('tests error no_file_found') . '</span></div>';
								}
							}
						?>
					</div>
				</div>

			<?php ; break; 

			case 'images': ?>

				<?php if($slide['stimuli_layout'] == 'IAT') { ?>

					<div class="top-text">
					<?php 
						echo $slide['top_text'];
					?>
					</div>
					<div class="image">
						<?php
							$file_name = "";

							foreach($stimuli_files as $key => $stimuli)
							{
								if($stimuli['slide_id'] == $slide['id'])
								{									
									$file_name = $stimuli['file_name'];
									unset($stimuli_files[$key]);
									break;
								}
							}

							if($file_name != "")
							{
								echo '<img src="/uploads/slides_files/' . $folder_name . '/images/' . $file_name . '" class="img-fluid" />'; 
							}
						?>
					</div>
				</div>

				<?php } else if($slide['stimuli_layout'] == '20_image_spread') { ?>
					
					<div class="top-text">
					<?php 
						echo $slide['top_text'];
					?>
					</div>
					<div class="centeredVertical">
						<table class="table images-spread">
						<?php

							$files_name = array();
							$count      = 0;

							foreach($stimuli_files as $key => $stimuli)
							{
								if($stimuli['slide_id'] == $slide['id'])
								{									
									$files_name[] = $stimuli['file_name'];
									unset($stimuli_files[$key]);
									$count++;

									if($count == 20){
										break;
									}
								}
							}

							$number = 1;
							$echo_tr = true;

							foreach($files_name as $file_name)
							{
								if($echo_tr){
									echo '<tr>';
									$echo_tr = false;
								}

								if($number % 5 == 0)
								{
									echo '<td class="tableCell"><img src="/uploads/slides_files/' . $folder_name . '/images/' . $file_name . '" class="image-spread img-fluid" data-number="' . $number .'" /></td></tr>'; 
									$echo_tr = true;
								}
								else
								{
									echo '<td class="tableCell"><img src="/uploads/slides_files/' . $folder_name . '/images/' . $file_name . '" class="image-spread img-fluid" data-number="' . $number .'" /></td>'; 
								}

								$number++;
							}
						?>
						</table>
					</div>
				</div>

				<?php } ?>

			<?php ; break; 

			case 'text': ?>

				<div class="top-text">
				<?php 
					echo $slide['top_text'];
				?>
				</div>

				<?php if($slide['stimuli_layout'] == 'IAT') { ?>

					<div class="slide-text">
						<?php
							$file_name = "";

							foreach($stimuli_files as $key => $stimuli)
							{
								if($stimuli['slide_id'] == $slide['id'])
								{									
									$file_name = $stimuli['file_name'];
									unset($stimuli_files[$key]);
									break;
								}
							}

							if($file_name != "")
							{
								@$file = fopen('uploads/slides_files/' . $folder_name . '/text/' . $file_name , "r");

								if($file)
								{									
									echo @nl2br(utf8_encode(fread($file,filesize('uploads/slides_files/' . $folder_name . '/text/' . $file_name))));
									@fclose($file);
								}
								else
								{
									echo ' <div class="alert alert-danger" role="alert"><span class="error">' . lang('tests error no_file_found') . '</span></div>';
								}
							}
						?>
					</div>

				<?php } else { ?>

					<div class="multi-texts">

					<?php
						$file_names = array();

							foreach($stimuli_files as $key => $stimuli)
							{
								if($stimuli['slide_id'] == $slide['id'])
								{									
									$file_names[] = $stimuli['file_name'];
									unset($stimuli_files[$key]);
								}
							}

							$number = 1;

							foreach($file_names as $file_name)
							{
								echo '<div class="text" data-number="' . $number . '">';

								@$file = fopen('uploads/slides_files/' . $folder_name . '/text/' . $file_name , "r");

								if($file)
								{									
									echo @nl2br(utf8_encode(fread($file,filesize('uploads/slides_files/' . $folder_name . '/text/' . $file_name))));
									@fclose($file);
								}
								else
								{
									echo ' <div class="alert alert-danger" role="alert"><span class="error">' . lang('tests error no_file_found') . '</span></div>';
								}

								echo '</div>';

								$number++;
							}
					?>

					</div>

				<?php } ?>
			</div>

			<?php ; break;

			case 'text-box': ?>
				<div class="top-text">
				<?php echo $slide['top_text']; ?>
				</div>
                <?php
                	$length = NULL;
                	$type   = NULL;

                	$input_data = array(
						'name'    => 'type',
						'value'   => '',
						'class'   => 'form-control w-100 slide-text-box'
                	);

                	if($slide['stimuli_filters']){
                		$input_data['class'] .= ' stimuli-filtered';
                		$stimuli_filters     = json_decode($slide['stimuli_filters'], TRUE);

                		if(isset($stimuli_filters['type'])){
                			$input_data['data-type'] = $stimuli_filters['type'];
                		}

                		if(isset($stimuli_filters['length'])){
							$input_data['data-length'] = $stimuli_filters['length'];
                		}
                	}

                	echo form_input($input_data);
                ?>
			</div>

			<?php ; break;
		}
		$first = false;

	endforeach;
?>
        <div class="slide-buttons">
            <div class="row">
                <div class="col-sm-6 col-lg-4 p-b-1">
                    <button type="button" id="btnLeft" class="btn btn-lg btn-outline-primary w-100"></button>
                </div>
                <div class="col-sm-6 col-lg-4 offset-lg-4 p-b-1">
                    <button type="button" id="btnRight" class="btn btn-lg btn-outline-primary w-100"></button>
                </div>
            </div>
        </div>