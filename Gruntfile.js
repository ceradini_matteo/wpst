module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy: {
          font: {
              expand: true,
              cwd: 'bower_components/Ionicons/fonts/',
              src: '*',
              dest: 'htdocs/assets/fonts',
              filter: 'isFile'
          },
          xeditable: {
              expand: true,
              cwd: 'bower_components/x-editable/dist/bootstrap4-editable/img/',
              src: '*',
              dest: 'htdocs/themes/admin/img',
              filter: 'isFile'
          }
        },
        concat: {
            js_libs: {
                files: {
                    'htdocs/assets/js/script.js': [
	                    'bower_components/tether/dist/js/tether.min.js',
	                    'bower_components/bootstrap/dist/js/bootstrap.min.js',
	                    'bower_components/bootbox.js/bootbox.js',
	                    'htdocs/dev/javascripts_libs/*.js',
                    ],
                    'htdocs/themes/admin/js/admin_libs.js': [
	                    'bower_components/moment/min/moment.min.js',
	                    'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',                      
                      'bower_components/bootstrap-validator/dist/validator.min.js'
                    ],
                }
            },
            css_libs: {
                files: {
                    'htdocs/assets/css/style.css': [
						        'bower_components/bootstrap/dist/css/bootstrap.min.css',
		                'bower_components/Ionicons/css/ionicons.min.css',
		                'htdocs/assets/css/style.css'
                    ],
                    
                    'htdocs/themes/admin/css/admin_libs.css': [
		                'bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css'
                    ],
                }
            }
        },
        sass: {
	        dev: {
              options: {
                  style: 'expanded'
              },
              files: {
                  'htdocs/assets/css/style.css': 'htdocs/dev/stylesheets/style.scss',
                  'htdocs/themes/admin/css/admin.css': 'htdocs/dev/stylesheets/admin.scss',
                  'htdocs/themes/default/css/login.css': 'htdocs/dev/stylesheets/login.scss',
              }
          },
          prod: {
              options: {
                  style: 'compressed'
              },
              files: {
                  'htdocs/assets/css/style.css': 'htdocs/dev/stylesheets/style.scss',
                  'htdocs/themes/admin/css/admin.css': 'htdocs/dev/stylesheets/admin.scss',
                  'htdocs/themes/default/css/login.css': 'htdocs/dev/stylesheets/login.scss',
              }
          }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                	'htdocs/themes/default/css/style.css': 'htdocs/themes/default/css/style.css',
					'htdocs/themes/admin/css/admin.css': 'htdocs/themes/admin/css/admin.css',
					'htdocs/themes/admin/css/admin_libs.css': 'htdocs/themes/admin/css/admin_libs.css',
					'htdocs/themes/default/css/login.css': 'htdocs/themes/default/css/login.css',
                }
            }
        },
        autoprefixer: {            
            single_file: {                
                options: {                    
                    browsers: ['last 2 version', 'ie 9']                
                },
                src: 'htdocs/assets/css/style.css',
                dest: 'htdocs/assets/css/style.css'
            }        
        },
        uglify: {
            prod: {
                files: {
                    'htdocs/assets/js/script.js': ['htdocs/assets/js/script.js'],
                    'htdocs/themes/admin/js/admin_libs.js': ['htdocs/themes/admin/js/admin_libs.js']
                }
            }
        },
        watch: {
            options: {
                livereload: true,
                spawn: true
            },
            libs: {
                files: [
              		'htdocs/themes/admin/javascripts/*.js',
			  		'htdocs/themes/core/javascripts/*.js',
                	'htdocs/themes/default/javascripts/*.js',
                	'bower_components/**/*.js',
                 ],
                tasks: ['concat']
            },
            styles: {
                files: [
                	'htdocs/dev/stylesheets/**/*.scss',
                ],
                tasks: ['sass:dev', 'autoprefixer', 'concat:css_libs']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-autoprefixer');

    grunt.registerTask('dev', ['copy', 'concat:js_libs', 'sass:dev','autoprefixer', 'concat:css_libs', 'watch']);
    grunt.registerTask('prod', ['copy', 'concat:js_libs', 'sass:prod','autoprefixer', 'concat:css_libs','cssmin','uglify']);
};
