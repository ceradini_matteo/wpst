$(document).ready(function(){
	var time_start, time1, time2, time3, time4, time5;
	var result_id;
	var start_slide;
	var first_slide = $(this).find('.first');
	var count_slide = $(this).find('.slide').length - 1;
	var error_slide = false;
	var attempts    = 0;
	var slide_answers, user_answers = new Array();
	var answer1, answer2, answer3, answer4, answer5;
	var trigger_mouse = true, trigger_key = false, trigger_time = false, trigger_time_value = null, trigger_timer, trigger_timer_interval;
	var window_location_origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: ''); //fix for ie
	var is_first_slide         = true;
	var current_answers;
	var left_key;
	var right_key;

	start_test();
	check_window_size();

	//Resize image when window is too small
	$(window).resize(function(){
		check_window_size();
	});

	function check_window_size()
	{
		var slide = $('body').find('.slide');

		if(window.innerHeight < 1200)
		{
			if(slide.data('type') == 'images' && slide.data('layout') == 'IAT')
			{
				var window_size = window.innerHeight;
				var image       = slide.find('.img-fluid').first()[0];	
				var da_togliere = $('body').find('.slide-buttons').height() + 350;

				if(window_size - da_togliere > 550)
				{
					image.style.maxHeight = "550px";
				}
				else
				{
					image.style.maxHeight = window_size - da_togliere + "px";
				}
			}	
			else if(slide.data('type') == 'images' && slide.data('layout') == '20_image_spread')
			{
				var window_size = window.innerHeight;
				var da_togliere = 85;
				var new_size    = (window_size / 4) - da_togliere;

				$.each($('.tableCell'), function(){
					var image       = $(this).find('.image-spread').first()[0];					

					if(new_size > 150)
					{
						image.style.maxHeight = "150px";
					}
					else
					{
						image.style.maxHeight = new_size + "px";
					}					
				});
			}
		}		
	}

	window.onbeforeunload = function () {
	    return "Are you sure do you want to exit?";
	};

	function start_test()
	{
		var test_id =  $("input[name=test_id]").val();
		left_key    = $("input[name=left_key]").val().substring(0,1).toUpperCase().charCodeAt(0);
		right_key   = $("input[name=right_key]").val().substring(0,1).toUpperCase().charCodeAt(0);
		var date    = new Date();
		start_slide = date.getTime();

		$.ajax( {
			type: 'GET',
		    dataType: 'json',
		    url: window_location_origin + '/mturk/start_test',
		    data:{
		    	test_id: test_id,
		    	started: start_slide
		    },
			success: function (data) {
				result_id     = data['id'];
				slide_answers = data['answers'];
				time_start = Date.now();

				$(".loader").fadeOut("fast");

				check_first_slide(first_slide);
			}
		});
	}

	function check_first_slide(slide)
	{
		var container = slide.closest('.container');
    	var btnLeft   = container.find('#btnLeft');
    	var btnRight  = container.find('#btnRight');  
		var type      = slide.data('type');
	    var layout    = slide.data('layout');

		if((layout == 'IAT' && type == 'images') || type == 'text' || type == 'instructions')
		{
			if(slide.data('left') != ""){
				btnLeft.html(slide.data('left'));
			}
			else{				
				btnLeft.addClass('hidden');
			}

			if(slide.data('right') != ""){
				btnRight.html(slide.data('right'));
			}
			else{				
				btnRight.addClass('hidden');
			}
		}
		else if(type == 'text-box')
		{
			btnRight.text('continue');
			btnLeft.addClass('hidden');
		}
		else
		{
			btnLeft.addClass('hidden');
			btnRight.addClass('hidden');
		}

		set_trigger(slide);

		if(trigger_time)
		{
			start_trigger_time(container, slide);
		}
	}

	$('body').on('click','#btnLeft',function() {
		if(trigger_mouse){
			elaborate_answer($(this).closest('.container'), 'left');
		}	
    });

	$('body').on('click','#btnRight',function() {
		if(trigger_mouse){
			elaborate_answer($(this).closest('.container'), 'right');
		}
    });

	$('body').on('click','.image-spread',function() {
		if(trigger_mouse){
			elaborate_answer($(this).closest('.container'), $(this).data('number'));	
		}
    });

    $('body').on('click','.multi-texts .text ',function() {
		if(trigger_mouse){
			elaborate_answer($(this).closest('.container'), $(this).data('number'));	
		}
    });

    $('body').keyup(function(e) {
		if(trigger_key)
		{
			var code = e.keyCode || e.which;
			var type = $(this).find('.container').find('.slide').data('type');

			if(type != 'text-box')
			{
				if(code == 37 || code == left_key) //arrow-left or W
				{
					elaborate_answer($(this).find('.container'), 'left');
				}
				else if(code == 39 || code == right_key) //arrow-right or O
				{
					elaborate_answer($(this).find('.container'), 'right');
				}
				else if(code >= 49 && code <= 57 || code >= 97 && code <= 105) //Number from 1 to 9 (and numpad)
				{
					switch(code)
					{
						case 49: case 97: elaborate_answer($(this).find('.container'), '1'); break; 
						case 50: case 98: elaborate_answer($(this).find('.container'), '2'); break;
						case 51: case 99: elaborate_answer($(this).find('.container'), '3'); break;
						case 52: case 100: elaborate_answer($(this).find('.container'), '4'); break;
						case 53: case 101: elaborate_answer($(this).find('.container'), '5'); break;
						case 54: case 102: elaborate_answer($(this).find('.container'), '6'); break;
						case 55: case 103: elaborate_answer($(this).find('.container'), '7'); break;
						case 56: case 104: elaborate_answer($(this).find('.container'), '8'); break;
						case 57: case 105: elaborate_answer($(this).find('.container'), '9'); break;
					}
				}
			}		
		}
    });

    function elaborate_answer(container, answer)
    {
    	var date  = new Date();
    	var now   = Date.now();

    	switch(attempts)
		{
			case 0: time1 = now - time_start; answer1 = answer; break;
			case 1: time2 = now - time_start; answer2 = answer; break;
			case 2: time3 = now - time_start; answer3 = answer; break;
			case 3: time4 = now - time_start; answer4 = answer; break;
			case 4: time5 = now - time_start; answer5 = answer; break;
		}

		attempts++;

		var slide  = container.find('.slide').first();
		var type   = slide.data('type');

		if(type == 'instructions')
		{
			next = check_slide_answer(slide, answer);

			if(!next)
			{
				var ended = date.getTime();

				$(".loader").fadeIn("fast");

				window.onbeforeunload = null;

				if(is_first_slide)
				{
					$.ajax( {
					    type: 'POST',
					    url: window_location_origin + '/mturk/delete_test_attempt',
					    data:{
					    	result_id:  result_id
					    }
					}).done(function(data) {
						window.location.href = window_location_origin + '/mturk/do_test';
					});
				}
				else
				{
					var slide_id = slide.data('id');

					current_answers = new Array(result_id, slide_id, start_slide, answer1, time1, answer2, time2, answer3, time3, answer4, time4, answer5, time5);
	    			user_answers.push(current_answers);

					$.ajax( {
					    type: 'POST',
					    url: window_location_origin + '/mturk/save_test_result',
					    data:{
					    	result_id:  result_id,
					    	ended:      ended,
					    	answers:    user_answers,
					    	last_answer: current_answers		    	
					    }
					}).done(function(data) {
						window.location.href = window_location_origin + '/mturk/do_test';
					});
				}
			}
			else
			{
				show_new_slide(slide);
			}
		}
		else if(type == 'text-box')
		{
			answer1 = slide.find('.slide-text-box').val();

			show_new_slide(slide);
		}
		else
		{
			if(attempts == 5)
			{
				next = true;
			}
			else
			{
				var next = false;

				if(type == 'images' || type == 'text')
				{
					next = check_slide_answer(slide, answer);
				}
			}				

			if(next)
			{
				show_new_slide(slide);
			}
			else
			{
				time_start = Date.now();
				show_error(container);
			}		
		}
    }

    function show_new_slide(slide)
    {
    	if(trigger_time)
    	{
    		clearTimeout(trigger_timer);
    		clearInterval(trigger_timer_interval);
    	}

    	if(count_slide > 0)
    	{
    		count_slide--;
	    	var slide_id  = slide.data('id');    
			var new_slide = slide.closest('.slide').next();
			slide.remove();
	    	var type      = new_slide.data('type');
	    	var layout    = new_slide.data('layout');
			var container = new_slide.closest('.container');
	    	var btnLeft   = container.find('#btnLeft');
	    	var btnRight  = container.find('#btnRight');    

	    	btnLeft.addClass('disabled');
	    	btnRight.addClass('disabled');
	    	btnLeft.removeClass('hidden');
	    	btnRight.removeClass('hidden');

	    	if(trigger_time){
	    		container.find('.timer').addClass('hidden');	    		
	    	}

	    	if(error_slide){
	    		container.find('.error-slide').addClass('hidden');
	    		error_slide = false;
	    	}
	    	
    		if(type == 'images' && layout == 'IAT' || type == 'text' || type == 'instructions')
    		{
    			if(new_slide.data('left') != ""){
					btnLeft.html(new_slide.data('left'));
				}
				else{				
					btnLeft.addClass('hidden');
				}

				if(new_slide.data('right') != ""){
					btnRight.html(new_slide.data('right'));
				}
				else{				
					btnRight.addClass('hidden');
				}
    		}
    		else if(type == 'text-box')
    		{
    			btnRight.text('continue');
    			btnLeft.addClass('hidden');
    		}
    		else
    		{
    			btnLeft.addClass('hidden');
    			btnRight.addClass('hidden');
    		}

    		if(is_first_slide){
    			is_first_slide = false;
    		}

	    	//triggers
	    	trigger_mouse = true;
	    	trigger_key   = trigger_time = false;
			set_trigger(new_slide);

			//show slide
			new_slide.removeClass('hidden');

			var date            = new Date();
			time_start          = Date.now();
			var new_start_slide = date.getTime();

			if(trigger_time)
			{
				start_trigger_time(container, new_slide);
			}

			current_answers = new Array(result_id, slide_id, start_slide, answer1, time1, answer2, time2, answer3, time3, answer4, time4, answer5, time5);	    	

	    	$.ajax( {
			    type: 'POST',
			    url: window_location_origin + '/mturk/save_slide_result',
			    data:{
			    	answers: current_answers
			    }
			});

			attempts = 0;
			time2    = time3 = time4 = time5 = null;

			check_window_size();

			user_answers.push(current_answers);
			answer1  = answer2 = answer3 = answer4 = answer5 = null;
			start_slide = new_start_slide;
    	}
    	else
    	{
    		var date  = new Date();
			var ended = date.getTime();

    		//Show loader before start to save test
			$(".loader").fadeIn("fast");

			window.onbeforeunload = null;

    		var slide_id = slide.data('id');

	    	current_answers = new Array(result_id, slide_id, start_slide, answer1, time1, answer2, time2, answer3, time3, answer4, time4, answer5, time5);
	    	user_answers.push(current_answers);

	    	$.ajax( {
			    type: 'POST',
			    url: window_location_origin + '/mturk/save_test_result',
			    data:{
			    	result_id:  result_id,
			    	ended:      ended,
			    	answers:    user_answers,
			    	last_answer: current_answers
			    }
			}).done(function(data) {
				window.location.href = window_location_origin + '/mturk/finish';
			});
    	}
    }

    function show_error(container)
    {
    	if(!error_slide)
    	{
    		container.find('.error-slide').removeClass('hidden');
    		error_slide = true;
    	}    	
    }

    function check_slide_answer(slide, answer)
    {
    	var next           = false;
    	var slide_id       = slide.data('id');
    	var correct_answer = JSON.parse(slide_answers[slide_id]['answer']);

    	if(correct_answer != null)
    	{
			for(i = 0; i < correct_answer.length; i++)
			{
				if(correct_answer[i] == answer)
				{
					next = true;
					break;
				}
			}
    	}
    	else
    	{
    		next = true;
    	}

    	return next;
    }

    function set_trigger(slide)
    {
    	var triggers = slide.data('triggers').split(';');

    	if(triggers != null && triggers != "")
    	{
    		trigger_mouse = false;

    		for(i = 0; i < triggers.length; i++)
	    	{
	    		var trigger = triggers[i];

	    		switch(trigger)
	    		{
	    			case 'mouse': trigger_mouse = true; break;
	    			case 'key': trigger_key = true; break;
	    			case 'time': trigger_time = true; trigger_time_value = triggers[i + 1] * 1000; i++; break;
	    		}
	    	}
    	}
    }

    var trigger_time_val;	// the trigger_time_value

    function start_trigger_time(container, slide)
    {
    	trigger_timer = setTimeout(function(){trigger_timeout(slide)}, trigger_time_value);

		var timer_container = container.find('.timer');
		var timer_value     = timer_container.find('#timerValue');
		timer_value.attr('max', trigger_time_value / 1000);
		timer_value.val(trigger_time_value / 1000);

		trigger_time_val = trigger_time_value;

		trigger_timer_interval = setInterval(function(){
			trigger_time_value -= 1000;
			timer_value.val(trigger_time_value / 1000);
		}, 1000);

		if(slide.data('timebar') == '1'){
			timer_container.removeClass('hidden');
		}
    }

    function trigger_timeout(slide)
    {
    	switch(attempts)
		{
			case 0: time1 = trigger_time_val; answer1 = ''; break;
			case 1: time2 = trigger_time_val; answer2 = ''; break;
			case 2: time3 = trigger_time_val; answer3 = ''; break;
			case 3: time4 = trigger_time_val; answer4 = ''; break;
			case 4: time5 = trigger_time_val; answer5 = ''; break;
		}

    	show_new_slide(slide);
    }

    // stimuli filters function
    var backspace;

	$('body').on('keydown','.stimuli-filtered', function(e) {
		var key = event.which || event.keyCode || event.charCode;

		backspace = key == 8 ? true : false; // set var backspace true if backspace was pressed
	});

	$('body').on('input','.stimuli-filtered', function(e) {
		var _this          = $(this);
		var stimuli_length = _this.data('length');
		var stimuli_type   = _this.data('type');
		var valid          = true;
		var value          = _this.val();

		if(stimuli_type){
			var new_value = '';
			var index     = 0;

			while(index < value.length)
			{
				switch(stimuli_type){
					case 'numeric':      new_value += value[index].match('[0-9]') ? value[index] : ''; break;
					case 'alphanumeric': new_value += value[index].match('[a-z]|[A-Z]|[0-9]') ? value[index] : ''; break;
					case 'date':         new_value += value[index].match('[0-9]|[/]') && index < 10 ? value[index] : ''; break;
					default:             new_value += value[index]; break;
				}

				index++;
			}

			if(stimuli_type == 'date'){
				switch(new_value.length){
					case 2: new_value += !backspace ? '/' : ''; break;
					case 3: new_value = new_value.replaceAt(2, '/');
					case 5: new_value += !backspace ? '/' : ''; break;
					case 6: new_value = new_value.replaceAt(5, '/');
				}

				// trim unsense value like /// or 1234567890 that is permitted and untriggeredable before
				if(new_value == '///'){
					new_value = '';
				}
				else if(new_value.indexOf('/') == -1 && new_value.length > 2){
					new_value = '';
				}

				new_value = new_value.replace('//','/');
			}

			value = new_value;
		}

		if(stimuli_length){
			value = value.substring(0, stimuli_length);
		}

		old_length = value.length;

		_this.val(value);
    });
});

String.prototype.replaceAt=function(index, char) {
    var a = this.split("");
    a[index] = char;
    return a.join("");
}