
$(document).ready(function() {
	if ($(".input-group.date").length) {
	  $('.input-group.date').datepicker({
      autoclose      : true,
      todayHighlight : true,
      clearBtn:true,
			daysOfWeekHighlighted: "0",
      language: 'it',
      templates:{
				leftArrow: '<span class="ion-chevron-left"></span>', 
				rightArrow: '<span class="ion-chevron-right"></span>'
			},
      format:'dd/mm/yyyy'
    });
  }
  
  $('#datepicker').datepicker({
			daysOfWeekHighlighted: "0",
      language: 'it',
      templates:{
				leftArrow: '<span class="ion-chevron-left"></span>', 
				rightArrow: '<span class="ion-chevron-right"></span>'
			},
      format:'yyyy-mm-dd'});
      
	$('#datepicker').on("changeDate", function() {
	    $('#deadline_date').val(
	        $('#datepicker').datepicker('getFormattedDate')
	    );
	});
	
	$('.add-years').on('click',function(){
		var date = $('#datepicker').datepicker('getDate');
		var years_num = parseFloat($(this).data('years'));
		
		date.setMonth(date.getMonth() + (years_num*12));
		$('#datepicker').datepicker('setDate',date);
	});
	
	$('.open-modal').on('click',function(){	
    var _this = $(this);
		var date = _this.data('date');
		var parts = date.split("-");
		var dt = new Date(parseInt(parts[0], 10),parseInt(parts[1], 10) - 1,parseInt(parts[2], 10));
		
    
    var tr = _this.parent().parent();
    var company = tr.find('.company').text();
    var firstname = tr.find('.firstname').text();
    var lastname = tr.find('.lastname').text();
    var type = tr.find('.type').text();
    
    $('#modalContent').text('Data scadenza '+ type + " di " + company + " " + firstname + " " + lastname);
    
    $('#customer_id').val(_this.data('customer_id'));
    $('#deadline_type').val(_this.data('type'));
		$('#datepicker').datepicker('setDate',dt);
		$('#deadlineModal').modal('show');
	});
	$('#remove-deadline').on('click',function(){
		updateDeadline('');
	});
	$('#update-deadline').on('click',function(){
		var dt = $('#datepicker').datepicker('getDate');
		var date = dt.getFullYear() + '-' + ('0' + (dt.getMonth()+1)).slice(-2) + '-' + ('0' + dt.getDate()).slice(-2);
		updateDeadline(date);
	});
});

function updateDeadline(date){
	var customer_id = $('#customer_id').val();
	var deadline_type = $('#deadline_type').val();
	$.ajax({
	  type: "GET",
	  url: '/deadline/update/'+customer_id+"/"+deadline_type+"/"+date,
	  success: function(data){
		  if(data==''){
				$('#deadlineModal').modal('hide');
				location.reload();
		  }
		}
	});
}
