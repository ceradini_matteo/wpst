<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico?v=<?php echo $this->settings->site_version; ?>">
	<link rel="icon" type="image/x-icon" href="/favicon.ico?v=<?php echo $this->settings->site_version; ?>">
    <title><?php echo strip_tags($page_header); ?> - <?php echo $this->settings->site_name; ?></title>
    <meta name="keywords" content="<?php echo $this->settings->meta_keywords; ?>">
    <meta name="description" content="<?php echo $this->settings->meta_description; ?>">
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
<?php // CSS files 
	  if (isset($css_files) && is_array($css_files)) :
		  foreach ($css_files as $css) :
			 if ( ! is_null($css)) : ?>
	<link rel="stylesheet" href="<?php echo $css; ?>?v=<?php echo $this->settings->site_version; ?>">
<?php endif; 
			endforeach;
    endif; ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   <script src="assets/js/rem.min.js" type="text/javascript">
    <![endif]-->
</head>
<body>
	<nav class="navbar bd-navbar" role="navigation">
		<div class="container">
	  <div class="navbar-title">
	    <button class="navbar-toggler pull-xs-right hidden-sm-up" type="button" data-toggle="collapse" data-target="#bd-main-nav">
	      &#9776;
	    </button>
	    <a class="navbar-brand" href="/">
		    <img src="/assets/images/logo.png" alt="wpst" />
	      <span class="hidden">wpst</span>
	    </a> 
	  </div>
	  <div class="collapse navbar-toggleable-xs" id="bd-main-nav">
	    <ul class="nav navbar-nav pull-sm-right">
		    <?php if ($this->session->userdata('logged_in')) : ?>
		    <li class="nav-item dropdown <?php echo (uri_string() == 'profile') ? 'active' : ''; ?>">
            	<a class=" nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <?php echo $this->session->userdata('logged_in')['username']; ?>
                </a>
			    <div class="dropdown-menu">
			      <?php if ($this->user['is_admin']) : ?>
	            <a class="dropdown-item" href="<?php echo base_url('admin'); ?>"><span class="ion-settings"></span> <?php echo lang('core button admin'); ?></a>
	            <?php endif; ?>
		          <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo base_url(''); ?>"><span class="ion-ios-home"></span> <?php echo lang('core button home'); ?></a>
                  <a class="dropdown-item" href="<?php echo base_url('profile'); ?>"><span class="ion-person"></span> <?php echo lang('core button reset_password'); ?></a>
			      <a class="dropdown-item" href="<?php echo base_url('logout'); ?>"><span class="ion-log-out"></span> <?php echo lang('core button logout'); ?></a>
			    </div>
		    </li>
		    <?php endif; ?>
	    </ul>
	  </div>
		</div>
	</nav>
	
  
	<div class="page-header">
			  <?php if(isset($page_title)){ ?>
			  
			  <div class="title hidden-xs-down">
			  <h1><?php echo $page_title; ?></h1>
			  <?php if(isset($page_sub_title)){ ?>
			  <p class="lead">	
				  <?php echo $page_sub_title; ?>
			  </p>
			  <?php } ?>
			  </div>
			  <?php } ?>
    </div>
    <?php // Main body ?>
    <div class="container page-container" role="main">

        <?php // System messages ?>
        <?php if ($this->session->flashdata('message')) : ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('message'); ?>
            </div>
        <?php elseif ($this->session->flashdata('error')) : ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('error'); ?>
            </div>
        <?php elseif (validation_errors()) : ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo validation_errors(); ?>
            </div>
        <?php elseif ($this->error) : ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->error; ?>
            </div>
        <?php endif; ?>

        <?php // Main content ?>
        <?php echo $content; ?>

    </div>

    <?php // Footer ?>
    <footer class="footer">
        <div class="container">
            <p>
                Copyright &copy; <?php echo date("Y"); ?> APR Testing Services. All right reserved.
            </p>
        </div>
    </footer>

    <?php // Javascript files ?>
    <?php if (isset($js_files) && is_array($js_files)) : ?>
        <?php foreach ($js_files as $js) : ?>
            <?php if ( ! is_null($js)) : ?>
                <?php echo "\n"; ?><script type="text/javascript" src="<?php echo $js; ?>?v=<?php echo $this->settings->site_version; ?>"></script><?php echo "\n"; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if (isset($js_files_i18n) && is_array($js_files_i18n)) : ?>
        <?php foreach ($js_files_i18n as $js) : ?>
            <?php if ( ! is_null($js)) : ?>
                <?php echo "\n"; ?><script type="text/javascript"><?php echo "\n" . $js . "\n"; ?></script><?php echo "\n"; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>

</body>
</html>
