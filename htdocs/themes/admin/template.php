<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Template
 */
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico?v=<?php echo $this->settings->site_version; ?>">
		<link rel="icon" type="image/x-icon" href="/favicon.ico?v=<?php echo $this->settings->site_version; ?>">
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <title><?php echo $page_title; ?> - <?php echo $this->settings->site_name; ?></title>

    <?php // CSS files ?>
    <?php if (isset($css_files) && is_array($css_files)) : ?>
        <?php foreach ($css_files as $css) : ?>
            <?php if ( ! is_null($css)) : ?>
                <link rel="stylesheet" href="<?php echo $css; ?>?v=<?php echo $this->settings->site_version; ?>"><?php echo "\n"; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<header class="navbar bd-navbar" role="banner">
        <div class="container-fluid">
            <div class="navbar-title">
                <button class="navbar-toggler pull-xs-right hidden-sm-up" type="button" data-toggle="collapse" data-target="#bd-main-nav">☰</button>
                <a class="navbar-brand" href="/">
                    <img src="/assets/images/logo.png" alt="wpst" />
                    <span class="hidden"><?php echo $this->settings->site_name; ?></span>
                </a> 
            </div>
            <div class="collapse navbar-toggleable-xs" id="bd-main-nav">
                <ul class="nav navbar-nav">
                    <li class="nav-item <?php echo (uri_string() == 'admin' OR uri_string() == 'admin/dashboard') ? 'active' : ''; ?>">
                        <a class="nav-link" href="<?php echo base_url('/admin'); ?>"><?php echo lang('admin button dashboard'); ?></a>
                    </li>
                    <li class="nav-item <?php echo (strstr(uri_string(), 'admin/admins')) ? ' active' : ''; ?>">  
                        <a class="nav-link <?php echo (uri_string() == 'admin/admins') ? 'active' : ''; ?>" href="<?php echo base_url('/admin/admins'); ?>"><?php echo lang('admin button admins'); ?></a>
                    </li>
                    <li class="nav-item <?php echo (strstr(uri_string(), 'admin/users')) ? ' active' : ''; ?>">  
                        <a class="nav-link <?php echo (uri_string() == 'admin/users') ? 'active' : ''; ?>" href="<?php echo base_url('/admin/users'); ?>"><?php echo lang('admin button users'); ?></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo lang('admin button tests'); ?>
                            <?php if($this->session->userdata('tests_unread') > 0) : ?>
                               <span class="badge badge-danger"><?php echo $this->session->userdata('tests_unread'); ?></span>
                            <?php endif; ?>
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item <?php echo (uri_string() == 'admin/tests') ? 'active' : ''; ?>" href="<?php echo base_url('/admin/tests'); ?>"><?php echo lang('admin button tests_form'); ?></a>
                            <a class="dropdown-item <?php echo (uri_string() == 'admin/tests_result') ? 'active' : ''; ?>" href="<?php echo base_url('/admin/tests_result'); ?>"><?php echo lang('admin button tests_result'); ?>
                                <?php if($this->session->userdata('tests_unread') > 0) : ?>
                                   <span class="badge badge-danger"><?php echo $this->session->userdata('tests_unread'); ?></span>
                                <?php endif; ?>
                            </a>
                        </div>
                    </li>
                    <li class="nav-item <?php echo (strstr(uri_string(), 'admin/batteries')) ? ' active' : ''; ?>">  
                        <a class="nav-link <?php echo (uri_string() == 'admin/batteries') ? 'active' : ''; ?>" href="<?php echo base_url('/admin/batteries'); ?>"><?php echo lang('admin button batteries'); ?></a>
                    </li>
                    <li class="nav-item <?php echo (strstr(uri_string(), 'admin/settings')) ? ' active' : ''; ?>">  
                        <a class="nav-link <?php echo (uri_string() == 'admin/settings') ? 'active' : ''; ?>" href="<?php echo base_url('/admin/settings'); ?>"><?php echo lang('admin button settings'); ?></a>
                    </li>     
                </ul> 
                <ul class="nav navbar-nav pull-xs-right">
                    <?php if (!$this->session->userdata('logged_in')) : ?>
                    <li class="nav-item <?php echo (uri_string() == 'login') ? 'active' : ''; ?>">
                        <a class="nav-link" href="<?php echo base_url('/login'); ?>"><span class="ion-log-in"></span> <?php echo lang('core button login'); ?></a>
                    </li>
                    <li class="nav-item <?php echo (uri_string() == 'register') ? 'active' : ''; ?>">
                        <a class="nav-link" href="<?php echo base_url('/user/register'); ?>"><span class="ion-person-add"></span> <?php echo lang('core button register'); ?></a>
                    </li>
                    <?php else: ?> 
                    <li class="m-r-2 nav-item dropdown <?php echo (uri_string() == 'profile') ? 'active' : ''; ?>">
                        <a class=" nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <?php echo $this->session->userdata('logged_in')['username']; ?>  
                        </a>
                        <div class="dropdown-menu">
                            <?php if ($this->user['is_admin']) : ?>
                            <a class="dropdown-item" href="<?php echo base_url('admin'); ?>"><span class="ion-settings"></span> <?php echo lang('core button admin'); ?></a>
                            <?php endif; ?>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="<?php echo base_url('logout'); ?>"><span class="ion-log-out"></span> <?php echo lang('core button logout'); ?></a>
                        </div>
                    </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
	</header>
    <?php // Fixed navbar ?>

    <?php // Main body ?>
    <div class="container-fluid" role="main">
	    
	    <?php if(isset($page_title)){ ?>
	    	<h1 class="admin-page-header"><?php echo $page_title; ?></h1>
	    <?php } ?>
	    	<?php if(isset($breadcrumb)){ ?>
	    	<ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('admin'); ?>"><span class="ion-home"></span></a></li>
                  <?php 
                    foreach($breadcrumb as $url => $text){
                        if($url==''){ $active = true; }else{ $active = false; }
                    ?>
                  <li class="breadcrumb-item <?php  echo ($active)?'active':''; ?>"><?php if(!$active){ ?><a href="<?php echo base_url($url); ?>"><?php }  echo $text;  if(!$active){ ?></a><?php } ?></li>
                  <?php } ?>
                </ol>
				<?php } ?>

        <?php // System messages ?>
        <?php if ($this->session->flashdata('message')) : ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('message'); ?>
            </div>
        <?php elseif ($this->session->flashdata('error')) : ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('error'); ?>
            </div>
        <?php elseif (validation_errors()) : ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo validation_errors(); ?>
            </div>
        <?php elseif ($this->error) : ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->error; ?>
            </div>
        <?php endif; ?>

        <?php // Main content ?>
        <?php echo $content; ?>

    </div>

    <?php // Footer ?>
    <footer class="footer">
        <div class="container">
            <p>
                Copyright &copy; <?php echo date("Y"); ?> APR Testing Services. All right reserved.
            </p>
        </div>
    </footer>
    <?php // Javascript files ?>
    <?php if (isset($js_files) && is_array($js_files)) : ?>
        <?php foreach ($js_files as $js) : ?>
            <?php if ( ! is_null($js)) : ?>
                <?php echo "\n"; ?><script type="text/javascript" src="<?php echo $js; ?>?v=<?php echo $this->config->item('site_version'); ?>"></script><?php echo "\n"; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if (isset($js_files_i18n) && is_array($js_files_i18n)) : ?>
        <?php foreach ($js_files_i18n as $js) : ?>
            <?php if ( ! is_null($js)) : ?>
                <?php echo "\n"; ?><script type="text/javascript"><?php echo "\n" . $js . "\n"; ?></script><?php echo "\n"; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>

</body>
</html>
