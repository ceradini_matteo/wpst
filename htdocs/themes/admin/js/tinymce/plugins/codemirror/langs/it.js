tinymce.addI18n('it',{
	'HTML source code': 'codice sorgente HTML',
	'Start search': 'Inizia la ricerca',
	'Find next': 'Trova successivo',
	'Find previous': 'Trova precedente',
	'Replace': 'Sostituisci',
	'Replace all': 'Sostituisci tutti'
});
