$(document).ready(function() {

	fixHeight();

	$("#checkAll").change(function () {
	    $("input:checkbox").prop('checked', $(this).prop("checked"));
	});

	$(".droptrue" ).sortable({ 
		connectWith: "ul",
		over: function(e, ui) { 
			sortableIn = 1;
		 },
		out: function(e, ui) { sortableIn = 0; },
		beforeStop: function(e, ui) {
		   if (sortableIn == 0) { 
		      ui.item.appendTo($('#tests')); 
		   } 
		}
	});

    $("#tests_added, #tests").disableSelection();

    $('#btnSave').on('click',function(){
    	var tests = [];

		$("#tests_added li").each(function(){
            tests.push($(this).data('id'));
        });	

        $("input[name='tests']").val(tests);	    
	});

	$('#removeBatteries').on('click',function(){
		var selected = [];
	    $.each($("input[name='selected[]']:checked"), function(){            
	        selected.push($(this).val());
	    });

	    if(selected.length > 0)
	    {
	    	$('#deleteMultipleModal').modal('show');
	    	$('#btn-delete-batteries').data('selected', selected);
	    }	    
	});

	$('#btn-delete-batteries').on('click',function() {
        var selected = $(this).data('selected');

        $.ajax( {
		    type: 'POST',
		    url: 'batteries/delete ',
		    data: { 
		    	selected: selected 
		   	}
		} ).done(function(data) {				
			window.location.href = "/admin/batteries";				
		});		
    });
	
	$('.btn-delete').on('click',function(){
		var id = $(this).data('id');
		$('#btn-delete-battery').data('id',id);
	    $('#deleteModal').modal('show');
	    var tr = $(this).closest('tr');
	    $('#modalBattery').text(tr.find('td:nth-child(2)').text().trim());
    });

	$('#btn-delete-battery').on('click',function() {
        window.location.href = "/admin/batteries/delete/" + $(this).data('id');
    });

    $('.get-info').tooltip({
    	html: true,
    	placement: 'right'
    });

    // this funcition fix height of tests box
    function fixHeight(){
    	var tests_added_height = $('#tests_added').height();
    	var tests_height       = $('#tests').height();

    	if(tests_added_height < tests_height){
    		$('#tests_added').height(tests_height);
    	}
    	else {
    		$('#tests').height(tests_added_height);
    	}
    }
});