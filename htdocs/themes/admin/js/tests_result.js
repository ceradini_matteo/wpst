$(function(){
	$("#checkAll").change(function () {
	    $("input:checkbox").prop('checked', $(this).prop("checked"));
	});

	$('#removeTests').on('click',function(){
		var selected = [];
	    $.each($("input[name='selected[]']:checked"), function(){            
	        selected.push($(this).val());
	    });

	    if(selected.length > 0)
	    {
	    	$('#deleteMultipleModal').modal('show');
	    	$('#btn-delete-tests').data('selected', selected);
	    }	    
	});

	$('#btn-delete-tests').on('click',function() {
        var selected = $(this).data('selected');

        $.ajax( {
		    type: 'POST',
		    url: 'tests_result/delete',
		    data: { 
		    	selected: selected
		   	}
		} ).done(function(data) {				
			window.location.href = "/admin/tests_result";				
		});		
    });
	
	$('.btn-delete').on('click',function(){
		var id = $(this).data('id');

		$('#btn-delete-test-result').data('id',id);
	    $('#deleteModal').modal('show');
    });

	$('#btn-delete-test-result').on('click',function() {
        window.location.href = "/admin/tests_result/delete/" + $(this).data('id');
    });
});