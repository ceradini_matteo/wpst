$(function(){
	// Validator
	show_modal_test();
	validation_on_load();

	var basic_type_compiled = false, basic_number_compiled = false;
	var basic_type, basic_number;
	var folder_name;
	var test_id = $("input[name=id]").val();
	var window_location_origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: ''); //fix for ie

	function show_modal_test()
	{
		var test_id = window.location.href.split("/").pop();

		if(test_id == 'form')
		{
			$('#testInformationModal').modal('show');
		}	
		else
		{
			folder_name = $("input[name=folder_name]").val();
		}	
	}

	$('body').on('input','.test-basic-type',function() {
		var val = $(this).val();

		if(val != '')
		{
			basic_type_compiled = true;
			basic_type          = val;

			if(basic_number_compiled)
			{
				$('#save-basic-test').removeClass('disabled');
			}
		}
		else
		{
			basic_type_compiled = false;
			$('#save-basic-test').addClass('disabled');
		}
	});

	$('body').on('input','.test-basic-number',function() {
		var val = $(this).val();

		if(val != '')
		{
			basic_number_compiled = true;
			basic_number          = val;

			if(basic_type_compiled)
			{
				$('#save-basic-test').removeClass('disabled');
			}
		}
		else
		{
			basic_number_compiled = false;
			$('#save-basic-test').addClass('disabled');
		}
	});

	$('body').on('click','#save-basic-test',function() {
		var _this = $(this);

		if(!_this.hasClass('disabled'))
		{
			$('.test-type').val(basic_type);
			$('.test-number').val(basic_number);

			$.ajax( {
			    type: 'GET',
			    url: 'create_test_folder',
			    data: { 
			    	test_id : test_id,
			    	type: basic_type,
			    	number: basic_number 
			    }
			} ).done(function(data) {
				folder_name = data;
				$('#fileManagerIFrame').attr('src', '/filemanager/dialog.php?fldr=' + folder_name);
				_this.closest('.modal').modal('hide');
				validation_on_load(false, true);
			});		
		}		
	});

	function validation_on_load(only_slides = false, no_slides = false)
	{
		$(".loader").fadeIn("fast");

		if(!only_slides)
		{
			form_validation();
		}		

		if(!no_slides)
		{
			$.each($('.slide'), function(){
				form_validation_slide($(this));
			});
		}		

		$(".loader").fadeOut("fast");
	}

	function form_validation()
	{
		var disabled = false;
		var test_type = $('.test-type');

		if(test_type.val() == '' || test_type.val() == null)
		{
			test_type.addClass('form-error');
			disabled = true;
		}
		else
		{
			test_type.removeClass('form-error');
		}

		var test_number = $('.test-number');

		if(test_number.val() == '' || test_number.val() == null)
		{
			test_number.addClass('form-error');
			disabled = true;
		}
		else
		{
			test_number.removeClass('form-error');
		} 

		// randomize range
		$.each($('.range'), function(){   
	    	var _this = $(this);         
			var from  = _this.find('.range-from');
			var to    = _this.find('.range-to');

			if(from.val() == '' || from.val() == null)
			{				
				from.addClass('form-error');
				disabled = true;
			}
			else
			{
				from.removeClass('form-error');
			}
			
			if(to.val() == '' || to.val() == null)
			{
				to.addClass('form-error');
				disabled = true;
			}
			else
			{
				to.removeClass('form-error');
			}
	    });	

	    $('.test-form').find('.btn-save').prop('disabled', disabled);
	}

	function form_validation_slide(slide)
	{	
		var number = slide.find('.slide-input-number');

		if(number.val() == '' || number.val() == null)
		{
			number.addClass('form-error');
			$('.test-form').find('.btn-save').prop('disabled', true);
		}
		else
		{
			number.removeClass('form-error');
			$('.test-form').find('.btn-save').prop('disabled', false);
		}	

		// stimuli
		var stimuli      = slide.find('.stimuli');
		var stimuli_type = stimuli.find('.stimuli-type').val();	

		if(stimuli_type == '' || stimuli_type == null)
		{
			stimuli.addClass('form-error-big');
		}
		else
		{
			if(stimuli_type != 'text-box')
			{
				var stimuli_answer = stimuli.find('select.stimuli-answer').val();	

				if(stimuli_answer == '' || stimuli_answer == null)
				{
					stimuli.addClass('form-error-big');
				}
				else
				{
					stimuli.removeClass('form-error-big');	
				} 
			}
			else
			{
				stimuli.removeClass('form-error-big');	
			}			
		} 

		//trigger
		var trigger = slide.find('.select-trigger');

		if(trigger.length == 0)
		{
			slide.find('.triggers').addClass('form-error-big');
		}
		else
		{
			slide.find('.triggers').removeClass('form-error-big');
		}

		$.each(slide.find('.trigger'), function(){ 

			var trigger       = $(this);
			var trigger_value = trigger.find('.trigger-value');

			if(trigger_value.length)
			{
				var value = trigger_value.val();

				if(value == "" || value == null || value < 0)
				{
					trigger.addClass('form-error-big');
				}
				else
				{
					trigger.removeClass('form-error-big');
				}
			}
		}); 
	}	

	$('body').on('input','.test-type',function() {
		form_validation();
    });

    $('body').on('input','.test-number',function() {
		form_validation();
    });

	$('body').on('input','.range-from',function() {
		form_validation();
    });

    $('body').on('input','.range-to',function() {
		form_validation();
    });

    $('body').on('change','.stimuli-files-added',function() {
		form_validation_slide($(this).closest('.slide'));
    });

    $('body').on('change','.stimuli-files-uploads',function() {
		form_validation_slide($(this).closest('.slide'));
    });

    $('body').on('change','select.stimuli-answer',function() {
		form_validation_slide($(this).closest('.slide'));
    });

    $('body').on('input','.trigger-value',function() {
		form_validation_slide($(this).closest('.slide'));
    });

	// Methods

	$("#checkAll").change(function () {
	    $("input:checkbox").prop('checked', $(this).prop("checked"));
	});

	$('#removeTests').on('click',function(){
		var selected = [];
	    $.each($("input[name='selected[]']:checked"), function(){            
	        selected.push($(this).val());
	    });

	    if(selected.length > 0)
	    {
	    	$('#deleteMultipleModal').modal('show');
	    	$('#btn-delete-tests').data('selected', selected);
	    }	    
	});

	$('#btn-delete-tests').on('click',function() {
        var selected = $(this).data('selected');

        $.ajax( {
		    type: 'POST',
		    url: 'tests/delete ',
		    data: { 
		    	selected: selected 
		    }
		} ).done(function(data) {
			window.location.href = "/admin/tests";
		});
    });
	
	$('.btn-delete').on('click',function(){
		var id= $(this).data('id');
		$('#btn-delete-test').data('id',id);
	    $('#deleteModal').modal('show');
	    var tr = $(this).closest('tr');
	    $('#modalNumber').text(tr.find('td:nth-child(2)').text().trim());
	    $('#modalType').text(tr.find('td:nth-child(3)').text().trim());
    });

	$('#btn-delete-test').on('click',function() {
        window.location.href = "/admin/tests/delete/" + $(this).data('id');
    });

	$('body').on('click','.btn-save',function() {
		// $(".loader").fadeIn("fast");
		var form             = $(".test-form");
		var values           = form.serialize();
		var files            = new FormData();
		var index_file       = 0;
		var file_slide_index = new Array();

		$('.slide').each(function () {
    		var _this           = $(this);
    		var index_slide     = _this.find('.slide-index').val();
    		var stimuli_uploads = _this.find('input[name^="stimuli_uploads"]');

    		file_slide_index[index_slide] = new Array();

    		for(var index = 0; index < stimuli_uploads.length; index++)
			{
				if(stimuli_uploads[index].files.length > 0)
				{
					jQuery.each(stimuli_uploads[index].files, function(i, file) {
						files.append(index_file, file);
						file_slide_index[index_slide][index + 1] = index_file;
						index_file++;
					});
				}
				else
				{
					file_slide_index[index_slide][index + 1] = null;
				}
			}
	    });

	    files.append('values', values);
	    files.append('file_slide_index', JSON.stringify(file_slide_index));

		$.ajax( {
		    type: 'POST',
		    url: '',
		    data: files,
		    cache: false,
		    contentType: false,
		    processData: false
		}).done(function(data) {
			window.location.href = "/admin/tests";
		});
    });

    $('body').on('click','#btn-delete-slides',function() {
    	var slides = $('.slides');

    	slides.find('.slide').each(function () {
    		$(this).remove();
	    });

		$('#deleteMultipleModal').modal('hide');
		$('#delete-slides').tooltip('dispose');
    });

    $('body').on('click','#add-range',function() {
    	var _this = $(this);

    	$.ajax( {
			    type: 'POST',
			    url: window_location_origin + '/admin/tests/getRange'
			} ).done(function(data) {				
				_this.closest('.randomize-range').append(data);	
				form_validation();	
		});
    });    

	$('body').on('click','.delete-range',function() {
		$(this).closest('.range').remove();
		form_validation();
    });

	var index = $('.slide').length;
	var card_hide = true;

	$('body').on('click','#add-slide',function() {
    	$(".loader").fadeIn("fast");

    	var _this         = $(this);
    	var number_slides = _this.closest('.card-header').find('.number-add-slide').val();
    	var done          = number_slides;
		var slides        = _this.closest('.card-block').find('.slides');	
		var html          = "";
		var start_number  = $('.slide').length + 1;

    	while(number_slides > 0)
    	{
			$.ajax( {
			    type: 'POST',
			    url: window_location_origin + '/admin/tests/getSlide',
			    data: {
			    	index: index,
			    	folder_name: folder_name
			    }
			} ).done(function(data) {							
				html += data;
				done--;	

				if(done == 0)
				{					
					slides.append(html);
					
					$('.slide').each(function() {
						_this_slide      = $(this);
						var slide_number = _this_slide.find('.slide-input-number');

						if(slide_number.val() == "" || slide_number.val() == null)
						{
							slide_number.val(start_number);
							form_validation_slide(_this_slide);
							start_number++;
						}						
					});	

					$(".loader").fadeOut("fast");
					$("html, body").animate({ scrollTop: $(document).height() }, 800);

					if(card_hide)
			    	{
			    		_this.closest('.card').find('.second-card').removeClass('hide');
			    		card_hide = false;
			    	}
				}		
			});

			index++;
			number_slides--;
    	}	
    });

    $('body').on('click','.delete-slide',function() {  
    	$(this).closest('.slide').remove();
    	validation_on_load();
    });

    $('body').on('input','.slide-input-number',function() {
    	var _this = $(this);
    	var number = _this.val();

		if(number == '' || number == null)
		{			
			_this.closest('.slide').find('.slide-number').text('');
		}
		else
		{
			_this.closest('.slide').find('.slide-number').text(number);
		}

		form_validation_slide(_this.closest('.slide'));
    });

    $('body').on('click','#add-trigger',function() {
    	var _this   = $(this);    	
    	var trigger = _this.closest('.triggers').find('.trigger');	

    	if(trigger.length >= 2)
		{
			_this.addClass('hide');
		}

    	var index_slide = _this.closest('.slide').data('index');
    	var types       = new Array();

		trigger.each(function() {
			var trigger_type = $(this).find('.select-trigger').val();

		    types.push(trigger_type);
		});	

    	$.ajax( {
		    type: 'POST',
		    url: window_location_origin + '/admin/tests/getTrigger',
		    data: {
		    	index: index_slide
		    }
		} ).done(function(data) {				
			_this.closest('.triggers').append(data);
			index++;

			trigger = _this.closest('.triggers').find('.trigger').last();		

			trigger.find('.select-trigger option').each(function() {
			    if(types.indexOf($(this).val()) != -1)
		    	{
		    		$(this).remove();
		    	}
			});

			if(trigger.find('.select-trigger').val() == 'time')
			{
				$.ajax( {
				    type: 'POST',
				    url: window_location_origin + '/admin/tests/getTriggerTime',
				    data: {
				    	index: index_slide
				    }
				} ).done(function(data) {	
					trigger.find('.trigger-time-container').append(data);					
				});
			}

			form_validation_slide(_this.closest('.slide'));	
		});
	});

    $('body').on('change','.select-trigger',function() {
		var _this   = $(this);
		var type    = _this.val();
		var trigger = _this.closest('.trigger');

		if(type == 'time')
		{
			var index_slide = _this.closest('.slide').data('index');

			$.ajax( {
			    type: 'POST',
			    url: window_location_origin + '/admin/tests/getTriggerTime',
			    data: {
			    	index: index_slide
			    }
			} ).done(function(data) {	
				trigger.find('.trigger-time-container').append(data);
				form_validation_slide(_this.closest('.slide'));
			});
		}
		else
		{	
			var trigger_time = trigger.find('.trigger-time');

			if(trigger_time.length)
			{
				trigger_time.remove();
			}
		}
    });

    $('body').on('click','.delete-trigger',function() {	
    	var _this    = $(this);	
    	var triggers = _this.closest('.triggers');

		triggers.find('#add-trigger').removeClass('hide');
		_this.closest('.trigger').remove();

		form_validation_slide(triggers.closest('.slide'));
    });

    $('body').on('input','.stimuli-type',function() {
		var _this                  = $(this);
		var type                   = _this.change().val();
		var stimuli                = _this.closest('.stimuli');
		var container              = stimuli.find('.stimuli-numbers');
		var stimuli_uploads        = stimuli.find('.stimuli-uploads');
		var stimuli_layout_preview = stimuli.find('.stimuli-layout-preview');
		var stimuli_answers        = stimuli.find('.stimuli-answers');
		var layout                 = stimuli.find('.stimuli-layouts');
		var slide                  = _this.closest('.slide');
		var index_slide            = slide.data('index');

    	if(type != "")
    	{
	    	$.ajax( {
			    type: 'POST',
			    url: window_location_origin + '/admin/tests/getStimuliNumber',
			    data: {
			    	index: index_slide,
			    	type: type
				}
			} ).done(function(data) {
				container.html(data);
				_this.closest('.stimuli').find('.stimuli-number').trigger('input');

				if(type == 'text-box'){
    				slide.find('.stimuli-filters').removeClass('hidden');
				}
				else {
    				slide.find('.stimuli-filters').addClass('hidden');
				}
			});
    	}
    	else
    	{
    		slide.find('.stimuli-filters').addClass('hidden');
    		form_validation_slide(slide);
    		stimuli.find('.stimuli-upload-table').addClass('hide');
    	}

    	container.empty();
    	layout.empty();
		stimuli_uploads.empty();
		stimuli_layout_preview.empty();
		stimuli_answers.empty();
    });

    $('body').on('input','.stimuli-number',function() {
    	var _this                  = $(this);
    	var number                 = _this.change().val();
    	var stimuli                = _this.closest('.stimuli');
    	var container              = stimuli.find('.stimuli-layouts');
    	var stimuli_uploads        = stimuli.find('.stimuli-uploads');
    	var stimuli_layout_preview = stimuli.find('.stimuli-layout-preview');
    	var index_slide            = _this.closest('.slide').data('index'); 

    	if(number != "")
    	{
    		var type = _this.closest('.stimuli').find('.stimuli-type').val();   		

    		$.ajax( {
			    type: 'POST',
			    url: window_location_origin + '/admin/tests/getStimuliLayout',
			    data: { 
			    	index:  index_slide,
			    	type:   type,
			    	number: number
				}
			} ).done(function(data) {	
				container.html(data);
				_this.closest('.stimuli').find('.stimuli-layout').trigger('input');
			});			
    	}
		else
    	{
    		container.empty();
    	}

    	stimuli_uploads.empty();
    	stimuli_layout_preview.empty();
    });

    $('body').on('input','.stimuli-layout',function() {		
    	var _this                  = $(this);
    	var layout                 = _this.change().val();	
		var stimuli                = _this.closest('.stimuli');
    	var slide                  = _this.closest('.slide');
    	var index_slide            = slide.data('index'); 
		var number                 = stimuli.find('.stimuli-number').val();
		var type                   = stimuli.find('.stimuli-type').val(); 		
    	var stimuli_uploads        = stimuli.find('.stimuli-uploads'); 
    	var stimuli_layout_preview = stimuli.find('.stimuli-layout-preview');

    	if(layout != "")
    	{
    		if(type != 'text-box')
    		{    			
    			stimuli.find('.stimuli-answers').removeClass('hide');
    			stimuli.find('.stimuli-upload-table').removeClass('hide');

    			$.ajax( {
				    type: 'POST',
				    url: window_location_origin + '/admin/tests/getStimuliUpload',
				    data: { 
				    	index:         index_slide,
				    	number:        number,
				    	test_id:       test_id,
				    	type:          type,
				    	folder_name:   folder_name
					}
				} ).done(function(data) {
					stimuli.find('.stimuli-upload-header').removeClass('hide');

					stimuli_uploads.html(data);

					$('.stimuli-files-added').selectpicker({					
						style: 'btn-secondary',
						size: 20
					});

					$.ajax( {
					    type: 'POST',
					    url: window_location_origin + '/admin/tests/getStimuliAnswer',
					    data: { 
					    	index:         index_slide,
					    	type:          type,
					    	number:        number
						}
					} ).done(function(data) {
						stimuli.find('.stimuli-answers').html(data);

						$('.stimuli-answer').selectpicker();
					});
				});
    		}
    		else
    		{
    			stimuli.find('.stimuli-answers').addClass('hide');
    			stimuli.find('.stimuli-upload-table').addClass('hide');
    		}

			$.ajax( {
			    type: 'POST',
			    url: window_location_origin + '/admin/tests/getStimuliLayoutPreview',
			    data: { 
			    	layout: layout,
			    	type:   type
				}
			} ).done(function(data) {	
				stimuli_layout_preview.html(data); 
				form_validation_slide(slide);
			});
    	}
    	else
    	{
    		stimuli_uploads.empty();
    		stimuli_layout_preview.empty();
    	}	
    });

    $('body').on('click','#upload-csv', function(){
    	var data = new FormData();     
		jQuery.each($('input[name^="userfile"]')[0].files, function(i, file) {
		    data.append(i, file);
		});

		$.ajax({
		    type: 'POST',
		    data: data,
		    url:  window_location_origin + '/admin/tests/import_slides/' + test_id,
		    cache: false,
		    contentType: false,
		    processData: false		    
		}).done(function(data) {
	    	$('#uploadModal').modal('hide');
	        $('.slides').html(data);
	        $('.stimuli-files-added').selectpicker();
	        $('.stimuli-answer').selectpicker();

	        validation_on_load();
		});
    });

    $('body').on('click','#reloadSlidesFiles',function() {
		$(".loader").fadeIn("fast");

		$.each($('.slide'), function(){
			reload_slide_files(test_id, $(this));
		});

		validation_on_load(true);

		$(".loader").fadeOut("fast");
    });

    function reload_slide_files(test_id, slide)
    {
    	if(type != 'text-box')
    	{
    		var index_slide     = slide.data('index');
	    	var number          = slide.find('.stimuli-number').val();
	    	var type            = slide.find('.stimuli-type').val();
	    	var stimuli_uploads = slide.find('.stimuli-uploads');

			$.each(stimuli_uploads, function(){
				var file_selected = [];
				var _this = $(this);

				$.each(_this.find('.stimuli-files-added :selected'), function(){
					file_selected.push($(this).val());
				});

				$.ajax({
				    type: 'POST',
				    url: window_location_origin + '/admin/tests/getStimuliUpload',
				    data: { 
				    	index:   index_slide,
				    	number:  number,
				    	test_id: test_id,
				    	type:    type,
				    	file_selected: file_selected,
				    	folder_name: folder_name
					}
				}).done(function(data) {
					stimuli_uploads.html(data);
					_this.find('.stimuli-upload').find('.stimuli-files-added').selectpicker();
				});

				var i = 0;

				$.each(_this.find('.stimuli-upload'), function(){
					$(this).find('select').val(file_selected[i]);
					i++;
				});
			});
    	}
    }
});