/**
 * Global admin functions
 */
$(document).ready(function() {

    /**
     * Enable tooltips
     */
    if ($('.tooltips').length) {
        $('.tooltips').tooltip();
    }
    
    $.fn.datetimepicker.defaults.icons = { 
        time: 'ion-ios-clock-outline', 
        date: 'ion-ios-calendar-outline', 
        up: 'ion-ios-arrow-up', 
        down: 'ion-ios-arrow-down', 
        previous: 'ion-ios-arrow-back', 
        next: 'ion-ios-arrow-forward', 
        today: 'ion-ios-calendar', 
        clear: 'ion-ios-trash-outline', 
        close: 'ion-ios-close-outline' 
    };



    /**
     * Activate any date pickers
     */
	$('.datetimepicker').each(function(el){
		var _this = $(this);
		var val = _this.val();
		_this.val('');
		_this.datetimepicker({
			defaultDate: val,
		})
	});

    $('.datepicker').each(function(el){
        var _this = $(this);
        var val = _this.val();
        _this.val('');
        _this.datetimepicker({
            viewMode: 'years',
            format: 'YYYY-MM-DD',
            defaultDate: val
        })
    });


    /**
     * Detect items per page change on all list pages and send users back to page 1 of the list
     */
    $('select#limit').change(function() {
        var limit = $(this).val();
        var currentUrl = document.URL.split('?');
        var uriParams = "";
        var separator;

        if (currentUrl[1] != undefined) {
            var parts = currentUrl[1].split('&');

            for (var i = 0; i < parts.length; i++) {
                if (i == 0) {
                    separator = "?";
                } else {
                    separator = "&";
                }

                var param = parts[i].split('=');

                if (param[0] == 'limit') {
                    uriParams += separator + param[0] + "=" + limit;
                } else if (param[0] == 'offset') {
                    uriParams += separator + param[0] + "=0";
                } else {
                    uriParams += separator + param[0] + "=" + param[1];
                }
            }
        } else {
            uriParams = "?limit=" + limit;
        }

        // reload page
        window.location.href = currentUrl[0] + uriParams;
    });

    $('body').on('click','.backups',function(){
        var _this = $(this);

        _this.parent().find('.active').removeClass('active');
        _this.addClass('active');
        $('input[name=backup]').val(_this.val());
    })

    $('body').on('click','.backup-page',function(){
        var _this = $(this);
        var page  = _this.data('page');
        var count = 1;

        _this.parent().find('.active').removeClass('active');
        _this.addClass('active');

        $('.backups').each(function () {
            if((count > (page - 1) * 10) && (count <= (page * 10)))
            {
                $(this).removeClass('hide');
            }
            else
            {
                $(this).addClass('hide');
            }

            count++;
        });
    })

    $('body').on('click','#next-backup-page',function(){
        var _this = $(this);
        var active = _this.parent().find('.active');
        var page = active.data('page') + 1;     
        
        if(_this.parent().find("[data-page='" + page + "']").length)
        {
            _this.parent().find('.active').removeClass('active');
            _this.parent().find("[data-page='" + page + "']").addClass('active');

            var count = 1;

            $('.backups').each(function () {
                if((count > (page - 1) * 10) && (count <= (page * 10)))
                {
                    $(this).removeClass('hide');
                }
                else
                {
                    $(this).addClass('hide');
                }

                count++;
            });
        }
    })

    $('body').on('click','#previous-backup-page',function(){
        var _this = $(this);
        var active = _this.parent().find('.active');
        var page = active.data('page') - 1;     
        
        if(_this.parent().find("[data-page='" + page + "']").length)
        {
            _this.parent().find('.active').removeClass('active');
            _this.parent().find("[data-page='" + page + "']").addClass('active');

            var count = 1;

            $('.backups').each(function () {
                if((count > (page - 1) * 10) && (count <= (page * 10)))
                {
                    $(this).removeClass('hide');
                }
                else
                {
                    $(this).addClass('hide');
                }

                count++;
            });
        }
    })

    /**
     * Enable WYSIWYG editor on any textareas with the 'editor' class
     */
    if ($('textarea.editor').length) {
     	tinymce.init({
	    selector: "textarea.editor",
	    height: 300,
		language: 'en',
	    link_list: [
	        {title: 'My page 1', value: 'http://www.tinymce.com'},
	        {title: 'My page 2', value: 'http://www.tecrail.com'}
	    ],
	    plugins: [
	         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
	         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking spellchecker",
	         "table contextmenu directionality emoticons paste textcolor responsivefilemanager codemirror  easyColorPicker fullscreen "
	   ],
	    relative_urls: false,
	    browser_spellcheck : true ,
	    filemanager_title:"Responsive Filemanager",
	    external_filemanager_path:"/filemanager/",
	    external_plugins: { "filemanager" : "/filemanager/plugin.min.js"},
	    codemirror: {
		    indentOnInit: true, // Whether or not to indent code on init. 
		    path: 'codemirror'
		  },
	  	content_css:'/assets/css/style.css',
	   image_advtab: true,
	   toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | styleselect | responsivefilemanager | image | media | link unlink | print preview code  |  easyColorPicker | fullscreen"
	 });
    }

});
