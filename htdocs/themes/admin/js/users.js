$(function(){

	$("#checkAll").change(function () {
	    $("input:checkbox").prop('checked', $(this).prop("checked"));
	});

	$('#removeUsers').on('click',function(){
		var selected = [];
	    $.each($("input[name='selected[]']:checked"), function(){            
	        selected.push($(this).val());
	    });

		if(selected.length > 0)
	    {
	    	$('#deleteMultipleModal').modal('show');
	    	$('#btn-delete-users').data('selected', selected);
	    }	    
	});
	
	$('#btn-delete-users').on('click',function() {
        var selected = $(this).data('selected');

        $.ajax( {
		    type: 'POST',
		    url: 'users/delete ',
		    data: { selected: selected }
		} ).done(function(data) {			
			window.location.href = "/admin/users";			
		});
    });

	$('#btn-delete-user').on('click',function() {
        window.location.href = "/admin/users/delete/" + $(this).data('id');
    });
    
    $('.btn-delete').on('click',function(){
		var id= $(this).data('id');
		$('#btn-delete-user').data('id',id);
	    $('#deleteModal').modal('show');
	    var tr = $(this).closest('tr');
	    $('#modalUsername').text(tr.find('td:nth-child(2)').text().trim());
    });
    
	$('body').on('click','.editable',function(){
		var _this = $(this);
		_this.off('click');
		if(!_this.data()['bs.popover']){
			var id       = _this.data('id');
			var type     = _this.data('type');
			var name     = _this.data('name');
			var value    = _this.data('value');
			var optional = _this.data('optional');
			
			$.ajax( {
			    type: 'POST',
			    url: 'users/getEditableForm',
			    data: { 
				    id: id,
				    type: type,
				    name: name,
				    value: value,
				    optional: optional
				}
			} ).done(function(data) {
				
				_this.popover({
					html:true,
					container:'body',
					placement:'top',
					content: data,
				});
				_this.popover('show');
				
			});
		}else{
			_this.popover('toggle');	
		}
	});

	$('body').on('click','.hide-popover',function(){
		$('body').trigger('click');
	})
	
	$('body').on('click','.save-editable',function(){
		var _this    = $(this);
		var id       = _this.data('id');
		var type     = _this.data('type');
		var name     = _this.data('name');
		var optional = _this.data('optional');
		var flag     = true;

		if(_this.parent().find('input').length){

			if(type == 'password')
			{
				var password = _this.parent().find('#password').val();
				var password_repeat = _this.parent().find('#password_repeat').val();

				
				if(password == password_repeat)
				{
					if(password.length >= 5)
					{
						var value = password;
					}
					else
					{
						$('.msg').html('<div class="alert alert-danger" role="alert">The Password field must be at least 5 characters in length.</div>');
						flag = false;
					}
				}
				else
				{
					$('.msg').html('<div class="alert alert-danger" role="alert">The Repeat Password field does not match the Password field.</div>');
					flag = false;
				}
			}
			else
			{
				var value = _this.parent().find('input').val();	
			}
		}

		if(_this.parent().find('select').length){
			var value = _this.parent().find('select').val();
			var text  = _this.parent().find(':selected').text();
		}

		if(flag && optional || value.length > 0)
		{
			$.ajax( {
			    type: 'POST',
			    url: 'users/save',
			    data: {
			     	id: id,
			     	name: name,
			     	value: value,
			     	type: type
			 	}
			} ).done(function(data) {
				if(value){
					if(type == 'select')
					{
						if(text.length == 0)
						{							
							text = 'n/a';
						}
						
						$('#'+name+id).text(text);
					}
					else 
					{
						if(type == 'password')
						{
							value = "********";
						}

						$('#'+name+id).text(value);
					}
				}				
				else {
					$('#'+name+id).text('n/a');
				}	
			});

			$('#'+name+id).popover('dispose');

			$.ajax( {
			    type: 'POST',
			    url: 'users/getEditableForm',
			    data: { 
				    id: id,
				    type: type,
				    name: name,
				    value: value,
				    optional: optional
				}
			} ).done(function(data) {
				$('#'+name+id).popover({
					html:true,
					container: 'body',
					placement:'top',
					content: data
				});
			});
		}
		else
		{
			$('.msg').html('<div class="alert alert-danger" role="alert">This field can\'t be empty</div>');
		}
	})

	$(document).on('click', function (e) {
	    $('.editable').each(function () {
	        //the 'is' for buttons that trigger popups
	        //the 'has' for icons within a button that triggers a popup
	        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {                
	            (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false
	        }
	
	    });
	});

	$('#clearStartTime').on('click',function(){
		$(this).closest('th').find('#start_time').val('');
	});

	$('#clearEndTime').on('click',function(){
		$(this).closest('th').find('#end_time').val('');
	});
});